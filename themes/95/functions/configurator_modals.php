<?php

function configurator_modal_contact_beamup(){
    
    if($_SERVER["REQUEST_METHOD"] == "POST") {
        
        
        $form = $_POST['form'];
        parse_str($form, $formArr);
        

        $firstname = $formArr['firstname'];
        $lastname = $formArr['lastname'];  
        $firma =  $formArr['firma'];   
        $phone = $formArr['phone'];   
        $email = $formArr['email'];  
        $message = $formArr['message'];
        $calcid = $formArr['calcidformcontact']; 
        
            
        

        $absender = get_field('configurator_contact_mail', 'option');
        $empfaenger = get_field('configurator_contact_mail', 'option');

        
        if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

            if ( ICL_LANGUAGE_CODE=='en' ) {
                $betreff = "Configurator beamup";
            }

            if ( ICL_LANGUAGE_CODE=='de' ) {
                $betreff = "Configurator beamup";
            }

            if ( ICL_LANGUAGE_CODE=='es' ) {
                $betreff = "Configurator beamup";
            }

        }
        
        
        
        $header = 'From: '.$absender . "\r\n" .
        'Reply-To: ' . $absender . "\r\n" .
        "MIME-Version: 1.0\r\n".
        "Content-Type: text/html; charset=UTF-8\r\n".
        'X-Mailer: PHP/' . phpversion();
        

        // Nachricht
        $body = "
        <html>
        <table>
        
        <tr>
            	<td>ID: ".$calcid."</td>
        </tr>
        
        
        
        <tr>
        <td>Name: ".$firstname." ".$lastname."</td>
        </tr>
         
        <tr>
        <td>Firma: ".$firma."</td>
        </tr>
        
        <tr>
        <td>Phone: ".$phone."</td>
        </tr>
        
        <tr>
        <td>E-Mail: ".$email."</td>
        </tr>
        
        <tr>
        <td>Message: ".$message."</td>
        </tr>
        
        <table>
        </html>
        ";
        

        $resultMail = wp_mail($empfaenger, $betreff, $body, $header);
        
        
        $arrays_collector_array = array();

        if(!$resultMail) {   
            //echo "Error"; 
            $arrays_collector_array["Success"] = "false";
        } else {
            //echo "Success";
            $arrays_collector_array["Success"] = "true";
        }

        echo json_encode($arrays_collector_array);
        die();
        
    }
    
};


function configurator_modal_contact_beamup_unable_calculate(){
    
    if($_SERVER["REQUEST_METHOD"] == "POST") {
        
        
        $form = $_POST['form'];
        parse_str($form, $formArr);
        

        $firstname = $formArr['firstname'];
        $lastname = $formArr['lastname'];  
        $firma =  $formArr['firma'];   
        $phone = $formArr['phone'];   
        $email = $formArr['email'];  
        $message = $formArr['message']; 
        $calcid = $formArr['calcidformcontacterrorcalulate']; 
        

        $absender = get_field('configurator_contact_mail', 'option');
        $empfaenger = get_field('configurator_contact_mail', 'option');

        
        if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

            if ( ICL_LANGUAGE_CODE=='en' ) {
                $betreff = "Configurator beamup Calculate Error";
            }

            if ( ICL_LANGUAGE_CODE=='de' ) {
                $betreff = "Configurator beamup Calculate Error";
            }

            if ( ICL_LANGUAGE_CODE=='es' ) {
                $betreff = "Configurator beamup Calculate Error";
            }

        }
        
        
        
        $header = 'From: '.$absender . "\r\n" .
        'Reply-To: ' . $absender . "\r\n" .
        "MIME-Version: 1.0\r\n".
        "Content-Type: text/html; charset=UTF-8\r\n".
        'X-Mailer: PHP/' . phpversion();
        

        // Nachricht
        $body = "
        <html>
        <table>
        
                <tr>
            	<td>ID: ".$calcid."</td>
        </tr>
        
        <tr>
        <td>Name: ".$firstname." ".$lastname."</td>
        </tr>
         
        <tr>
        <td>Firma: ".$firma."</td>
        </tr>
        
        <tr>
        <td>Phone: ".$phone."</td>
        </tr>
        
        <tr>
        <td>E-Mail: ".$email."</td>
        </tr>
        
        <tr>
        <td>Message: ".$message."</td>
        </tr>
        
        <table>
        </html>
        ";
        

        $resultMail = wp_mail($empfaenger, $betreff, $body, $header);
        
        
        $arrays_collector_array = array();

        if(!$resultMail) {   
            //echo "Error"; 
            $arrays_collector_array["Success"] = "false";
        } else {
            //echo "Success";
            $arrays_collector_array["Success"] = "true";
        }

        echo json_encode($arrays_collector_array);
        die();
        
    }
    
};










function configurator_modal_contact_beamup_error(){
    
    if($_SERVER["REQUEST_METHOD"] == "POST") {

        $formError = $_POST['formerror'];
        parse_str($formError, $formArrError);
        
        
        $firstname = $formArrError['firstnameerror'];
        $lastname = $formArrError['lastnameerror'];  
        $firma =  $formArrError['firmaerror'];   
        $phone = $formArrError['phoneerror'];   
        $email = $formArrError['emailerror'];  
        $message = $formArrError['messageerror'];
        $csvid = $formArrError['calcidformcontacterror']; 
        

        $absender = get_field('configurator_contact_mail', 'option');
        $empfaenger = get_field('configurator_contact_mail', 'option');
 
        
        if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

            if ( ICL_LANGUAGE_CODE=='en' ) {
                $betreff = "Configurator beamup error";
            }

            if ( ICL_LANGUAGE_CODE=='de' ) {
                $betreff = "Configurator beamup error";
            }

            if ( ICL_LANGUAGE_CODE=='es' ) {
                $betreff = "Configurator beamup error";
            }

        }

        
        
        
        
        $header = 'From: '.$absender . "\r\n" .
        'Reply-To: ' . $absender . "\r\n" .
        "MIME-Version: 1.0\r\n".
        "Content-Type: text/html; charset=UTF-8\r\n".
        'X-Mailer: PHP/' . phpversion();
        

        // Nachricht
        $body = "
        <html>
        <table>
        
        <tr>
        <td>ID: ".$csvid."</td>
        </tr>
        
        <tr>
        <td>Name: ".$firstname." ".$lastname."</td>
        </tr>
         
        <tr>
        <td>Firma: ".$firma."</td>
        </tr>
        
        <tr>
        <td>Phone: ".$phone."</td>
        </tr>
        
        <tr>
        <td>E-Mail: ".$email."</td>
        </tr>
        
        <tr>
        <td>Message: ".$message."</td>
        </tr>
        
        <table>
        </html>
        ";
        

        $resultMail = wp_mail($empfaenger, $betreff, $body, $header);
        
        
        $arrays_collector_array_error = array();

        if(!$resultMail) {   
            //echo "Error"; 
            $arrays_collector_array_error["Success"] = "false";
        } else {
            //echo "Success";
            $arrays_collector_array_error["Success"] = "true";
        }

        echo json_encode($arrays_collector_array_error);
        die();
        
    }
    
};


add_action('wp_ajax_configurator_modal_contact_beamup', 'configurator_modal_contact_beamup'); // This is for authenticated users
add_action('wp_ajax_nopriv_configurator_modal_contact_beamup', 'configurator_modal_contact_beamup'); // This is for unauthenticated users.

add_action('wp_ajax_configurator_modal_contact_beamup_error', 'configurator_modal_contact_beamup_error'); // This is for authenticated users
add_action('wp_ajax_nopriv_configurator_modal_contact_beamup_error', 'configurator_modal_contact_beamup_error'); // This is for unauthenticated users.


add_action('wp_ajax_configurator_modal_contact_beamup_unable_calculate', 'configurator_modal_contact_beamup_unable_calculate'); // This is for authenticated users
add_action('wp_ajax_nopriv_configurator_modal_contact_beamup_unable_calculate', 'configurator_modal_contact_beamup_unable_calculate'); // This is for unauthenticated users.

?>