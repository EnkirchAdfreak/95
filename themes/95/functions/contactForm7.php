<?php
    // br und p tags entfernen die contact form 7 setzt
    //add_filter( 'wpcf7_autop_or_not', '__return_false' );


    //add_filter( 'wpcf7_validate_configuration', '__return_false' );


    function rankya_contactform7check_dequeue() {
        //set to false so that you have something to check against
        $check_cf7 = false;
        // if its page named 'kontakt' then set to true
        // if your contact form 7 is on page named 'contact-us'
        // then change below accordingly
        if( is_page('kontakt')) {
            $check_cf7 = true;
        }
        //so therefore dequeue only if is false 
        if( !$check_cf7 ) {
            wp_dequeue_script( 'contact-form-7' );
            wp_dequeue_style( 'contact-form-7' );
        }
    }
    //add_action( 'wp_enqueue_scripts', 'rankya_contactform7check_dequeue', 77 );
?>