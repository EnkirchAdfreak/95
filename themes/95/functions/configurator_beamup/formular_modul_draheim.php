<?php
/*
<!-- Submit the form to admin-post.php -->
<form action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="POST">

    <!-- Your form fields go here -->

    <!-- Add a hidden form field with the name "action" and a unique value that you can use to handle the form submission  -->
    <input type="hidden" name="action" value="my_simple_form">

</form>
*/
/*
function handle_form_submit(){
	
	if(isset($_POST["Belastung_qEd"])):
		echo $_POST["Belastung_qEd"]."<br>";
	endif;
	if($_POST["Belastung_qEd"]==""):
		echo "leer<br>";
	endif;
	
	//include_once esc_url( admin_url('p-form-edit.php') );
	include get_theme_file_path( '../../../wp-admin/p-form-edit.php' ); //locate_template()

}

// Use your hidden "action" field value when adding the actions
add_action( 'admin_post_the_modul_form', 'handle_form_submit' );
add_action( 'admin_post_nopriv_the_modul_form', 'handle_form_submit' );
*/

function javascript_variables(){ ?>
    <script type="text/javascript">
        var ajax_url = '<?php echo admin_url( "admin-ajax.php" ); ?>';
		var p_characters_testing_js = '<?php echo admin_url( "js/p-js/p-characters-testing.js" ); ?>';
        var ajax_nonce = '<?php echo wp_create_nonce( "secure_nonce_name" ); ?>';
    </script><?php
}
add_action ( 'wp_head', 'javascript_variables' );
 
function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
function edit_form(){
     
    if($_SERVER["REQUEST_METHOD"] == "POST") {
		
		// Diese 4 Zeichen " %% " werden für explode bei ajax Ausgabe für die Zuordnung zum jeweiligen Input benötigt
		
     
        
		$form_callback_data = array();
           
		
		// Deckentyp
		//  index = 1-3
		
		$Deckentyp = $_POST['Deckentyp'];
		$form_callback_data["Deckentyp"] = $Deckentyp;
		
		
		// #####
		
		
		// Festigkeitsklasse Beton
		//  index = 1 -7
		
		$Festigkeitsklasse_Beton = $_POST['Festigkeitsklasse_Beton'];
		$form_callback_data["Festigkeitsklasse_Beton"] = $Festigkeitsklasse_Beton;
		
		
		// #####
		
		
		// Expositionsklasse
		// index = 1-2
		
		$Expositionsklasse = $_POST['Expositionsklasse'];
		$form_callback_data["Expositionsklasse"] = $Expositionsklasse;
		
		
		// #####
		
		
		// Belastung qEd
		// Grenzen: >=0; <= 300 Falls fehlerhafte Eingabe: „Die Belastung liegt nicht im richtigen Bereich. Sie muss zwischen qEd = 0 kN/m und 500 kN/m liegen“  Wert: double
		
		if($_POST["Belastung_qEd"]==""){
			echo "Belastung_qEd %% Leeres Feld";
			wp_die();
		}
		if($_POST["Belastung_qEd"]!=""){
			$Belastung_qEd = test_input($_POST["Belastung_qEd"]);
		}
		if(!preg_match("/^[0-9.]*$/",$Belastung_qEd)){
			echo"Belastung_qEd %% Unerlaubte Zeichen ( Erlaubt 0-9 und Point)";
			wp_die();
		}
		if(!is_numeric($Belastung_qEd )):
			echo "Belastung_qEd %% Keine numerische Eingabe<br>";
			wp_die();
		endif;
		if($Belastung_qEd<0||$Belastung_qEd>300){
			echo'Belastung_qEd %% „Die Belastung liegt nicht im richtigen Bereich. Sie muss zwischen qEd = 0 kN/m und 500 kN/m liegen“ Grenzwerte: >=0 und <= 300';
			wp_die();
		}
		if(!preg_match("#([0-9]+\.[0-9]+)#",$Belastung_qEd )):
			echo "Belastung_qEd %% Number is no double<br>";
			wp_die();
		endif;
		
		$qEd = $Belastung_qEd;
		
		//$Belastung_qEd = doubleval  ( $Belastung_qEd );
		$form_callback_data["Belastung_qEd"] = $Belastung_qEd;
		
		
		// #####
		
		
		// Länge Öffnung Lges
		// Grenzen: >0.5; <10 ?
		
		if($_POST["Laenge_Oeffnung_Lges"]==""){
			echo "Laenge_Oeffnung_Lges %% Leeres Feld";
			wp_die();
		}
		if($_POST["Laenge_Oeffnung_Lges"]!=""){
			$Laenge_Oeffnung_Lges = test_input($_POST["Laenge_Oeffnung_Lges"]);
		}
		if(!preg_match("/^[0-9.]*$/",$Laenge_Oeffnung_Lges)){
			echo"Laenge_Oeffnung_Lges %% Unerlaubte Zeichen ( Erlaubt 0-9 und Point)";
			wp_die();
		}
		if(!is_numeric($Laenge_Oeffnung_Lges )):
			echo "Laenge_Oeffnung_Lges %% Keine numerische Eingabe<br>";
			wp_die();
		endif;
		if($Laenge_Oeffnung_Lges<=0.5||$Laenge_Oeffnung_Lges>=10){
			echo "Laenge_Oeffnung_Lges %% Eigabe Grenzwerte:   >0.5 und <10 ?";
			wp_die();
		}
		if(!preg_match("#([0-9]+\.[0-9]+)#",$Laenge_Oeffnung_Lges )):
			echo "Laenge_Oeffnung_Lges %% Number is no double<br>";
			wp_die();
		endif;
		
		$Lges = $Laenge_Oeffnung_Lges;
		
		//$Laenge_Oeffnung_Lges = doubleval  ( $Laenge_Oeffnung_Lges );
		$form_callback_data["Laenge_Oeffnung_Lges"] = $Laenge_Oeffnung_Lges;
		
		
		// #####
		
		
		// Anzahl der Öffnungen
		// ist -> Grenzen: n > 1 , n <= 5 ! /  soll? Grenzen: n >= 1 , n <= 5 ?   Wert: Integer
		
		if($_POST["Anzahl_der_Oeffnungen"]==""){
			echo "Anzahl_der_Oeffnungen %% Leeres Feld";
			wp_die();
		}
		if($_POST["Anzahl_der_Oeffnungen"]!=""){
			$Anzahl_der_Oeffnungen = test_input($_POST["Anzahl_der_Oeffnungen"]);
		}
		if(!preg_match("/^[0-9]*$/",$Anzahl_der_Oeffnungen)){
			echo"Anzahl_der_Oeffnungen %% Unerlaubte Zeichen ( Erlaubt 0-9 )";
			wp_die();
		}
		if(!is_numeric($Anzahl_der_Oeffnungen )):
			echo "Anzahl_der_Oeffnungen %% Keine numerische Eingabe<br>";
			wp_die();
		endif;
		if($Anzahl_der_Oeffnungen<1||$Anzahl_der_Oeffnungen>5){
			echo "Anzahl_der_Oeffnungen %% Grenzwerte: n >= 1 und n <= 5";
			wp_die();
		}
		/*
		if(!preg_match("#([0-9]+\.[0-9]+)#",$Anzahl_der_Oeffnungen )):
			echo "Anzahl_der_Oeffnungen %% Number is no double<br>";
			wp_die();
		endif;
		*/
		
		$n = $Anzahl_der_Oeffnungen;
		
		//$Anzahl_der_Oeffnungen = doubleval  ( $Anzahl_der_Oeffnungen );
		$form_callback_data["Anzahl_der_Oeffnungen"] = $Anzahl_der_Oeffnungen;
		
		
		// #####
		
		
		// Länge der Einzelöffnungen L1
		// L1 > 0.5 * Lges/n; L1< 2 * Lges/n  Wert: double
		
		if($_POST["Laenge_der_Einzeloeffnungen_L1"]==""){
			echo "Laenge_der_Einzeloeffnungen_L1 %% Leeres Feld";
			wp_die();
		}
		if($_POST["Laenge_der_Einzeloeffnungen_L1"]!=""){
			$Laenge_der_Einzeloeffnungen_L1 = test_input($_POST["Laenge_der_Einzeloeffnungen_L1"]);
		}
		if(!preg_match("/^[0-9.]*$/",$Laenge_der_Einzeloeffnungen_L1)){
			echo"Laenge_der_Einzeloeffnungen_L1 %% Unerlaubte Zeichen ( Erlaubt 0-9 und Point)";
			wp_die();
		}
		if(!is_numeric($Laenge_der_Einzeloeffnungen_L1 )):
			echo "Laenge_der_Einzeloeffnungen_L1 %% Keine numerische Eingabe<br>";
			wp_die();
		endif;
		
		//Grenzwerte L1 > 0.5 * Lges/n; L1< 2 * Lges/n
		
		$L1_min = 0.5*$Lges/$n;
		$L1_max = 2*$Lges/$n;
		
		if($Laenge_der_Einzeloeffnungen_L1<=$L1_min||$Laenge_der_Einzeloeffnungen_L1>=$L1_max){
			echo "Laenge_der_Einzeloeffnungen_L1 %% Grenzwerte: L1 > ".$L1_min." und L1 < ".$L1_max;
			wp_die();
		}
		if(!preg_match("#([0-9]+\.[0-9]+)#",$Laenge_der_Einzeloeffnungen_L1 )):
			echo "Laenge_der_Einzeloeffnungen_L1 %% Number is no double<br>";
			wp_die();
		endif;
		
		//$Laenge_der_Einzeloeffnungen_L1 = doubleval  ( $Laenge_der_Einzeloeffnungen_L1 );
		$form_callback_data["Laenge_der_Einzeloeffnungen_L1"] = $Laenge_der_Einzeloeffnungen_L1;
		
		
		
		// #####
		
		
		
		// Länge der Einzelöffnungen L2
		// L2 > 0.5 * Lges/n; L2< 2 * Lges/n Falls der Wert nicht eingeben wurde, weil nur 1 Öffnung vorhanden, soll in die CSV Datei der Wert 0 eingetragen werden  Wert:Double
		
		/*
		if($_POST["Laenge_der_Einzeloeffnungen_L2"]==""){
			echo "Laenge_der_Einzeloeffnungen_L2 %% Leeres Feld";
			wp_die();
		}
		*/
		if($_POST["Laenge_der_Einzeloeffnungen_L2"] != "" && $_POST["Laenge_der_Einzeloeffnungen_L2"] != 0){
			
			if($_POST["Laenge_der_Einzeloeffnungen_L2"]!=""){
				$Laenge_der_Einzeloeffnungen_L2 = test_input($_POST["Laenge_der_Einzeloeffnungen_L2"]);
			}
			if(!preg_match("/^[0-9.]*$/",$Laenge_der_Einzeloeffnungen_L2)){
				echo"Laenge_der_Einzeloeffnungen_L2 %% Unerlaubte Zeichen ( Erlaubt 0-9 und Point)";
				wp_die();
			}
			if(!is_numeric($Laenge_der_Einzeloeffnungen_L2 )):
				echo "Laenge_der_Einzeloeffnungen_L2 %% Keine numerische Eingabe<br>";
				wp_die();
			endif;
			
			//Grenzwerte L2 > 0.5 * Lges/n; L2< 2 * Lges/n
			
			$L2_min = 0.5*$Lges/$n;
			$L2_max = 2*$Lges/$n;
			
			if($Laenge_der_Einzeloeffnungen_L2<=$L2_min||$Laenge_der_Einzeloeffnungen_L2>=$L2_max){
				echo "Laenge_der_Einzeloeffnungen_L2 %% Grenzwerte: L2 > ".$L2_min." und L2 < ".$L2_max;
				wp_die();
			}
			if(!preg_match("#([0-9]+\.[0-9]+)#",$Laenge_der_Einzeloeffnungen_L2 )):
				echo "Laenge_der_Einzeloeffnungen_L2 %% Number is no double<br>";
				wp_die();
			endif;
			
			//$Laenge_der_Einzeloeffnungen_L2 = doubleval  ( $Laenge_der_Einzeloeffnungen_L2 );
			$form_callback_data["Laenge_der_Einzeloeffnungen_L2"] = $Laenge_der_Einzeloeffnungen_L2;
			
		}
		else if($_POST["Laenge_der_Einzeloeffnungen_L2"]==""||$_POST["Laenge_der_Einzeloeffnungen_L2"]==0){
			
			$form_callback_data["Laenge_der_Einzeloeffnungen_L2"] = "0";
			
		}
		
		
		// #####
		
		
		// Länge der Einzelöffnungen L3
		// L3 > 0.5 * Lges/n; L3< 2 * Lges/n Falls der Wert nicht eingeben wurde, weil nur 2 Öffnungen vorhanden, soll in die CSV Datei der Wert 0 eingetragen werden  Wert:Double
		
		/*
		if($_POST["Laenge_der_Einzeloeffnungen_L3"]==""){
			echo "Laenge_der_Einzeloeffnungen_L3 %% Leeres Feld";
			wp_die();
		}
		*/
		if($_POST["Laenge_der_Einzeloeffnungen_L3"]!=""&&$_POST["Laenge_der_Einzeloeffnungen_L3"]!=0){
			
			if($_POST["Laenge_der_Einzeloeffnungen_L3"]!=""){
				$Laenge_der_Einzeloeffnungen_L3 = test_input($_POST["Laenge_der_Einzeloeffnungen_L3"]);
			}
			if(!preg_match("/^[0-9.]*$/",$Laenge_der_Einzeloeffnungen_L3)){
				echo"Laenge_der_Einzeloeffnungen_L3 %% Unerlaubte Zeichen ( Erlaubt 0-9 und Point)";
				wp_die();
			}
			if(!is_numeric($Laenge_der_Einzeloeffnungen_L3 )):
				echo "Laenge_der_Einzeloeffnungen_L3 %% Keine numerische Eingabe<br>";
				wp_die();
			endif;
			
			//Grenzwerte L3 > 0.5 * Lges/n; L3< 2 * Lges/n
			
			$L3_min = 0.5*$Lges/$n;
			$L3_max = 2*$Lges/$n;
			
			if($Laenge_der_Einzeloeffnungen_L3<=$L3_min||$Laenge_der_Einzeloeffnungen_L3>=$L3_max){
				echo "Laenge_der_Einzeloeffnungen_L3 %% Grenzwerte: L3 > ".$L3_min." und L3 < ".$L3_max;
				wp_die();
			}
			if(!preg_match("#([0-9]+\.[0-9]+)#",$Laenge_der_Einzeloeffnungen_L3 )):
				echo "Laenge_der_Einzeloeffnungen_L3 %% Number is no double<br>";
				wp_die();
			endif;
			
			//$Laenge_der_Einzeloeffnungen_L3 = doubleval  ( $Laenge_der_Einzeloeffnungen_L3 );
			$form_callback_data["Laenge_der_Einzeloeffnungen_L3"] = $Laenge_der_Einzeloeffnungen_L3;
			
		}
		else if($_POST["Laenge_der_Einzeloeffnungen_L3"]==""||$_POST["Laenge_der_Einzeloeffnungen_L3"]==0){
			
			$form_callback_data["Laenge_der_Einzeloeffnungen_L3"] = "0";
			
		}
		
		
		// #####
		
		
		// Länge der Einzelöffnungen L4
		// L4 > 0.5 * Lges/n; L4< 2 * Lges/n Falls der Wert nicht eingeben wurde, weil nur 3 Öffnungen vorhanden, soll in die CSV Datei der Wert 0 eingetragen werden  Wert:Double
		
		/*
		if($_POST["Laenge_der_Einzeloeffnungen_L4"]==""){
			echo "Laenge_der_Einzeloeffnungen_L4 %% Leeres Feld";
			wp_die();
		}
		*/
		if($_POST["Laenge_der_Einzeloeffnungen_L4"]!=""&&$_POST["Laenge_der_Einzeloeffnungen_L4"]!=0){
			
			if($_POST["Laenge_der_Einzeloeffnungen_L4"]!=""){
				$Laenge_der_Einzeloeffnungen_L4 = test_input($_POST["Laenge_der_Einzeloeffnungen_L4"]);
			}
			if(!preg_match("/^[0-9.]*$/",$Laenge_der_Einzeloeffnungen_L4)){
				echo"Laenge_der_Einzeloeffnungen_L4 %% Unerlaubte Zeichen ( Erlaubt 0-9 und Point)";
				wp_die();
			}
			if(!is_numeric($Laenge_der_Einzeloeffnungen_L4 )):
				echo "Laenge_der_Einzeloeffnungen_L4 %% Keine numerische Eingabe<br>";
				wp_die();
			endif;
			
			//Grenzwerte L4 > 0.5 * Lges/n; L4< 2 * Lges/n
			
			$L4_min = 0.5*$Lges/$n;
			$L4_max = 2*$Lges/$n;
			
			if($Laenge_der_Einzeloeffnungen_L4<=$L4_min||$Laenge_der_Einzeloeffnungen_L4>=$L4_max){
				echo "Laenge_der_Einzeloeffnungen_L4 %% Grenzwerte: L4 > ".$L4_min." und L4 < ".$L4_max;
				wp_die();
			}
			if(!preg_match("#([0-9]+\.[0-9]+)#",$Laenge_der_Einzeloeffnungen_L4 )):
				echo "Laenge_der_Einzeloeffnungen_L4 %% Number is no double<br>";
				wp_die();
			endif;
			
			//$Laenge_der_Einzeloeffnungen_L4 = doubleval  ( $Laenge_der_Einzeloeffnungen_L4 );
			$form_callback_data["Laenge_der_Einzeloeffnungen_L4"] = $Laenge_der_Einzeloeffnungen_L4;
		}
		else if($_POST["Laenge_der_Einzeloeffnungen_L4"]==""||$_POST["Laenge_der_Einzeloeffnungen_L4"]==0){
			
			$form_callback_data["Laenge_der_Einzeloeffnungen_L4"] = "0";
			
		}
		
		// #####
		
		// Länge der Einzelöffnungen L5
		// L5 > 0.5 * Lges/n; L5< 2 * Lges/n Falls der Wert nicht eingeben wurde, weil nur 4 Öffnungen vorhanden, soll in die CSV Datei der Wert 0 eingetragen werden  Wert:Double
		
		/*
		if($_POST["Laenge_der_Einzeloeffnungen_L5"]==""){
			echo "Laenge_der_Einzeloeffnungen_L5 %% Leeres Feld";
			wp_die();
		}
		*/
		if($_POST["Laenge_der_Einzeloeffnungen_L5"]!=""&&$_POST["Laenge_der_Einzeloeffnungen_L5"]!=0){
			
			if($_POST["Laenge_der_Einzeloeffnungen_L5"]!=""){
				$Laenge_der_Einzeloeffnungen_L5 = test_input($_POST["Laenge_der_Einzeloeffnungen_L5"]);
			}
			if(!preg_match("/^[0-9.]*$/",$Laenge_der_Einzeloeffnungen_L5)){
				echo"Laenge_der_Einzeloeffnungen_L5 %% Unerlaubte Zeichen ( Erlaubt 0-9 und Point)";
				wp_die();
			}
			if(!is_numeric($Laenge_der_Einzeloeffnungen_L5 )):
				echo "Laenge_der_Einzeloeffnungen_L5 %% Keine numerische Eingabe<br>";
				wp_die();
			endif;
			
			//Grenzwerte L5 > 0.5 * Lges/n; L5< 2 * Lges/n
			
			$L5_min = 0.5*$Lges/$n;
			$L5_max = 2*$Lges/$n;
			
			if($Laenge_der_Einzeloeffnungen_L5<=$L5_min||$Laenge_der_Einzeloeffnungen_L5>=$L5_max){
				echo "Laenge_der_Einzeloeffnungen_L5 %% Grenzwerte: L5 > ".$L5_min." und L5 < ".$L5_max;
				wp_die();
			}
			if(!preg_match("#([0-9]+\.[0-9]+)#",$Laenge_der_Einzeloeffnungen_L5 )):
				echo "Laenge_der_Einzeloeffnungen_L5 %% Number is no double<br>";
				wp_die();
			endif;
			
			//$Laenge_der_Einzeloeffnungen_L5 = doubleval  ( $Laenge_der_Einzeloeffnungen_L5 );
			$form_callback_data["Laenge_der_Einzeloeffnungen_L5"] = $Laenge_der_Einzeloeffnungen_L5;
		}
		else if($_POST["Laenge_der_Einzeloeffnungen_L5"]==""||$_POST["Laenge_der_Einzeloeffnungen_L5"]==0){
			
			$form_callback_data["Laenge_der_Einzeloeffnungen_L5"] = "0";
			
		}
		
		
		// #####
		
		
		// Breite Betonbalken
		// Grenzen: >0.1; <1.5? (muss noch ermittelt werden)  Wert: Double
		
		if($_POST["Breite_Betonbalken"]==""){
			echo "Breite_Betonbalken %% Leeres Feld";
			wp_die();
		}
		if($_POST["Breite_Betonbalken"]!=""){
			$Breite_Betonbalken = test_input($_POST["Breite_Betonbalken"]);
		}
		if(!preg_match("/^[0-9.]*$/",$Breite_Betonbalken)){
			echo"Breite_Betonbalken %% Unerlaubte Zeichen ( Erlaubt 0-9 und Point)";
			wp_die();
		}
		if(!is_numeric($Breite_Betonbalken )):
			echo "Breite_Betonbalken %% Keine numerische Eingabe<br>";
			wp_die();
		endif;
		if($Breite_Betonbalken<=0.1||$Breite_Betonbalken>=1.5){
			echo "Breite_Betonbalken %% Grenzwerte: >0.1 und <1.5";
			wp_die();
		}
		if(!preg_match("#([0-9]+\.[0-9]+)#",$Breite_Betonbalken )):
			echo "Breite_Betonbalken %% Number is no double<br>";
			wp_die();
		endif;
		
		//$Breite_Betonbalken = doubleval  ( $Breite_Betonbalken );
		$form_callback_data["Breite_Betonbalken"] = $Breite_Betonbalken;
		
		
		// #####
		
		
		// Deckenhöhe hc
		// Grenzen: > 0.1; <1 ?  Wert: Double
		
		if($_POST["Deckenhoehe_hc"]==""){
			echo "Deckenhoehe_hc %% Leeres Feld";
			wp_die();
		}
		if($_POST["Deckenhoehe_hc"]!=""){
			$Deckenhoehe_hc = test_input($_POST["Deckenhoehe_hc"]);
		}
		if(!preg_match("/^[0-9.]*$/",$Deckenhoehe_hc)){
			echo"Deckenhoehe_hc %% Unerlaubte Zeichen ( Erlaubt 0-9 und Point)";
			wp_die();
		}
		if(!is_numeric($Deckenhoehe_hc )):
			echo "Deckenhoehe_hc %% Keine numerische Eingabe<br>";
			wp_die();
		endif;
		if($Deckenhoehe_hc<=0.1||$Deckenhoehe_hc>=1){
			echo "Deckenhoehe_hc %% Grenzwerte: >0.1 und <1";
			wp_die();
		}
		if(!preg_match("#([0-9]+\.[0-9]+)#",$Deckenhoehe_hc )):
			echo "Deckenhoehe_hc %% Number is no double<br>";
			wp_die();
		endif;
		
		$hc = $Deckenhoehe_hc;
		
		
		// #####
		
		
		// Höhe hges
		// Grenzen: >hc; <2 ? (muss noch ermittelt werden)  Wert: Double
		
		if($_POST["Hoehe_hges"]==""){
			echo "Hoehe_hges %% Leeres Feld";
			wp_die();
		}
		if($_POST["Hoehe_hges"]!=""){
			$Hoehe_hges = test_input($_POST["Hoehe_hges"]);
		}
		if(!preg_match("/^[0-9.]*$/",$Hoehe_hges)){
			echo"Hoehe_hges %% Unerlaubte Zeichen ( Erlaubt 0-9 und Point)";
			wp_die();
		}
		if(!is_numeric($Hoehe_hges )):
			echo "Hoehe_hges %% Keine numerische Eingabe<br>";
			wp_die();
		endif;
		
		if(empty($hc)){
			echo "Deckenhoehe_hc %% Deckenhoehe_hc wird benötigt für Hoehe_hges";
			wp_die();
		}
		
		if($Hoehe_hges<=$hc||$Hoehe_hges>=2){
			echo "Hoehe_hges %% Grenzwerte: >".$hc." und <2";
			wp_die();
		}
		if(!preg_match("#([0-9]+\.[0-9]+)#",$Hoehe_hges )):
			echo "Hoehe_hges %% Number is no double<br>";
			wp_die();
		endif;
		
		//$Hoehe_hges = doubleval  ( $Hoehe_hges );
		$form_callback_data["Hoehe_hges"] = $Hoehe_hges;
		
		
		//$Deckenhoehe_hc = doubleval  ( $Deckenhoehe_hc );
		$form_callback_data["Deckenhoehe_hc"] = $Deckenhoehe_hc;
		
		
		
		// #####
		
		
		// Qed,1
		// Grenzen: > -500? (muss noch ermittelt werden)  Wert: Double
		
		if($_POST["Qed_1"]==""){
			echo "Qed_1 %% Leeres Feld";
			wp_die();
		}
		if($_POST["Qed_1"]!=""){
			$Qed_1 = test_input($_POST["Qed_1"]);
		}
		if(!preg_match("/^[0-9.-]*$/",$Qed_1)){
			echo"Qed_1 %% Unerlaubte Zeichen ( Erlaubt 0-9 und Point)";
			wp_die();
		}
		if(!is_numeric($Qed_1 )):
			echo "Qed_1 %% Keine numerische Eingabe<br>";
			wp_die();
		endif;
		if($Qed_1<=-500){
			echo "Qed_1 %% Grenzwerte: > -500";
			wp_die();
		}
		if(!preg_match("#([0-9]+\.[0-9]+)#",$Qed_1 )):
			echo "Qed_1 %% Number is no double<br>";
			wp_die();
		endif;
		
		
		
		// #####
		
		
		// Med,1
		// Grenzen: > 0; <10000? (muss noch ermitteltw erden) UND: Med,1 > qEd * Lges^2 /2 – Qed,1 * Lges  Wert: Double
		
		if($_POST["Med_1"]==""){
			echo "Med_1 %% Leeres Feld";
			wp_die();
		}
		if($_POST["Med_1"]!=""){
			$Med_1 = test_input($_POST["Med_1"]);
		}
		if(!preg_match("/^[0-9.-]*$/",$Med_1)){
			echo"Med_1 %% Unerlaubte Zeichen ( Erlaubt 0-9 und Point)";
			wp_die();
		}
		if(!is_numeric($Med_1 )):
			echo "Med_1 %% Keine numerische Eingabe<br>";
			wp_die();
		endif;
		
		$Lges_hoch_2 = bcpow($Lges,'2',0);
		//$form_callback_data["Lges_hoch_2"] = $Lges_hoch_2;
		
		if(empty($Qed_1)){
			echo "Qed_1 %% Qed_1 wird benötigt für Med_1";
			wp_die();
		}
		
		$Med_1_und = ($qEd*$Lges_hoch_2/2)-($Qed_1*$Lges);
		
		if($Med_1<=0||$Med_1<=$Med_1_und||$Med_1>=10000){
			echo "Med_1 %% Grenzwerte: > 0 und > ".$Med_1_und." und <10000";
			wp_die();
		}
		
		if(!preg_match("#([0-9]+\.[0-9]+)#",$Med_1 )):
			echo "Med_1 %% Number is no double<br>";
			wp_die();
		endif;
		
		//$Med_1 = doubleval  ( $Med_1 );
		$form_callback_data["Med_1"] = $Med_1;
		
		//$Qed_1 = doubleval  ( $Qed_1 );
		$form_callback_data["Qed_1"] = $Qed_1;
		
		
		// #####
		
		
		//  Qedmax
		// Grenzen: >= Qed,1 > -500?; (muss noch ermittelt werden)
		
		if($_POST["Qedmax"]==""){
			echo "Qedmax %% Leeres Feld";
			wp_die();
		}
		if($_POST["Qedmax"]!=""){
			$Qedmax = test_input($_POST["Qedmax"]);
		}
		if(!preg_match("/^[0-9.-]*$/",$Qedmax)){
			echo"Qedmax %% Unerlaubte Zeichen ( Erlaubt 0-9 und Point)";
			wp_die();
		}
		if(!is_numeric($Qedmax )):
			echo "Qedmax %% Keine numerische Eingabe<br>";
			wp_die();
		endif;
		if($Qedmax < $Qed_1 || $Qedmax <= -500){
			echo "Qedmax %% Grenzwerte: >= ".$Qed_1." und Qedmax > -500";
			wp_die();
		}
		if(!preg_match("#([0-9]+\.[0-9]+)#",$Qedmax )):
			echo "Qedmax %% Number is no double<br>";
			wp_die();
		endif;
		
		//$Qedmax = doubleval  ( $Qedmax );
		$form_callback_data["Qedmax"] = $Qedmax;
		
		
		
		// Ende der Form-Fields Tests
		// ################################################################################
		
		
		
		//include get_theme_file_path( '../../../wp-admin/p-form-edit.php' );
		
		ini_set("error_reporting", 2047);

		ini_set("display_errors",0);
		ini_set("log_errors",1);
		ini_set("error_log", "csv_dra/error_log_functions_php.txt");

		$p_form_process_report=array();

		$csv_header="";
		$csv_data="";

		$csv_header.="ID;";

		$id_file="csv_dra/id.txt";
		$fp=fopen($id_file,"r+");
		flock($fp,LOCK_EX);
		$id=fgets($fp,10);
		$new_id=$id+1;
		ftruncate($fp,0);
		fseek ($fp,0);
		fputs($fp,$new_id);
		flock($fp,LOCK_UN);
		fclose($fp);

		$date=date("Ymd");

		if(!empty($id)){
			$csv_data.=$date.$id.",";
		}else{
			$p_form_process_report["Form_CSV_ID_error"]=date("d.m.Y, h:i:s")." -> Form CSV ID nicht vorhanden in ".__FILE__ ." on Line: ".__LINE__;
		}

		if(!empty($form_callback_data)){
			
            
           
            
			$csv_file="csv_dra/form_input/ID_".$date.$id.".csv";
			$fp_csv_file=fopen($csv_file,"a");
			flock($fp_csv_file,LOCK_EX);
            

          
           
			
			foreach($form_callback_data as $index => $value){
				$index=preg_replace("/_/"," ",$index);
				$csv_header.=$index.";";
				$csv_data.=$value.",";
			}
			
			$csv_header=rtrim($csv_header,",")."\n";
			$csv_data=rtrim($csv_data,",")."\n";

			//fputs ($fp_csv_file,$csv_header);
			fputs ($fp_csv_file,$csv_data);
			flock($fp_csv_file,LOCK_UN);
			fclose($fp_csv_file);

            /*
            echo "csv_file: ".$csv_file." - ";
            echo "<br>";
            echo "fp_csv_file: ".$fp_csv_file." - ";
            
            echo "csv_data: ".$csv_data." - ";
            echo "<br>";
            echo "csv_header: ".$csv_header." - ";
            */
                   
            
            
            
		}
		
		$local_csv_file = $csv_file;
		if(file_exists($local_csv_file)){
			//$form_callback_data["local_csv_file"] = $local_csv_file.": exist";
			$p_form_process_report["local_csv_file"]=date("d.m.Y, h:i:s")." ".$local_csv_file.": exist";
		}
		else{
			$form_callback_data["local_csv_file"] = $local_csv_file." no exist";
			$p_form_process_report["local_csv_file"]=date("d.m.Y, h:i:s")." ".$local_csv_file.": no exist";
		}
		
		//$ftp_server = "18.196.72.86";
        $ftp_server = "85.215.146.38";
		
		$ftp_conn = ftp_connect($ftp_server) or die("Could not connect to $ftp_server");
		
		if($ftp_conn==false){
			$form_callback_data["ftp_connect"] = "Could not connect to $ftp_server";
			$p_form_process_report["ftp_connect"] = date("d.m.Y, h:i:s").": Could not connect to $ftp_server";
		}
		else{
			//$form_callback_data["ftp_connect"] = "Connect is ok";
			$p_form_process_report["ftp_connect"] = date("d.m.Y, h:i:s").": Ftp connect to $ftp_server is ok";
		}
		
		//$ftp_username="ftp_user";
		//$ftp_userpass="jashk87%U";
        
		$ftp_username="FTPWinUser";
		$ftp_userpass="eZ6wT7F8dUMLfjszawVVuMtBJksThh";        
        
		
		if (ftp_login($ftp_conn, $ftp_username, $ftp_userpass)) {
			//$form_callback_data["ftp_login"] = "is ok";
			$p_form_process_report["ftp_login"] = date("d.m.Y, h:i:s").": is ok";
		} else {
			$form_callback_data["ftp_login"] = "is false";
			$p_form_process_report["ftp_login"]=date("d.m.Y, h:i:s").": is false";
		}
		
		// initiate upload
		$server_csv_file="ID_".$date.$id.".csv";
		
		if (ftp_alloc($ftp_conn, filesize($local_csv_file), $result)) {
			//$form_callback_data["ftp_alloc"] = "Platz wurde erfolgreich auf dem Server reseviert.";
			$p_form_process_report["ftp_alloc"] = "Platz wurde erfolgreich auf dem Server reseviert.";

			ftp_pasv($ftp_conn, true);
			
			$ftp_nb_put = ftp_nb_put($ftp_conn, $server_csv_file, $local_csv_file, FTP_ASCII); //FTP_BINARY

			if($ftp_nb_put==FTP_FINISHED){
				//$form_callback_data["FTP_FINISHED"] = "Ftp upload success";
				$p_form_process_report["FTP_FINISHED"]=date("d.m.Y, h:i:s")." Ftp upload success";
			}
			else{
				$form_callback_data["FTP_FINISHED"] = "Ftp upload no success";
				$p_form_process_report["FTP_FINISHED"]=date("d.m.Y, h:i:s")." Ftp upload no success";
			}
		  
		  
		} else {
			$form_callback_data["ftp_alloc"] = "Platz konnte nicht auf dem Server reserviert werden. Serverantwort: $result";
			$p_form_process_report["ftp_alloc"] = "Platz konnte nicht auf dem Server reserviert werden. Serverantwort: $result";
		}
		
		
		
		//################################
		// Abschnitt nur zum testen Anfang
		
		$old_file="csv_dra/old_file/old_file.txt";
		if(file_exists($old_file)){
			$fp=fopen($old_file,"r");
			flock($fp,LOCK_EX);
			$old_file_name=fgets($fp,100);
			flock($fp,LOCK_UN);
			fclose($fp);
			
			//$form_callback_data["old_file_name"] = $old_file_name;
			$new_file_name=$server_csv_file;
			//$form_callback_data["new_file_name"] = $new_file_name;
			
			// file umbenen
			ftp_chdir($ftp_conn, 'output');
			//$form_callback_data["Verzeichnis_wechseln to"] = "output";
			
			//$form_callback_data["Aktuelles_Verzeichnis"] = ftp_pwd($ftp_conn);
			
			ftp_rename($ftp_conn, $old_file_name, $new_file_name);
			
			ftp_cdup($ftp_conn);
			//$form_callback_data["Aktuelles_Verzeichnis"] = ftp_pwd($ftp_conn);
		}
		else{
			//$form_callback_data["old_file"] = "no File old_file.txt";
		}
		
		$old_file="csv_dra/old_file/old_file.txt";
		$fp=fopen($old_file,"w");
		flock($fp,LOCK_EX);
		fputs($fp,$server_csv_file);
		flock($fp,LOCK_UN);
		fclose($fp);
		
		// Abschnitt nur zum testen Ende
		//##############################
		
		
		
		$save_local_csv_file="csv_dra/get_server_result/".$server_csv_file;
		$get_server_csv_file="output/".$server_csv_file;
		
		$start_unix_time=date("U");
		//$form_callback_data["start_unix_time"] = $start_unix_time;
		
		$start_unix_time_for_ftp_get=date("U");
		
		$first_loop=false;
		
		for($x=0;$x<1000000;$x++){
			
			if(!$first_loop){
				
				$first_loop=true;
				
				$ftp_get = @ftp_get($ftp_conn, $save_local_csv_file, $get_server_csv_file, FTP_ASCII);
						
				//$form_callback_data["ftp_get"] = $ftp_get;
				
				if($ftp_get){
					//$form_callback_data["ftp_get_FINISHED"] = date("d.m.Y, h:i:s")." Ftp DOWNLOAD success";
					$p_form_process_report["ftp_get_FINISHED"]=date("d.m.Y, h:i:s")." Ftp DOWNLOAD success";
					break;
				}
				
			}
			
			$current_unix_time_for_ftp_get=date("U");
			$running_time_for_ftp_get=$current_unix_time_for_ftp_get - $start_unix_time_for_ftp_get;
			
			// Interval für Abfragen für Output-CSV-File in Sekunden
			if($running_time_for_ftp_get>5){
				
				$start_unix_time_for_ftp_get=date("U");
				
				$ftp_get = @ftp_get($ftp_conn, $save_local_csv_file, $get_server_csv_file, FTP_ASCII);
					
				//$form_callback_data["ftp_get"] = $ftp_get;
				
				if($ftp_get){
					//$form_callback_data["ftp_get_FINISHED"] = "Ftp DOWNLOAD success";
					$p_form_process_report["FTP_FINISHED"]=date("d.m.Y, h:i:s")." Ftp upload success";
					break;
				}
				
			}
			else{
				
				$current_unix_time=date("U");
				$running_time=$current_unix_time - $start_unix_time;
				
				// Wartezeit auf Output-CSV-File in Sekunden insegsammt
				if($running_time>15){
					
					//$form_callback_data["running_time"] = $running_time;
					$form_callback_data["ftp_get_FINISHED"] = date("d.m.Y, h:i:s")." Ftp DOWNLOAD no success";
					$p_form_process_report["FTP_FINISHED"]=date("d.m.Y, h:i:s")." Ftp upload no success";
					break;
					
				}
				
			}
			
		}
		
		
		
		//$p_form_process_report["p_form_process_report_test_1"]="p_form_process_report_test";

		$file_process_report="csv_dra/file_process_report.txt";
		$fp_file_process_report=fopen($file_process_report,"a");
		flock($fp_file_process_report,LOCK_EX);

		if(!empty($p_form_process_report)){
			foreach($p_form_process_report as $index => $value){
				
				fputs ($fp_file_process_report, $index." : ".$value."\n");
				
			}
		}
		flock($fp_file_process_report,LOCK_UN);
		fclose($fp_file_process_report);

		
		// get output file cells names
		
		$output_file_cells_names_array=file("csv_dra/output_csv_file_cells_names.txt");
		$output_file_cells_names_array_count=count($output_file_cells_names_array);
		//$form_callback_data["output_file_cells_names_array_count"] = $output_file_cells_names_array_count;
		$y=0;
		
		// Ausgabe Array
		$get_server_result_data = array();
		
		if(file_exists($save_local_csv_file)){
			
			$fp_save_local_csv_file=fopen($save_local_csv_file,"r");
			
			if($fp_save_local_csv_file){
				while(($zeile_save_local_csv_file=fgetcsv($fp_save_local_csv_file,1000,','))!== FALSE){
					
					for($y;$y<$output_file_cells_names_array_count;$y++){
							
						$get_server_result_data[$output_file_cells_names_array[$y]]=$zeile_save_local_csv_file[$y];
						
					}
					
				}
				fclose($fp_save_local_csv_file);
			}
			else{
				$form_callback_data["get_server_result"] = "Kein Output CSV File";
			}
			
			$arrays_collector_array=array();
			$arrays_collector_array["form_callback_data"]=$form_callback_data;
			$arrays_collector_array["get_server_result_data"]=$get_server_result_data;
		
		}
		else{
			
			$arrays_collector_array=array();
			$arrays_collector_array["form_callback_data"]=$form_callback_data;
			
		}
		
		
		
		
		$form_callback_data["test_end"] = "ok";
		
		//echo json_encode($form_callback_data);
		//echo json_encode($get_server_result_data);
        //print_r($arrays_collector_array);
        
		echo json_encode($arrays_collector_array);
        die();
		
		
		/*
		$array=array();
		$array[0]="Name";
		$serial_arr=urlencode(base64_encode(serialize($array)));
		*/
		
	}
	
	
	
	
	
    wp_die();
}

// Here we register our "send_form" function to handle our AJAX request, do you remember the "superhypermega" hidden field? Yes, this is what it refers, the "send_form" action.
add_action('wp_ajax_send_form', 'edit_form'); // This is for authenticated users
add_action('wp_ajax_nopriv_send_form', 'edit_form'); // This is for unauthenticated users.
?>