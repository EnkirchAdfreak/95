<?php
/*
echo "error_reporting: " . ini_get("error_reporting")."<br>";
echo "display_errors: " . ini_get("display_errors")."<br>";
echo "log_errors: " . ini_get("log_errors")."<br>";
echo "error_log: " . ini_get("error_log")."<br>";
echo"<p></p>";
*/
//ini_set("error_reporting", 2047);

ini_set("display_errors",0);
ini_set("log_errors",0);
ini_set("error_log", get_home_path()."wp-content/configurator_data/beamup_data/log_files/error_log_hkr_index.txt");

/*
echo "error_reporting: " . ini_get("error_reporting")."<br>";
echo "display_errors: " . ini_get("display_errors")."<br>";
echo "log_errors: " . ini_get("log_errors")."<br>";
echo "error_log: " . ini_get("error_log")."<br>";
*/

function javascript_variables(){ ?>
    <script type="text/javascript">
        var ajax_url = '<?php echo admin_url( "admin-ajax.php" ); ?>';
        var ajax_nonce = '<?php echo wp_create_nonce( "secure_nonce_name" ); ?>';
    </script><?php
}
add_action ( 'wp_head', 'javascript_variables' );
 
function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
function edit_form(){
     
    if($_SERVER["REQUEST_METHOD"] == "POST") {
		
		//echo"functions.php\n";
		//echo get_home_path();
		//exit;
		
		ini_set("error_reporting", 2047);
		ini_set("display_errors",0);
		ini_set("log_errors",1);
		ini_set("error_log", get_home_path()."wp-content/configurator_data/beamup_data/log_files/error_log_functions_php.txt");
		
		$form_callback_data = array();
		$form_input_errors_array = array();
		$form_inputs_data_for_csv_array = array();
		
		$form_callback_data["get_home_path"] = get_home_path();
		
		
		// Deckentyp
		//  index = 1-3
        
        $currentLanguage = $_POST['language'];
        
		
		$Deckentyp = $_POST['Deckentyp'];
		if($Deckentyp=="0"){
			
			
            
            
            if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                if ( $currentLanguage == 'en' ) {
                    $form_input_errors_array["Deckentyp_errors"]="Please select index";
                }

                if ( $currentLanguage == 'de' ) {
                     $form_input_errors_array["Deckentyp_errors"]="Bitte Index auswählen";
                }

                if ( $currentLanguage == 'es' ) {
                    $form_input_errors_array["Deckentyp_errors"]="Por favor, seleccione el índice";
                }

            }
            
            $form_callback_data["Deckentyp"] = $Deckentyp;
            
            
		}
		else{
			$form_callback_data["Deckentyp"] = $Deckentyp;
			$form_inputs_data_for_csv_array["Deckentyp"] = $Deckentyp;
		}
		//echo"Deckentyp: ".$Deckentyp;
		//exit;
		
		// #####
		
		
		// Festigkeitsklasse Beton
		//  index = 1 -7
		
		$Festigkeitsklasse_Beton = $_POST['Festigkeitsklasse_Beton'];
		if($Festigkeitsklasse_Beton=="0"){
			
            
            
            if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                if ( $currentLanguage == 'en' ) {
                   
                    $form_input_errors_array["Festigkeitsklasse_Beton_errors"]="Please select index";
                }

                if ( $currentLanguage == 'de' ) {
                    $form_input_errors_array["Festigkeitsklasse_Beton_errors"]="Bitte Index auswählen";
                }

                if ( $currentLanguage == 'es' ) {
                    $form_input_errors_array["Festigkeitsklasse_Beton_errors"]="Por favor, seleccione el índice";
                }

            }
            
            
            
			$form_callback_data["Festigkeitsklasse_Beton"] = $Festigkeitsklasse_Beton;
		}
		else{
			$form_callback_data["Festigkeitsklasse_Beton"] = $Festigkeitsklasse_Beton;
			$form_inputs_data_for_csv_array["Festigkeitsklasse_Beton"] = $Festigkeitsklasse_Beton;
		}
		
		
		// #####
		
		
		// Expositionsklasse
		// index = 1-2
		
		$Expositionsklasse = $_POST['Expositionsklasse'];
		if($Expositionsklasse=="0"){
			
            
            if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                if ( $currentLanguage == 'en' ) {
                   
                    $form_input_errors_array["Expositionsklasse_errors"]="Please select index";
                }

                if ( $currentLanguage == 'de' ) {
                   $form_input_errors_array["Expositionsklasse_errors"]="Bitte Index auswählen";
                }

                if ( $currentLanguage == 'es' ) {
                   $form_input_errors_array["Expositionsklasse_errors"]="Por favor, seleccione el índice";
                }

            }
            
            
			$form_callback_data["Expositionsklasse"] = $Expositionsklasse;
		}
		else{
			$form_callback_data["Expositionsklasse"] = $Expositionsklasse;
			$form_inputs_data_for_csv_array["Expositionsklasse"] = $Expositionsklasse;
		}
		
		
		// #####
		
		
		// Feuerwiderstandsklasse
		// index = 1-4
		
		$Feuerwiderstandsklasse = $_POST['feuerwiderstandsklasse'];
		if($Feuerwiderstandsklasse=="0"){
			
            
            if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                if ( $currentLanguage == 'en' ) {
                  
                    $form_input_errors_array["Feuerwiderstandsklasse_errors"]="Please select index";
                }

                if ( $currentLanguage == 'de' ) {
                    $form_input_errors_array["Feuerwiderstandsklasse_errors"]="Bitte Index auswählen";
                }

                if ( $currentLanguage == 'es' ) {
                   $form_input_errors_array["Feuerwiderstandsklasse_errors"]="Por favor, seleccione el índice";
                }

            }
            
            
			$form_callback_data["Feuerwiderstandsklasse"] = $Feuerwiderstandsklasse;
		}
		else{
			$form_callback_data["Feuerwiderstandsklasse"] = $Feuerwiderstandsklasse;
			$form_inputs_data_for_csv_array["Feuerwiderstandsklasse"] = $Feuerwiderstandsklasse;
		}
		
		
		// #####
		
		
		// Belastung qEd (kN/m)
		// Grenzen: >=0; <= 300 Falls fehlerhafte Eingabe: „Die Belastung liegt nicht im richtigen Bereich. Sie muss zwischen qEd = 0 kN/m und 500 kN/m liegen“  Wert: double
		
		$Belastung_qEd = $_POST["Belastung_qEd"];
		//echo $Belastung_qEd."\n";
		
		$qEd="";
		$qEd_ok = false;
		
		if($Belastung_qEd==""){
			
            
            
            if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                if ( $currentLanguage == 'en' ) {
                   
                    
                     $form_input_errors_array["Belastung_qEd_errors"]="Please enter value. Limit values: >= 0 and <= 300 in (kN/m)";
                }

                if ( $currentLanguage == 'de' ) {
                   $form_input_errors_array["Belastung_qEd_errors"]="Bitte Wert eintragen. Grenzwerte: >= 0 und <= 300 in (kN/m)";
                }

                if ( $currentLanguage == 'es' ) {
                   $form_input_errors_array["Belastung_qEd_errors"]="Por favor, introduzca el valor. Valores límite: >= 0 and <= 300 in (kN/m)";
                }

            }
            
            
		}else if($Belastung_qEd!=""){
			$Belastung_qEd = test_input($Belastung_qEd);
			if(preg_match("/,/",$Belastung_qEd)){
				$Belastung_qEd=preg_replace("/,/",".",$Belastung_qEd);
			}
			if(!is_numeric($Belastung_qEd)&&preg_match("/[^0-9.,]/",$Belastung_qEd)){
				
                
                
                if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                    if ( $currentLanguage == 'en' ) {
                      
                        $form_input_errors_array["Belastung_qEd_errors"]="Unauthorized characters, allowed are 0-9";
                    }

                    if ( $currentLanguage == 'de' ) {
                         $form_input_errors_array["Belastung_qEd_errors"]="Unerlaubte Zeichen, erlaubt sind 0-9";
                    }

                    if ( $currentLanguage == 'es' ) {
                       $form_input_errors_array["Belastung_qEd_errors"] = "Los caracteres no autorizados, permitidos son 0-9";
                    }

                }
                
			}
			else{
				$Belastung_qEd = doubleval($Belastung_qEd);
				$Belastung_qEd=round($Belastung_qEd,4);
				
				if($Belastung_qEd < 0 || $Belastung_qEd > 300){
					
                    
                
                    if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                        if ( $currentLanguage == 'en' ) {
                          
                             $form_input_errors_array["Belastung_qEd_errors"]="Limit values: >=0 and <= 300 in (kN/m)";
                        }

                        if ( $currentLanguage == 'de' ) {
                           $form_input_errors_array["Belastung_qEd_errors"]="Grenzwerte: >=0 und <= 300 in (kN/m)";
                        }

                        if ( $currentLanguage == 'es' ) {
                           $form_input_errors_array["Belastung_qEd_errors"]="Valores límite: >=0 Y <= 300 in (kN/m)";
                        }

                    }
                    
				}else{
					$qEd = $Belastung_qEd;
					$qEd_ok = true;
		
					$form_callback_data["Belastung_qEd"] = $Belastung_qEd;
					$form_inputs_data_for_csv_array["Belastung_qEd"] = $Belastung_qEd;
		
					$form_callback_data["Belastung_qEd"] = $Belastung_qEd;
					$form_inputs_data_for_csv_array["Belastung_qEd"] = $Belastung_qEd;
				}
			}
		}
		
		
		// #####
		
		
		// Länge Öffnung Lges (m)
		// Grenzen: >=0.5; <=10
		// Summe der Einzeloeffnungen ist erforderlich
		// Lx > 0.5 * Lges/n; Lx< 2 * Lges/n  Wert: double
		
		$Einzeloeffnungen_array=array();
		
		if(!empty($_POST["Laenge_der_Einzeloeffnungen_L1"])){
			$L1=$_POST["Laenge_der_Einzeloeffnungen_L1"];
		}
		else{
			$L1="";
		}
			
		if($L1==""){
			
            
                    if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                        if ( $currentLanguage == 'en' ) {
                           $form_input_errors_array["Laenge_der_Einzeloeffnungen_L1_errors"]="L1 Please enter value. Limit values: >= 0.5 * Lges/n; and <= 2 * Lges/n in (m)";
                        }

                        if ( $currentLanguage == 'de' ) {
                           
                            
                            $form_input_errors_array["Laenge_der_Einzeloeffnungen_L1_errors"]="L1 Bitte Wert eintragen. Grenzwerte: >= 0.5 * Lges/n; und <= 2 * Lges/n in (m)";
                        }

                        if ( $currentLanguage == 'es' ) {
                          $form_input_errors_array["Laenge_der_Einzeloeffnungen_L1_errors"]="L1 Por favor, introduzca el valor. Limits: >= 0.5 * Lges/n; y <= 2 * Lges/n in (m)";
                        }

                    }
            
		}
		else{
			$L1 = test_input($L1);
			if(preg_match("/,/",$L1)){
				$L1=preg_replace("/,/",".",$L1);
			}
			if(!is_numeric($L1)&&preg_match("/[^0-9.,]/",$L1)){
				
                
                    if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                        if ( $currentLanguage == 'en' ) {
                           $form_input_errors_array["Laenge_der_Einzeloeffnungen_L1_errors"]="Unauthorized characters, allowed are 0-9";
                        }

                        if ( $currentLanguage == 'de' ) {
                            $form_input_errors_array["Laenge_der_Einzeloeffnungen_L1_errors"]="Unerlaubte Zeichen, erlaubt sind 0-9";
                        }

                        if ( $currentLanguage == 'es' ) {
                         $form_input_errors_array["Laenge_der_Einzeloeffnungen_L1_errors"]= "Los caracteres no autorizados, permitidos son 0-9";
                        }

                    }
                
			}
			else{
				$L1=doubleval($L1);
				$L1=round($L1,4);
			}
		}
		$Einzeloeffnungen_array["Laenge_der_Einzeloeffnungen_L1"]=$L1;
		//$form_callback_data["L1"] = $L1;
		
		if(!empty($_POST["Laenge_der_Einzeloeffnungen_L2"])){
			$L2=$_POST["Laenge_der_Einzeloeffnungen_L2"];
		}
		else{
			$L2="";
		}
		
		if($L2==""){
			$L2=0;
		}
		else{
			$L2 = test_input($L2);
			if(preg_match("/,/",$L2)){
				$L2=preg_replace("/,/",".",$L2);
			}
			if(!is_numeric($L2)&&preg_match("/[^0-9.,]/",$L2)){
				
                
                
                    if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                        if ( $currentLanguage == 'en' ) {
                           $form_input_errors_array["Laenge_der_Einzeloeffnungen_L2_errors"]="Unauthorized characters, allowed are 0-9";
                        }

                        if ( $currentLanguage == 'de' ) {
                          $form_input_errors_array["Laenge_der_Einzeloeffnungen_L2_errors"]="Unerlaubte Zeichen, erlaubt sind 0-9";
                        }

                        if ( $currentLanguage == 'es' ) {
                         $form_input_errors_array["Laenge_der_Einzeloeffnungen_L2_errors"]= "Los caracteres no autorizados, permitidos son 0-9";
                        }

                    }
                
			}
			else{
				$L2=doubleval($L2);
				$L2=round($L2,4);
			}
		}
		$Einzeloeffnungen_array["Laenge_der_Einzeloeffnungen_L2"]=$L2;
		//$form_callback_data["L2"] = $L2;
		
		if(!empty($_POST["Laenge_der_Einzeloeffnungen_L3"])){
			$L3=$_POST["Laenge_der_Einzeloeffnungen_L3"];
		}
		else{
			$L3="";
		}
		
		if($L3==""){
			$L3=0;
		}
		else{
			$L3 = test_input($L3);
			if(preg_match("/,/",$L3)){
				$L3=preg_replace("/,/",".",$L3);
			}
			if(!is_numeric($L3)&&preg_match("/[^0-9.,]/",$L3)){
				
                
                    if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                        if ( $currentLanguage == 'en' ) {
                           $form_input_errors_array["Laenge_der_Einzeloeffnungen_L3_errors"]="Unauthorized characters, allowed are 0-9";
                        }

                        if ( $currentLanguage == 'de' ) {
                          $form_input_errors_array["Laenge_der_Einzeloeffnungen_L3_errors"]="Unerlaubte Zeichen, erlaubt sind 0-9";
                        }

                        if ( $currentLanguage == 'es' ) {
                         $form_input_errors_array["Laenge_der_Einzeloeffnungen_L3_errors"]= "Los caracteres no autorizados, permitidos son 0-9";
                        }

                    }
                
			}
			else{
				$L3=doubleval($L3);
				$L3=round($L3,4);
			}
		}
		$Einzeloeffnungen_array["Laenge_der_Einzeloeffnungen_L3"]=$L3;
		//$form_callback_data["L3"] = $L3;
		
		if(!empty($_POST["Laenge_der_Einzeloeffnungen_L4"])){
			$L4=$_POST["Laenge_der_Einzeloeffnungen_L4"];
		}
		else{
			$L4="";
		}
		
		if($L4==""){
			$L4=0;
		}
		else{
			$L4 = test_input($L4);
			if(preg_match("/,/",$L4)){
				$L4=preg_replace("/,/",".",$L4);
			}
			if(!is_numeric($L4)&&preg_match("/[^0-9.,]/",$L4)){
				
                
                    if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                        if ( $currentLanguage == 'en' ) {
                           
                            $form_input_errors_array["Laenge_der_Einzeloeffnungen_L4_errors"]="Unauthorized characters, allowed are 0-9";
                        }

                        if ( $currentLanguage == 'de' ) {
                          $form_input_errors_array["Laenge_der_Einzeloeffnungen_L4_errors"]="Unerlaubte Zeichen, erlaubt sind 0-9";
                        }

                        if ( $currentLanguage == 'es' ) {
                         $form_input_errors_array["Laenge_der_Einzeloeffnungen_L4_errors"]= "Los caracteres no autorizados, permitidos son 0-9";
                        }

                    }
                
			}
			else{
				$L4=doubleval($L4);
				$L4=round($L4,4);
			}
		}
		$Einzeloeffnungen_array["Laenge_der_Einzeloeffnungen_L4"]=$L4;
		//$form_callback_data["L4"] = $L4;
		
		if(!empty($_POST["Laenge_der_Einzeloeffnungen_L5"])){
			$L5=$_POST["Laenge_der_Einzeloeffnungen_L5"];
		}
		else{
			$L5="";
		}
		
		if($L5==""){
			$L5=0;
		}
		else{
			$L5 = test_input($L5);
			if(preg_match("/,/",$L5)){
				$L5=preg_replace("/,/",".",$L5);
			}
			if(!is_numeric($L5)&&preg_match("/[^0-9.,]/",$L5)){
				
                
                    if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                        if ( $currentLanguage == 'en' ) {
                           $form_input_errors_array["Laenge_der_Einzeloeffnungen_L5_errors"]="Unauthorized characters, allowed are 0-9";
                        }

                        if ( $currentLanguage == 'de' ) {
                         
                            $form_input_errors_array["Laenge_der_Einzeloeffnungen_L5_errors"]="Unerlaubte Zeichen, erlaubt sind 0-9";
                        }

                        if ( $currentLanguage == 'es' ) {
                         $form_input_errors_array["Laenge_der_Einzeloeffnungen_L5_errors"]= "Los caracteres no autorizados, permitidos son 0-9";
                        }

                    }
                
			}
			else{
				$L5=doubleval($L5);
				$L5=round($L5,4);
			}
		}
		$Einzeloeffnungen_array["Laenge_der_Einzeloeffnungen_L5"]=$L5;
		//$form_callback_data["L5"] = $L5;
		
		$Laenge_Oeffnung_Lges = $_POST["Laenge_Oeffnung_Lges"];
		//echo $Laenge_Oeffnung_Lges."\n";
		
		$Lges="";
		$Lges_ok = false;
		
		if($Laenge_Oeffnung_Lges==""){
			
            
                    if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                        if ( $currentLanguage == 'en' ) {
                          $form_input_errors_array["Laenge_Oeffnung_Lges_errors"]="Please enter value. Limit values: >=0.5; <=10 in (m)";
                        }

                        if ( $currentLanguage == 'de' ) {
                         
                            $form_input_errors_array["Laenge_Oeffnung_Lges_errors"]="Bitte Wert eintragen. Grenzwerte: >=0.5; <=10 in (m)";
                        }

                        if ( $currentLanguage == 'es' ) {
                         $form_input_errors_array["Laenge_Oeffnung_Lges_errors"]="Por favor, introduzca el valor. Valores límite: >=0.5; <=10 in (m)";
                        }

                    }
            
		}else if($Laenge_Oeffnung_Lges!=""){
			$Laenge_Oeffnung_Lges = test_input($Laenge_Oeffnung_Lges);
			if(preg_match("/,/",$Laenge_Oeffnung_Lges)){
				$Laenge_Oeffnung_Lges=preg_replace("/,/",".",$Laenge_Oeffnung_Lges);
			}
			if(!is_numeric($Laenge_Oeffnung_Lges)&&preg_match("/[^0-9.,]/",$Laenge_Oeffnung_Lges)){
				
                
                    if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                        if ( $currentLanguage == 'en' ) {
                         
                            $form_input_errors_array["Laenge_Oeffnung_Lges_errors"]="Unauthorized characters, allowed are 0-9";
                        }

                        if ( $currentLanguage == 'de' ) {
                          $form_input_errors_array["Laenge_Oeffnung_Lges_errors"]="Unerlaubte Zeichen, erlaubt sind 0-9";
                        }

                        if ( $currentLanguage == 'es' ) {
                         $form_input_errors_array["Laenge_Oeffnung_Lges_errors"]= "Los caracteres no autorizados, permitidos son 0-9";
                        }

                    }
                
			}
			else{
				$Laenge_Oeffnung_Lges = doubleval($Laenge_Oeffnung_Lges);
				$Laenge_Oeffnung_Lges=round($Laenge_Oeffnung_Lges,4);
				
				$Summe_Lges=$L1+$L2+$L3+$L4+$L5;
				//$gettype_Summe_Lges=gettype($Summe_Lges);
				//$form_callback_data["gettype_Summe_Lges"] = $gettype_Summe_Lges;
				
				$Summe_Lges=round($Summe_Lges,4);
		
				$form_callback_data["Summe_Lges"] = $Summe_Lges;
				$form_callback_data["Laenge_Oeffnung_Lges"] = $Laenge_Oeffnung_Lges;
				
				
				if($Laenge_Oeffnung_Lges<0.5||$Laenge_Oeffnung_Lges>10){
					
                    
                   /* 
                   if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                        if ( $currentLanguage == 'en' ) {
                            $form_input_errors_array["Laenge_Oeffnung_Lges_errors"]="Grenzwerte: >=0.5 und <=10 in (m).<br>Der Wert Lges muss der Summe der Länge der Einzelöffnungen gleich sein";
                        }

                        if ( $currentLanguage == 'de' ) {
                            $form_input_errors_array["Laenge_Oeffnung_Lges_errors"]="Grenzwerte: >=0.5 und <=10 in (m).<br>Der Wert Lges muss der Summe der Länge der Einzelöffnungen gleich sein";
                        }

                        if ( $currentLanguage == 'es' ) {
                            $form_input_errors_array["Laenge_Oeffnung_Lges_errors"]="Grenzwerte: >=0.5 und <=10 in (m).<br>Der Wert Lges muss der Summe der Länge der Einzelöffnungen gleich sein";
                        }

                    }
                    */
                    
                    
                    if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                        if ( $currentLanguage == 'en' ) {
                            $form_input_errors_array["Laenge_Oeffnung_Lges_errors"]="Limit values: >=0.5 and <=10 in (m).<br>The total length Lges does not equal the sum of the length of the individual openings";
                           
                        }

                        if ( $currentLanguage == 'de' ) {
                           $form_input_errors_array["Laenge_Oeffnung_Lges_errors"]="Grenzwerte: >=0.5 und <=10 in (m).<br>Der Wert Lges muss der Summe der Länge der Einzelöffnungen gleich sein";  
                        }

                        if ( $currentLanguage == 'es' ) {
                            $form_input_errors_array["Laenge_Oeffnung_Lges_errors"]="Limits: >=0.5 y <=10 in (m).<br>La longitud total Lges no es igual a la suma de la longitud de las aberturas individuales";
                        }

                    }
                    
                    
				}else{
					if($Laenge_Oeffnung_Lges!==$Summe_Lges){
						
                        /*
                        if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                            if ( $currentLanguage == 'en' ) {
                                $form_input_errors_array["Laenge_Oeffnung_Lges_errors"]="Der Wert Lges muss der Summe der Einzelöffnungen gleich sein";
                            }

                            if ( $currentLanguage == 'de' ) {
                                $form_input_errors_array["Laenge_Oeffnung_Lges_errors"]="Der Wert Lges muss der Summe der Einzelöffnungen gleich sein";
                            }

                            if ( $currentLanguage == 'es' ) {
                                $form_input_errors_array["Laenge_Oeffnung_Lges_errors"]="Der Wert Lges muss der Summe der Einzelöffnungen gleich sein";
                            }

                        }
                        */
                        
                        
                        if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                            if ( $currentLanguage == 'en' ) {
                                
                                $form_input_errors_array["Laenge_Oeffnung_Lges_errors"]="The total length Lges does not equal the sum of the length of the individual openings";
                            }

                            if ( $currentLanguage == 'de' ) {
                                $form_input_errors_array["Laenge_Oeffnung_Lges_errors"]="Der Wert Lges muss der Summe der Einzelöffnungen gleich sein";
                            }

                            if ( $currentLanguage == 'es' ) {
                                $form_input_errors_array["Laenge_Oeffnung_Lges_errors"]="La longitud total Lges no es igual a la suma de la longitud de las aberturas individuales";
                            }

                        }
                        
                        
					}else{
						$Lges = $Laenge_Oeffnung_Lges;
						$Lges_ok = true;
						$form_callback_data["Laenge_Oeffnung_Lges"] = $Laenge_Oeffnung_Lges;
						$form_inputs_data_for_csv_array["Laenge_Oeffnung_Lges"] = $Laenge_Oeffnung_Lges;
					}
				}
			}
		}
		
		
		// #####
		
		
		// Anzahl der Öffnungen
		// ist angegeben -> Grenzen: n >= 1 , n <= 5  Wert: Integer
		
		$Anzahl_der_Oeffnungen = $_POST["anzahl_oeffnungen"];
		//echo $Anzahl_der_Oeffnungen."\n";
		
		// Abfrage beim text input
		/*
		if($Anzahl_der_Oeffnungen==""){
			$form_input_errors_array["Anzahl_der_Oeffnungen_errors"]="Bitte Wert eintragen. Grenzwerte: >= 1 und <= 5 in (n)";
		}else if($Anzahl_der_Oeffnungen!=""){
			$Anzahl_der_Oeffnungen = test_input($Anzahl_der_Oeffnungen);
			if(preg_match("/,/",$Anzahl_der_Oeffnungen)){
				$Anzahl_der_Oeffnungen=preg_replace("/,/",".",$Anzahl_der_Oeffnungen);
			}
			if(!is_numeric($Anzahl_der_Oeffnungen)&&preg_match("/[^0-9.,]/",$Anzahl_der_Oeffnungen)){
				$form_input_errors_array["Anzahl_der_Oeffnungen_errors"]="Unerlaubte Zeichen, erlaubt sind 0-9.,";
			}
			else{
				$Anzahl_der_Oeffnungen =intval($Anzahl_der_Oeffnungen);
				
				if($Anzahl_der_Oeffnungen<1||$Anzahl_der_Oeffnungen>5){
					$form_input_errors_array["Anzahl_der_Oeffnungen_errors"]="Grenzwerte: >= 1 und <= 5" i (n);
				}else{
					$n = $Anzahl_der_Oeffnungen;
					$form_callback_data["Anzahl_der_Oeffnungen"] = $Anzahl_der_Oeffnungen;
					$form_inputs_data_for_csv_array["Anzahl_der_Oeffnungen"] = $Anzahl_der_Oeffnungen;
				}
			}
		}
		*/
		
		// Abfrage biem select Element
		$n_ok=false;
		if($Anzahl_der_Oeffnungen=="0"){
			
            
                        if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                            if ( $currentLanguage == 'en' ) {
                                
                                $form_input_errors_array["Anzahl_der_Oeffnungen_errors"]="Please select index";
                            }

                            if ( $currentLanguage == 'de' ) {
                                $form_input_errors_array["Anzahl_der_Oeffnungen_errors"]="Bitte Index auswählen";
                            }

                            if ( $currentLanguage == 'es' ) {
                                $form_input_errors_array["Anzahl_der_Oeffnungen_errors"]="Por favor, seleccione el índice";
                            }

                        }
            
			$form_callback_data["Anzahl_der_Oeffnungen"] = $Anzahl_der_Oeffnungen;
		}
		else{
			$Anzahl_der_Oeffnungen =intval($Anzahl_der_Oeffnungen);
			$n = $Anzahl_der_Oeffnungen;
			$n_ok=true;
			
			$anzahl_der_belegten_Einzeloeffnungen=0;
			foreach($Einzeloeffnungen_array as $key => $value){
				if($value!=""){
					$anzahl_der_belegten_Einzeloeffnungen++;
				}
			}
			
			if($Anzahl_der_Oeffnungen!=$anzahl_der_belegten_Einzeloeffnungen){
				
                
                        if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                            if ( $currentLanguage == 'en' ) {
                                $form_input_errors_array["Anzahl_der_Oeffnungen_errors"]="Please adjust the index of the individual openings";
                                
                            }

                            if ( $currentLanguage == 'de' ) {
                                $form_input_errors_array["Anzahl_der_Oeffnungen_errors"]="Bitte den Index der ausgefühlten Einzeloeffnungen anzupassen";
                            }

                            if ( $currentLanguage == 'es' ) {
                                $form_input_errors_array["Anzahl_der_Oeffnungen_errors"]="Por favor, ajuste el índice de las aberturas individuales rellenadas";
                            }

                        }
                
			}
			
			
			$form_callback_data["Anzahl_der_Oeffnungen"] = $Anzahl_der_Oeffnungen;
			$form_inputs_data_for_csv_array["Anzahl_der_Oeffnungen"] = $Anzahl_der_Oeffnungen;
		}
		
		
		// #####
		
		
		// Länge der Einzelöffnungen L1 (m)
		// L1 >= 0.5 * Lges/n; L1<= 2 * Lges/n  Wert: double
		if(!empty($_POST["Laenge_der_Einzeloeffnungen_L1"])){
			$Laenge_der_Einzeloeffnungen_L1 = $_POST["Laenge_der_Einzeloeffnungen_L1"];
			//echo $Laenge_der_Einzeloeffnungen_L1."\n";
			
			if($Laenge_der_Einzeloeffnungen_L1==""){
				
                
                        if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                            if ( $currentLanguage == 'en' ) {
                                $form_input_errors_array["Laenge_der_Einzeloeffnungen_L1_errors"]="L1 Please enter value. Limit values: >= 0.5 * Lges/n and <= 2 * Lges/n in (m)";
                                
                            }

                            if ( $currentLanguage == 'de' ) {
                                $form_input_errors_array["Laenge_der_Einzeloeffnungen_L1_errors"]="L1 Bitte Wert eintragen. Grenzwerte: >= 0.5 * Lges/n und <= 2 * Lges/n in (m)";
                            }

                            if ( $currentLanguage == 'es' ) {
                                $form_input_errors_array["Laenge_der_Einzeloeffnungen_L1_errors"]="L1 Por favor, introduzca el valor. Valores límite: >= 0.5 * Lges/n y <= 2 * Lges/n in (m)";
                            }

                        }
                
			}else if($Laenge_der_Einzeloeffnungen_L1!=""){
				$Laenge_der_Einzeloeffnungen_L1 = test_input($Laenge_der_Einzeloeffnungen_L1);
				if(preg_match("/,/",$Laenge_der_Einzeloeffnungen_L1)){
					$Laenge_der_Einzeloeffnungen_L1=preg_replace("/,/",".",$Laenge_der_Einzeloeffnungen_L1);
				}
				if(!is_numeric($Laenge_der_Einzeloeffnungen_L1)&&preg_match("/[^0-9.,]/",$Laenge_der_Einzeloeffnungen_L1)){
					
                    
                        if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                            if ( $currentLanguage == 'en' ) {
                                $form_input_errors_array["Laenge_der_Einzeloeffnungen_L1_errors"]="L1 Unauthorized characters, allowed are 0-9";
                                
                            }

                            if ( $currentLanguage == 'de' ) {
                               $form_input_errors_array["Laenge_der_Einzeloeffnungen_L1_errors"]="L1 Unerlaubte Zeichen, erlaubt sind 0-9";
                            }

                            if ( $currentLanguage == 'es' ) {
                               $form_input_errors_array["Laenge_der_Einzeloeffnungen_L1_errors"]= "Los caracteres no autorizados, permitidos son 0-9";
                            }

                        }
                    
				}
				else{
					$Laenge_der_Einzeloeffnungen_L1 = doubleval($Laenge_der_Einzeloeffnungen_L1);
					$Laenge_der_Einzeloeffnungen_L1=round($Laenge_der_Einzeloeffnungen_L1,4);
					
					if($Lges_ok == true && $n_ok ==true){
						$L1_min = 0.5*$Lges/$n;
						$form_callback_data["L1_min"] = $L1_min;
						$L1_min=round($L1_min,4);
						$form_callback_data["L1_min_round"] = $L1_min;
						$L1_max = 2*$Lges/$n;
						$L1_max=round($L1_max,4);
						
						if($Laenge_der_Einzeloeffnungen_L1 < $L1_min||$Laenge_der_Einzeloeffnungen_L1 > $L1_max){
							
                            
                            if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                                if ( $currentLanguage == 'en' ) {
                                    $form_input_errors_array["Laenge_der_Einzeloeffnungen_L1_errors"]="L1 Limit values: >= ".$L1_min." and <= ".$L1_max." in (m)";
                                    
                                    
                                }

                                if ( $currentLanguage == 'de' ) {
                                   $form_input_errors_array["Laenge_der_Einzeloeffnungen_L1_errors"]="L1 Grenzwerte: >= ".$L1_min." und <= ".$L1_max." in (m)";
                                }

                                if ( $currentLanguage == 'es' ) {
                                   $form_input_errors_array["Laenge_der_Einzeloeffnungen_L1_errors"]="L1 Valores límite: >= ".$L1_min." y <= ".$L1_max." in (m)";
                                }

                            }
                            
							/*
							if(!is_nan($L1_min)&&!is_nan($L1_max&&!is_finite($L1_max))){
								$form_input_errors_array["Laenge_der_Einzeloeffnungen_L1_errors"]="L1 Grenzwerte: >= ".$L1_min." und <= ".$L1_max." in (m)";
							}
							else{
								$form_input_errors_array["Laenge_der_Einzeloeffnungen_L1_errors"]="L1 Eingabe nicht richtig! Grenzwerte: >= 0.5 * Lges/n und <= 2 * Lges/n in (m)";
							}
							*/
						}else{
							$form_callback_data["Laenge_der_Einzeloeffnungen_L1"] = $Laenge_der_Einzeloeffnungen_L1;
							$form_inputs_data_for_csv_array["Laenge_der_Einzeloeffnungen_L1"] = $Laenge_der_Einzeloeffnungen_L1;
						}
						
					}
				}
			}
		}
		else{
			
            
                if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                    if ( $currentLanguage == 'en' ) {
                        
                        $form_input_errors_array["Laenge_der_Einzeloeffnungen_L1_errors"]="L1 Mandatory field. Please enter value. Limit values: L1 >= 0.5 * Lges/n; L1<= 2 * Lges/n in (m)";
                    }

                    if ( $currentLanguage == 'de' ) {
                       $form_input_errors_array["Laenge_der_Einzeloeffnungen_L1_errors"]="L1 Pflicht Feld. Bitte Wert eintragen. Grenzwerte: L1 >= 0.5 * Lges/n; L1<= 2 * Lges/n in (m)";
                    }

                    if ( $currentLanguage == 'es' ) {
                       $form_input_errors_array["Laenge_der_Einzeloeffnungen_L1_errors"]="L1 Campo obligatorio. Please enter value. Limits: L1 >= 0.5 * Lges/n; L1<= 2 * Lges/n in (m)";
                    }

                }
            
		}
		
		
		// #####
		
		
		// Länge der Einzelöffnungen L2 (m)
		// L2 >= 0.5 * Lges/n; L2<= 2 * Lges/n Falls der Wert nicht eingeben wurde, weil nur 1 Öffnung vorhanden, soll in die CSV Datei der Wert 0 eingetragen werden  Wert:Double
		if(!empty($_POST["Laenge_der_Einzeloeffnungen_L2"])){
			$Laenge_der_Einzeloeffnungen_L2 = $_POST["Laenge_der_Einzeloeffnungen_L2"];
			//echo $Laenge_der_Einzeloeffnungen_L2."\n";
		
			if($Laenge_der_Einzeloeffnungen_L2==""){
				$Laenge_der_Einzeloeffnungen_L2=0;
				$form_callback_data["Laenge_der_Einzeloeffnungen_L2"] = "0";
				$form_inputs_data_for_csv_array["Laenge_der_Einzeloeffnungen_L2"] = "0";
				
			}else if($Laenge_der_Einzeloeffnungen_L2!=""){
				$Laenge_der_Einzeloeffnungen_L2 = test_input($Laenge_der_Einzeloeffnungen_L2);
				if(preg_match("/,/",$Laenge_der_Einzeloeffnungen_L2)){
					$Laenge_der_Einzeloeffnungen_L2=preg_replace("/,/",".",$Laenge_der_Einzeloeffnungen_L2);
				}
				if(!is_numeric($Laenge_der_Einzeloeffnungen_L2)&&preg_match("/[^0-9.,]/",$Laenge_der_Einzeloeffnungen_L2)){
					
                    
                    if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                        if ( $currentLanguage == 'en' ) {
                            
                            $form_input_errors_array["Laenge_der_Einzeloeffnungen_L2_errors"]="L2 Unauthorized characters, allowed are 0-9";
                        }

                        if ( $currentLanguage == 'de' ) {
                           $form_input_errors_array["Laenge_der_Einzeloeffnungen_L2_errors"]="L2 Unerlaubte Zeichen, erlaubt sind 0-9";
                        }

                        if ( $currentLanguage == 'es' ) {
                           $form_input_errors_array["Laenge_der_Einzeloeffnungen_L2_errors"]="L2 Los caracteres no autorizados, permitidos son 0-9";
                        }

                    }
                    
				}
				else{
					$Laenge_der_Einzeloeffnungen_L2 = doubleval($Laenge_der_Einzeloeffnungen_L2);
					$Laenge_der_Einzeloeffnungen_L2=round($Laenge_der_Einzeloeffnungen_L2,4);
					
					if($Lges_ok == true && $n_ok ==true){
						$L2_min = 0.5*$Lges/$n;
						$L2_min=round($L2_min,4);
						$L2_max = 2*$Lges/$n;
						$L2_max=round($L2_max,4);
						
						if($Laenge_der_Einzeloeffnungen_L2 < $L2_min||$Laenge_der_Einzeloeffnungen_L2 > $L2_max){
							
                            
                            
                            if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                                if ( $currentLanguage == 'en' ) {
                                    $form_input_errors_array["Laenge_der_Einzeloeffnungen_L2_errors"]="L2 Limit values: >= ".$L2_min." and <= ".$L2_max." in (m)";
                                  
                                }

                                if ( $currentLanguage == 'de' ) {
                                    $form_input_errors_array["Laenge_der_Einzeloeffnungen_L2_errors"]="L2 Grenzwerte: >= ".$L2_min." und <= ".$L2_max." in (m)";
                                }

                                if ( $currentLanguage == 'es' ) {
                                  $form_input_errors_array["Laenge_der_Einzeloeffnungen_L2_errors"]="L2 Valores límite: >= ".$L2_min." y <= ".$L2_max." in (m)";
                                }

                            }
                            
                            
							/*
							if(!is_nan($L2_min)&&!is_nan($L2_max)&&!is_finite($L2_max)){
								$form_input_errors_array["Laenge_der_Einzeloeffnungen_L2_errors"]="L2 Grenzwerte: >= ".$L2_min." und <= ".$L2_max." in (m)";
							}
							else{
								$form_input_errors_array["Laenge_der_Einzeloeffnungen_L2_errors"]="L2 Grenzwerte: >= 0.5 * Lges/n und <= 2 * Lges/n in (m)";
							}
							*/
						}else{
							$form_callback_data["Laenge_der_Einzeloeffnungen_L2"] = $Laenge_der_Einzeloeffnungen_L2;
							$form_inputs_data_for_csv_array["Laenge_der_Einzeloeffnungen_L2"] = $Laenge_der_Einzeloeffnungen_L2;
						}
						
					}
				}
			}
		}
		else{
			$form_callback_data["Laenge_der_Einzeloeffnungen_L2"] = "0";
			$form_inputs_data_for_csv_array["Laenge_der_Einzeloeffnungen_L2"] = "0";
		}
			
		
		// #####
		
		
		// Länge der Einzelöffnungen L3 (m)
		// L3 >= 0.5 * Lges/n; L3<= 2 * Lges/n Falls der Wert nicht eingeben wurde, weil nur 2 Öffnungen vorhanden, soll in die CSV Datei der Wert 0 eingetragen werden  Wert:Double
		if(!empty($_POST["Laenge_der_Einzeloeffnungen_L3"])){
			$Laenge_der_Einzeloeffnungen_L3 = $_POST["Laenge_der_Einzeloeffnungen_L3"];
			//echo $Laenge_der_Einzeloeffnungen_L3."\n";

			if($Laenge_der_Einzeloeffnungen_L3==""){
				$Laenge_der_Einzeloeffnungen_L3=0;
				$form_callback_data["Laenge_der_Einzeloeffnungen_L3"] = "0";
				$form_inputs_data_for_csv_array["Laenge_der_Einzeloeffnungen_L3"] = "0";
				
			}else if($Laenge_der_Einzeloeffnungen_L3!=""){
				$Laenge_der_Einzeloeffnungen_L3 = test_input($Laenge_der_Einzeloeffnungen_L3);
				if(preg_match("/,/",$Laenge_der_Einzeloeffnungen_L3)){
					$Laenge_der_Einzeloeffnungen_L3=preg_replace("/,/",".",$Laenge_der_Einzeloeffnungen_L3);
				}
				if(!is_numeric($Laenge_der_Einzeloeffnungen_L3)&&preg_match("/[^0-9.,]/",$Laenge_der_Einzeloeffnungen_L3)){
					
                    
                            if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                                if ( $currentLanguage == 'en' ) {
                                 
                                    $form_input_errors_array["Laenge_der_Einzeloeffnungen_L3_errors"]="L3 Unauthorized characters, allowed are 0-9";
                                }

                                if ( $currentLanguage == 'de' ) {
                                     $form_input_errors_array["Laenge_der_Einzeloeffnungen_L3_errors"]="L3 Unerlaubte Zeichen, erlaubt sind 0-9";
                                }

                                if ( $currentLanguage == 'es' ) {
                                  $form_input_errors_array["Laenge_der_Einzeloeffnungen_L3_errors"]="L3 Los caracteres no autorizados, permitidos son 0-9";
                                }

                            }
                    
				}
				else{
					$Laenge_der_Einzeloeffnungen_L3 = doubleval($Laenge_der_Einzeloeffnungen_L3);
					$Laenge_der_Einzeloeffnungen_L3=round($Laenge_der_Einzeloeffnungen_L3,4);
					
					if($Lges_ok == true && $n_ok ==true){
						$L3_min = 0.5*$Lges/$n;
						$L3_min=round($L3_min,4);
						$L3_max = 2*$Lges/$n;
						$L3_max=round($L3_max,4);
						
						if($Laenge_der_Einzeloeffnungen_L3<$L3_min||$Laenge_der_Einzeloeffnungen_L3>$L3_max){
							
                            
                            if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                                if ( $currentLanguage == 'en' ) {
                                    $form_input_errors_array["Laenge_der_Einzeloeffnungen_L3_errors"]="L3 Limit values: >= ".$L3_min." and <= ".$L3_max." in (m)";
                                   
                                }

                                if ( $currentLanguage == 'de' ) {
                                  $form_input_errors_array["Laenge_der_Einzeloeffnungen_L3_errors"]="L3 Grenzwerte: >= ".$L3_min." und <= ".$L3_max." in (m)";
                                }

                                if ( $currentLanguage == 'es' ) {
                                  $form_input_errors_array["Laenge_der_Einzeloeffnungen_L3_errors"]="L3 Valores límite: >= ".$L3_min." y <= ".$L3_max." in (m)";
                                }

                            }
                            
                            
							/*
							if(!is_nan($L3_min)&&!is_nan($L3_max)&&!is_finite($L3_max)){
								$form_input_errors_array["Laenge_der_Einzeloeffnungen_L3_errors"]="L3 Grenzwerte: >= ".$L3_min." und <= ".$L3_max." in (m)";
							}
							else{
								$form_input_errors_array["Laenge_der_Einzeloeffnungen_L3_errors"]="L3 Grenzwerte: >= 0.5 * Lges/n und <= 2 * Lges/n in (m)";
							}
							*/
						}else{
							$form_callback_data["Laenge_der_Einzeloeffnungen_L3"] = $Laenge_der_Einzeloeffnungen_L3;
							$form_inputs_data_for_csv_array["Laenge_der_Einzeloeffnungen_L3"] = $Laenge_der_Einzeloeffnungen_L3;
						}
						
					}
				}
			}
		}
		else{
			$form_callback_data["Laenge_der_Einzeloeffnungen_L3"] = "0";
			$form_inputs_data_for_csv_array["Laenge_der_Einzeloeffnungen_L3"] = "0";
		}
			
			
		// #####
		
		
		// Länge der Einzelöffnungen L4 (m)
		// L4 >= 0.5 * Lges/n; L4 <= 2 * Lges/n Falls der Wert nicht eingeben wurde, weil nur 3 Öffnungen vorhanden, soll in die CSV Datei der Wert 0 eingetragen werden  Wert:Double
		if(!empty($_POST["Laenge_der_Einzeloeffnungen_L4"])){
			$Laenge_der_Einzeloeffnungen_L4 = $_POST["Laenge_der_Einzeloeffnungen_L4"];
			//echo $Laenge_der_Einzeloeffnungen_L4."\n";

			if($Laenge_der_Einzeloeffnungen_L4==""){
				$Laenge_der_Einzeloeffnungen_L4=0;
				$form_callback_data["Laenge_der_Einzeloeffnungen_L4"] = "0";
				$form_inputs_data_for_csv_array["Laenge_der_Einzeloeffnungen_L4"] = "0";
				
			}else if($Laenge_der_Einzeloeffnungen_L4!=""){
				$Laenge_der_Einzeloeffnungen_L4 = test_input($Laenge_der_Einzeloeffnungen_L4);
				if(preg_match("/,/",$Laenge_der_Einzeloeffnungen_L4)){
					$Laenge_der_Einzeloeffnungen_L4=preg_replace("/,/",".",$Laenge_der_Einzeloeffnungen_L4);
				}
				if(!is_numeric($Laenge_der_Einzeloeffnungen_L4)&&preg_match("/[^0-9.,]/",$Laenge_der_Einzeloeffnungen_L4)){
					
                    
                            if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                                if ( $currentLanguage == 'en' ) {
                                  
                                    
                                    $form_input_errors_array["Laenge_der_Einzeloeffnungen_L4_errors"]="L4 Unauthorized characters, allowed are 0-9";
                                }

                                if ( $currentLanguage == 'de' ) {
                                  $form_input_errors_array["Laenge_der_Einzeloeffnungen_L4_errors"]="L4 Unerlaubte Zeichen, erlaubt sind 0-9";
                                }

                                if ( $currentLanguage == 'es' ) {
                                  $form_input_errors_array["Laenge_der_Einzeloeffnungen_L4_errors"]="L4 Los caracteres no autorizados, permitidos son 0-9";
                                }

                            }
                    
				}
				else{
					$Laenge_der_Einzeloeffnungen_L4 = doubleval($Laenge_der_Einzeloeffnungen_L4);
					$Laenge_der_Einzeloeffnungen_L4=round($Laenge_der_Einzeloeffnungen_L4,4);
					
					if($Lges_ok == true && $n_ok ==true){
						$L4_min = 0.5*$Lges/$n;
						$L4_min=round($L4_min,4);
						$L4_max = 2*$Lges/$n;
						$L4_max=round($L4_max,4);
						
						if($Laenge_der_Einzeloeffnungen_L4 < $L4_min||$Laenge_der_Einzeloeffnungen_L4 > $L4_max){
							
                            
                            
                            if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                                if ( $currentLanguage == 'en' ) {
                                  
                                     $form_input_errors_array["Laenge_der_Einzeloeffnungen_L4_errors"]="L4 Limit values: >= ".$L4_min." and <= ".$L4_max." in (m)";
                                }

                                if ( $currentLanguage == 'de' ) {
                                    $form_input_errors_array["Laenge_der_Einzeloeffnungen_L4_errors"]="L4 Grenzwerte: >= ".$L4_min." und <= ".$L4_max." in (m)";
                                }

                                if ( $currentLanguage == 'es' ) {
                                  $form_input_errors_array["Laenge_der_Einzeloeffnungen_L4_errors"]="L4 Valores límite: >= ".$L4_min." y <= ".$L4_max." in (m)";
                                }

                            }
                            
                            
							/*
							if(!is_nan($L4_min)&&!is_nan($L4_max)&&!is_finite($L4_max)){
								$form_input_errors_array["Laenge_der_Einzeloeffnungen_L4_errors"]="L4 Grenzwerte: >= ".$L4_min." und <= ".$L4_max." in (m)";
							}
							else{
								$form_input_errors_array["Laenge_der_Einzeloeffnungen_L4_errors"]="L4 Grenzwerte: >= 0.5 * Lges/n und <= 2 * Lges/n in (m)";
							}
							*/
						}else{
							$form_callback_data["Laenge_der_Einzeloeffnungen_L4"] = $Laenge_der_Einzeloeffnungen_L4;
							$form_inputs_data_for_csv_array["Laenge_der_Einzeloeffnungen_L4"] = $Laenge_der_Einzeloeffnungen_L4;
						}
						
					}
				}
			}
		}
		else{
			$form_callback_data["Laenge_der_Einzeloeffnungen_L4"] = "0";
			$form_inputs_data_for_csv_array["Laenge_der_Einzeloeffnungen_L4"] = "0";
		}

		
		// #####
		
		
		// Länge der Einzelöffnungen L5 (m)
		// L5 >= 0.5 * Lges/n; L5 <= 2 * Lges/n Falls der Wert nicht eingeben wurde, weil nur 4 Öffnungen vorhanden, soll in die CSV Datei der Wert 0 eingetragen werden  Wert:Double
		if(!empty($_POST["Laenge_der_Einzeloeffnungen_L5"])){
			$Laenge_der_Einzeloeffnungen_L5 = $_POST["Laenge_der_Einzeloeffnungen_L5"];
			//echo $Laenge_der_Einzeloeffnungen_L5."\n";

			if($Laenge_der_Einzeloeffnungen_L5==""){
				$Laenge_der_Einzeloeffnungen_L5=0;
				$form_callback_data["Laenge_der_Einzeloeffnungen_L5"] = "0";
				$form_inputs_data_for_csv_array["Laenge_der_Einzeloeffnungen_L5"] = "0";
				
			}else if($Laenge_der_Einzeloeffnungen_L5!=""){
				$Laenge_der_Einzeloeffnungen_L5 = test_input($Laenge_der_Einzeloeffnungen_L5);
				if(preg_match("/,/",$Laenge_der_Einzeloeffnungen_L5)){
					$Laenge_der_Einzeloeffnungen_L5=preg_replace("/,/",".",$Laenge_der_Einzeloeffnungen_L5);
				}
				if(!is_numeric($Laenge_der_Einzeloeffnungen_L5)&&preg_match("/[^0-9.,]/",$Laenge_der_Einzeloeffnungen_L5)){
					
                    
                           if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                                if ( $currentLanguage == 'en' ) {
                                  
                                    $form_input_errors_array["Laenge_der_Einzeloeffnungen_L5_errors"]="L5 Unauthorized characters, allowed are 0-9";
                                }

                                if ( $currentLanguage == 'de' ) {
                                    $form_input_errors_array["Laenge_der_Einzeloeffnungen_L5_errors"]="L5 Unerlaubte Zeichen, erlaubt sind 0-9";
                                }

                                if ( $currentLanguage == 'es' ) {
                                  $form_input_errors_array["Laenge_der_Einzeloeffnungen_L5_errors"]="L5 Los caracteres no autorizados, permitidos son 0-9";
                                }

                            }
                    
				}
				else{
					$Laenge_der_Einzeloeffnungen_L5 = doubleval($Laenge_der_Einzeloeffnungen_L5);
					$Laenge_der_Einzeloeffnungen_L5=round($Laenge_der_Einzeloeffnungen_L5,4);
					
					if($Lges_ok == true && $n_ok ==true){
						$L5_min = 0.5*$Lges/$n;
						$L5_min=round($L5_min,4);
						$L5_max = 2*$Lges/$n;
						$L5_max=round($L5_max,4);
						
						if($Laenge_der_Einzeloeffnungen_L5 < $L5_min||$Laenge_der_Einzeloeffnungen_L5 > $L5_max){
							
                            
                           if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                                if ( $currentLanguage == 'en' ) {
                                  
                                     $form_input_errors_array["Laenge_der_Einzeloeffnungen_L5_errors"]="L5 Limit values: >= ".$L5_min." and <= ".$L5_max." in (m)";
                                }

                                if ( $currentLanguage == 'de' ) {
                                    $form_input_errors_array["Laenge_der_Einzeloeffnungen_L5_errors"]="L5 Grenzwerte: >= ".$L5_min." und <= ".$L5_max." in (m)";
                                }

                                if ( $currentLanguage == 'es' ) {
                                  $form_input_errors_array["Laenge_der_Einzeloeffnungen_L5_errors"]="L5 Valores límite: >= ".$L5_min." y <= ".$L5_max." in (m)";
                                }

                            }
                            
							/*
							if(!is_nan($L5_min)&&!is_nan($L5_max)&&!is_finite($L5_max)){
								$form_input_errors_array["Laenge_der_Einzeloeffnungen_L5_errors"]="L5 Grenzwerte: >= ".$L5_min." und <= ".$L5_max." in (m)";
							}
							else{
								$form_input_errors_array["Laenge_der_Einzeloeffnungen_L5_errors"]="L5 Grenzwerte: >= 0.5 * Lges/n und <= 2 * Lges/n in (m)";
							}
							*/
						}else{
							$form_callback_data["Laenge_der_Einzeloeffnungen_L5"] = $Laenge_der_Einzeloeffnungen_L5;
							$form_inputs_data_for_csv_array["Laenge_der_Einzeloeffnungen_L5"] = $Laenge_der_Einzeloeffnungen_L5;
						}
						
					}
				}
			}
		}
		else{
			$form_callback_data["Laenge_der_Einzeloeffnungen_L5"] = "0";
			$form_inputs_data_for_csv_array["Laenge_der_Einzeloeffnungen_L5"] = "0";
		}
	
	
		// #####
		
		
		// Breite Betonbalken (cm)
		// Für Deckentyp = 1: Grenzen >= 20; <= 150
		// Für Deckentyp =2 oder 3 :Grenzen: >=15; <=150
		// Wert: Double
		$Breite_Betonbalken = $_POST["Breite_Betonbalken"];
		//echo $Breite_Betonbalken."\n";
		
		if($Breite_Betonbalken==""){
			
            
            
           if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                if ( $currentLanguage == 'en' ) {
                  
                    $form_input_errors_array["Breite_Betonbalken_errors"]="Please enter value. For ceiling type =1: Limit values >=20 and <=150, for ceiling type =2 or =3: Limit values >=15 and <=150 in (cm)";
                }

                if ( $currentLanguage == 'de' ) {
                    $form_input_errors_array["Breite_Betonbalken_errors"]="Bitte Wert eintragen. Für Deckentyp =1: Grenzwerte >=20 und <=150, für Deckentyp =2 oder =3: Grenzwerte >=15 und <=150 in (cm)";
                }

                if ( $currentLanguage == 'es' ) {
                  $form_input_errors_array["Breite_Betonbalken_errors"]="Por favor, introduzca el valor. Para el tipo de techo =1: Valores límite >=20 y <=150, para el tipo de techo =2 o =3: Valores límite >=15 y <=150 in (cm)";
                }

            }
            
            
		}else if($Breite_Betonbalken!=""){
			$Breite_Betonbalken = test_input($Breite_Betonbalken);
			if(preg_match("/,/",$Breite_Betonbalken)){
				$Breite_Betonbalken=preg_replace("/,/",".",$Breite_Betonbalken);
			}
			if(!is_numeric($Breite_Betonbalken)&&preg_match("/[^0-9.,]/",$Breite_Betonbalken)){
				
                
               if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                    if ( $currentLanguage == 'en' ) {
                     
                        $form_input_errors_array["Breite_Betonbalken_errors"]="Unauthorized characters, allowed are 0-9";
                    }

                    if ( $currentLanguage == 'de' ) {
                        $form_input_errors_array["Breite_Betonbalken_errors"]="Unerlaubte Zeichen, erlaubt sind 0-9";
                    }

                    if ( $currentLanguage == 'es' ) {
                      $form_input_errors_array["Breite_Betonbalken_errors"]="Los caracteres no autorizados, permitidos son 0-9";
                    }

                }
                
			}
			else{
				$Breite_Betonbalken = doubleval($Breite_Betonbalken);
				$Breite_Betonbalken=round($Breite_Betonbalken,4);
				
				if($Deckentyp=="0"){
					//$form_input_errors_array["Deckentyp_errors"]="Bitte Index auswählen";
					
                    
                   if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                        if ( $currentLanguage == 'en' ) {
                         
                            $form_input_errors_array["Breite_Betonbalken_errors"]="Ceiling type is required";
                        }

                        if ( $currentLanguage == 'de' ) {
                            $form_input_errors_array["Breite_Betonbalken_errors"]="Deckentyp ist erforderlich";
                        }

                        if ( $currentLanguage == 'es' ) {
                          $form_input_errors_array["Breite_Betonbalken_errors"]="Se requiere un tipo de techo";
                        }

                    }
                    
					$form_callback_data["Deckentyp"] = $Deckentyp;
				}
				else if($Deckentyp=="1"){
					if($Breite_Betonbalken<20||$Breite_Betonbalken>150){
						
                        
                        
                       if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                            if ( $currentLanguage == 'en' ) {
                             
                                $form_input_errors_array["Breite_Betonbalken_errors"]="Limit values: >=20 and <=150 in (cm)";
                            }

                            if ( $currentLanguage == 'de' ) {
                                $form_input_errors_array["Breite_Betonbalken_errors"]="Grenzwerte: >=20 und <=150 in (cm)";
                            }

                            if ( $currentLanguage == 'es' ) {
                             $form_input_errors_array["Breite_Betonbalken_errors"]="Valores límite: >=20 y <=150 in (cm)";
                            }

                        }
                        
                        
                        
					}else{
						$form_callback_data["Breite_Betonbalken"] = $Breite_Betonbalken;
						$form_inputs_data_for_csv_array["Breite_Betonbalken"] = $Breite_Betonbalken;
					}
				}
				else if($Deckentyp=="2"||$Deckentyp=="3"){
					if($Breite_Betonbalken<15||$Breite_Betonbalken>150){
						
                        
                       if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                            if ( $currentLanguage == 'en' ) {
                             
                                $form_input_errors_array["Breite_Betonbalken_errors"]="Limit values: >=15 and <=150 in (cm)";
                            }

                            if ( $currentLanguage == 'de' ) {
                                $form_input_errors_array["Breite_Betonbalken_errors"]="Grenzwerte: >=15 und <=150 in (cm)";
                            }

                            if ( $currentLanguage == 'es' ) {
                             $form_input_errors_array["Breite_Betonbalken_errors"]="Valores límite: >=15 y <=150 in (cm)";
                            }

                        }
                        
					}else{
						$form_callback_data["Breite_Betonbalken"] = $Breite_Betonbalken;
						$form_inputs_data_for_csv_array["Breite_Betonbalken"] = $Breite_Betonbalken;
					}
				}
			}
		}
		
		
		// #####
		
		
		// Deckenhöhe hc (cm)
		// Grenzen: >=10; <=50  Wert: Double
		$Deckenhoehe_hc = $_POST["Deckenhoehe_hc"];
		//echo $Deckenhoehe_hc."\n";
		// $form_callback_data["Deckenhoehe_eingang"] = $Deckenhoehe_hc;
		
		$hc="";
		
		if($Deckenhoehe_hc==""){
			
            
            if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                if ( $currentLanguage == 'en' ) {
                    
                    $form_input_errors_array["Deckenhoehe_hc_errors"]="Please enter value. Limit values: >=10 and <=50 in (cm)";
                }

                if ( $currentLanguage == 'de' ) {
                    $form_input_errors_array["Deckenhoehe_hc_errors"]="Bitte Wert eintragen. Grenzwerte: >=10 und <=50 in (cm)";
                }

                if ( $currentLanguage == 'es' ) {
                    $form_input_errors_array["Deckenhoehe_hc_errors"]="Por favor, introduzca el valor. Valores límite: >=10 y <=50 in (cm)";
                }

            }
            
            
            
		}else if($Deckenhoehe_hc!=""){
			$Deckenhoehe_hc = test_input($Deckenhoehe_hc);
			if(preg_match("/,/",$Deckenhoehe_hc)){
				$Deckenhoehe_hc=preg_replace("/,/",".",$Deckenhoehe_hc);
			}
			if(!is_numeric($Deckenhoehe_hc)&&preg_match("/[^0-9.,]/",$Deckenhoehe_hc)){
				
                
                
                if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                    if ( $currentLanguage == 'en' ) {
                        
                         $form_input_errors_array["Deckenhoehe_hc_errors"]="Unauthorized characters, allowed are 0-9";
                    }

                    if ( $currentLanguage == 'de' ) {
                       $form_input_errors_array["Deckenhoehe_hc_errors"]="Unerlaubte Zeichen, erlaubt sind 0-9";
                    }

                    if ( $currentLanguage == 'es' ) {
                        $form_input_errors_array["Deckenhoehe_hc_errors"]="Los caracteres no autorizados, permitidos son 0-9";
                    }

                }
                
			}
			else{
				$Deckenhoehe_hc = doubleval($Deckenhoehe_hc);
				$Deckenhoehe_hc=round($Deckenhoehe_hc,4);
				
				// $form_callback_data["Deckenhoehe_hc_vor_prüfung"] = $Deckenhoehe_hc;
				
				if($Deckenhoehe_hc <10 || $Deckenhoehe_hc >50){
					
                    
                    if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                        if ( $currentLanguage == 'en' ) {
                            
                            $form_input_errors_array["Deckenhoehe_hc_errors"]="Limit values: >=10 and <=50 in (cm)";
                        }

                        if ( $currentLanguage == 'de' ) {
                            $form_input_errors_array["Deckenhoehe_hc_errors"]="Grenzwerte: >=10 und <=50 in (cm)";
                        }

                        if ( $currentLanguage == 'es' ) {
                            $form_input_errors_array["Deckenhoehe_hc_errors"]="Valores límite: >=10 y <=50 in (cm)";
                        }

                    }
                    
                    
					// $form_callback_data["Deckenhoehe_hc_nicht in Grenzwerten"] = $Deckenhoehe_hc;
				}
				else{
					$hc = $Deckenhoehe_hc;
					// $form_callback_data["Deckenhoehe_hc_in Grenzwerten"] = $Deckenhoehe_hc;
				}
			}
		}
		
		
		// #####
		
		
		// Höhe hges (cm)
		// Grenzen: >=hc+10; <=200  Wert: Double
		$Hoehe_hges = $_POST["Hoehe_hges"];
		//echo $Hoehe_hges."\n";
		
		if($Hoehe_hges==""){
			
            
            
            if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                if ( $currentLanguage == 'en' ) {
                    
                    $form_input_errors_array["Hoehe_hges_errors"]="Please enter value. Limit values: >=hc+10 and <=200 in (cm)";
                }

                if ( $currentLanguage == 'de' ) {
                    $form_input_errors_array["Hoehe_hges_errors"]="Bitte Wert eintragen. Grenzwerte: >=hc+10 und <=200 in (cm)";
                }

                if ( $currentLanguage == 'es' ) {
                    $form_input_errors_array["Hoehe_hges_errors"]="Por favor, introduzca el valor. Valores límite: >=hc+10 y <=200 in (cm)";
                }

            }
            
            
		}else if($Hoehe_hges!=""){
			$Hoehe_hges = test_input($Hoehe_hges);
			if(preg_match("/,/",$Hoehe_hges)){
				$Hoehe_hges=preg_replace("/,/",".",$Hoehe_hges);
			}
			if(!is_numeric($Hoehe_hges)&&preg_match("/[^0-9.,]/",$Hoehe_hges)){
				
                
                if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                    if ( $currentLanguage == 'en' ) {
                       
                         $form_input_errors_array["Hoehe_hges_errors"]="Unauthorized characters, allowed are 0-9";
                    }

                    if ( $currentLanguage == 'de' ) {
                        $form_input_errors_array["Hoehe_hges_errors"]="Unerlaubte Zeichen, erlaubt sind 0-9";
                    }

                    if ( $currentLanguage == 'es' ) {
                        $form_input_errors_array["Hoehe_hges_errors"]="Los caracteres no autorizados, permitidos son 0-9";
                    }

                }
                
			}
			else{
				$Hoehe_hges = doubleval($Hoehe_hges);
				$Hoehe_hges=round($Hoehe_hges,4);
				
				if($hc!=""){
					if($Hoehe_hges <($hc+10) || $Hoehe_hges>200){
						
                        
                        
                        if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                            if ( $currentLanguage == 'en' ) {
                                
                                $form_input_errors_array["Hoehe_hges_errors"]="Limit values: >=".($hc+10)." and <=200 in (cm)";
                            }

                            if ( $currentLanguage == 'de' ) {
                                $form_input_errors_array["Hoehe_hges_errors"]="Grenzwerte: >=".($hc+10)." und <=200 in (cm)";
                            }

                            if ( $currentLanguage == 'es' ) {
                                $form_input_errors_array["Hoehe_hges_errors"]="Valores límite: >=".($hc+10)." y <=200 in (cm)";
                            }

                        }
                        
					}
					else{
						$form_callback_data["Hoehe_hges"] = $Hoehe_hges;
						$form_inputs_data_for_csv_array["Hoehe_hges"] = $Hoehe_hges;
						
						$form_callback_data["Deckenhoehe_hc"] = $Deckenhoehe_hc;
						$form_inputs_data_for_csv_array["Deckenhoehe_hc"] = $Deckenhoehe_hc;
					}
				}
				else if($hc==""){
					
                    
                    
                        if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                            if ( $currentLanguage == 'en' ) {
                               
                                $form_input_errors_array["Hoehe_hges_errors"]="hc is required";
                            }

                            if ( $currentLanguage == 'de' ) {
                                 $form_input_errors_array["Hoehe_hges_errors"]="hc ist erforderlich";
                            }

                            if ( $currentLanguage == 'es' ) {
                                $form_input_errors_array["Hoehe_hges_errors"]="Se requiere hc";
                            }

                        }
                    
					//$form_input_errors_array["Deckenhoehe_hc_errors"]="Grenzwerte: >hc+10 und <200 in (cm)";
				}
			}
		}
		
		
		// #####
		
		
		// Qed,1 (kN)
		// Grenzen: >= -500; <= 500  Wert: Double
		$Qed_1 = $_POST["Qed_1"];
		$Qed_1_ok = false;
		//echo $Qed_1."\n";
		
		if($Qed_1==""){
			
            
            
            if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                if ( $currentLanguage == 'en' ) {
                    
                    $form_input_errors_array["Qed_1_errors"]="Please enter value. Limit values: >=-500 and <=500 in (kN)";
                }

                if ( $currentLanguage == 'de' ) {
                    $form_input_errors_array["Qed_1_errors"]="Bitte Wert eintragen. Grenzwerte: >=-500 und <=500 in (kN)";
                }

                if ( $currentLanguage == 'es' ) {
                    $form_input_errors_array["Qed_1_errors"]="Por favor, introduzca el valor. Valores límite: >=-500 y <=500 in (kN)";
                }

            }
            
            
            
            
		}else if($Qed_1!=""){
			$Qed_1 = test_input($Qed_1);
			if(preg_match("/,/",$Qed_1)){
				$Qed_1=preg_replace("/,/",".",$Qed_1);
			}
			if(!is_numeric($Qed_1)&&preg_match("/[^0-9.,-]/",$Qed_1)){
				
                
                if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                    if ( $currentLanguage == 'en' ) {
                        
                        $form_input_errors_array["Qed_1_errors"]="Unauthorized characters, allowed are 0-9";
                    }

                    if ( $currentLanguage == 'de' ) {
                       $form_input_errors_array["Qed_1_errors"]="Unerlaubte Zeichen, erlaubt sind 0-9";
                    }

                    if ( $currentLanguage == 'es' ) {
                        $form_input_errors_array["Qed_1_errors"]="Los caracteres no autorizados, permitidos son 0-9";
                    }

                }
                
			}
			else{
				$Qed_1 = doubleval($Qed_1);
				$Qed_1=round($Qed_1,4);
				
				if($Qed_1<-500 || $Qed_1>500){
					
                    
                    
                    if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                        if ( $currentLanguage == 'en' ) {
                           
                            $form_input_errors_array["Qed_1_errors"]="Limit values: >= -500 and <=500 in (kN)";
                        }

                        if ( $currentLanguage == 'de' ) {
                            $form_input_errors_array["Qed_1_errors"]="Grenzwerte: >= -500 und <=500 in (kN)";
                        }

                        if ( $currentLanguage == 'es' ) {
                            $form_input_errors_array["Qed_1_errors"]="Valores límite: >= -500 y <=500 in (kN)";
                        }

                    }
                    
                    
				}else{
					$Qed_1_ok = true;
				}
			}
		}
		

		// #####
		
		
		// Med,1 (kNm)
		// Grenzen: > 1; <10000 UND: Med,1 > qEd * Lges^2 /2 – Qed,1 * Lges +1  
		// Wert: Double
		$Med_1 = $_POST["Med_1"];
		//echo $Med_1."\n";
		
		if($Med_1!=""){
			$Med_1 = test_input($Med_1);
			if(preg_match("/,/",$Med_1)){
				$Med_1=preg_replace("/,/",".",$Med_1);
			}
			if(!is_numeric($Med_1)&&preg_match("/[^0-9.,]/",$Med_1)){
				
                
                
                    if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                        if ( $currentLanguage == 'en' ) {
                           
                            $form_input_errors_array["Med_1_errors"]="Unauthorized characters, allowed are 0-9";
                        }

                        if ( $currentLanguage == 'de' ) {
                            $form_input_errors_array["Med_1_errors"]="Unerlaubte Zeichen, erlaubt sind 0-9";
                        }

                        if ( $currentLanguage == 'es' ) {
                           $form_input_errors_array["Med_1_errors"]="Los caracteres no autorizados, permitidos son 0-9";
                        }

                    }
                
                
			}
			else{
				$Med_1 = doubleval($Med_1);
				$Med_1=round($Med_1,4);
				
				if($Lges_ok == true && $qEd_ok == true && $Qed_1_ok == true){
					//$Lges_hoch_2 = bcpow($Lges,"2",0);
					//$form_callback_data["Lges_hoch_2"] = $Lges_hoch_2;
					
					$Lges_hoch_2 = pow($Lges,2);
					$form_callback_data["Lges_hoch_2_pow"] = $Lges_hoch_2;
					
					/*
					$form_callback_data["Lges"] = $Lges;
					$Lges_hoch_2 = $Lges*$Lges;
					$form_callback_data["Lges_hoch_2"] = $Lges_hoch_2;
					*/
					
					$Lges_hoch_2=round($Lges_hoch_2,4);
					$form_callback_data["Lges_hoch_2_round"] = $Lges_hoch_2;
					
					$qEd = doubleval($qEd);
					$qEd=round($qEd,4);
					$Lges_hoch_2 = doubleval($Lges_hoch_2);
					$Lges_hoch_2=round($Lges_hoch_2,4);
					$form_callback_data["Lges_hoch_2_doubleval"] = $Lges_hoch_2;
					
					
					$Med_1_und = $qEd*$Lges_hoch_2/2-$Qed_1*$Lges+1;
					$Med_1_und=round($Med_1_und,4);
					$form_callback_data["Med_1_und"] = $Med_1_und;
					
					$Med_1_min = 1;
					
					if($Med_1_und > $Med_1_min){
						$Med_1_min = $Med_1_und;
					}
					
					$form_callback_data["Med_1_min"] = $Med_1_min;
					
					if($Med_1 <= $Med_1_min || $Med_1 >= 10000){
						
                        
                        
                        if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                            if ( $currentLanguage == 'en' ) {
                                
                                $form_input_errors_array["Med_1_errors"]="Limit values: >".$Med_1_min." and <10000";
                            }

                            if ( $currentLanguage == 'de' ) {
                               $form_input_errors_array["Med_1_errors"]="Grenzwerte: >".$Med_1_min." und <10000";
                            }

                            if ( $currentLanguage == 'es' ) {
                               $form_input_errors_array["Med_1_errors"]="Valores límite: >".$Med_1_min." y <10000";
                            }

                        }
                        
                        
					}
					else{
						$form_callback_data["Med_1"] = $Med_1;
						$form_inputs_data_for_csv_array["Med_1"] = $Med_1;
						
						$form_callback_data["Qed_1"] = $Qed_1;
						$form_inputs_data_for_csv_array["Qed_1"] = $Qed_1;
					}
					
					
					
				}
				else{
					
                    
                        if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                            if ( $currentLanguage == 'en' ) {
                                
                                $form_input_errors_array["Med_1_errors"]="Lges qEd and Qed,1 are required";
                            }

                            if ( $currentLanguage == 'de' ) {
                               $form_input_errors_array["Med_1_errors"]="Lges qEd und Qed,1 sind erforderlich";
                            }

                            if ( $currentLanguage == 'es' ) {
                               $form_input_errors_array["Med_1_errors"]="Se requieren Lges qEd y Qed,1";
                            }

                        }
                    
                    
				}
			}
		}else{
			
			if($Lges_ok == true && $qEd_ok == true && $Qed_1_ok == true){
				//$Lges_hoch_2 = bcpow($Lges,"2",0);
				//$form_callback_data["Lges_hoch_2"] = $Lges_hoch_2;
				
				$Lges_hoch_2 = pow($Lges,2);
				$form_callback_data["Lges_hoch_2_pow"] = $Lges_hoch_2;
				
				/*
				$form_callback_data["Lges"] = $Lges;
				$Lges_hoch_2 = $Lges*$Lges;
				$form_callback_data["Lges_hoch_2"] = $Lges_hoch_2;
				*/
				
				$Lges_hoch_2=round($Lges_hoch_2,4);
				$form_callback_data["Lges_hoch_2_round"] = $Lges_hoch_2;
				
				$qEd = doubleval($qEd);
				$qEd=round($qEd,4);
				$Lges_hoch_2 = doubleval($Lges_hoch_2);
				$Lges_hoch_2=round($Lges_hoch_2,4);
				$form_callback_data["Lges_hoch_2_doubleval"] = $Lges_hoch_2;
				
				
				$Med_1_und = $qEd*$Lges_hoch_2/2-$Qed_1*$Lges+1;
				$Med_1_und=round($Med_1_und,4);
				$form_callback_data["Med_1_und"] = $Med_1_und;
				
				$Med_1_min = 1;
				
				if($Med_1_und > $Med_1_min){
					$Med_1_min = $Med_1_und;
				}
				
				
                
                        if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                            if ( $currentLanguage == 'en' ) {
                                
                                $form_input_errors_array["Med_1_errors"]="Please enter value. Limit values:  >".$Med_1_und." and <10000 (kNm)";
                            }

                            if ( $currentLanguage == 'de' ) {
                               $form_input_errors_array["Med_1_errors"]="Bitte Wert eintragen. Grenzwerte:  >".$Med_1_und." und <10000 (kNm)";
                            }

                            if ( $currentLanguage == 'es' ) {
                               $form_input_errors_array["Med_1_errors"]="Por favor, introduzca el valor. Valores límite:  >".$Med_1_und." y <10000 (kNm)";
                            }

                        }
                
                
				
			}else{
				
                
                        if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                            if ( $currentLanguage == 'en' ) {
                               
                                $form_input_errors_array["Med_1_errors"]="Please enter value in (kNm)";
                            }

                            if ( $currentLanguage == 'de' ) {
                                $form_input_errors_array["Med_1_errors"]="Bitte Wert eintragen in (kNm)";
                            }

                            if ( $currentLanguage == 'es' ) {
                               $form_input_errors_array["Med_1_errors"]="Por favor, introduzca el valor en (kNm)";
                            }

                        }
                
			}
		}
		
		
		// #####
		
		
		// Qedmax (kN)
		// Grenzen: >= abs(Qed,1) , <= 5000, >=-5000  Wert: Double
		$Qedmax = $_POST["Qedmax"];
		//echo $Qedmax."\n";
		
		if($Qedmax!=""){
			$Qedmax = test_input($Qedmax);
			if(preg_match("/,/",$Qedmax)){
				$Qedmax=preg_replace("/,/",".",$Qedmax);
			}
			if(!is_numeric($Qedmax)&&preg_match("/[^0-9.,-]/",$Qedmax)){
				
                
                        if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                            if ( $currentLanguage == 'en' ) {
                              
                                 $form_input_errors_array["Qedmax_errors"]="Unauthorized characters, allowed are 0-9";
                            }

                            if ( $currentLanguage == 'de' ) {
                               $form_input_errors_array["Qedmax_errors"]="Unerlaubte Zeichen, erlaubt sind 0-9";
                            }

                            if ( $currentLanguage == 'es' ) {
                               $form_input_errors_array["Qedmax_errors"]="Los caracteres no autorizados, permitidos son 0-9";
                            }

                        }
                
			}
			else{
				$Qedmax = doubleval($Qedmax);
				$Qedmax=round($Qedmax,4);
				
				if($Qed_1_ok == true){
					$abs_Qed_1 = abs($Qed_1);
					$form_callback_data["abs_Qed_1"] = $abs_Qed_1;
					
					$Qedmax_min = -5000;
					
					if($abs_Qed_1 > $Qedmax_min){
						$Qedmax_min = $abs_Qed_1;
					}
					
					if($Qedmax < $Qedmax_min || $Qedmax > 5000){
						
                        
                        
                        if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                            if ( $currentLanguage == 'en' ) {
                               
                                 $form_input_errors_array["Qedmax_errors"]="Limit values: >= ".$Qedmax_min." and <=5000 in (kN)";
                            }

                            if ( $currentLanguage == 'de' ) {
                              $form_input_errors_array["Qedmax_errors"]="Grenzwerte: >= ".$Qedmax_min." und <=5000 in (kN)";
                            }

                            if ( $currentLanguage == 'es' ) {
                               $form_input_errors_array["Qedmax_errors"]="Valores límite: >= ".$Qedmax_min." y <=5000 in (kN)";
                            }

                        }
                        
                        
					}
					else{
						$form_callback_data["Qedmax"] = $Qedmax;
						$form_inputs_data_for_csv_array["Qedmax"] = $Qedmax;
					}
				}
			}
		}else{
			
            
            
                        if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                            if ( $currentLanguage == 'en' ) {
                               
                                $form_input_errors_array["Qedmax_errors"]="Please enter value. Limit values: >= ".$Qedmax_min." and <=5000 in (kN)";
                            }

                            if ( $currentLanguage == 'de' ) {
                              $form_input_errors_array["Qedmax_errors"]="Bitte Wert eintragen. Grenzwerte: >= ".$Qedmax_min." und <=5000 in (kN)";
                            }

                            if ( $currentLanguage == 'es' ) {
                              $form_input_errors_array["Qedmax_errors"]="Por favor, introduzca el valor. Valores límite: >= ".$Qedmax_min." y <=5000 in (kN)";
                            }

                        }
            
		}
		
		
		// #####
		
		
		// Spannweite Decke LD1 (m)
		// Falls der Wert nicht eingeben wurde, weil Deckentyp = 1 , soll in die CSV Datei der Wert 0
		// Grenzen: Grenzen >= 0; < 100 (nur für Deckentyp = 2 oder 3)
		// eingetragen werden  Wert: Double
		if(!empty($_POST["spannweite_decke_ld1"])){
			$Spannweite_Decke_LD1 = $_POST["spannweite_decke_ld1"];
			//echo $Spannweite_Decke_LD1."\n";
			
			if($Deckentyp=="2"||$Deckentyp=="3"){
				if($Spannweite_Decke_LD1==""){
					
                    
                        if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                            if ( $currentLanguage == 'en' ) {
                               
                                $form_input_errors_array["Spannweite_Decke_LD1_errors"]="Please enter value. Limit values: >=0 and < 100 in (m)";
                            }

                            if ( $currentLanguage == 'de' ) {
                              $form_input_errors_array["Spannweite_Decke_LD1_errors"]="Bitte Wert eintragen. Grenzwerte: >=0 und < 100 in (m)";
                            }

                            if ( $currentLanguage == 'es' ) {
                              $form_input_errors_array["Spannweite_Decke_LD1_errors"]="Por favor, introduzca el valor. Valores límite: >=0 y < 100 in (m)";
                            }

                        }
                    
                    
				}else if($Spannweite_Decke_LD1!=""){
					$Spannweite_Decke_LD1 = test_input($Spannweite_Decke_LD1);
					if(preg_match("/,/",$Spannweite_Decke_LD1)){
						$Spannweite_Decke_LD1=preg_replace("/,/",".",$Spannweite_Decke_LD1);
					}
					if(!is_numeric($Spannweite_Decke_LD1)&&preg_match("/[^0-9.,]/",$Spannweite_Decke_LD1)){
						
                        
                        
                        if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                            if ( $currentLanguage == 'en' ) {
                               
                                $form_input_errors_array["Spannweite_Decke_LD1_errors"]="Unauthorized characters, allowed are 0-9";
                            }

                            if ( $currentLanguage == 'de' ) {
                              $form_input_errors_array["Spannweite_Decke_LD1_errors"]="Unerlaubte Zeichen, erlaubt sind 0-9";
                            }

                            if ( $currentLanguage == 'es' ) {
                             $form_input_errors_array["Spannweite_Decke_LD1_errors"]="Los caracteres no autorizados, permitidos son 0-9";
                            }

                        }
                        
					}
					else{
						$Spannweite_Decke_LD1 = doubleval($Spannweite_Decke_LD1);
						$Spannweite_Decke_LD1=round($Spannweite_Decke_LD1,4);
						
						if($Spannweite_Decke_LD1 <0 || $Spannweite_Decke_LD1 >=100){
							
                            
                            
                            if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                                if ( $currentLanguage == 'en' ) {
                                  
                                    $form_input_errors_array["Spannweite_Decke_LD1_errors"]="Limit values: >=0 and <100 in (m)";
                                }

                                if ( $currentLanguage == 'de' ) {
                                  $form_input_errors_array["Spannweite_Decke_LD1_errors"]="Grenzwerte: >=0 und <100 in (m)";
                                }

                                if ( $currentLanguage == 'es' ) {
                                 $form_input_errors_array["Spannweite_Decke_LD1_errors"]="Valores límite: >=0 y <100 in (m)";
                                }

                            }
                            
						}
						else{
							$form_callback_data["Spannweite_Decke_LD1"] = $Spannweite_Decke_LD1;
							$form_inputs_data_for_csv_array["Spannweite_Decke_LD1"] = $Spannweite_Decke_LD1;
						}
					}
				}
			}
			else if($Deckentyp=="1"){
				$form_callback_data["Spannweite_Decke_LD1"] = "0";
				$form_inputs_data_for_csv_array["Spannweite_Decke_LD1"] = "0";
			}
			else if($Deckentyp=="0"){
				//$form_input_errors_array["Spannweite_Decke_LD1_errors"]="Deckentyp ist erforderlich";
				//$form_input_errors_array["Deckentyp_errors"]="Bitte Index auswählen";
			}
			
		}
		else{
			$form_callback_data["Spannweite_Decke_LD1"] = "0";
			$form_inputs_data_for_csv_array["Spannweite_Decke_LD1"] = "0";
		}
		
		
		// #####
		
		
		// Spannweite Decke LD2 (m)
		// Grenzen: Grenzen >= 0; < 100 (nur für Deckentyp = 2 oder 3)
		// Falls der Wert nicht eingeben wurde, weil Deckentyp = 1 , soll in die CSV Datei der Wert 0
		// eingetragen werden  Wert: Double
		if(!empty($_POST["spannweite_decke_ld2"])){
			$Spannweite_Decke_LD2 = $_POST["spannweite_decke_ld2"];
			//echo $Spannweite_Decke_LD2."\n";
			
			if($Deckentyp=="2"||$Deckentyp=="3"){
				if($Spannweite_Decke_LD2==""){
					
                    
                    
                            if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                                if ( $currentLanguage == 'en' ) {
                                  
                                     $form_input_errors_array["Spannweite_Decke_LD2_errors"]="Please enter value. Limit values: >=0 and < 100 in (m)";
                                }

                                if ( $currentLanguage == 'de' ) {
                                    $form_input_errors_array["Spannweite_Decke_LD2_errors"]="Bitte Wert eintragen. Grenzwerte: >=0 und < 100 in (m)";
                                }

                                if ( $currentLanguage == 'es' ) {
                                 $form_input_errors_array["Spannweite_Decke_LD2_errors"]="Por favor, introduzca el valor. Valores límite: >=0 y < 100 in (m)";
                                }

                            }
                    
                    
				}else if($Spannweite_Decke_LD2!=""){
					$Spannweite_Decke_LD2 = test_input($Spannweite_Decke_LD2);
					if(preg_match("/,/",$Spannweite_Decke_LD2)){
						$Spannweite_Decke_LD2=preg_replace("/,/",".",$Spannweite_Decke_LD2);
					}
					if(!is_numeric($Spannweite_Decke_LD2)&&preg_match("/[^0-9.,]/",$Spannweite_Decke_LD2)){
						
                        
                            if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                                if ( $currentLanguage == 'en' ) {
                                  
                                    $form_input_errors_array["Spannweite_Decke_LD2_errors"]="Unauthorized characters, allowed are 0-9";
                                }

                                if ( $currentLanguage == 'de' ) {
                                    $form_input_errors_array["Spannweite_Decke_LD2_errors"]="Unerlaubte Zeichen, erlaubt sind 0-9";
                                }

                                if ( $currentLanguage == 'es' ) {
                                 $form_input_errors_array["Spannweite_Decke_LD2_errors"]="Los caracteres no autorizados, permitidos son 0-9";
                                }

                            }
                        
					}
					else{
						$Spannweite_Decke_LD2 = doubleval($Spannweite_Decke_LD2);
						$Spannweite_Decke_LD2=round($Spannweite_Decke_LD2,4);
						
						if($Spannweite_Decke_LD2 <0 || $Spannweite_Decke_LD2 >=100){
							
                            
                            if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                                if ( $currentLanguage == 'en' ) {
                                    
                                    $form_input_errors_array["Spannweite_Decke_LD2_errors"]="Limit values: >=0 and <100 in (m)";
                                }

                                if ( $currentLanguage == 'de' ) {
                                     $form_input_errors_array["Spannweite_Decke_LD2_errors"]="Grenzwerte: >=0 und <100 in (m)";
                                }

                                if ( $currentLanguage == 'es' ) {
                                    $form_input_errors_array["Spannweite_Decke_LD2_errors"]="Valores límite: >=0 y <100 in (m)";
                                }

                            }
                            
						}
						else{
							$form_callback_data["Spannweite_Decke_LD2"] = $Spannweite_Decke_LD2;
							$form_inputs_data_for_csv_array["Spannweite_Decke_LD2"] = $Spannweite_Decke_LD2;
						}
					}
				}
			}
			else if($Deckentyp=="1"){
				$form_callback_data["Spannweite_Decke_LD2"] = "0";
				$form_inputs_data_for_csv_array["Spannweite_Decke_LD2"] = "0";
			}
			else if($Deckentyp=="0"){
				//$form_input_errors_array["Spannweite_Decke_LD2_errors"]="Deckentyp ist erforderlich";
				//$form_input_errors_array["Deckentyp_errors"]="Bitte Index auswählen";
			}
			
		}
		else{
			$form_callback_data["Spannweite_Decke_LD2"] = "0";
			$form_inputs_data_for_csv_array["Spannweite_Decke_LD2"] = "0";
		}
		
		
		// #####
		
		
		// Spannweite Balken L (m)
		// Grenzen: > Lges+ 0.5; <100; (nur für Deckentyp = 2 oder 3)
		// Falls der Wert nicht eingeben wurde, weil Deckentyp = 1 , soll in die CSV Datei der Wert 0
		// eingetragen werden  Wert: Double
		if(!empty($_POST["spannweite_balken_l"])){
			$Spannweite_Balken_L = $_POST["spannweite_balken_l"];
			//echo $Spannweite_Balken_L."\n";
			
			if($Deckentyp=="2"||$Deckentyp=="3"){
				if($Spannweite_Balken_L!=""){
					$Spannweite_Balken_L = test_input($Spannweite_Balken_L);
					if(preg_match("/,/",$Spannweite_Balken_L)){
						$Spannweite_Balken_L=preg_replace("/,/",".",$Spannweite_Balken_L);
					}
					if(!is_numeric($Spannweite_Balken_L)&&preg_match("/[^0-9.,]/",$Spannweite_Balken_L)){
						
                        
                            if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                                if ( $currentLanguage == 'en' ) {
                                   
                                    $form_input_errors_array["Spannweite_Balken_L_errors"]="Unauthorized characters, allowed are 0-9";
                                }

                                if ( $currentLanguage == 'de' ) {
                                   $form_input_errors_array["Spannweite_Balken_L_errors"]="Unerlaubte Zeichen, erlaubt sind 0-9";
                                }

                                if ( $currentLanguage == 'es' ) {
                                   $form_input_errors_array["Spannweite_Balken_L_errors"]="Los caracteres no autorizados, permitidos son 0-9";
                                }

                            }
                        
                        
					}
					else{
						$Spannweite_Balken_L = doubleval($Spannweite_Balken_L);
						$Spannweite_Balken_L=round($Spannweite_Balken_L,4);
						
						if($Lges_ok == true){
							$Spannweite_Balken_L_min = ($Lges + 0.5);
							$form_callback_data["Spannweite_Balken_L_min"] = $Spannweite_Balken_L_min;
							
							if($Spannweite_Balken_L <= $Spannweite_Balken_L_min || $Spannweite_Balken_L >= 100){
								
                                
                                
                        
                                if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                                    if ( $currentLanguage == 'en' ) {
                                       
                                        $form_input_errors_array["Spannweite_Balken_L_errors"]="Limit values: >".$Spannweite_Balken_L_min." and <100 in (m)";
                                    }

                                    if ( $currentLanguage == 'de' ) {
                                         $form_input_errors_array["Spannweite_Balken_L_errors"]="Grenzwerte: >".$Spannweite_Balken_L_min." und <100 in (m)";
                                    }

                                    if ( $currentLanguage == 'es' ) {
                                        $form_input_errors_array["Spannweite_Balken_L_errors"]="Valores límite: >".$Spannweite_Balken_L_min." y <100 in (m)";
                                    }

                                }
                                
							}
							else{
								$form_callback_data["Spannweite_Balken_L"] = $Spannweite_Balken_L;
								$form_inputs_data_for_csv_array["Spannweite_Balken_L"] = $Spannweite_Balken_L;
							}
						}else{
							
                            
                                if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                                    if ( $currentLanguage == 'en' ) {
                                      
                                        $form_input_errors_array["Spannweite_Balken_L_errors"]="Lges is required";
                                    }

                                    if ( $currentLanguage == 'de' ) {
                                         $form_input_errors_array["Spannweite_Balken_L_errors"]="Lges ist erforderlich";
                                    }

                                    if ( $currentLanguage == 'es' ) {
                                       $form_input_errors_array["Spannweite_Balken_L_errors"]="Lges is required";
                                    }

                                }
                            
                            
						}
					}
				}else{
					if($Lges_ok == true){
						
                        
                                if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                                    if ( $currentLanguage == 'en' ) {
                                       
                                        $form_input_errors_array["Spannweite_Balken_L_errors"]="Please enter value. Limit values: >".$Spannweite_Balken_L_min." and <100 in (m)";
                                    }

                                    if ( $currentLanguage == 'de' ) {
                                        $form_input_errors_array["Spannweite_Balken_L_errors"]="Bitte Wert eintragen. Grenzwerte: >".$Spannweite_Balken_L_min." und <100 in (m)";
                                    }

                                    if ( $currentLanguage == 'es' ) {
                                      $form_input_errors_array["Spannweite_Balken_L_errors"]="Por favor, introduzca el valor. Valores límite: >".$Spannweite_Balken_L_min." y <100 in (m)";
                                    }

                                }
                        
                        
					}else{
						
                        
                                if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

                                    if ( $currentLanguage == 'en' ) {
                                       
                                        $form_input_errors_array["Spannweite_Balken_L_errors"]="Please enter value. Limit values: >Lges + 0.5 and <100 in (m)";
                                    }

                                    if ( $currentLanguage == 'de' ) {
                                        $form_input_errors_array["Spannweite_Balken_L_errors"]="Bitte Wert eintragen. Grenzwerte: >Lges + 0.5 und <100 in (m)";
                                    }

                                    if ( $currentLanguage == 'es' ) {
                                      $form_input_errors_array["Spannweite_Balken_L_errors"]="Por favor, introduzca el valor. Valores límite: >Lges + 0.5 y <100 in (m)";
                                    }

                                }
                        
					}
				}
			}
			else if($Deckentyp=="1"){
				$form_callback_data["Spannweite_Balken_L"] = "0";
				$form_inputs_data_for_csv_array["Spannweite_Balken_L"] = "0";
			}
			else if($Deckentyp=="0"){
				//$form_input_errors_array["Spannweite_Balken_L_errors"]="Deckentyp ist erforderlich";
				//$form_input_errors_array["Deckentyp_errors"]="Bitte Index auswählen";
			}
			
		}
		else{
			$form_callback_data["Spannweite_Balken_L"] = "0";
			$form_inputs_data_for_csv_array["Spannweite_Balken_L"] = "0";
		}
		
		/*
		foreach($form_input_errors_array as $key => $value){
			echo $key." : ".$value."\n";
		}
		
		echo "\n";
		
		foreach($form_inputs_data_for_csv_array as $key => $value){
			echo $key." : ".$value."\n";
		}
		
		exit;
		*/
		
		
		
		// Ende der Form-Fields Tests
		// ################################################################################
		
		$arrays_collector_array=array();
		
		// errors output
		if(!empty($form_input_errors_array)){
			$arrays_collector_array["form_callback_data"]=$form_callback_data;
			$arrays_collector_array["form_input_errors_array"]=$form_input_errors_array;
			echo json_encode($arrays_collector_array);
			wp_die();
		}
		
		
		
		//include get_theme_file_path();

		$p_form_process_report=array();

		$csv_header="";
		$csv_data="";

		$csv_header.="ID;";

		/*
		$id_file=get_home_path()."wp-content/configurator_data/beamup_data/id.txt";
		$fp=fopen($id_file,"r+");
		flock($fp,LOCK_EX);
		$id=fgets($fp,10);
		$new_id=$id+1;
		ftruncate($fp,0);
		fseek ($fp,0);
		fputs($fp,$new_id);
		flock($fp,LOCK_UN);
		fclose($fp);
		*/
		
		$id_file=get_home_path()."wp-content/configurator_data/beamup_data/id.txt";
		
		$fp_r=fopen($id_file,"r");
		$id=fgets($fp_r,10);
		fclose($fp_r);
		
		$new_id=$id+1;
		
		$fp_w=fopen($id_file,"w");
		//flock($fp,LOCK_EX);
		fputs($fp_w,$new_id);
		//flock($fp,LOCK_UN);
		fclose($fp_w);
		

		$date=wp_date("Ymd");

		if(!empty($id)){
			$csv_data.=$date.$id.",";
		}else{
			$p_form_process_report["Form_CSV_ID_error"]=wp_date("d.m.Y, H:i:s")." -> Form CSV ID nicht vorhanden in ".__FILE__ ." on Line: ".__LINE__ ;
		}

		if(!empty($form_inputs_data_for_csv_array)){
			
			$input_csv_file="ID_".$date.$id.".csv";
			$csv_file=get_home_path().'wp-content/configurator_data/beamup_data/form_input/'.$input_csv_file;
			$form_callback_data["ID"] = $input_csv_file;
			
			$fp_csv_file=fopen($csv_file,"w");
			//flock($fp_csv_file,LOCK_EX);
			
			foreach($form_inputs_data_for_csv_array as $index => $value){
				$index=preg_replace("/_/"," ",$index);
				$csv_header.=$index.";";
				$csv_data.=$value.",";
			}
			
			$csv_header=rtrim($csv_header,",")."\n";
			$csv_data=rtrim($csv_data,",")."\n";

			//fputs ($fp_csv_file,$csv_header);
			fputs ($fp_csv_file,$csv_data);
			//flock($fp_csv_file,LOCK_UN);
			fclose($fp_csv_file);

		}
		
		$local_csv_file = $csv_file;
		if(file_exists($local_csv_file)){
			$form_callback_data["local_csv_file"]=$local_csv_file." exist";
			$p_form_process_report["Local_csv_file"]=$local_csv_file." exist";
		}
		else{
			$form_callback_data["local_csv_file"]=$local_csv_file." no exist";
			$p_form_process_report["Local_csv_file"]=$local_csv_file." no exist";
		}
		
		
		/*
		$form_input_dir=get_home_path().'wp-content/configurator_data/beamup_data/form_input/';
		//$form_callback_data["form_input_dir"]=$form_input_dir;
		$form_input_dir_handle = opendir($form_input_dir);
		if(!is_dir($form_input_dir)){
			$form_callback_data["form_input_dir"]="Verzeichnis: ".$form_input_dir." no exist";
		}
		$file_zaehler=0;
		$files_limit_in_dir_form_input=110;
		
		while ( $file = readdir ( $form_input_dir_handle ) ){
			if( $file != '.' && $file != '..'){
				//echo $file . '<br>';
				$file_zaehler++;
			}
		}
		closedir ( $form_input_dir_handle );
		
		//$form_callback_data["file_zaehler"]=$file_zaehler;
		
		$file_unlik_zaehler=$file_zaehler;
		for($x=0;$x<$file_zaehler;$x++){
			
			if($file_unlik_zaehler>$files_limit_in_dir_form_input){
				
				//$form_callback_data["file_unlik_zaehler"]=$file_unlik_zaehler;
				
				$form_input_dir_handle = opendir($form_input_dir);
				while ( $file = readdir ( $form_input_dir_handle ) ){
					if( $file != '.' && $file != '..'){
						unlink($form_input_dir.$file);
						break;
					}
				}
				closedir ( $form_input_dir_handle );
			}
			$file_unlik_zaehler--;
			
		}
		*/
		
		//exit;
		
		$server_conect=true;
		
		if($server_conect){
		
			//$ftp_server = "18.196.72.86";
            $ftp_server = "85.215.146.38";
			
			
			$ftp_conn = ftp_connect($ftp_server) or die("Could not connect to $ftp_server");
			
			if($ftp_conn==false){
				$form_callback_data["ftp_connect"] = "Could not connect to $ftp_server";
				$p_form_process_report["Ftp_connect"] = wp_date("d.m.Y, H:i:s")." Could not connect to $ftp_server";
			}
			else{
				$form_callback_data["ftp_connect"]="Connect is ok";
				$p_form_process_report["Ftp_connect"]=wp_date("d.m.Y, H:i:s")." to $ftp_server is ok";
			}
			
			//$ftp_username="ftp_user";
			//$ftp_userpass="jashk87%U";
            
			$ftp_username="FTPWinUser";
			$ftp_userpass="eZ6wT7F8dUMLfjszawVVuMtBJksThh";            
			
			if (ftp_login($ftp_conn, $ftp_username, $ftp_userpass)) {
				$form_callback_data["ftp_login"]="is ok";
				$p_form_process_report["Ftp_login"]=wp_date("d.m.Y, H:i:s")." is ok";
			} else {
				$form_callback_data["ftp_login"]="is false";
				$p_form_process_report["Ftp_login"]=wp_date("d.m.Y, H:i:s")." is false";
			}
			
			/*
			// initiate upload
			$server_csv_file="ID_".$date.$id.".csv";
			
			if (ftp_alloc($ftp_conn, filesize($local_csv_file), $result)) {
				$form_callback_data["ftp_alloc"] = "Platz wurde erfolgreich auf dem Server reseviert.";
				$p_form_process_report["Ftp_alloc"] = "Platz wurde erfolgreich auf dem Server reseviert";

				ftp_pasv($ftp_conn, true);
				
				
				$ftp_nb_put = ftp_nb_put($ftp_conn, $server_csv_file, $local_csv_file, FTP_ASCII); //FTP_BINARY

				if($ftp_nb_put==FTP_FINISHED){
					$form_callback_data["FTP_Upload"] = "Ftp upload success";
					$p_form_process_report["FTP_Upload"]=wp_date("d.m.Y, H:i:s")." success";
				}
				else{
					$form_callback_data["FTP_Upload"] = "Ftp upload no success";
					$p_form_process_report["FTP_Upload"]=wp_date("d.m.Y, H:i:s")." success";
				}
				
			  
			  
			} else {
				$form_callback_data["Ftp_alloc"] = "Platz konnte nicht auf dem Server reserviert werden. Serverantwort: $result";
				$p_form_process_report["Ftp_alloc"] = "Platz konnte nicht auf dem Server reserviert werden. Serverantwort: ".$result;
			}
			*/
			
			// initiate upload
			$server_csv_file="ID_".$date.$id.".csv";
			
			ftp_pasv($ftp_conn, true);
				
			$ftp_put = ftp_nb_put($ftp_conn, $server_csv_file, $local_csv_file, FTP_ASCII); //FTP_BINARY

			if($ftp_put==FTP_FINISHED){
				$form_callback_data["FTP_Upload"] = "Ftp upload success";
				$p_form_process_report["FTP_Upload"]=wp_date("d.m.Y, H:i:s")." success";
			}
			else{
				$form_callback_data["FTP_Upload"] = "Ftp upload no success";
				$p_form_process_report["FTP_Upload"]=wp_date("d.m.Y, H:i:s")." success";
			}
			
		}
		
		unlink($local_csv_file);
		
		
		//################################
		// Abschnitt nur zum testen Anfang
		
		/*
		$old_file="csv_dra/old_file/old_file.txt";
		if(file_exists($old_file)){
			$fp=fopen($old_file,"r");
			flock($fp,LOCK_EX);
			$old_file_name=fgets($fp,100);
			flock($fp,LOCK_UN);
			fclose($fp);
			
			//$form_callback_data["old_file_name"] = $old_file_name;
			$new_file_name=$server_csv_file;
			//$form_callback_data["new_file_name"] = $new_file_name;
			
			// file umbenen
			ftp_chdir($ftp_conn, 'output');
			//$form_callback_data["Verzeichnis_wechseln to"] = "output";
			
			//$form_callback_data["Aktuelles_Verzeichnis"] = ftp_pwd($ftp_conn);
			
			ftp_rename($ftp_conn, $old_file_name, $new_file_name);
			
			ftp_cdup($ftp_conn);
			//$form_callback_data["Aktuelles_Verzeichnis"] = ftp_pwd($ftp_conn);
		}
		else{
			//$form_callback_data["old_file"] = "no File old_file.txt";
		}
		
		$old_file="csv_dra/old_file/old_file.txt";
		$fp=fopen($old_file,"w");
		flock($fp,LOCK_EX);
		fputs($fp,$server_csv_file);
		flock($fp,LOCK_UN);
		fclose($fp);
		*/
		
		// Abschnitt nur zum testen Ende
		//##############################
		
		if($server_conect&&$ftp_put==FTP_FINISHED){
		
			$server_result_csv_file=get_home_path()."wp-content/configurator_data/beamup_data/get_server_result/".$server_csv_file;
			$get_server_csv_file="output/".$server_csv_file;
			
			$start_unix_time=wp_date("U");
			//$form_callback_data["start_unix_time"] = $start_unix_time;
			
			$start_unix_time_for_ftp_get=wp_date("U");
			
			$first_loop=false;
			$running_time=0;
			$Interval_for_Query_time=3;
			$max_Query_time=60;
			
			for($x=0;$x<1000000000;$x++){
				
				if(!$first_loop){
					
					$first_loop=true;
					
					$ftp_get = @ftp_get($ftp_conn, $server_result_csv_file, $get_server_csv_file, FTP_ASCII);
							
					//$form_callback_data["ftp_get"] = $ftp_get;
					
					if($ftp_get){
						$form_callback_data["FTP_Download"]=wp_date("d.m.Y, H:i:s")." success";
						$form_callback_data["Runningtime"]=$running_time." Sek";
						$p_form_process_report["FTP_Download"]=wp_date("d.m.Y, H:i:s")." success";
						if(file_exists($server_result_csv_file)){
							$form_callback_data["server_result_csv_file"]=$server_result_csv_file." exist";
							$p_form_process_report["server_result_csv_file"]=$server_result_csv_file." exist";
						}
						else{
							$form_callback_data["server_result_csv_file"]=$server_result_csv_file." no exist";
							$p_form_process_report["server_result_csv_file"]=$server_result_csv_file." no exist";
						}
						$p_form_process_report["Runningtime"]=$running_time." Sek";
						break;
					}
					
				}
				
				$current_unix_time_for_ftp_get=wp_date("U");
				$running_time_for_ftp_get=$current_unix_time_for_ftp_get - $start_unix_time_for_ftp_get;
				$Query_time_exceeded_array=array();
				
				// Interval für Abfragen für Output-CSV-File in Sekunden
				if($running_time_for_ftp_get>$Interval_for_Query_time){
					
					$start_unix_time_for_ftp_get=wp_date("U");
					
					$ftp_get = @ftp_get($ftp_conn, $server_result_csv_file, $get_server_csv_file, FTP_ASCII);
						
					//$form_callback_data["ftp_get"] = $ftp_get;
					
					if($ftp_get){
						$form_callback_data["FTP_Download"] = " success";
						$form_callback_data["Runningtime"]=$running_time." Sek";
						$p_form_process_report["FTP_Download"]=wp_date("d.m.Y, H:i:s")." success";
						if(file_exists($server_result_csv_file)){
							$form_callback_data["server_result_csv_file"]=$server_result_csv_file." exist";
							$p_form_process_report["server_result_csv_file"]=$server_result_csv_file." exist";
						}
						else{
							$form_callback_data["server_result_csv_file"]=$server_result_csv_file." no exist";
							$p_form_process_report["server_result_csv_file"]=$server_result_csv_file." no exist";
						}
						$p_form_process_report["Runningtime"]=$running_time." Sek";
						break;
					}
					
				}
				else{
					
					$current_unix_time=wp_date("U");
					$running_time=$current_unix_time - $start_unix_time;
					
					// Wartezeit auf Output-CSV-File in Sekunden insegsammt
					if($running_time>$max_Query_time){
						
						$form_callback_data["FTP_Download"]=wp_date("d.m.Y, H:i:s")." no success";
						$form_callback_data["Runningtime"]=$running_time." Sek";
						$p_form_process_report["FTP_Download"]=wp_date("d.m.Y, H:i:s")." no success";
						if(file_exists($server_result_csv_file)){
							$form_callback_data["server_result_csv_file"]=$server_result_csv_file." exist";
							$p_form_process_report["server_result_csv_file"]=$server_result_csv_file." exist";
						}
						else{
							$form_callback_data["server_result_csv_file"]=$server_result_csv_file." no exist";
							$p_form_process_report["server_result_csv_file"]=$server_result_csv_file." no exist";
						}
						$p_form_process_report["Runningtime"]=$running_time." Sek";
						
						if( ICL_LANGUAGE_CODE == 'de' ) {
							
							$Query_time_exceeded_array["Query_time_exceeded"]="Maximal eingestellte Abfragezeit von ".$max_Query_time." Sekunden abgelaufen";
							$form_callback_data["Query_time_exceeded"]="Maximal eingestellte Abfragezeit von ".$max_Query_time." Sekunden abgelaufen";

						}else if( ICL_LANGUAGE_CODE == 'en' ){
							
							$Query_time_exceeded_array["Query_time_exceeded"]="The maximum set query time of ".$max_Query_time." seconds has expired";
							$form_callback_data["Query_time_exceeded"]="The maximum set query time of ".$max_Query_time." seconds has expired";

						}else if( ICL_LANGUAGE_CODE == 'es' ){
							
							$Query_time_exceeded_array["Query_time_exceeded"]="El tiempo máximo de consulta establecido de ".$max_Query_time." segundos ha expirado";
							$form_callback_data["Query_time_exceeded"]="El tiempo máximo de consulta establecido de ".$max_Query_time." segundos ha expirado";

						}
						
						$arrays_collector_array["Query_time_exceeded_array"]=$Query_time_exceeded_array;
						
						$p_form_process_report["Query_time_exceeded"]="Maximal eingestellte Abfragezeit von ".$max_Query_time." Sekunden abgelaufen";
						
						break;
						
					}
					
				}
				
			}
		
		}else{
			$p_form_process_report["ftp_put"]="false";
		}
		
		//$p_form_process_report["p_form_process_report_test_1"]="p_form_process_report_test";

		$file_process_report=get_home_path()."wp-content/configurator_data/beamup_data/file_process_report.txt";
		$fp_file_process_report=fopen($file_process_report,"a");
		//flock($fp_file_process_report,LOCK_EX);

		if(!empty($p_form_process_report)){
			foreach($p_form_process_report as $index => $value){
				
				fputs ($fp_file_process_report, $index." : ".$value."\n");
				
			}
			fputs ($fp_file_process_report, "##########\n");
		}
		//flock($fp_file_process_report,LOCK_UN);
		fclose($fp_file_process_report);

		
		// get output file cells names
		
		$output_file_cells_names_array=file(get_home_path()."wp-content/configurator_data/beamup_data/output_csv_file_cells_names.txt");
		$output_file_cells_names_array_count=count($output_file_cells_names_array);
		//$form_callback_data["output_file_cells_names_array_count"] = $output_file_cells_names_array_count;
		$y=0;
		
		// Ausgabe Array
		$get_server_result_data = array();
		
		if(@file_exists($server_result_csv_file)){
			
			$fp_save_local_csv_file=fopen($server_result_csv_file,"r");
			
			if($fp_save_local_csv_file){
				while(($zeile_save_local_csv_file=fgetcsv($fp_save_local_csv_file,1000,','))!== FALSE){
					
					for($y;$y<$output_file_cells_names_array_count;$y++){
							
						//$output_file_cells_names_array[$y]=trim($output_file_cells_names_array[$y]);
                        
                        $output_file_cells_names_array[$y]=utf8_decode($output_file_cells_names_array[$y]);
                        $output_file_cells_names_array[$y]=trim($output_file_cells_names_array[$y]);
                        $output_file_cells_names_array[$y]=ltrim($output_file_cells_names_array[$y],"?");
                        
                        
						$zeile_save_local_csv_file[$y]=trim($zeile_save_local_csv_file[$y]);
						$get_server_result_data[$output_file_cells_names_array[$y]]=$zeile_save_local_csv_file[$y];
						
					}
					
				}
				fclose($fp_save_local_csv_file);
			}
			else{
				$form_callback_data["get_server_result"] = "Kein Output CSV File";
			}
			
			//$arrays_collector_array=array();
			$arrays_collector_array["form_callback_data"]=$form_callback_data;
			$arrays_collector_array["get_server_result_data"]=$get_server_result_data;
		
		}
		else{
			
			//$arrays_collector_array=array();
			$arrays_collector_array["form_callback_data"]=$form_callback_data;
			
		}
		
		/*
		$get_server_result_dir=get_home_path().'wp-content/configurator_data/beamup_data/get_server_result/';
		//$form_callback_data["get_server_result_dir"]=$get_server_result_dir;
		$get_server_result_dir_handle = opendir($get_server_result_dir);
		if(!is_dir($get_server_result_dir)){
			$form_callback_data["get_server_result_dir"]="Verzeichnis: ".$get_server_result_dir." no exist";
		}
		$file_zaehler=0;
		$files_limit_in_get_server_result=500;
		
		while ( $file = readdir ( $get_server_result_dir_handle ) ){
		  if ( $file != '.' && $file != '..'){
			//echo $file . '<br>';
			$file_zaehler++;
		  }
		}
		closedir ( $get_server_result_dir_handle );
		
		//$form_callback_data["file_zaehler"]=$file_zaehler;
		
		$file_unlik_zaehler=$file_zaehler;
		for($x=0;$x<$file_zaehler;$x++){
			
			if($file_unlik_zaehler>$files_limit_in_get_server_result){
				
				//$form_callback_data["file_unlik_zaehler"]=$file_unlik_zaehler;
				
				$get_server_result_dir_handle = opendir($get_server_result_dir);
				while ( $file = readdir ( $get_server_result_dir_handle ) ){
					if( $file != '.' && $file != '..'){
						unlink($get_server_result_dir.$file);
						break;
					}
				}
				closedir ( $get_server_result_dir_handle );
			}
			$file_unlik_zaehler--;
			
		}
		*/
		
		if(file_exists($server_result_csv_file)){
			unlink($server_result_csv_file);
		}
		
		
		
		
		//$form_callback_data["test_end"] = "ok";
		
		//echo json_encode($form_callback_data);
		//echo json_encode($get_server_result_data);
		echo json_encode($arrays_collector_array);

		
		
		
		
		/*
		$array=array();
		$array[0]="Name";
		$serial_arr=urlencode(base64_encode(serialize($array)));
		*/
		
	}
	
	
	
	
	
    wp_die();
}

// Here we register our "send_form" function to handle our AJAX request, do you remember the "superhypermega" hidden field? Yes, this is what it refers, the "send_form" action.
add_action('wp_ajax_send_form', 'edit_form'); // This is for authenticated users
add_action('wp_ajax_nopriv_send_form', 'edit_form'); // This is for unauthenticated users.
?>