<?php

//---------------------------------------------------------------------------------------------;
// ACF Option pages
//---------------------------------------------------------------------------------------------;


if( function_exists('acf_add_options_page') ) {
    
    
    acf_add_options_page(array(
        'page_title' 	=> 'Configurator Settings',
        'menu_title' 	=> 'Configurator-Settings',
        'menu_slug' 	=> 'configurator_setting',
        'capability' 	=> 'edit_posts',
        'icon_url'      => 'dashicons-performance',
        'redirect' 	    => false
    ));

    acf_add_options_page(array(
        'page_title' 	=> 'BeamUp',
        'menu_title' 	=> 'BeamUp',
        'menu_slug' 	=> 'configurator_setting_beamup',
        'capability' 	=> 'edit_posts',
        'parent_slug'   => 'configurator_setting',
        'redirect' 	=> false
    ));
    
    acf_add_options_page(array(
        'page_title' 	=> '95 Settings',
        'menu_title' 	=> '95 Settings',
        'menu_slug' 	=> 'af_settings',
        'capability' 	=> 'edit_posts',
        'icon_url'      => 'dashicons-performance',
        'redirect' 	    => true
    ));
    
    
    acf_add_options_page(array(
        'page_title' 	=> 'Global',
        'menu_title' 	=> 'Global',
        'menu_slug' 	=> 'af_global_settings',
        'capability' 	=> 'edit_posts',
        'parent_slug'   => 'af_settings',
        'redirect' 	=> false
    ));
    
    
    acf_add_options_page(array(
        'page_title' 	=> 'Footer',
        'menu_title' 	=> 'Footer',
        'menu_slug' 	=> 'af_global_footer_settings',
        'capability' 	=> 'edit_posts',
        'parent_slug'   => 'af_settings',
        'redirect' 	=> false
    ));
    
    
    
    acf_add_options_page(array(
        'page_title' 	=> 'Maps',
        'menu_title' 	=> 'Maps',
        'menu_slug' 	=> 'af_global_maps_settings',
        'capability' 	=> 'edit_posts',
        'parent_slug'   => 'af_settings',
        'redirect' 	=> false
    ));
    
    
    acf_add_options_page(array(
        'page_title' 	=> 'Search',
        'menu_title' 	=> 'Search',
        'menu_slug' 	=> 'af_search_settings',
        'capability' 	=> 'edit_posts',
        'parent_slug'   => 'af_settings',
        'redirect' 	=> false
    ));
    
    
    acf_add_options_page(array(
        'page_title' 	=> 'Meta/SEO',
        'menu_title' 	=> 'Meta/SEO',
        'menu_slug' 	=> 'af_seo_settings',
        'capability' 	=> 'edit_posts',
        'parent_slug'   => 'af_settings',
        'redirect' 	=> false
    ));
    
    
    acf_add_options_page(array(
        'page_title' 	=> '404',
        'menu_title' 	=> '404',
        'menu_slug' 	=> 'af_404_settings',
        'capability' 	=> 'edit_posts',
        'parent_slug'   => 'af_settings',
        'redirect' 	=> false
    )); 

    

    
}





//--------------------------------------;
// ACF Google Maps
//--------------------------------------;

function my_acf_init() {
    $apikey = get_field('settings_googlemapsapikey','options');
    acf_update_setting('google_api_key', $apikey);
}
add_action('acf/init', 'my_acf_init');


//---------------------------------------------------------------------------------------------;
// Enqueue styles
// Register Styles and Scripts
//---------------------------------------------------------------------------------------------;


function af_enqueue_styles() {

    // set stylesheets in dev mode to non-minified
    if( af_is_dev_mode() ) {
        $min_string = '';
    } else {
        $min_string = '.min';
    }
    
    $dataToFrontend = array(
      'themePath' => get_template_directory_uri(),
      'ajaxUrl' => admin_url('admin-ajax.php')
    );

    //Deregister jquery
	//footer false wegen Plugin redirect cf7
    wp_deregister_script('jquery');
	wp_enqueue_script( 'jquery', get_template_directory_uri() . '/dist/js/00_jquery.min.js', array(), NULL, false );
    
    // include minified theme styles
    wp_enqueue_style( 'theme_style', get_template_directory_uri() . '/dist/css/style'.$min_string.'.css',false, filemtime(get_stylesheet_directory() . '/dist/css/style'.$min_string.'.css'),'all');

    wp_register_script('mainjs', get_template_directory_uri() . '/dist/js/scripts'.$min_string.'.js', array(), filemtime(get_stylesheet_directory() . '/dist/js/scripts'.$min_string.'.js'), true );
    wp_localize_script('mainjs', 'themeData', $dataToFrontend);
    wp_enqueue_script('mainjs');

    
    
    
    
    
    wp_enqueue_style( 'theme_style_bemessung_css', get_template_directory_uri() . '/templates-frontend/m-page-bemessung/format.css',false, false,'all');
    
     //wp_enqueue_style( 'theme_style_bemessung_js', get_template_directory_uri() . '/templates-frontend/m-page-bemessung/funktion2.js',false, false,'all');
    
    

}

add_action( 'wp_enqueue_scripts', 'af_enqueue_styles' );


function af_enqueue_styles_googlemaps() {
    //only load if needed
    //function called in templates/flexible-content-wrapper.php

    //google maps api
    $apikey = get_field('settings_googlemapsapikey','options');    
    wp_enqueue_script( 'google_js', 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key='.$apikey, '', '' );

}


//---------------------------------------------------------------------------------------------;
// async load register js Files
//---------------------------------------------------------------------------------------------;


function add_async_forscript($url){
    //Fuer asynload einer JS File bei "wp_enqueue_scripts" das hinteansetzen #asyncload
    //BSP wp_register_script('particles', get_template_directory_uri() . '/dist/js/particles/particles.min_customFPSLimiter.js#asyncload', array(), NULL, true);
    if (strpos($url, '#asyncload')===false)
        return $url;
    else if (is_admin())
        return str_replace('#asyncload', '', $url);
    else
        return str_replace('#asyncload', '', $url)."' async='async"; 
}
//add_filter('clean_url', 'add_async_forscript', 11, 1);


//---------------------------------------------------------------------------------------------;
// Wp login stylen/admin css
//---------------------------------------------------------------------------------------------;


function kb_custom_login_logo() {
    wp_enqueue_style( 'custom-login', get_stylesheet_directory_uri() . '/admin.css' );
}

add_action( 'login_enqueue_scripts', 'kb_custom_login_logo' );


//---------------------------------------------------------------------------------------------;
// Disable Scripts
//---------------------------------------------------------------------------------------------;

function disableScripts() {
   wp_dequeue_style( 'wp-block-library' ); //Gutenberg-enqueued CSS
}
add_action( 'wp_enqueue_scripts', 'disableScripts' );


//---------------------------------------------------------------------------------------------;
// Disable the creation of images with -scale in the name
// https://make.wordpress.org/core/2019/10/09/introducing-handling-of-big-images-in-wordpress-5-3/
//---------------------------------------------------------------------------------------------;

add_filter( 'big_image_size_threshold', '__return_false' );

//---------------------------------;
// add custom file image sizes
//---------------------------------;


function af_custom_image_sizes() {
    add_theme_support( 'post-thumbnails' );
    add_image_size( 'uhd', 4096 );              // UHD Width
    add_image_size( 'full-hd', 1920 );          // Full HD Width
    add_image_size( 'row-width', 1200 );        // Row Width (inside container)
    add_image_size( 'col-md', 991 );            // Col MD 100%
    add_image_size( 'col-sm', 768 );            // Col SM 100%
}
add_action( 'after_setup_theme', 'af_custom_image_sizes' );


//---------------------------------;
// add custom menues
//---------------------------------;

function register_af_menus() {
    register_nav_menus(
        /*array(
            'header-menu' => 'Header Menu',
            'footer-menue-contact-2' => 'Footer Contact Links',
            'footer-menu-legal' => 'Footer Legal'
            
        )*/
        array(
            'header-menu' => 'Header Menu',
            'footer-menue-contact-2' => 'Footer Contact Links'
            
        )
    );
}
add_action( 'init', 'register_af_menus' );


//---------------------------------------------------------------------------------------------;
// ACF Google MAPS
//---------------------------------------------------------------------------------------------;

function my_acf_init_maps_key() {
	acf_update_setting('google_api_key', 'XXXXXXX');
}
//add_action('acf/init', 'my_acf_init_maps_key');




//---------------------------------------------------------------------------------------------;
// Custom Post Type
//---------------------------------------------------------------------------------------------;

function create_customPostType() {
    register_post_type( 'product',
    // CPT Options
        array(
          'labels' => array(
           'name' => 'Products',
           'singular_name' => 'Product'
          ),
          'show_ui' => true,
          'public' => true,
          'supports' => array( 'thumbnail','excerpt', 'title' ),
          'has_archive' => true,
          'rewrite' => array('slug' => "produkt", 'with_front' => false, 'pages' => true ),
         )
    ); 
    
    
    register_post_type( 'reference',
    // CPT Options
        array(
          'labels' => array(
           'name' => 'References',
           'singular_name' => 'References'
          ),
          'show_ui' => true,
          'public' => true,
          'supports' => array( 'thumbnail','excerpt', 'title' ),
          'has_archive' => true,
          'rewrite' => array('slug' => "refererenz", 'with_front' => false, 'pages' => true ),
         )
    );  
	
	
	
    register_post_type( 'news',
    // CPT Options
        array(
          'labels' => array(
           'name' => 'News',
           'singular_name' => 'News'
          ),
          'show_ui' => true,
          'public' => true,
          'supports' => array( 'thumbnail','excerpt', 'title' ),
          'has_archive' => true,
          'rewrite' => array('slug' => "news", 'with_front' => false, 'pages' => true ),
         )
    );
    
    
}
add_action( 'init', 'create_customPostType' );


?>