<?php

function hide_admin_bar(){ 
    
    $userAdminbar = wp_get_current_user();
    
    if (in_array('administrator', $userAdminbar->roles)) {
        return true;
    }else{
       return false; 
    }
    return false; 
     
}
add_filter( 'show_admin_bar', 'hide_admin_bar' );

// Includes a module via its module slug
// module type is for better readability
function af_include_module ($module_slug, $module_type = 'm') {
    include( get_stylesheet_directory() . '/templates-frontend/'.$module_type.'-'. $module_slug  .'/'.$module_type.'-'.$module_slug .'.php' );
}

// Includes a template via its template slug
function af_include_template ($template_slug ) {
    include( get_stylesheet_directory() . '/templates/'. $template_slug . '.php' );
}

function loadTextDomain() {
    load_theme_textdomain( 'neun', get_template_directory() . '/languages' );
}

add_action('after_setup_theme','loadTextDomain');


// Returns the state of the ACF Option to turn off debugging developement functions
function af_is_dev_mode ( ) {

    $dev_mode_setting = get_field('entwicklungsmodus','options');

    if( $dev_mode_setting ) {
        $is_dev_mode = true;
    } else {
        $is_dev_mode = false;
    }

    return $is_dev_mode;
}


//---------------------------------;
//Comments - Kommentare
//---------------------------------;

function wpb_move_comment_field_to_bottom( $fields ) {
    $comment_field = $fields['comment'];
    unset( $fields['comment'] );
    $fields['comment'] = $comment_field;
    return $fields;
}
 
//add_filter( 'comment_form_fields', 'wpb_move_comment_field_to_bottom' );



//---------------------------------;
// add Post Type Support
//---------------------------------;

function addPostTypeSupport() {
    add_post_type_support( 'page', 'excerpt' );
}
add_action('init', 'addPostTypeSupport');



//---------------------------------;
// Excerpt - Change Label Text
//---------------------------------;
add_filter( 'gettext', 'changeExcerptLabel', 10, 2 );
function changeExcerptLabel( $translation, $original ){
    if ( 'Excerpt' == $original ) {
        return 'Excerpt: This text is a short description. It appears for example in the search results.';
    }
    return $translation;
}


//---------------------------------;
// Custom Excerpt
//---------------------------------;

function get_custom_excerpt($limit, $post_id = null){
    $excerpt = get_the_excerpt();
    $excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
    $excerpt = strip_shortcodes($excerpt);
    $excerpt = strip_tags($excerpt);
    $excerpt = substr($excerpt, 0, $limit);
    $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
    $excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
    $excerpt = $excerpt.'...';
    echo $excerpt;
}


//---------------------------------;
// Blog Pagination
//---------------------------------;
function pagination($pages = '', $range = 4){
     $showitems = ($range * 2)+1;  
     global $paged;
     if(empty($paged)) $paged = 1;
     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         { $pages = 1; }
     }   
 
     if(1 != $pages)
     {
         echo "<nav class=\"pagination\"><ul>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<li><a href='".get_pagenum_link(1)."'>&laquo;</a></li>";
         if($paged > 1 && $showitems < $pages) echo "<li><a href='".get_pagenum_link($paged - 1)."'>&lsaquo;</a></li>";
 
         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<li><span class=\"current\">".$i."</span></li>":"<li><a href='".get_pagenum_link($i)."' class=\"inactive \">".$i."</a></li>";
             }
         }
 
         if ($paged < $pages && $showitems < $pages) echo "<li><a href=\"".get_pagenum_link($paged + 1)."\">&rsaquo;</a></li>";
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<li><a href='".get_pagenum_link($pages)."'>&raquo;</a></li>";
         echo "</ul></nav>\n";
     }
}

//--------------------------------------;
//Seiten suche auf posts beschraenken
//--------------------------------------;

function restrichtSearch(){
    if (!is_admin()) {
        function wpb_search_filter($query) {
            if ($query->is_search) {
                $query->set('post_type', 'post');
            }
            return $query;
        }
        //add_filter('pre_get_posts','wpb_search_filter');
    } 
}

//restrichtSearch();









function array_sort($array, $on, $order)
{
    $new_array = array();
    $sortable_array = array();
    
    //print_r($on);

    if (count($array) > 0) {
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $k2 => $v2) {
                    if ($k2 == $on) {
                        $sortable_array[$k] = $v2;
                    }
                }
            } else {
                $sortable_array[$k] = $v;
            }
        }

        switch ($order) {
            case SORT_ASC:
                asort($sortable_array);
            break;
            case SORT_DESC:
                arsort($sortable_array);
            break;
        }

        foreach ($sortable_array as $k => $v) {
            $new_array[$k] = $array[$k];
        }
    }

    //print_r($new_array);
    
    return $new_array;
}


//--------------------------------------;
// Add slugs to custom menu items
//--------------------------------------;
    function add_slug_class_to_menu_item($output){
      $ps = get_option('permalink_structure');

      if(!empty($ps)){
        $idstr = preg_match_all('/<li id="menu-item-(\d+)/', $output, $matches);


          foreach($matches[1] as $mid){
            $id = get_post_meta($mid, '_menu_item_object_id', true);


            $menueurl;
            $menueurlTarget;

            $menuitemurlmeta = get_post_meta($mid, '_menu_item_url', false);
            $menueurlTarget = get_post_meta($mid, '_menu_item_target', false); 
            $menueurlTarget = $menueurlTarget[0];

            if($menuitemurlmeta["0"] != ""){
                $menueurl = $menuitemurlmeta[0];
            }else{
                //customlink
                //normal item, page
               $menueurl = get_permalink($id);
            }

            $classes = get_post_meta($mid,'_menu_item_classes',true);
            $classes = $classes["0"];

            $slug = basename(get_permalink($id));
            $output = preg_replace('/menu-item-'.$mid.'">/', 'menu-item-'.$mid.' menu-item-'.$slug.'" data-target="'.$menueurlTarget.'" data-url="'.$menueurl.'" data-classes="'.$classes.'" data-menueitemslug="'.$slug.'" data-menueitemid="'.$mid.'" data-menueitemobjectid="'.$id.'">', $output, 1);
          }
      }
      return $output;
    }
    add_filter('wp_nav_menu', 'add_slug_class_to_menu_item');







//--------------------------------------;
// Page Title, Meta_Title
//--------------------------------------;


function af_get_title () {

    $global_suffix = get_field('options_seo_meta_global_suffix','options');
    
    $title_main = get_the_title();
    


    if(is_search()){
        //Search, Suche
        //$pageName = get_field('options_search_page_headline','options');
        $pageName = get_field('options_seo_meta_title_searchpage','option');
        
        $title_text = $pageName . $global_suffix;
    }else if(is_page()){
        //page, seite
        $title_text = $title_main . $global_suffix;
    }else if(is_404()){
        $pageName = get_field('options_seo_meta_title_404','option');
        $title_text = $pageName . $global_suffix;
        
    }else{
        //fallback
        $title_text = $title_main . $global_suffix;
    }
    
    echo '<title>'.$title_text.'</title>';
}



function af_get_meta_description() {
    $meta;
    
    if(get_field('pageoption_metadescription') != ""){
        //feld vorhanden, Page Meta vorhanden, feld nicht leer
        $meta = get_field('pageoption_metadescription');

    }else{
        //feld nicht vorhanden, general Meta, feld leer
        
        
        if(is_search()){
            //if search meta is empty, general Meta Description
            //else use search meta description

            if(get_field('options_seo_meta_description_searchpage','option') == ""){
                $meta = get_field('options_seo_meta_description','option');
            }else{
                $meta = get_field('options_seo_meta_description_searchpage','option');
            }
        }else if(is_404()){
            
            if(get_field('options_seo_meta_description_404','option') == ""){
                $meta = get_field('options_seo_meta_description','option');
            }else{
                $meta = get_field('options_seo_meta_description_404','option');
            }
            
           
            
        }else{
            //fallback
            $meta = get_field('options_seo_meta_description','option');
        }
        

    }


    
    echo "<meta name='description' content='".$meta."' />";
    
    
}


//--------------------------------------;
// Enable SVG, IFC
//--------------------------------------;


function kb_svg ( $svg_mime ){
	$svg_mime['svg'] = 'image/svg+xml';
	$svg_mime['ifc'] = 'text/plain';
	return $svg_mime;
}

add_filter( 'upload_mimes', 'kb_svg' );



function kb_ignore_upload_ext($checked, $file, $filename, $mimes){

 if(!$checked['type']){
 $wp_filetype = wp_check_filetype( $filename, $mimes );
 $ext = $wp_filetype['ext'];
 $type = $wp_filetype['type'];
 $proper_filename = $filename;

 if($type && 0 === strpos($type, 'image/') && $ext !== 'svg'){
 $ext = $type = false;
 }

 $checked = compact('ext','type','proper_filename');
 }

 return $checked;
}

add_filter('wp_check_filetype_and_ext', 'kb_ignore_upload_ext', 10, 4);


?>