<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
    
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-176154704-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-176154704-1');
</script>
    
    
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo get_stylesheet_directory_uri(); ?>/favicons/apple-touch-icon-57x57.png" />
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo get_stylesheet_directory_uri(); ?>/favicons/apple-touch-icon-114x114.png" />
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo get_stylesheet_directory_uri(); ?>/favicons/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo get_stylesheet_directory_uri(); ?>/favicons/apple-touch-icon-144x144.png" />
	<link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?php echo get_stylesheet_directory_uri(); ?>/favicons/apple-touch-icon-60x60.png" />
	<link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo get_stylesheet_directory_uri(); ?>/favicons/apple-touch-icon-120x120.png" />
	<link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo get_stylesheet_directory_uri(); ?>/favicons/apple-touch-icon-76x76.png" />
	<link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo get_stylesheet_directory_uri(); ?>/favicons/apple-touch-icon-152x152.png" />
	<link rel="icon" type="image/png" href="<?php echo get_stylesheet_directory_uri(); ?>/favicons/favicon-196x196.png" sizes="196x196" />
	<link rel="icon" type="image/png" href="<?php echo get_stylesheet_directory_uri(); ?>/favicons/favicon-96x96.png" sizes="96x96" />
	<link rel="icon" type="image/png" href="<?php echo get_stylesheet_directory_uri(); ?>/favicons/favicon-32x32.png" sizes="32x32" />
	<link rel="icon" type="image/png" href="<?php echo get_stylesheet_directory_uri(); ?>/favicons/favicon-16x16.png" sizes="16x16" />
	<link rel="icon" type="image/png" href="<?php echo get_stylesheet_directory_uri(); ?>/favicons/favicon-128.png" sizes="128x128" />
	<meta name="application-name" content="&nbsp;"/>
	<meta name="msapplication-TileColor" content="#FFFFFF" />
	<meta name="msapplication-TileImage" content="<?php echo get_stylesheet_directory_uri(); ?>/favicons/mstile-144x144.png" />
	<meta name="msapplication-square70x70logo" content="<?php echo get_stylesheet_directory_uri(); ?>/favicons/mstile-70x70.png" />
	<meta name="msapplication-square150x150logo" content="<?php echo get_stylesheet_directory_uri(); ?>/favicons/mstile-150x150.png" />
	<meta name="msapplication-wide310x150logo" content="<?php echo get_stylesheet_directory_uri(); ?>/favicons/mstile-310x150.png" />
	<meta name="msapplication-square310x310logo" content="<?php echo get_stylesheet_directory_uri(); ?>/favicons/mstile-310x310.png" />

    <!-- WP HEAD -->
    <?php wp_head();?>
    
    
    <?php af_get_title(); ?>
    <?php af_get_meta_description(); ?>

    
    
    

</head>

<?php
    $addHeaderStyle = "";
?>

<body <?php body_class(array( $addHeaderStyle )); ?> class="" 
    data-browser="<?php echo get_browser_name($_SERVER['HTTP_USER_AGENT']); ?>"
    data-ismobile=""  
      
    data-currentlanguage="<?php echo ICL_LANGUAGE_CODE; ?>"
      
    >
    

    
    <header class="pdfcrowd-remove">
        <div class="container-fluid container-fluid-max-width mx-0 mx-md-auto h100 topNavigation">
            <div class="row h100">
                <div class="col-12 col-md-12 col-lg-6 align-self-center">
                    <div id="sitelogo" class="logo-container" >
                        <a href="<?php echo get_home_url();?>">
                            <img class="logo" src="<?php echo get_stylesheet_directory_uri(); ?>/images/header/Logo_95_Construction_blue.jpg">
                        </a>
                    </div>
                    <div class="desktopHamburgerContainer d-none d-lg-inline-block">
                        <div class="hamburg">
                            <span class="line"></span>
                            <span class="line"></span>
                            <span class="line"></span>
                        </div>
                    </div>
                    <div class="mobileHamburgerContainer d-inline-block d-lg-none">
                        <div class="hamburg">
                            <span class="line"></span>
                            <span class="line"></span>
                            <span class="line"></span>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-6 d-none d-lg-block align-self-center">
                    
                    <div class="rightContent d-flex align-items-center">
                    
                        <div class="lngContainer">
                            <?php
                                function icl_post_languages(){
                                    $languages = icl_get_languages('skip_missing=0');

                                    $langs = array();
                                    $id = 0;
                                    if(1 < count($languages)){
                                       // print_r($languages);
                                        foreach($languages as $l){
                                            //print_r($l);
                                            $code = $l['code'];
                                            $native_name = $l['native_name'];
                                            $default_locale = $l['default_locale'];
                                            $translated_name = $l['translated_name'];
                                            $country_flag_url = $l['country_flag_url'];
                                            $language_code = $l['language_code'];

                                            $langs[$id]["url"] = $l['url'];
                                            $langs[$id]["native_name"] = $l['native_name'];
                                            $langs[$id]["active"] = $l['active'];
                                            $langs[$id]["default_locale"] = $l['default_locale'];
                                            $langs[$id]["language_code"] = $l['language_code'];
                                            
                                            
                                            $id++;
                                        }
                                    }

                                    $sortLNG = array_sort($langs, 'active', SORT_DESC);
                                    echo "<div class='languages'>";
                                    foreach ($sortLNG as $item) {
                                        //print_r($item);
                                        $url = $item["url"];
                                        $nativeName = $item["native_name"];
                                        $active = $item["active"];
                                        $default_locale = $item["default_locale"];
                                        $language_code = $item["language_code"];
                                        
                                        
                                        echo "<div>";
                                        echo "<a href=".$url.">".$language_code."</a>";
                                        echo "</div>";

                                    }
                                    echo "</div>";
                                    
                                }
                                icl_post_languages(); 
                            
                            ?>
                        </div>
                        
                        <div class="searchContainer">
                            <img class="icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/header/Icon_Search_blue.png">
                            
                        </div>
                        
                        
                    </div>
                    
                 
                </div>
            </div>
        </div>
        
        

        
        <!-- start desktop -->
        <div class="container-fluid container-fluid-max-width px-0 menueNavigationDesktop ">
            
            
            <div class="row ">
                <div class="col-12 col-sm-6 col-lg-7">
                    <?php
                        wp_nav_menu( array(
                            'menu'              => 'primary',
                            'theme_location'    => 'header-menu',
                            'depth'             => 0,
                            'container'         => 'ul',
                            'menu_id'   => 'menueNavigation',
                            'container_id'      => '',
                            'menu_class'        => '',
                            'after'      => ''
                        ));
                      ?>
                </div>

                <div class="col-12 col-sm-6 col-lg-5">
                    
                    
                    <ul class="menueinfotext">
                    <?php
                        $menu_items = wp_get_nav_menu_items("main");
                      
                        foreach ( (array) $menu_items as $key => $menu_item ) {
                            
                              
                            
                            $menuItemParent = $menu_item;
                            
                           

                           
                               // echo $title;
                                $id = $menu_item->ID;
                                $title = $menu_item->title;
                                
                                $text = get_field('menue_top_description', $id);

                                $menufirst = wp_get_nav_menu_object("main");
                                $headline = get_field('main_menue_first_text_headline', $menufirst);
                                $firsttext = get_field('main_menue_first_text', $menufirst);
                                
                                //echo "<li>";
                                //echo "<div class='headline'>".$headline."</div>";
                                //echo "<div class='textbox'>".$firsttext."</div>";
                                //echo "</li>";

                                echo "<li class='' data-menueitemid='$id' >";
                                echo "<div class='headline'>".$title."</div>";
                                echo "<div class='textbox'>".$text."</div>";
                                echo "</li>";
                            

                        };
                    ?>
                    </ul>
                    
                    
                </div>
            </div>
        </div>
        
        <!-- end desktop -->
        
        
        <!-- start mobile -->
        
        <div class="container-fluid menueNavigationMobile ">
            <div class="row">
                <div class="col-12">
                    
                    <div class="submenueHeaderContainer">
                        <div class="submenueHeaderContainerHeadline">
                            <img class="icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/menue/Icon_Submenu_back_arrow.png">
                            <span class="text">test</span>
                        
                        </div>
                    </div>
                    
                    <div class="navigationContainer">
                        <?php
                            wp_nav_menu( array(
                                'menu'              => 'primary',
                                'theme_location'    => 'header-menu',
                                'depth'             => 0,
                                'container'         => 'ul',
                                'menu_id'   => 'menueNavigationMobile',
                                'container_id'      => '',
                                'menu_class'        => '',
                                'after'      => ''
                            ));
                          ?> 
                    </div>
                    
                    
                    <div class="searchContainerMobile">
                        <div class="content">
                            <div class="text"><?php _e('menue_mobile_text_search', 'neun'); ?></div>
                            <div class="searchcontainer">
                            
                                <form id="searchformMobile" method="get" action="<?php echo home_url('/'); ?>">
                                    <input type="text" class="search-field-mobile" name="s" placeholder="<?php _e('search_placeholder_searchinput', 'neun'); ?>" value="<?php the_search_query(); ?>">
                                    <!--<input type="submit" value="Search">-->
                                </form>
                            
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="mobileLanguageSwitcherContainer">
                        <ul>
                        <?php
                            function icl_post_languages_smartphone(){
                                $languages = icl_get_languages('skip_missing=0');
                                if(1 < count($languages)){
                                   
                                    foreach($languages as $l){
                                        $code = $l['code'];
                                        $native_name = $l['native_name'];
                                        $default_locale = $l['default_locale'];
                                        $translated_name = $l['translated_name'];
                                        $country_flag_url = $l['country_flag_url'];
                                        $language_code = $l['language_code'];

                                        $active = $l['active'];
                                        
                                        if($active == 0){
                                            echo "<li>";
                                            echo "<a href='".$l['url']."'>".$language_code."</a>";
                                            echo "<span class='divider'> | </span>";
                                            echo "</li>";
                                        }

                                    }
                                   
                                }
                            }
                            icl_post_languages_smartphone(); 
                        ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        
        
        <!-- end mobile -->
        
        
        
    </header>
<div class="body-content-wrapper">