<?php


get_header();

?>

<div class="sectionAttachment">
    <div class="container" >
        <div class="row">
            <div class="col-12" >
                <?php 
                    echo wp_get_attachment_image(get_the_ID(), 'full', "false", array( "class" => "attachmentImage"));
                ?>
            </div>
        </div>
    </div>
</div>


<?php

get_footer();

?>

