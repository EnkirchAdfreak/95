<?php
    $content = get_sub_field('inhalte');

   


    $postObject = $content["page-list-products-single_product"];

    $postObjectID = $postObject->ID; 

    setup_postdata( $postObject ); 


    $productTitle = get_the_title($postObjectID);
    $productLink = get_permalink($postObjectID);
    $productImageID = get_field( "products_tile_image", $postObjectID);
    $productText = get_field( "products_tile_text", $postObjectID);
   
    wp_reset_postdata();
    wp_reset_query();


?>


<div class="m-page-list-products-single">
    <div class="container container-custom-width" >
        <div class="row">
            <div class="col-12 col-md-6 col-lg-5 col-xl-3 offset-xl-0  ">
                <div class="animatedParent animateOnce">
                
                    <div class="imageContainer animated fadeInLeftShort">
                        <?php echo wp_get_attachment_image($productImageID, 'col-sm', "false", array( "class" => "articleImage"));?>
                    </div>
                    
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-7 col-xl-9 offset-xl-0 mouseover animatedParent animateOnce">
                
                <div class=" animated fadeIn">
                
                    <h2 class="headline  ">
                        <?php echo $productTitle; ?>
                    </h2>
                    <div class="textbox">
                        <?php echo $productText; ?>
                    </div>
                    <div class="buttonContainer">
                        <a class="buttonProduct button95Light" href="<?php echo esc_url( $productLink ); ?>" target="_self"><?php _e('module_listproducts_button_text', 'neun'); ?></a>
                </div>
                </div>

                
            </div>
        </div>
    </div>
</div>



     