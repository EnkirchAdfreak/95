$(function() {
    if ($('.m-page-product-detail_page_desktop').length) {
		$( ".m-page-product-detail_page_desktop" ).each(function( index ) {
            var ele = $(this);
            
            
            var variableHolder = {}; //Object
            var svgTransparent = "rgba(125,125,125,0)";
            var fillStrokeHover = $(this).data("colororange");
            
            
            var buttonNext = $(this).find(".ctrlButtonNext");
            var buttonPrev = $(this).find(".ctrlButtonPrev");
            
            
            var svgurl = ele.data("svgurl");
            var svgimg = new Image();
            svgimg.onload = svgimgloaded;
            svgimg.src = svgurl;
            
            

            
            
            
            //-------------------------------;
            //Desktop
            //-------------------------------;
            

            
            // Get the Object by ID
            var svgEBID;
            // Get the SVG document inside the Object tag
            var svgDoc;    

            
        
            

            function svgExplodeTestB(){
                svgEBID = document.getElementById("svgObjectbeamup");
                svgDoc = svgEBID.contentDocument;    

                var bss = svgDoc.querySelectorAll('[data-name="clickparent"]  [data-name]');
                //var svgDocB = bss.contentDocument;

                
                variableHolder = bss;
           
                
                //console.log("svgEBID: ",svgEBID)
                //console.log("svgDoc: ",svgDoc)
                //console.log("bss",bss)
                //console.log("variableHolder",variableHolder)
                

                $.each(variableHolder, function(index, value) {
                    //console.log("svgExplodeTestB() each")
                    //console.log("each",variableHolder);
                    //console.log("index",index);
                    
                    var index = variableHolder[index];
                    
                    $(index).css("stroke", svgTransparent);
                    $(index).css("fill", svgTransparent);
                    
                    $(index).mouseover(function() {
                        var prodID = $(this).data("name")
                        
                        var array = prodID.split("-");
                        var id_part1 = array[0];
                        var id_part2 = array[1]; // the number

                        $(this).css("stroke", fillStrokeHover);
                        $(this).css("fill", svgTransparent);
                        
                        
                        showInfoProductNew(id_part2);

                    }); 
                    
                    
                    $(index).mouseleave(function() {
                        
                        $.each(variableHolder, function(index, value) {
                            //alle highlights weg
                           
                            var index2 = variableHolder[index];
                            $(this).css("stroke", svgTransparent);
                            $(this).css("fill", svgTransparent)
                        });
                        
                        
                        hideInfoProductNew()
                        

                    }); 
                    
                 });
                
                //console.log("loaded")
                
                ele.find(".loadingAnimation").fadeOut();
                    
            }
            
            function svgimgloaded(){
                //console.log("svgimgloaded()")
                setTimeout(svgExplodeTestB, 500);
            }
          
            //setTimeout(svgExplodeTestB, 500);
            

            function showInfoProductNew(prodID){
                var panel = prodID;
                ele.find("ul.textlist").hide();
                ele.find("ul.textlist li").hide();
                
                ele.find("ul.textlist").stop().fadeIn("fast");
                ele.find("ul.textlist").find(".click-"+panel).fadeIn("fast");
            } 
            
            
            function hideInfoProductNew(){
                ele.find("ul.textlist").stop().hide();
                ele.find("ul.textlist li").stop().hide()
            }
            
            

            
            
            //-------------------------------;
            //Mobile
            //-------------------------------;

            var containerSlider = ele.find(".containerSliderProductDetail");
            var owlProduct = ele.find('.owlCarouseldetailproduct');
            
            owlProduct.on('initialized.owl.carousel', function(event) {
                //When the plugin has initialized.
                //before the plugin init
                //console.log(event)
                ele.find(".mobileContainer").css("visibility","visible");
            })
            
 
            owlProduct.owlCarousel({
                loop: false,
                margin: 30,
                responsiveClass: true,
                items: 1,
                dots: true,
                dotsContainer: ".customdots",
                autoplay: false,
                autoplayHoverPause: false,
                responsive:{
                    0:{
                    },
                    600:{
                    },
                    1000:{
                    }
                }
            });
            
            
            
            buttonNext.click(function() {
                // With optional speed parameter
                // Parameters has to be in square bracket '[]'
                owlProduct.trigger('next.owl.carousel', [300]);
            })
            
            
            // Go to the previous item
            buttonPrev.click(function() {
                // With optional speed parameter
                // Parameters has to be in square bracket '[]'
                owlProduct.trigger('prev.owl.carousel', [300]);
            })
            
            
            
            

            
            
            owlProduct.on('translate.owl.carousel', function(event) {
                //When the translation of the stage starts.
                //console.log(event)
                
                //var bss = svgDoc.querySelectorAll('polygon[data-name]');
                var bss = svgDoc.querySelectorAll('[data-name="clickparent"] [data-name]');
                
                variableHolder = bss;
        
                $.each(variableHolder, function(index, value) {
                    var index2 = variableHolder[index];
                    $(this).css("stroke", svgTransparent);
                    $(this).css("fill", svgTransparent)
                });
            })
            
            owlProduct.on('translated.owl.carousel', function(event) {
                //When the translation of the stage ended.
                var currentSlide = event.item.index;
                currentSlide = currentSlide + 1;

                //var strs = "polygon[data-name='click-"+currentSlide+"']";
                
                var strs = "[data-name='clickparent'] [data-name='click-"+currentSlide+"']";
                

                var bss = svgDoc.querySelectorAll(strs);
                var variableHolderSlided = bss;

                $.each(variableHolderSlided, function(index, value) {
                    var index2 = variableHolder[index];
                    $(this).css("stroke", fillStrokeHover);
                    $(this).css("fill", svgTransparent)
                });
                
            })
            
            

            

            
		});	//end each
    }//end length
});//end jrdy