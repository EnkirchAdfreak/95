<?php
    $content = get_sub_field('inhalte');
    //Product
    //$productSelect = $content["productselect"];

    $headline = $content["headline"];
    $textbox = $content["textbox"];
    $htag = "h2";
?>



<div class="m-page-product-detail-text">
    <div class="container container-custom-width" >
        <div class="row">
            <div class="col-12 col-md-10 offset-md-1 col-lg-8 offset-lg-2 col-xl-12 offset-xl-0">
                <div class="textContainer">
                    <?php if( $headline != "" ){ ?>
                    <?php
                        echo "<".$htag." class='headline headlineSection headlineSectionLineCentered'>";
                        echo $headline;
                        echo "</".$htag.">";
                    ?>
                    <?php }; ?>
                    <div class="textbox"><?php echo $textbox; ?></div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php
    //$basepath = $_SERVER['DOCUMENT_ROOT'];
    /*if($productSelect == "beamup"){
        require_once 'm-page-product-detail_beamup.php';
    }*/

    require_once 'm-page-product-detail_page.php';

?>