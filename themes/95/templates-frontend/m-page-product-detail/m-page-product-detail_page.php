<?php
    $content = get_sub_field('inhalte');

    $beamupSVG = $content["productdetailsvg"]["url"];


    $productsArr = Array();
    $countIDArr = 0;
    if( have_rows('inhalte') ): while ( have_rows('inhalte') ) : the_row(); 
        //echo get_sub_field('horlur');
        if( have_rows('product_details_beamup_repeater') ): while ( have_rows('product_details_beamup_repeater') ) : the_row(); 

            $productsArr[$countIDArr]["head"] = get_sub_field( "product_details_beamup_repeater_headline");
            $productsArr[$countIDArr]["text"] = get_sub_field( "product_details_beamup_repeater_text");
            $countIDArr++;
        endwhile; endif;
    endwhile; endif;

    $colorOrange = "#fc773c";
?>


<div class="m-page-product-detail m-page-product-detail_page_desktop" data-firstappearance="false"  data-colororange="<?php echo $colorOrange; ?>"   data-svgurl="<?php echo $beamupSVG; ?>" >
    <div class="container" >
        
        
        <div class="row ">
            <div class="col-12">
                
                <div class="loadingAnimation">
                    <div class="lds-facebook"><div></div><div></div><div></div></div>
                </div>
                
                <div class="containerContent">
                    <ul class="textlist">
                    <?php
                        $textCounter = 1;
                        foreach ($productsArr as $item) {
                            $head = $item["head"];
                            $text = $item["text"];
                    ?>
                        <li class="click-<?php echo $textCounter; ?>">
                            <div class="head"><?php echo $head; ?></div>
                            <div class="body"><?php echo $text; ?></div>
                        </li>
                    <?php
                            $textCounter++;
                        };
                    ?>
                    </ul>
                    <div class="containerImage">
                        <object id="svgObjectbeamup" data="<?php echo $beamupSVG; ?>" type="image/svg+xml" width="600" height="193"></object>
                    </div>
                </div>
            </div>
        </div>
        
        
        
        
        <div class="row  rowMobileContainer">
            <div class="col-2">
                <img class="ctrlButtonPrev" src="<?php echo get_stylesheet_directory_uri(); ?>/images/product/Icon_Submenu_back_arrow.png">
            </div>
            
            
            
            <div class="col-8 px-0">
                <div class="mobileContainer">
                    <div class="containerSliderProductDetail">
                        <div class="customdots" ></div>
                        
                        <div class="owl-carousel owl-theme owlCarouseldetailproduct">
                            <?php
                                //$countRefSlider = 0;
                                $textCounterMobile = 1;
                            
                                foreach ($productsArr as $item) {
                                    $head = $item["head"];
                                    $text = $item["text"];
                                ?>
                                <div class="slide" data-slidename="click<?php echo $textCounterMobile; ?>"> 
                                    <div class="contentContainer">
                                        <div class="head"><?php echo $head; ?></div> 
                                        <div class="text"><?php echo $text; ?></div> 
                                    </div>
                                </div>

                                <?php
                                    $textCounterMobile++;
                                    };//end for each
                                ?>

                        </div>
                    </div>

                </div>

            </div>
            
            
            <div class="col-2">
                <img class="ctrlButtonNext" src="<?php echo get_stylesheet_directory_uri(); ?>/images/product/Icon_Submenu_next_arrow.png">
            </div>
            
        </div>   
        
    </div>
</div>