<?php
    $content = get_sub_field('inhalte');

    //$headline = $content["headline"];
    //$textbox = $content["textblock"];

    $argsProducts = array(
        'post_type'=> 'product',
        'orderby'    => 'date',
        'post_status' => 'publish',
        'order'    => 'ASC',
        'posts_per_page' => -1
    );
    $resultProducts = new WP_Query( $argsProducts );

    $productsArr = Array();
    $countIDArr = 0;

    if( have_posts() ) : while( $resultProducts->have_posts() ) : $resultProducts->the_post(); 
        $productsArr[$countIDArr]["title"] = get_the_title();
        $productsArr[$countIDArr]["link"] = get_permalink();
        $productsArr[$countIDArr]["postID"] = get_the_ID();
        $productsArr[$countIDArr]["image"] = get_field( "products_tile_image", get_the_ID());
        $productsArr[$countIDArr]["text"] = get_field( "products_tile_text", get_the_ID());
        $countIDArr++;
    endwhile; 
    endif;

    wp_reset_query();
  
?>


<div class="m-page-list-products-mobile d-block d-md-none">
    <div class="container " >
        <div class="row">
            <div class="col-12">
                <?php
                
                $even = true;
                $arrCount = count($productsArr);
                $currentItem = 1;
                

                foreach ($productsArr as $item => $value) {
                    $title = $value["title"];
                    $signaturImage = $value["image"];
                    $text = $value["text"];
                    $link_url = $value["link"];
 
                    $orderClass;
                    
                    $rowClass1;
                    $rowClass2;
                    
                    $alignmentClass;

                    $animateTextClass;
                    $animateImageClass;
                    
                    
                    if($even == false){
                        $orderClass = "order-md-2";
                        $even = true;
                        
                        $alignmentClass = "even";
                        
                        $rowClass1 = "col-12 col-md-6 col-lg-3 col-xl-3  ";
                        $rowClass2 = "col-12 col-md-6 col-lg-4 col-xl-4  offset-lg-2 ";

                        $animateTextClass = "fadeIn";
                        $animateImageClass = "fadeIn";

                    }else{
                        $orderClass = "";
                        $even = false;
                        
                        $alignmentClass = "odd";
                      
                        
                        $rowClass1 = "col-12 col-md-6 col-lg-3 col-xl-3  offset-lg-3";
                        $rowClass2 = "col-12 col-md-6 col-lg-4 col-xl-4";

                        $animateTextClass = "fadeIn";
                        $animateImageClass = "fadeIn ";

                    }
                    

                    if($currentItem < $arrCount){
                        $rowMarginBottom = "rowMarginBottom";
                    }else{
                        $rowMarginBottom = "";
                    }
                    
                    $currentItem++;
                    
                ?>
                <div class="row rowProduct <?php echo $rowMarginBottom." ".$alignmentClass; ?>  ">    
                    <div class="imageRow <?php echo $rowClass1; ?> <?php echo $orderClass; ?>  mouseover animatedParent animateOnce">
                        <div class=" imageContainer animated <?php echo $animateImageClass; ?>">
                            <a href="<?php echo $link_url; ?>" target="_self">
                                <?php echo wp_get_attachment_image($signaturImage, 'row-width', "false", array( "class" => "articleImage"));?>
                            </a>
                        </div>
                    </div>
                    <div class="<?php echo $rowClass2; ?>  mouseover animatedParent animateOnce">
                        
                        <div class="animated  <?php echo $animateTextClass; ?>   contentContainer">
                        
                            <h2 class="headline<?php echo $alignmentClass; ?>">
                                <?php echo $title; ?>
                            </h2>
                            <div class="textbox">
                                <?php echo $text; ?>
                            </div>
                            <div class="buttonContainer">
                                <a class="button buttonProduct" href="<?php echo esc_url( $link_url ); ?>" target="_self"><?php _e('module_listproducts_button_text', 'neun'); ?></a>
                            </div>
                        
                        </div>
                        
                    </div>
                </div>
                

                <?php
                    };//end for each
                ?>
            </div>
        </div>
    </div>
</div>



<div class="m-page-list-products-desktop d-none d-md-block">
    <div class="container container-custom-width" >
        <div class="row">
            <div class="col-12">
                <?php
                
                $even = true;
                $arrCount = count($productsArr);
                $currentItem = 1;
                

                foreach ($productsArr as $item => $value) {
                    $title = $value["title"];
                    $signaturImage = $value["image"];
                    $text = $value["text"];
                    $link_url = $value["link"];
 
                    $orderClass;
                    
                    $rowClass1;
                    $rowClass2;
                    
                    $alignmentClass;

                    $animateTextClass;
                    $animateImageClass;
                    
                    
                    if($even == false){
                        $orderClass = "order-md-2";
                        $even = true;
                        
                        $alignmentClass = "even";
                        
                        $rowClass1 = "col-12 col-md-6 col-lg-3 col-xl-5  aa 1";
                        $rowClass2 = "col-12 col-md-6 col-lg-4 col-xl-6  offset-lg-2 offset-xl-0 aaa2";

                        $animateTextClass = "fadeIn";
                        $animateImageClass = "fadeInRightShort";

                    }else{
                        $orderClass = "";
                        $even = false;
                        
                        $alignmentClass = "odd";
                      
                        
                        $rowClass1 = "col-12 col-md-6 col-lg-3 col-xl-5  offset-lg-3 offset-xl-1 bb1";
                        $rowClass2 = "col-12 col-md-6 col-lg-4 col-xl-6 bbb2";

                        $animateTextClass = "fadeIn";
                        $animateImageClass = "fadeInLeftShort ";

                    }
                    

                    if($currentItem < $arrCount){
                        $rowMarginBottom = "rowMarginBottom";
                    }else{
                        $rowMarginBottom = "";
                    }
                    
                    $currentItem++;
                    
                ?>
                <div class="row rowProduct <?php echo $rowMarginBottom." ".$alignmentClass; ?>  ">    
                    <div class="imageRow <?php echo $rowClass1; ?> <?php echo $orderClass; ?>  animatedParent animateOnce">
                        <div class="imageContainer animated <?php echo $animateImageClass; ?>">
                             <a href="<?php echo $link_url; ?>" target="_self">
                                <?php echo wp_get_attachment_image($signaturImage, 'row-width', "false", array( "class" => "articleImage"));?>
                            </a>
                        </div>
                    </div>
                    <div class="<?php echo $rowClass2; ?>  mouseover animatedParent animateOnce">
                        
                        <div class=" animated <?php echo $animateTextClass; ?> contentContainer">
                        
                            <h2 class="headline<?php echo $alignmentClass; ?>">
                                <?php echo $title; ?>
                            </h2>
                            <div class="textbox">
                                <?php echo $text; ?>
                            </div>
                            <div class="buttonContainer">
                                <a class="button buttonProduct" href="<?php echo esc_url( $link_url ); ?>" target="_self"><?php _e('module_listproducts_button_text', 'neun'); ?></a>
                            </div>
                        
                        </div>
                        
                    </div>
                </div>
                

                <?php
                    };//end for each
                ?>
            </div>
        </div>
    </div>
</div>