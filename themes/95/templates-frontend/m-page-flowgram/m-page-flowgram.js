$(function() {
    if ($('.m-page-flowgram').length) {
		$( ".m-page-flowgram" ).each(function( index ) {
            var module = $(this);
            
            //headlineOrangeGlow
            //headlineBlueGlow

            $('.contentBox1 .headline, .contentBox1 .textbox, .numberContainer1').hover(function() {
                $(".numberContainer1 .numberBg3").addClass("numberBg3GlowOdd");
                $(".contentBox1 .headline").addClass("headlineOrangeGlow");
            }, function(){
                $(".numberContainer1 .numberBg3").removeClass("numberBg3GlowOdd");
                $(".contentBox1 .headline").removeClass("headlineOrangeGlow");
            });

            $('.contentBox3 .headline, .contentBox3 .textbox, .numberContainer3').hover(function() {
                $(".numberContainer3 .numberBg3").addClass("numberBg3GlowOdd");
                $(".contentBox3 .headline").addClass("headlineOrangeGlow");
            }, function(){
                $(".numberContainer3 .numberBg3").removeClass("numberBg3GlowOdd");
                $(".contentBox3 .headline").removeClass("headlineOrangeGlow");
            });  

            $('.contentBox5 .headline, .contentBox5 .textbox, .numberContainer5').hover(function() {
                $(".numberContainer5 .numberBg3").addClass("numberBg3GlowOdd");
                $(".contentBox5 .headline").addClass("headlineOrangeGlow");
            }, function(){
                $(".numberContainer5 .numberBg3").removeClass("numberBg3GlowOdd");
                $(".contentBox5 .headline").removeClass("headlineOrangeGlow");
            });  

            $('.contentBox7 .headline,  .contentBox7 .textbox, .numberContainer7').hover(function() {
                $(".numberContainer7 .numberBg3").addClass("numberBg3GlowOdd");
                $(".contentBox7 .headline").addClass("headlineOrangeGlow");
            }, function(){
                $(".numberContainer7 .numberBg3").removeClass("numberBg3GlowOdd");
                $(".contentBox7 .headline").removeClass("headlineOrangeGlow");
            });  


            
            
            $('.contentBox2 .headline,  .contentBox2 .textbox, .numberContainer2').hover(function() {
                $(".numberContainer2 .numberBg3").addClass("numberBg3GlowEven");
                $(".contentBox2 .headline").addClass("headlineBlueGlow");
            }, function(){
                $(".numberContainer2 .numberBg3").removeClass("numberBg3GlowEven");
                $(".contentBox2 .headline").removeClass("headlineBlueGlow");
            });   

            $('.contentBox4 .headline, .contentBox4 .textbox, .numberContainer4').hover(function() {
                $(".numberContainer4 .numberBg3").addClass("numberBg3GlowEven");
                $(".contentBox4 .headline").addClass("headlineBlueGlow");
            }, function(){
                $(".numberContainer4 .numberBg3").removeClass("numberBg3GlowEven");
                $(".contentBox4 .headline").removeClass("headlineBlueGlow");
            });  

            $('.contentBox6 .headline, .contentBox6 .textbox, .numberContainer6').hover(function() {
                $(".numberContainer6 .numberBg3").addClass("numberBg3GlowEven");
                $(".contentBox6 .headline").addClass("headlineBlueGlow");
            }, function(){
                $(".numberContainer6 .numberBg3").removeClass("numberBg3GlowEven");
                $(".contentBox6 .headline").removeClass("headlineBlueGlow");
            });  
            
            
		});	//end each
    }//end length
    
    
    
    if ($('.m-page-flowgram-mobile').length) {
		$( ".m-page-flowgram-mobile" ).each(function( index ) {
            var module = $(this);

		});	//end each
    }//end length
    
    
});//end jrdy