<?php
    $content = get_sub_field('inhalte');

    $productsArr = Array();
    $countIDArr = 0;

    if( have_rows('inhalte') ): while ( have_rows('inhalte') ) : the_row(); 
        //echo get_sub_field('horlur');
        if( have_rows('page-flowgram_repeater') ): while ( have_rows('page-flowgram_repeater') ) : the_row(); 

            $productsArr[$countIDArr]["headline"] = get_sub_field( "headline");
            $productsArr[$countIDArr]["textbox"] = get_sub_field( "textbox");
            $countIDArr++;

        endwhile; endif;
    endwhile; endif;


    $foreachCounterLeft = 0;
    $foreachCounterRight = 0;

?>



<div class="m-page-flowgram d-none d-md-block " data-firstappearancemobile="true">
    <div class="container" >
        <div class="row">
            <div class="col-12 col-md-4 col-xl-3 offset-xl-1">
                <ul class="flowprocessTextContent flowprocessTextContentLeft">
                <?php
                    foreach ($productsArr as $item) {
                        $nameLeft = $item["headline"];
                        $textbox = $item["textbox"];
                        if($foreachCounterLeft % 2 == 0){ 
                            echo "<li class='contentBox".( $foreachCounterLeft + 1)."'  >";
                            echo "<span class='headline'>".$nameLeft."</span>";
                            echo "<span class='textbox'>".$textbox."</span>";
                            echo "</li>";
                        }
                        $foreachCounterLeft++;
                    }
                ?>
                </ul>
            </div>
            
            <div class="col-12 col-md-4 col-xl-4 ">
                <div class="ImageContainer">
                <?php
                    for ($i = 1; $i <= 7; $i++) {  
                ?>
                    <div class="numberContainer numberContainer<?php  echo $i; ?>">
                        <div class="numberBg1"></div>
                        <div class="numberBg2"></div>
                        <div class="numberBg3">
                            <div class="textnumber"><?php  echo $i; ?></div>
                        </div>
                    </div>
                    <?php
                    }
                ?>
                    <img class="flowbg" src="<?php echo get_stylesheet_directory_uri(); ?>/images/modul/flowgram/Process_Steps_blue.png">
                </div>

            </div>
            <div class="col-12 col-md-4 col-xl-3 ">
                <ul class="flowprocessTextContent flowprocessTextContentRight">
                <?php
                     foreach ($productsArr as $item) {
                        $nameLeft = $item["headline"];
                        $textbox = $item["textbox"];

                        if($foreachCounterRight % 2 != 0){ 
                          
                            echo "<li class='contentBox".( $foreachCounterRight + 1)."'  >";
                            echo "<span class='headline  '>".$nameLeft."</span>";
                            echo "<span class='textbox  '>".$textbox."</span>";
                            echo "</li>";
                        }
                        
                         $foreachCounterRight++;
                    }
                ?>
              </ul>
            </div>
        </div>
    </div>
</div>





<div class="m-page-flowgram-mobile d-block d-md-none">
    <div class="container" >
        <div class="row">  
            <div class="col-12">
                <ul class="flowlistMobile">
                    <?php
                    $itemMobileIndex = 1; 
                    foreach ($productsArr as $itemMobile) {
                        $headline = $itemMobile["headline"];
                        $textbox = $itemMobile["textbox"];
                    ?>
                    <li>
                        <div class="number">
                            <div class="numberContainer">
                                <div class="numberBg1"></div>
                                <div class="numberBg2"></div>
                                <div class="numberBg3">
                                    <div class="textnumber"><?php echo $itemMobileIndex; ?></div>
                                </div>
                            </div>
                            <?php
                                if($itemMobileIndex < 7){
                            ?>
                                <img class="flowbg" src="<?php echo get_stylesheet_directory_uri(); ?>/images/modul/flowgram/mobile_step.svg">
                            
                            <?php
                                }else{
                            ?>
                                <img class="flowbg" src="<?php echo get_stylesheet_directory_uri(); ?>/images/modul/flowgram/mobile_step_last.svg">
                            <?php
                                };
                            ?>
                        </div>
                        <div class="textbox">
                            <div class="headline"><?php echo $headline; ?></div>
                            <div class="text"><?php echo $textbox; ?></div>
                        </div>
                    </li>
                    
                    
                    <?php
                        $itemMobileIndex++;
                    
                    };
                    
                    ?>

                </ul>

            </div>

        </div>
    </div>
</div>
