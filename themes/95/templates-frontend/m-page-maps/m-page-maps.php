<?php
    $content = get_sub_field('inhalte');

    $headline = get_field('optionsMaps_headline','options');
    $htag = "h2";

    $text = $content['text'];


    $location;
    $location = get_field('optionsMaps_karte','options');


    $adress = get_field('optionsMaps_adress','options');
    $phone = get_field('optionsMaps_phone','options');
    $mail = get_field('optionsMaps_mail','options');


?>
<div class="m-page-maps">
    <div class="container container-custom-width">
        <div class="row">

            <div class="col-12 col-md-6 col-lg-6 col-xl-6 offset-xl-0 order-2 order-md-1">
                <div class="map-container h-100">
					<div class="acf-map" data-zoom="16">
						<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
					</div>
				</div>
            </div>

            <div class="col-12 col-md-6 col-lg-6 col-xl-5  order-md-2">
                <div class="textContainer">
                    <div class="headlineContainer">
                        <?php
                            echo "<".$htag." class='headline headlineSection headlineSectionLineLeft'>";
                            echo $headline;
                            echo "</".$htag.">";
                        ?>
                    </div>
                    <ul class="contactContainer">
                        <li class="adress"><?php echo $adress; ?></li>
                        <li class="phone"><?php echo $phone; ?></li>
                        <li class="mail">
                            <p><a href="mailto:<?php echo $mail; ?>"><?php echo $mail; ?></a></p>
                            
                        </li>
                    </ul>

                </div>
            </div>    


        </div> 
    </div>
</div>