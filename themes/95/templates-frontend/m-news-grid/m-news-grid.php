<?php
    $content = get_sub_field('inhalte');

    $isShuffle = $content["shuffle"];
        


    $productsArr = Array();
    $countProductsArr = 0;

    $argsNews = array(
        'post_type'=> 'news',
        'orderby'    => 'date',
        'post_status' => 'publish',
        'order'    => 'DESC',
        'posts_per_page' => -1
    );
    $resultNews = new WP_Query( $argsNews );

    if( have_posts() ) : while( $resultNews->have_posts() ) : $resultNews->the_post(); 
        $productsArr[$countProductsArr]["name"] = get_the_title();
        $productsArr[$countProductsArr]["permalink"] = get_permalink();
        $productsArr[$countProductsArr]["postID"] = get_the_ID();
        $productsArr[$countProductsArr]["city"] = get_field( "news_tile_location", get_the_ID());
        $imageid = get_field( "news_tile_image", get_the_ID());
        

        $productsArr[$countProductsArr]["bgimage"] = wp_get_attachment_image_src( $imageid, 'col-sm' );

        $countProductsArr++;




    endwhile; 
    endif;

    wp_reset_query();

    $animateDelay = "250";
    $animateDelayAdd = "250";

    $animateCount = 0;
    $countTiles = 1;
    

    if($isShuffle == true){
    
        shuffle($productsArr);
        $productsArr = array_slice($productsArr, 0, 3); 
    }


?>

<div class="m-news-grid">
    <div class="container px-0 px-md-3 container-custom-width" >
        <div class="row">
            <div class="col-12">
				<ul class='gridrefs ' >
                   <?php
                    foreach (array_slice($productsArr, 0) as $item => $value) {
                        $name = $value["name"];
                        $city = $value["city"];
                        $permalink = $value["permalink"];
                        $bgimage = $value["bgimage"];
                    ?>
					<li class="animatedParent animateOnce " data-appear-top-offset='150' >
                        <a href="<?php echo $permalink; ?>" class=" animated bounceInCustom  customdelay-<?php echo $animateDelay; ?>  ">
                            <div class="contentbox    ">
                                <div class="content">
                                    <div class="name"><?php echo $name; ?></div>
                                    <div class="city"><?php echo $city; ?></div>
                                </div>
                                <div class="bgimage" style="background-image: url('<?php echo $bgimage[0]; ?>')"></div>
                            </div>
                        </a>
					</li>
                    <?php
                        $countTiles++;
                        $animateDelay = $animateDelay + $animateDelayAdd;
                        $animateCount++;
                    ?>
                <?php
                    };//end for each
                ?>
                </ul>
             </div>
        </div>    
    </div>
</div>