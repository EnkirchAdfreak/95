<?php
    $content = get_sub_field('inhalte');

    $headline = $content["headline"];
    $textbox = $content["textblock"];

    $link = $content["button"];
    if( $link ): 
        $link_url = $link['url'];
        $link_title = $link['title'];
        $link_target = $link['target'] ? $link['target'] : '_self';
    endif;

    $ytvideo = $content["youtube_video"];//url


    $backgroundcolor = $content["backgroundcolor"]; // transparent white blue  lightestgrey
    if($backgroundcolor == ""){
        $backgroundcolor = "transparent";
    }

    //button class
    $buttonStyle;

    if($backgroundcolor == "blue"){
        $buttonStyle = "button95Dark";
    }else{
        $buttonStyle = "button95Light";
    }

    //headline Tag
    $htag = $content["headlineTag"]; // h1 h2 div
    if($htag == "" || empty($htag)){
        $htag = "h2";
    }


    $rowwidth = $content["rowwidth"];


    if($rowwidth == "small"){
        $containerRow = "col-12 col-md-10 offset-md-1 col-lg-8 offset-lg-2 col-xl-4 offset-xl-4";
    }else if($rowwidth == "large"){
        $containerRow = "col-12 col-md-10 offset-md-1 col-lg-8 offset-lg-2 col-xl-12 offset-xl-0";
    }else{
        $containerRow = "col-12 col-md-10 offset-md-1 col-lg-8 offset-lg-2 col-xl-4 offset-xl-4";
    }
    

        $nomarginHeadline;
    
    if($textbox == "" && $ytvideo == "" && $link == ""){
        $nomarginHeadline = "style='margin:0px;'";
    }
        

?>
<div class="m-page-text-centered m-page-text-centered-<?php echo $backgroundcolor; ?>      pdfcrowd-remove">
    <div class="container container-custom-width" >
        <div class="row">
            <div class="<?php echo $containerRow ; ?>">
                <div class="textContainer">
                    
                    <?php if( $headline != "" ){ ?>
                    
                    <?php
                        echo "<".$htag." class='headline headlineSection headlineSectionLineCentered'    $nomarginHeadline >";
                        echo $headline;
                        echo "</".$htag.">";
                    ?>
                    <?php }; ?>

                    
                    
                    <?php if( $ytvideo != "" ){ ?>
                    
                    <div class="videobox">
                        <div class="embedContainer">
                            <iframe 
                                    width="560" 
                                    height="315" 
                                    src="<?php echo $ytvideo; ?>?controls=0" 
                                    frameborder="0" 
                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
                                    allowfullscreen>
                            </iframe>
                        </div>
                    </div>
                    <?php }; ?>
                    
                    
                    <div class="textbox"><?php echo $textbox; ?></div>

                    <?php if( $link ): ?>
                    <div class="buttonContainer">
                        <a class="button <?php echo $buttonStyle; ?>" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
                    </div>
                    <?php endif; ?>
                    
                </div>
            </div>
        </div>
    </div>
</div>