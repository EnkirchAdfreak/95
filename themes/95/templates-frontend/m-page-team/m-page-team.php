<?php
    $content = get_sub_field('inhalte');

    $headline = $content['headline'];
    $text = $content['text'];


    $productsArr = Array();
    $countIDArr = 0;

    if( have_rows('inhalte') ): while ( have_rows('inhalte') ) : the_row(); 
        //echo get_sub_field('horlur');
        if( have_rows('team_repeater') ): while ( have_rows('team_repeater') ) : the_row(); 

            $productsArr[$countIDArr]["imageID"] = get_sub_field( "team_repeater_image");
            $productsArr[$countIDArr]["name"] = get_sub_field( "team_repeater_name");
            $productsArr[$countIDArr]["position"] = get_sub_field( "team_repeater_position");
            $countIDArr++;

        endwhile; endif;
    endwhile; endif;



?>
<div class="m-page-team">
    <div class="container container-custom-width">
        <div class="row">

            <div class="col-12 col-md-12 col-lg-8 offset-lg-2 col-xl-12 offset-xl-0">
                
                <div class="contentContainer">
                    
                    <?php if( $headline != "" ){ ?>
                    
                    <?php
    
                        $htag = "h2";

                        echo "<".$htag." class='headline headlineSection headlineSectionLineCentered'>";
                        echo $headline;
                        echo "</".$htag.">";
    
                    ?>
                        
                    <?php }; ?>
                    
                    
                    <div class="textBox">
                        <?php echo $text; ?>
                    </div>
                 
                </div>
                
                
            </div>
            
            
            <div class="col-12 d-flex justify-content-center">
            
                <ul class="memberList animatedParent animateOnce"   data-sequence='500'>
                    
                    <?php
                        
                        $teamcount = 1;
                        foreach ($productsArr as $item) {
                            
                        $name = $item["name"];
                        $position = $item["position"];
                        $imageID = $item["imageID"];
                            
                    ?>
                    <li class='animated zoomInCustom' data-id='<?php echo $teamcount; ?>'>
                        <div class="content">
                            <div class="name"><?php echo $name; ?></div>
                            <div class="position"><?php echo $position; ?></div>
                        </div>
                        
                        
                        <div class="shadow"></div>
                        
                        <?php echo wp_get_attachment_image($imageID, 'col-md', "false", array( "class" => "articleImage"));?>
                        
                     
                    </li>
                
                    <?php
                            
                            $teamcount++;
                            
                        };
                    ?>
                        
                </ul>
                
            
            </div>
            
            
        </div> 
    </div>
</div>