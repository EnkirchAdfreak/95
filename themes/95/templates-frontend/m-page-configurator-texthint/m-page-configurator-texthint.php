<?php
    $content = get_sub_field('inhalte');
    $headline = $content["headline"];
    $image = $content["image"];
    $text = $content["text"];



    $htag = "h2";
?>
<div class="m-page-configurator-texthint   pdfcrowd-remove">
    <div class="container" >
        <div class="row">
            <div class="col-12 col-xl-6 offset-xl-3">
                
                <div class="headlineContainer">
                    <?php
                        echo "<".$htag." class='headline'>";
                        echo $headline;
                        echo "</".$htag.">";
                    ?>
                </div>
                
                <div class="imageContainer">
                    <?php echo wp_get_attachment_image($image, 'col-sm', "false", array( "class" => "articleImage"));?>
                </div>
                
                <div class="textboxContainer">
                    <?php echo $text; ?>
                </div>
                
            </div>
        </div>    
    </div>
</div>