<?php
    $content = get_sub_field('inhalte');

    $browser = get_browser_name($_SERVER['HTTP_USER_AGENT']);
    

    $headline = $content['headline'];
    $htag = "h2";

    $textbox = $content["textblock"];


    $link = $content["button"];
    if( $link ): 
        $link_url = $link['url'];
        $link_title = $link['title'];
        $link_target = $link['target'] ? $link['target'] : '_self';
    endif;


    $contentVerticalCenter = $content['center_content_vertical'];
    $contentVerticalCenterClass;




    if($browser != "Internet Explorer"){
        if($contentVerticalCenter == true){
           $contentVerticalCenterClass = "m-page-contactformsended-vcenter";
        }else{
            $contentVerticalCenterClass = "";
        }
    }else{
        $contentVerticalCenterClass = "";
        
    }

?>


<div class="m-page-contactformsended <?php echo $contentVerticalCenterClass; ?>">
    <div class="container container-custom-width" >
        <div class="row">
            <div class="col-12 col-md-10 offset-md-1 col-lg-8 offset-lg-2   col-xl-12 offset-xl-0">
                <img class="sendedicon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/modul/contact/Icon_Confirmation-Tick.png" alt="">
                <div class="textContainer">
                    <?php if( $headline != "" ){ ?>
                    <?php
                        echo "<".$htag." class='headline headlineSection headlineSectionLineCentered'>";
                        echo $headline;
                        echo "</".$htag.">";
                    ?>
                    <?php }; ?>
                    <div class="textbox"><?php echo $textbox; ?></div>
                    <?php if( $link ): ?>
                    <div class="buttonContainer">
                        <a class="button button95Light" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>