<?php
    $content = get_sub_field('inhalte');


    $headline = $content["headline"];
    

    //headline Tag
    /* $htag = $content["headlineTag"]; // h1 h2 div
    if($htag == "" || empty($htag)){
        $htag = "h2";
    }*/
    $htag = "h2";


    $productsArr = Array();
    $countIDArr = 0;

    if( have_rows('inhalte') ): while ( have_rows('inhalte') ) : the_row(); 
        //echo get_sub_field('horlur');
        if( have_rows('uploadliste_repeater') ): while ( have_rows('uploadliste_repeater') ) : the_row(); 

            $productsArr[$countIDArr]["file"] = get_sub_field( "uploadliste_repeater_file");
            $productsArr[$countIDArr]["fileURL"] = $productsArr[$countIDArr]["file"]["url"];
            $productsArr[$countIDArr]["name"] = get_sub_field( "uploadliste_repeater_name");





            $countIDArr++;

        endwhile; endif;
    endwhile; endif;



?>
<div class="m-page-uploadliste">
    <div class="container" >
        <div class="row">
            <div class="col-12 col-md-10 offset-md-1 col-lg-8 offset-lg-2 col-xl-12 offset-xl-0">
                <div class="textContainer">
                    <?php if( $headline != "" ){ ?>
                    
                    <?php
                        echo "<".$htag." class='headline headlineSection headlineSectionLineCentered'    $nomarginHeadline >";
                        echo $headline;
                        echo "</".$htag.">";
                    ?>
                    <?php }; ?>
                    
                    
                    
                    
                    <div class="textbox">
                        <ul class="listtext ">
                        <?php
                            foreach ($productsArr as $item) {
                                $text = $item["text"];
                                echo "<li class='contentBox'>";
                                echo "<a href='".$item["fileURL"]."'  target='_blank'>  ";
                                echo $item["name"];
                                echo "</a>";
                                echo "</li>";
                            }
                        ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>