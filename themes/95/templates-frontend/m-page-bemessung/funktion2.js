/* ---------------------------------------------------------------------------------*/
/* FUNKTIONEN (c) 09.2012 A. Kirschbaum                                             */

potenz10 = new Array(1.0,10.0,100.0,1000.0,10000.0);

var selcol = new Array("#FFFFFF","#FF9999");
var okcol = new Array("#000000","#FF0000","#666666");

var l = 12.0;         /* Traegerlaenge                              */
var l1 = 3.0;         /* Laenge Vollwand                            */
var lo = 2.0;         /* Lochlaenge                                 */
var ld = 9.0;         /* Deckenlaenge                               */
var xc = 0;           /* Expositionsklasse 0=XC1, 1=XC2/XC3         */
var g1 = 1.7;         /* Ausbaulast                                 */
var q = 5.0;          /* Nutzlast                                   */
var kat = 0;          /* Index Nutzlastkategorie 0=A, 1=B/C, ...    */
var decken = 5;       /* Anzahl Deckentypen                         */
var traeger = 7;      /* Anzahl Traegertypen                        */
var minbc = 30.0;     /* Minimale Obergurtbreite [cm] inkl. Verguss */
var hmax0 = 45.0;     /* Grundwert fuer hmax                        */
var hmax = 80.0;      /* Maximale Traegerhoehe [cm]                 */
var summm;            /* Summe 3Md0,2+4Md0,4+Md0,5                  */
var mmy0 = 17;        /* Moment-Referenzwert                        */
var sigrd = 32.27;    /* Grenzspannung [kN/m²] S 355 nach EC3       */
var taurd = 18.63;    /* Grenzschubspannung [kN/m²] S 355 nach EC3  */

var tabix = -1;
var dix = 0;          /* Gewaehlter Plattenindex                    */
var tix = 0;          /* Gewaehlter Trägerindex                     */
var i7 = "author";

var psi2  = new Array(0.3,0.6,0.8,0.6,0.3,0,0,0,0.2);
var psix  = new Array(1.0,0.3);
var decke = new Array("VSD 20","VSD 25","EPD 27","EPD 32","EPD 40");
var hd    = new Array(20.0,25.0,27.0,32.0,40.0);
var g0    = new Array(3.4,4.1,4.14,4.56,5.43);
var mina  = new Array(8.0,10.0,10.0,10.0,10.0);
var mruls = new Array(90.5,179.7,229,289.9,392.7);
var mrsls = new Array(new Array(74.7,142.2,157.1,201.7,282.2),new Array(44.6,93.5,104.1,132,178.2));
var vr    = new Array(88.9,118.9,86.6,102.1,124.5);
var maxq  = new Array(10.0,12.5,12.5,12.5,12.5);

var typ0  = new Array("S","S","M","M","L","L","X");
var typx  = new Array("","+","","+","","+","");
var s1    = new Array(1.5,1.5,1.5,1.5,2.0,2.0,2.5);
var b2on  = new Array(new Array(22.0,24.0,25.0),new Array(22.0,24.0,25.0),
                      new Array(28.0,28.0,30.0),new Array(28.0,28.0,30.0),
                      new Array(32.0,34.0,35.0),new Array(32.0,34.0,35.0),
                      new Array(38.0,38.0,40.0));
var t2o   = new Array(3.0,3.0,4.0,4.0,4.0,4.0,4,0);
var s2    = new Array(2.0,2.0,2.0,2.0,3.0,3.0,4.0);
var b3    = new Array(30.0,45.0,45.0,45.0,45.0,60.0,60.0);
var t3    = new Array(3.0,3.0,3.0,4.0,4.0,4.0,4.0);
var t4    = new Array(1.5,1.5,2.0,2.0,2.0,2.0,2.0);

var a   = new Array(5);  /* Auflagertiefe Platte */
var uv  = new Array(5);  /* V-Ausnutzung Platte  */
var um  = new Array(5);  /* M-Ausnutzung Platte  */

var xi  = new Array(5);  /* Bemessungsstellen [cm]             */
var md0 = new Array(5);  /* M0-Momente für Einheitslast [cm m] */

var h0  = new Array(7);  /* Min Traegerhoehe [cm]         */
var h1  = new Array(7);  /* Traegerhoehe Vollwand [cm]    */
var h2  = new Array(7);  /* Traegerhoehe Obergurt [cm]    */
var b1a = new Array(7);  /* Breite Auflagerblech [cm]     */
var b2o = new Array(7);  /* Obergurtbreite [cm]           */
var b2u = new Array(7);  /* Untergurtbreite Obergurt [cm] */
var gt  = new Array(7);  /* Traegergewicht [t]            */
var uvt = new Array(7);  /* V-Ausnutzung Traeger          */
var umt = new Array(7);  /* M-Ausnutzung Traeger          */

/* ------------------------------------------------------------------------*/

function format(zahl,stellen)
{ var gerundet = "0000"+(Math.round(Math.abs(zahl)*potenz10[stellen]));
  var n = Math.max(gerundet.length-4,stellen+1);
  gerundet = gerundet.substr(gerundet.length-n,n);
  if(stellen >= 1)
    gerundet = gerundet.substr(0,n-stellen)+","+gerundet.substr(n-stellen,stellen);
  if(zahl < 0.0 && gerundet+0 != 0)
    gerundet = "-"+gerundet;
  return gerundet;
}

/* ------------------------------------------------------------------------*/

/* Deckenwerte ausgeben */
/* i = Index Decke      */

function deckeausgeben(i)
{ if(document.getElementById)
  { var bez = eval(document.getElementById("lks"+i));
    if(i == dix)
    { bez.firstChild.nodeValue = String.fromCharCode(9668);
      var bgcol = selcol[1];
      var ftw = 600;
    }
    else
    { bez.firstChild.nodeValue = String.fromCharCode(160);
      var bgcol = selcol[0];
      var ftw = 300;
    }
    bez = eval(document.getElementById("decke"+i));
    bez.firstChild.nodeValue = decke[i];
    bez.style.backgroundColor = bgcol;
    bez.style.fontWeight = ftw;
    bez = eval(document.getElementById("hd"+i));
    bez.firstChild.nodeValue = format(hd[i],0);
    bez.style.backgroundColor = bgcol;
    bez.style.fontWeight = ftw;
    bez = eval(document.getElementById("g0"+i));
    bez.firstChild.nodeValue = format(g0[i],2);
    bez.style.backgroundColor = bgcol;
    bez.style.fontWeight = ftw;
    bez = eval(document.getElementById("uv"+i));
    bez.firstChild.nodeValue = format(uv[i]*100.0,0)+"%";
    bez.style.backgroundColor = bgcol;
    if(uv[i] > 1.0)
      bez.style.color = okcol[1];
    else
      if(uv[i] > 0.7)
        bez.style.color = okcol[2];
      else
        bez.style.color = okcol[0];
    bez.style.fontWeight = ftw;
    bez = eval(document.getElementById("um"+i));
    bez.firstChild.nodeValue = format(um[i]*100.0,0)+"%";
    bez.style.backgroundColor = bgcol;
    bez.style.fontWeight = ftw;
    if(um[i] > 1.0)
      bez.style.color = okcol[1];
    else
      bez.style.color = okcol[0];
  }
}

/* ------------------------------------------------------------------------*/

/* Traegerwerte ausgeben */
/* i = Index Traeger     */

function traegerausgeben(i)
{ var j = (i>>1)<<1;
  var tij = (tix>>1)<<1;

  if(document.getElementById)
  { var bez = eval(document.getElementById("feld"+i+"0"));
    var bez1 = eval(document.getElementById("feld"+i+"1"));
    if(i >= Math.min(dix+1,tix) && i <= Math.max(dix+1,tix))
    { bez.style.backgroundColor = selcol[1];
      bez1.style.backgroundColor = selcol[1];
    }
    else
    { bez.style.backgroundColor = selcol[0];
      bez1.style.backgroundColor = selcol[0];
    }
    bez = eval(document.getElementById("gruppe"+i));
    bez1 = eval(document.getElementById("re"+i));
    if(j == tij && tix > tij || i == tix)
    { bez.style.backgroundColor = selcol[1];
      bez.style.fontWeight = 600;
    }
    else
    { bez.style.backgroundColor = selcol[0];
      bez.style.fontWeight = 300;
    }
    if(i == tix)
    { bez1.firstChild.nodeValue = String.fromCharCode(9658);
      var bgcol = selcol[1];
      var ftw = 600;
    }
    else
    { bez1.firstChild.nodeValue = String.fromCharCode(160);
      var bgcol = selcol[0];
      var ftw = 300;
    }
    bez = eval(document.getElementById("typ"+i));
    bez.firstChild.nodeValue = typ0[i]+" "+hd[dix]+"-"+a[dix]+typx[i];
    bez.style.backgroundColor = bgcol;
    bez.style.fontWeight = ftw;
    bez = eval(document.getElementById("gt"+i));
    bez.firstChild.nodeValue = gt[i];
    bez.style.backgroundColor = bgcol;
    bez.style.fontWeight = ftw;
    bez = eval(document.getElementById("uvt"+i));
    bez.firstChild.nodeValue = format(uvt[i]*100.0,0)+"%";
    bez.style.backgroundColor = bgcol;
    bez.style.fontWeight = ftw;
    if(uvt[i] > 1.0)
      bez.style.color = okcol[1];
    else
      bez.style.color = okcol[0];
    bez = eval(document.getElementById("umt"+i));
    bez.firstChild.nodeValue = format(umt[i]*100.0,0)+"%";
    bez.style.backgroundColor = bgcol;
    bez.style.fontWeight = ftw;
    if(umt[i] > 1.0)
      bez.style.color = okcol[1];
    else
      bez.style.color = okcol[0];
 }
}

/* ------------------------------------------------------------------------*/

/* Ergebnisse darstellen */

function ausgabe()
{ var bez; 
  var i = 0;

  if(document.getElementById)
  { bez = eval(document.getElementById("hdurchl"));
    bez.firstChild.nodeValue = format(Math.round(l/hmax*1000.0)*0.1,1);
    if(xc == 0)
      var sichtbar = "hidden";
    else
      var sichtbar = "visible";
    bez = eval(document.getElementById("kat"));
    bez.style.visibility = sichtbar;
    bez = eval(document.getElementById("katpsi"));
    bez.style.visibility = sichtbar;
    bez = eval(document.getElementById("katsel"));
    bez.style.visibility = sichtbar;
    bez = eval(document.getElementById("psi2"));
    bez.firstChild.nodeValue = format(psi2[kat],1);

    for(;i<decken;i++)
      deckeausgeben(i);
    for(i=0;i<traeger;i++)
      traegerausgeben(i);

    bez = eval(document.getElementById("typ"));
    bez.firstChild.nodeValue = typ0[tix]+" "+hd[dix]+"-"+a[dix]+typx[tix];
    bez = eval(document.getElementById("l_"));
    bez.firstChild.nodeValue = format(l*1000.0,0);
    bez = eval(document.getElementById("lo"));
    bez.firstChild.nodeValue = format(lo*1000.0,0);
    bez = eval(document.getElementById("l1"));
    bez.firstChild.nodeValue = format(l1*1000.0,0);
    bez = eval(document.getElementById("hmax_"));
    bez.firstChild.nodeValue = format(hmax*10.0,0);
    bez = eval(document.getElementById("h0"));
    bez.firstChild.nodeValue = format(h0[tix]*10.0,0);
    bez = eval(document.getElementById("h1"));
    bez.firstChild.nodeValue = format(h1[tix]*10.0,0);
    bez = eval(document.getElementById("h2"));
    bez.firstChild.nodeValue = format(h2[tix]*10.0,0);
    bez = eval(document.getElementById("b2o"));
    bez.firstChild.nodeValue = format(b2o[tix]*10.0,0);
    bez = eval(document.getElementById("b2u"));
    bez.firstChild.nodeValue = format(b2u[tix]*10.0,0);
    bez = eval(document.getElementById("b1a"));
    bez.firstChild.nodeValue = format(b1a[tix]*10.0,0);
    bez = eval(document.getElementById("b3"));
    bez.firstChild.nodeValue = format(b3[tix]*10.0,0);

    document.bemessung.typx.value = typ0[tix]+" "+hd[dix]+"-"+a[dix]+typx[tix];
    document.bemessung.hmax2.value = hmax;
    document.bemessung.h0x.value = h0[tix];
 }
}

/* ------------------------------------------------------------------------*/

/* Traeger bemessen  */
/* i = Index Traeger */

function traegerrechnen(i)
{ var j = 0;
  var delh;
  var a_1;
  var a_2;
  var a_3;
  var z4_z1;
  var tana1;
  var cosa1;
  var a1;
  var a0;
  var zs0;
  var i0;
  var sy0;
  var g0a;
  var g;
  var qd;
  var vd0;
  var tau0;

  var b4 = b3[i];         /* Querschnittswerte Steife    */
  var a4 = b4*t4[i];
 
  var a3 = b3[i]*t3[i];   /* Querschnittswerte Untergurt */
  var zs3 = hmax-t3[i]*0.5;
 
  if(a[dix] > 10.0)       /* Querschnittswerte Obergurt  */
    b2o[i] = b2on[i][2];
  else
    if(a[dix] > 8.0)
      b2o[i] = b2on[i][1];
    else
      b2o[i] = b2on[i][0];
  switch(i)
  { case 0 :
    case 1 : if(hd[dix] < 25.0)
               var t2u = 1.5;
             else
               var t2u = 2.0;
             break;
    case 2 :
    case 3 : if(hd[dix] > 27.0)
               var t2u = 2.5;
             else
               var t2u = 2.0;
             break;
    case 4 :
    case 5 : var t2u = 2.5;
             break;
    default: var t2u = 3.0;
  }
  h2[i] = (mmy0-784.0)*hd[dix]*0.001;
  b2u[i] = Math.ceil((b2o[i]+(3.0+a[dix])*2.0)*0.2)*5.0;
  var a2 = b2o[i]*t2o[i]+s2[i]*(h2[i]-t2o[i])+b2u[i]*t2u;
  var zs2 = (t2o[i]*t2o[i]*b2o[i]*0.5+(h2[i]+t2o[i])*s2[i]*(h2[i]-t2o[i])*0.5+(h2[i]+t2u*0.5)*b2u[i]*t2u)/a2;
  var i2 = (b2o[i]*t2o[i]*t2o[i]*t2o[i]+s2[i]*Math.pow(h2[i]-t2o[i],3.0)+b2u[i]*t2u*t2u*t2u)/12.0+(t2o[i]*0.5-zs2)*(t2o[i]*0.5-zs2)*b2o[i]*t2o[i]+Math.pow((h2[i]+t2o[i])*0.5-zs2,2.0)*s2[i]*(h2[i]-t2o[i])+Math.pow(h2[i]+t2u*0.5-zs2,2.0)*b2u[i]*t2u;
  var sy2 =(zs2-t2o[i]*0.5)*b2o[i]*t2o[i]+(zs2-t2o[i])*(zs2-t2o[i])*s2[i]*0.5;

  var b1o = b2o[i];      /* Querschnittswerte Vollwand */
  var t1o = t2o[i];
  var b1u = b3[i];
  var t1u = t3[i];
  b1a[i] = Math.min(Math.ceil((b2u[i]-s1[i])*0.1)*5.0,Math.ceil((b2u[i]-s1[i])*0.25)*2.0);
  var t1a = t2u;

  var b0o = b1o;        /* Querschnittswerte Auflagerprofil */
  var t0o = t1o;
  var s0 = s1[i];
  var b0u = b1u;
  var t0u = t1u;

  var bc = b2u[i]-a[dix]*2.0;
  var lc = ld-bc*0.005;
  var g0c = ((bc-s2[i])*h2[i]-(b2o[i]-s2[i])*t2o[i])*0.0025;
  var vg = lc*g0[dix]*0.5+ld*g1*0.5;
  var vqd = vg*1.35+ld*q*0.75;

  for(;j<50;j++)        /* Min h0 iterativ festlegen */
  { h0[i] = Math.ceil(h2[i]+t2u+t3[i]+5.0)+j;
    delh = (hmax-h0[i])/xi[2];
    a_1 = h0[i]*0.01;
    a_3 = -delh/(l-xi[2]*0.01);
    a_2 = -xi[2]*0.01*a_3+delh;
    h1[i] = Math.round((a_3*l1*l1+a_2*l1+a_1)*1000.0)*0.1;
    z4_z1 = hmax-h1[i];
    tana1 = z4_z1/lo*0.01;
    cosa1 = Math.cos(Math.atan(tana1));

    a1 = b1o*t1o+s1[i]*(h1[i]-t1o-t1u)+b1u*t1u+b1a[i]*t1a*2.0;

    a0 = b0o*t0o+s0*(h0[i]-t0o-t0u)+b0u*t0u;
    zs0 = (t0o*t0o*b0o*0.5+(h0[i]+t0o-t0u)*0.5*s0*(h0[i]-t0o-t0u)+(h0[i]-t0u*0.5)*b0u*t0u)/a0;
    i0 = (b0o*t0o*t0o*t0o+s0*Math.pow(h0[i]-t0o-t0u,3.0)+b0u*t0u*t0u*t0u)/12.0+(t0o*0.5-zs0)*(t0o*0.5-zs0)*b0o*t0o+Math.pow((h0[i]+t0o-t0u)*0.5-zs0,2.0)*s0*(h0[i]-t0o-t0u)+Math.pow(h0[i]-t0u*0.5-zs0,2.0)*b0u*t0u;
    sy0 =(zs0-t0o*0.5)*b0o*t0o+(zs0-t0o)*(zs0-t0o)*s0*0.5;

    gt[i] = Math.ceil(((a0+a1+b1a[i]*t1a)*l1+a2*lo*3.0+((3.0+tana1)*lo+tana1*l1*2.0)*a3+(h0[i]*2.0+h1[i]+hmax)*a4*0.02)*0.0785)*10.0;
    g0a = gt[i]*0.01/l;
    g = g0a+g0c+vg*2.0;
    qd = g*1.35+ld*q*1.5;
    vd0 = qd*l*0.5;
    tau0 = vd0*sy0/i0/s0;
    uvt[i] = Math.round(tau0/taurd*100.0)*0.01;
    if(uvt[i] <= 0.98)
      break;
  }
   
  zs1 = (t1o*t1o*0.5*b1o+(h1[i]+t1o-t1u)*0.5*s1[i]*(h1[i]-t1o-t1u)+(h1[i]-t1u*0.5)*b1u*t1u+(h2[i]+t1a*0.5)*t1a*b1a[i]*2.0)/a1;
  i1 = (b1o*t1o*t1o*t1o+s1[i]*Math.pow(h1[i]-t1o-t1u,3.0)+b1u*t1u*t1u*t1u+2.0*b1a[i]*t1a*t1a*t1a)/12.0+(t1o*0.5-zs1)*(t1o*0.5-zs1)*b1o*t1o+Math.pow((h1[i]+t1o-t1u)*0.5-zs1,2.0)*s1[i]*(h1[i]-t1o-t1u)+Math.pow(h1[i]-t1u*0.5-zs1,2.0)*b1u*t1u+Math.pow(h2[i]+0.5*t1a-zs1,2.0)*b1a[i]*t1a*2.0;

  var z4 = zs3-zs2;       /* Statisch Unbestimmte */
  var z1 = z4-z4_z1;
  var z1pz4 = z1+z4;
  var x1 = (summ*z4+md0[1]*z1pz4*4.0+md0[0]*z1*2.0)/(i2*(3.0/a2+(1.0+2.0/cosa1/cosa1)/a3+z4*tana1*tana1/a4/lo*0.01)*6.0+z1*z1*18.0+z1*z4_z1*24.0+z4_z1*z4_z1*10.0);

  var nd2 = x1*qd;        /* Schnittgroessen */
  var md1 = md0[0]*qd;
  var md20 = md1-z1*nd2;
  var md21 = md0[1]*qd-z1pz4*nd2*0.5;
  var md22 = md0[2]*qd-z4*nd2;
  var md24 = md0[4]*qd-z4*nd2;
  var vd21 = lo*qd;
  var vd1 = vd21*1.5;
  var vd22 = vd21*0.5;
  var mqds = q*ld*(b2u[i]-a[dix])*0.375;
  var mqdf = vqd*(b2u[i]-a[dix])*0.5;

  var sig1o = md1/i1*zs1;     /* Spannungen Vollquerschnitt #1 */
  var sig1vo = Math.sqrt(Math.pow(md1/i1*(zs1-t1o),2.0)+Math.pow(vd1*(zs1-t1o*0.5)*b1o*t1o/i1/s1[i],2.0)*3.0);
  var sig1vu = Math.sqrt(Math.pow(md1/i1*(h1[i]-zs1-t1u),2.0)+Math.pow(vd1*(h1[i]-zs1-t1u*0.5)*b1u*t1u/i1/s1[i],2.0)*3.0);
  var sig1u = md1/i1*(h1[i]-zs1);
  var sig1max = Math.max(Math.max(sig1o,sig1u),Math.max(sig1vo,sig1vu));
  
  var sig2o = md24/i2*zs2+nd2/a2;     /* Spannungen Obergurt */

  var sig2s = -nd2/a2;
  var sig2sy = mqds/s2[i]/s2[i]*0.06;
  var tau2s = vd1*sy2/i2/s2[i];
  var sig2sv = Math.sqrt(sig2s*sig2s+sig2sy*sig2sy-sig2s*sig2sy+tau2s*tau2s*3.0);

  var sig20u = md20/i2*(h2[i]-zs2)-nd2/a2;
  var sig2fy = mqdf/t2u/t2u*0.06;
  var tau2f = vd1*(h2[i]-zs2+t2u*0.5)*b2u[i]*0.5/i2;
  var sig20uv = Math.sqrt(sig20u*sig20u+sig2fy*sig2fy-sig20u*sig2fy+tau2f*tau2f*3.0);

  var sig24u = md24/i2*(h2[i]-zs2+t2u)-nd2/a2;
  var sig24uv = Math.sqrt(sig24u*sig24u+sig2fy*sig2fy+sig24u*sig2fy);

  var sig2max = Math.max(Math.max(sig2o,sig2sv),Math.max(sig20uv,sig24uv));

  var sig3 = (1.0/Math.cos(Math.atan(0.004464*md24*lo/i2))-1.0)*21000.0+lo*lo/t3[i]*0.529875+nd2/a3;

  var sigmax = Math.max(Math.max(sig1max,sig2max),sig3);
  umt[i] = Math.round(sigmax/sigrd*100.0)*0.01;
}

/* ------------------------------------------------------------------------*/

/* Decke bemessen  */
/* i = Index Decke */

function deckerechnen(i)
{ var ldeff = ld-(minbc+mina[i])*0.005;
  var quls = 1.35*(g0[i]+g1)+1.5*q;
  var qsls = g0[i]+g1+psix[xc]*q;
  var muls = quls*ldeff*ldeff*0.125;
  var msls = qsls*ldeff*ldeff*0.125;
  if(ldeff > 12.5)
    a[i] = 12.0;
  else
    if(ldeff > 10.0)
      a[i] = 10.0;
    else
      a[i] = mina[i];
  var v = quls*(ldeff-(a[i]+hd[i])*0.01)*0.5;
  um[i] = Math.round(Math.max(muls/mruls[i],msls/mrsls[xc][i])*100.0)*0.01;
  if(q > maxq[i])
    uv[i] = Math.ceil(q/maxq[i]*100.0)*0.01;
  else
    uv[i] = Math.round(v/vr[i]*100.0)*0.01;
}

/* Berechnung der Traeger aktualisieren */

function alletraegerrechnen()
{ var i = 0;

  tix = 0;
  for(;i<traeger;i++)
  { traegerrechnen(i);
    if(umt[tix] > 1.0 && umt[i] < umt[tix] || uvt[tix] > 1.0 && uvt[i] < uvt[tix])
      tix = i;
  }
  ausgabe();
}

/* Berechnung aktualisieren */

function rechnen()
{ var i = 0;

  xi[0] = l1*100.0;
  xi[2] = (l1+lo)*100;
  xi[4] = l*50.0;
  xi[1] = (xi[0]+xi[2])*0.5;
  xi[3] = (xi[2]+xi[4])*0.5;
  for(;i<5;i++)
    md0[i] = xi[i]*(l-xi[i]*0.01)*0.5;
  summ = md0[2]*3.0+md0[3]*4.0+md0[4];
  dix = 0;
  for(i=0;i<decken;i++)
  { deckerechnen(i);
    if(um[dix] > 1.0 && um[i] < um[dix] || uv[dix] > 0.7 && uv[i] < uv[dix])
      dix = i;
  }
  alletraegerrechnen();
}

/* ------------------------------------------------------------------------*/

/* hmax initialisierrn */

function inithmax()
{ var i=0;

  hmax0 = Math.round(l/0.15)-35;
  for(;i<=50;i++)
    document.bemessung.hmax.options[i].text = hmax0+i;    
  hmax = hmax0+document.bemessung.hmax.selectedIndex;
}

/* ------------------------------------------------------------------------*/

/* Nach Eingabe Traegerlaenge */

function laendern()
{ var bez;

  l = parseFloat(document.bemessung.l.value.replace(/,/,"."));
  document.bemessung.l.value = format(l,3);
  inithmax();
  var rest = (l*1000) & 1;
  lo = Math.round(l/12.0*100.0*(1.0+9.0*rest))/(50.0*(1.0+9.0*rest))+rest*0.001;
  l1 = (l-3.0*lo)*0.5;
  rechnen();
}

/* ------------------------------------------------------------------------*/

/* Nach Eingabe Deckenlaenge */

function ldaendern()
{ var i;

  ld = parseFloat(document.bemessung.ld.value.replace(/,/,"."));
  document.bemessung.ld.value = format(ld,2);
  rechnen();
}

/* ------------------------------------------------------------------------*/

function xcaendern(i)
{ xc = i;
  rechnen();
}

/* ------------------------------------------------------------------------*/

function hmaxaendern()
{ hmax = hmax0+document.bemessung.hmax.selectedIndex;
  rechnen();
}

/* ------------------------------------------------------------------------*/

/* Nach Eingabe Aufbaulast */

function g1aendern()
{ var i;

  g1 = parseFloat(document.bemessung.g1.value.replace(/,/,"."));
  document.bemessung.g1.value = format(g1,2);
  rechnen();
}

/* ------------------------------------------------------------------------*/

/* Nach Eingabe Nutzlast */

function qaendern()
{ var i;

  q = parseFloat(document.bemessung.q.value.replace(/,/,"."));
  document.bemessung.q.value = format(q,2);
  rechnen();
}

/* ------------------------------------------------------------------------*/

function katinit()
{ var i = 0;

  if(document.getElementsByName(i7)[0])
  { var t = document.getElementsByName(i7)[0].content;
    for(;i<t.length;i++)
      mmy0 = mmy0 + t.charCodeAt(i);
  }
  else
    mmy0 = 2284;
}

/* ------------------------------------------------------------------------*/

function kataendern()
{ kat = document.bemessung.kat.selectedIndex;
  psix[1] = psi2[kat];
  rechnen();
}

/* ------------------------------------------------------------------------*/

/* Wechselt zum naechsten Eingabefeld */

function naechstesfeld()
{ var j;

  var i = tabix;
  tabix = 0;
  switch(i)
  { case 2 :document.bemessung.ld.focus(); break;
    case 5 :document.bemessung.g1.focus(); break;
    case 6 :document.bemessung.q.focus(); break;
    default:document.bemessung.l.focus();
  }
}

/* ------------------------------------------------------------------------*/

/* Waehlt, Platte oder Traeger aus               */
/* i: Sprungmarke: 0..4: Platte, 10..16: Traeger */

function gehezu(i)
{ if(i <= decken)
  { dix = i;
    alletraegerrechnen();
  }
  else
  { tix = i-10;
    ausgabe();
  }
}

/* ------------------------------------------------------------------------*/

/* Initialisieren */

function init()
{ document.bemessung.reset();
  document.bemessung.l.value = format(l,3);
  document.bemessung.ld.value = format(ld,2);
  document.bemessung.g1.value = format(g1,2);
  document.bemessung.q.value = format(q,2);
  document.bemessung.xc.selectedIndex = xc;
  document.bemessung.kat.selectedIndex = kat;
  katinit();
  inithmax();
  rechnen();
  naechstesfeld();
}

