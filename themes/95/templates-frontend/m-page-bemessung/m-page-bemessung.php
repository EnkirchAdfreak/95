<!--
<iframe style="width: 100%;
    height: 690px;
    min-height: 100%;" src="<?php echo get_stylesheet_directory_uri(); ?>/templates-frontend/m-page-bemessung/draheimtraegerbemessung.htm"></iframe> 


<meta http-equiv="content-type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="format.css">
<script language="JavaScript" src="funktion2.js" type="text/javascript"></script>
-->



<?php

$content = get_sub_field('inhalte');

$headline = $content["headline"];
$infotext = $content["infotext"];
$hintextmobile = $content["hinweistext_mobile"];
$hintextmobilebutton = $content["hinweistext_mobile_button"];

$htag = "h2";


$languageB = ICL_LANGUAGE_CODE;

?>




<div class="m-page-text-centerered-bemessung">
    <div class="container container-custom-width" >
        <div class="row">
            <div class="col-12 col-md-10 offset-md-1 col-lg-8 offset-lg-2 col-xl-12 offset-xl-0">
                <div class="textContainer">
                    
                    <?php if( $headline != "" ){ ?>
                    
                    <?php
                        echo "<".$htag." class='headline headlineSection headlineSectionLineCentered'>";
                        echo $headline;
                        echo "</".$htag.">";
                    ?>
                    <?php }; ?>


                    <div class="textbox"><?php echo $infotext; ?></div>
                    
                    
 
          
                    
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-12 d-md-none">
                <div class="textContainer">
                    
                    <div class="textboxMobile"><?php echo $hintextmobile; ?></div>

                    <div class="mobilebutton">
                        <?php
                            $hintextmobilebutton = $content["hinweistext_mobile_button"];
                            if( $hintextmobilebutton ): 
                                $link_url = $hintextmobilebutton['url'];
                                $link_title = $hintextmobilebutton['title'];
                                $link_target = $hintextmobilebutton['target'] ? $hintextmobilebutton['target'] : '_self';
                                ?>
                                <a class="button button95Light" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
                        
                        <?php
                            endif;
                        ?>
                        
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>

<div class="m-page-bemessung-form">
    <div class="container container-custom-width" >
        <div class="row">
            <div class="col-12">



<form name="bemessung" action="javascript:naechstesfeld()">
<button style="position:absolute; top:100px; left:100px; clip:rect(0px 0px 0px 0px)" type="submit" name="anfrage" value="Anfrage senden" id="sendbemessung"></button>
<table class="haupt" border="0" padding="0" cellspacing="0">

<tr>
    <td colspan="5" class="bemessung_bold_headline">
    
        
    <?php
        
        if($languageB == "de"){ echo "<p><b>System, Belastung und Umgebung</b>";};       
                                                        
        if($languageB == "en"){ echo "<p><b>System, load and environment</b>";}; 
        
        if($languageB == "es"){ echo "<p><b>Sistema, carga y entorno</b>";}; 
                                                        
      ?>                                        
        
    </td>
</tr>

<tr>
  <td colspan="5" class="bemessung_bold_headline">
    
    <?php
    
        if($languageB == "de"){ echo "<p><b>Auswahl und Vorbemessung von Decke und Tr&auml;ger</b>";};       
                                                        
        if($languageB == "en"){ echo "<p><b>Selection and pre-dimensioning of slab and beam</b>";}; 
        
        if($languageB == "es"){ echo "<p><b>Selecci&#243;n y predimensionamiento de la losa y la viga</b>";}; 
                                                        
      ?>  
    
</td>
    
     
    
</tr>

   <tr height="30">
  
</tr>
    
  

<tr>


  <td colspan="2"><table cellpadding="0" cellspacing="0">
    
    <tr>
        <td>
          
    <?php
    
        if($languageB == "de"){ echo "<p>Tr&auml;gerl&auml;nge";};       
                                                        
        if($languageB == "en"){ echo "<p>Beam length";}; 
        
        if($languageB == "es"){ echo "<p>La longitud del rayo";}; 
                                                        
      ?>  
        
        </td>
      <td align="right"><p>L = </td>
      <td><input class="maske" name="l" size="10" maxlength="10" value="12,000" onChange="laendern()" width="150px" onFocus="tabix=2" tabindex="1"></td>
      <td><p>m</td>
        <td rowspan="2" colspan="2" style="text-align:right;vertical-align:top">
        <p>
            
    <?php
    
        if($languageB == "de"){ echo "Decke F90 XC1";};       
                                                        
        if($languageB == "en"){ echo "Ceiling R90 XC1";}; 
        
        if($languageB == "es"){ echo "Techo R90 XC1";}; 
                                                        
      ?>       
        
        <input type="radio" name="xc" value="0" checked="checked" onClick="xcaendern(0)" onFocus="tabix=4" tabindex="3">
            
        <br>
        XC2/XC3 <input type="radio" name="xc" value="1" onClick="xcaendern(1)" onFocus="tabix=5">
            
            
      </td>

    </tr>
    <tr>
      <td>
          
        
    <?php
    
        if($languageB == "de"){ echo "<p>Deckenspannweite";};       
                                                        
        if($languageB == "en"){ echo "<p>Ceiling span width";}; 
        
        if($languageB == "es"){ echo "<p>Ancho de la luz del techo";}; 
                                                        
      ?>   
    </td>
      <td align="right"><p>L<sub>D</sub> = </td>
      <td><input class="maske" name="ld" size="10" maxlength="10" value="9,00" onChange="ldaendern()" width="150px" onFocus="tabix=5" tabindex="2"></td>
      <td><p>m</td>
    </tr>
    <tr>
      <td>
         
    <?php
    
        if($languageB == "de"){ echo "<p>Tr&auml;gerh&ouml;he";};       
                                                        
        if($languageB == "en"){ echo "<p>Beam height";}; 
        
        if($languageB == "es"){ echo "<p>Altura del rayo";}; 
                                                        
      ?>
        
    </td>
      <td align="right"><p>h<sub>max</sub> = </td>
      <td><p><select class="maske" name="hmax" size="1" onChange="hmaxaendern()" tabindex="4">
        <option value="0">45</option>
        <option value="1">46</option>
        <option value="2">47</option>
        <option value="3">48</option>
        <option value="4">49</option>
        <option value="5">50</option>
        <option value="6">51</option>
        <option value="7">52</option>
        <option value="8">53</option>
        <option value="9">54</option>
        <option value="10">55</option>
        <option value="11">56</option>
        <option value="12">57</option>
        <option value="13">58</option>
        <option value="14">59</option>
        <option value="15">60</option>
        <option value="16">61</option>
        <option value="17">62</option>
        <option value="18">63</option>
        <option value="19">64</option>
        <option value="20">65</option>
        <option value="21">66</option>
        <option value="22">67</option>
        <option value="23">68</option>
        <option value="24">69</option>
        <option value="25">70</option>
        <option value="26">71</option>
        <option value="27">72</option>
        <option value="28">73</option>
        <option value="29">74</option>
        <option value="30">75</option>
        <option value="31">76</option>
        <option value="32">77</option>
        <option value="33">78</option>
        <option value="34">79</option>
        <option value="35" selected="selected">80</option>
        <option value="36">81</option>
        <option value="37">82</option>
        <option value="38">83</option>
        <option value="39">84</option>
        <option value="40">85</option>
        <option value="41">86</option>
        <option value="42">87</option>
        <option value="43">88</option>
        <option value="44">89</option>
        <option value="45">90</option>
        <option value="46">91</option>
        <option value="47">92</option>
        <option value="48">93</option>
        <option value="49">94</option>
        <option value="50">95</option>
      </select>cm</td>
      <td colspan="3"><p>= L/<var id="hdurchl">15,0</var></td>
   </tr>
   <tr>
      <td>
          
       
    <?php
    
        if($languageB == "de"){ echo "<p>Aufbaulast";};       
                                                        
        if($languageB == "en"){ echo "<p>Body load";}; 
        
        if($languageB == "es"){ echo "<p>Carga corporal";}; 
                                                        
      ?>
       
       </td>
      <td align="right"><p>g<sub>1</sub> = </td>
      <td><input class="maske" name="g1" size="10" maxlength="10" value="1,70" onChange="g1aendern()" width="150px" onFocus="tabix=6" tabindex="5"></td>
      <td><p>kN/m&#178;</td>
    <td id="kat" style="visibility:hidden; padding-left: 30px;">
        
       
    
    <?php
    
        if($languageB == "de"){ echo "<p>Kategorie";};       
                                                        
        if($languageB == "en"){ echo "<p>Category";}; 
        
        if($languageB == "es"){ echo "<p>Categor&#237;a";}; 
                                                        
      ?>
       
    </td>
      <td id="katpsi" align="right" style="visibility:hidden"><p>psi<sub>2</sub> = <var id="psi2">0,3</var></td>
    </tr>
    <tr>
      <td>
          
        
       
    <?php
    
        if($languageB == "de"){ echo "<p>Nutzlast";};       
                                                        
        if($languageB == "en"){ echo "<p>Payload";}; 
        
        if($languageB == "es"){ echo "<p>Carga &#250;til";}; 
                                                        
      ?>
              

        </td>
      <td align="right"><p>q = </td>
      <td><input class="maske" name="q" size="10" maxlength="10" value="5,00" onChange="qaendern()" width="150px" onFocus="tabix=8" tabindex="6"></td>
      <td><p>kN/m&#178;</td>
   <td colspan="2" id="katsel" style="visibility:hidden; padding-left: 22px;"><select class="maske" name="kat" size="1" onChange="kataendern()" tabindex="7">

       
       
       
    <?php
        if($languageB == "de"){
    ?>        
        <option value="0" selected="selected">A/B Wohnen, B&uuml;ro</option>
        <option value="1">C/D Versammlung, Verkauf</option>
        <option value="2">E Lager</option>
        <option value="3">F Fahrzeuge bis 30 kN</option>
        <option value="4">G Fahrzeuge bis 160 kN</option>
        <option value="5">H </option>
        <option value="6">Wind</option>
        <option value="7">Schnee bis +1000 m&uuml;NN</option>
        <option value="8">Schnee &#252;ber +1000 m&uuml;NN</option>     
    <?php
        };   
    ?> 
       
       
       
    <?php
        if($languageB == "en"){
    ?>        
        <option value="0" selected="selected">A/B Residential, Office</option>
        <option value="1">C/D Assembly, Sale</option>
        <option value="2">E Bearing</option>
        <option value="3">F Vehicle up to 30 kN</option>
        <option value="4">G Vehicle up to 160 kN</option>
        <option value="5">H </option>
        <option value="6">Wind</option>
        <option value="7">Snow up to +1000m&#252;NN</option>
        <option value="8">Snow above +1000m&#252;NN</option>     
    <?php
        };   
    ?> 
       
       
    <?php
        if($languageB == "es"){
    ?>        
        <option value="0" selected="selected">A/B Residencia, Oficina</option>
        <option value="1">C/D Montaje, Venta</option>
        <option value="2">E Rodamiento </option>
        <option value="3">F Veh&#237;culo de hasta 30 kN</option>
        <option value="4">G Veh&#237;culo de hasta 160 kN</option>
        <option value="5">H </option>
        <option value="6">Viento</option>
        <option value="7">Nieve hasta +1000m&#252;NN</option>
        <option value="8">Nieve por encima de +1000m&#252;NN</option>     
    <?php
        };   
    ?> 
                                                        
                                       
        
       
       
       
       
       
       
       
       
      </select></td>
    </tr>
  </table></td>

  <td class="bild" rowspan="2"><table cellpadding="0" cellspacing="0">
    <tr><td><img src="<?php echo get_stylesheet_directory_uri(); ?>/templates-frontend/m-page-bemessung/schnitt11.jpg" width="135px" style="margin-top:10px"></td></tr>
  </table></td>

</tr>

<tr>
  <td colspan="2" class="bemessung_bold_headline_2">
      
    
    <?php
    
        if($languageB == "de"){ echo "<p><b>Auswahl und Vorbemessung von Decke und Tr&auml;ger</b>";};       
                                                        
        if($languageB == "en"){ echo "<p><b>Selection and pre-dimensioning of slab and beam</b>";}; 
        
        if($languageB == "es"){ echo "<p><b>Selecci&#243;n y predimensionamiento de la losa y la viga</b>";}; 
                                                        
      ?>
    
    </td>
</tr>

<tr><td colspan="2"><table cellpadding="0" cellspacing="0">
  <tr>
    <td class="our" colspan="5">
        
      

    <?php
    
        if($languageB == "de"){ echo "<p>VMM Decke";};       
                                                        
        if($languageB == "en"){ echo "<p>VMM Ceiling";}; 
        
        if($languageB == "es"){ echo "<p>Techo del VMM";}; 
                                                        
      ?>
      
      </td>
    <td class="our" colspan="2" rowspan="3">&nbsp;</td>
    <td class="ou" colspan="5"><p>DRAHEIM TR&Auml;GER</td>
  </tr>
  <tr>
    <td class="ur" rowspan="2">
        
      
    <?php
    
        if($languageB == "de"){ echo "<p>Typ";};       
                                                        
        if($languageB == "en"){ echo "<p>Type";}; 
        
        if($languageB == "es"){ echo "<p>Escriba";}; 
                                                        
      ?>
      
      
      </td>
    <td class="urm"><p>h<sub>D</sub></td>
    <td class="urm"><p>g<sub>0</sub></td>
    <td class="urm" colspan="2">
        
      
    <?php
    
        if($languageB == "de"){ echo "<p>Ausnutzung";};       
                                                        
        if($languageB == "en"){ echo "<p>Exploitation";}; 
        
        if($languageB == "es"){ echo "<p>Explotaci&#243;n";}; 
                                                        
      ?>
      
      </td>
    <td class="ur" colspan="2" rowspan="2"><p>Typ</td>
    <td class="urm">
        
      
    <?php
    
        if($languageB == "de"){ echo "<p>G";};       
                                                        
        if($languageB == "en"){ echo "<p>W";}; 
        
        if($languageB == "es"){ echo "<p>P";}; 
                                                        
      ?>
      
      </td>
    <td class="um" colspan="2">

    <?php
    
        if($languageB == "de"){ echo "<p>Ausnutzung";};       
                                                        
        if($languageB == "en"){ echo "<p>Exploitation";}; 
        
        if($languageB == "es"){ echo "<p>Explotaci&#243;n";}; 
                                                        
      ?>
      
    </td>
  </tr>
  <tr>
    <td class="urm"><p>cm</td>
    <td class="urm"><p>kN/m&#178;</td>
    <td class="urm"><p>V</td>
    <td class="urm"><p>M</td>
    <td class="urm"><p>kg</td>
    <td class="urm"><p>V</td>
    <td class="um"><p>M</td>
  </tr>
  <tr>
    <td class="r">&nbsp;</td>
    <td class="r">&nbsp;</td>
    <td class="r">&nbsp;</td>
    <td class="r">&nbsp;</td>
    <td class="r">&nbsp;</td>
    <td id="feld00">&nbsp;</td>
      <td class="r" id="feld01"><p><var id="re0">&nbsp;</var></p></td>
    <td class="r" onClick="gehezu(10)" style="cursor:pointer">
        
      
    <?php
    
        if($languageB == "de"){ echo "<p id='gruppe0'>Leicht";};       
                                                        
        if($languageB == "en"){ echo "<p id='gruppe0'>Light";}; 
        
        if($languageB == "es"){ echo "<p id='gruppe0'>Luz";}; 
                                                        
      ?>
      
      </td>
    <td class="r" onClick="gehezu(10)" style="cursor:pointer"><p id="typ0">S 25-10</td>
    <td class="ar" onClick="gehezu(10)" style="cursor:pointer"><p id="gt0">1000</td>
    <td class="ar" onClick="gehezu(10)" style="cursor:pointer"><p id="uvt0">100%</td>
    <td class="a" onClick="gehezu(10)" style="cursor:pointer"><p id="umt0">100%</td>
  </tr>
  <tr>
    <td class="ur" onClick="gehezu(0)" style="cursor:pointer"><p id="decke0">VSD 20</td>
    <td class="aur" onClick="gehezu(0)" style="cursor:pointer"><p id="hd0">20</td>
    <td class="aur" onClick="gehezu(0)" style="cursor:pointer"><p id="g00">3,40</td>
    <td class="aur" onClick="gehezu(0)" style="cursor:pointer"><p id="uv0">100%</td>
    <td class="aur" onClick="gehezu(0)" style="cursor:pointer"><p id="um0">100%</td>
      <td id="feld10"><p><var id="lks0">&nbsp;</var></p></td>
    <td class="r" id="feld11"><p><var id="re1">&nbsp;</var></p></td>
    <td class="ur" onClick="gehezu(11)" style="cursor:pointer"><p id="gruppe1">&nbsp;</td>
    <td class="ur" onClick="gehezu(11)" style="cursor:pointer"><p id="typ1">S 25-10+</td>
    <td class="aur" onClick="gehezu(11)" style="cursor:pointer"><p id="gt1">1000</td>
    <td class="aur" onClick="gehezu(11)" style="cursor:pointer"><p id="uvt1">100%</td>
    <td class="au" onClick="gehezu(11)" style="cursor:pointer"><p id="umt1">100%</td>
  </tr>
  <tr>
    <td class="ur" onClick="gehezu(1)" style="cursor:pointer"><p id="decke1">VSD 25</td>
    <td class="aur" onClick="gehezu(1)" style="cursor:pointer"><p id="hd1">25</td>
    <td class="aur" onClick="gehezu(1)" style="cursor:pointer"><p id="g01">4,10</td>
    <td class="aur" onClick="gehezu(1)" style="cursor:pointer"><p id="uv1">100%</td>
    <td class="aur" onClick="gehezu(1)" style="cursor:pointer"><p id="um1">100%</td>
    <td id="feld20"><p><var id="lks1">&nbsp;</var></p></td>
    <td class="r" id="feld21"><p><var id="re2">&nbsp;</var></p></td>
    <td class="r" onClick="gehezu(12)" style="cursor:pointer">

    <?php
    
        if($languageB == "de"){ echo "<p id='gruppe2'>Mittel";};       
                                                        
        if($languageB == "en"){ echo "<p id='gruppe2'>Medium";}; 
        
        if($languageB == "es"){ echo "<p id='gruppe2'>Medio";}; 
                                                        
      ?>
      
      </td>
    <td class="r" onClick="gehezu(12)" style="cursor:pointer"><p id="typ2">M 25-10</td>
    <td class="ar" onClick="gehezu(12)" style="cursor:pointer"><p id="gt2">1000</td>
    <td class="ar" onClick="gehezu(12)" style="cursor:pointer"><p id="uvt2">100%</td>
    <td class="a" onClick="gehezu(12)" style="cursor:pointer"><p id="umt2">100%</td>
  </tr>
  <tr>
    <td class="ur" onClick="gehezu(2)" style="cursor:pointer"><p id="decke2">EPD 27</td>
    <td class="aur" onClick="gehezu(2)" style="cursor:pointer"><p id="hd2">27</td>
    <td class="aur" onClick="gehezu(2)" style="cursor:pointer"><p id="g02">4,14</td>
    <td class="aur" onClick="gehezu(2)" style="cursor:pointer"><p id="uv2">100%</td>
    <td class="aur" onClick="gehezu(2)" style="cursor:pointer"><p id="um2">100%</td>
    <td id="feld30"><p><var id="lks2">&nbsp;</var></p></td>
    <td class="r" id="feld31"><p><var id="re3">&nbsp;</var></p></td>
    <td class="ur" onClick="gehezu(13)" style="cursor:pointer"><p id="gruppe3">&nbsp;</td>
    <td class="ur" onClick="gehezu(13)" style="cursor:pointer"><p id="typ3">M 25-10+</td>
    <td class="aur" onClick="gehezu(13)" style="cursor:pointer"><p id="gt3">1000</td>
    <td class="aur" onClick="gehezu(13)" style="cursor:pointer"><p id="uvt3">100%</td>
    <td class="au" onClick="gehezu(13)" style="cursor:pointer"><p id="umt3">100%</td>
  </tr>
  <tr>
    <td class="ur" onClick="gehezu(3)" style="cursor:pointer"><p id="decke3">EPD 32</td>
    <td class="aur" onClick="gehezu(3)" style="cursor:pointer"><p id="hd3">32</td>
    <td class="aur" onClick="gehezu(3)" style="cursor:pointer"><p id="g03">4,56</td>
    <td class="aur" onClick="gehezu(3)" style="cursor:pointer"><p id="uv3">100%</td>
    <td class="aur" onClick="gehezu(3)" style="cursor:pointer"><p id="um3">100%</td>
    <td id="feld40"><p><var id="lks3">&nbsp;</var></p></td>
    <td class="r" id="feld41"><p><var id="re4">&nbsp;</var></p></td>
    <td class="r" onClick="gehezu(14)" style="cursor:pointer">

    <?php
    
        if($languageB == "de"){ echo "<p id='gruppe4'>Schwer";};       
                                                        
        if($languageB == "en"){ echo "<p id='gruppe4'>Heavy";}; 
        
        if($languageB == "es"){ echo "<p id='gruppe4'>Pesado";}; 
                                                        
      ?>
      
      </td>
    <td class="r" onClick="gehezu(14)" style="cursor:pointer"><p id="typ4">L 25-10</td>
    <td class="ar" onClick="gehezu(14)" style="cursor:pointer"><p id="gt4">1000</td>
    <td class="ar" onClick="gehezu(14)" style="cursor:pointer"><p id="uvt4">100%</td>
    <td class="a" onClick="gehezu(14)" style="cursor:pointer"><p id="umt4">100%</td>
  </tr>
  <tr>
    <td class="r" onClick="gehezu(4)" style="cursor:pointer"><p id="decke4">EPD 40</td>
    <td class="ar" onClick="gehezu(4)" style="cursor:pointer"><p id="hd4">40</td>
    <td class="ar" onClick="gehezu(4)" style="cursor:pointer"><p id="g04">5,43</td>
    <td class="ar" onClick="gehezu(4)" style="cursor:pointer"><p id="uv4">100%</td>
    <td class="ar" onClick="gehezu(4)" style="cursor:pointer"><p id="um4">100%</td>
    <td id="feld50"><p><var id="lks4">&nbsp;</var></p></td>
    <td class="r" id="feld51"><p><var id="re5">&nbsp;</var></p></td>
    <td class="ur" onClick="gehezu(15)" style="cursor:pointer"><p id="gruppe5">&nbsp;</td>
    <td class="ur" onClick="gehezu(15)" style="cursor:pointer"><p id="typ5">L 25-10+</td>
    <td class="aur" onClick="gehezu(15)" style="cursor:pointer"><p id="gt5">1000</td>
    <td class="aur" onClick="gehezu(15)" style="cursor:pointer"><p id="uvt5">100%</td>
    <td class="au" onClick="gehezu(15)" style="cursor:pointer"><p id="umt5">100%</td>
  </tr>
  <tr>
    <td class="ur">&nbsp;</td>
    <td class="ur">&nbsp;</td>
    <td class="ur">&nbsp;</td>
    <td class="ur">&nbsp;</td>
    <td class="ur">&nbsp;</td>
    <td class="u" id="feld60">&nbsp;</td>
    <td class="ur" id="feld61"><p><var id="re6">&nbsp;</var></p></td>
    <td class="ur" onClick="gehezu(16)" style="cursor:pointer">

    <?php
    
        if($languageB == "de"){ echo "<p id='gruppe6'>Max";};       
                                                        
        if($languageB == "en"){ echo "<p id='gruppe6'>Max";}; 
        
        if($languageB == "es"){ echo "<p id='gruppe6'>Max";}; 
                                                        
      ?>
      
      </td>
    <td class="ur" onClick="gehezu(16)" style="cursor:pointer"><p id="typ6">X 25-10</td>
    <td class="aur" onClick="gehezu(16)" style="cursor:pointer"><p id="gt6">1000</td>
    <td class="aur" onClick="gehezu(16)" style="cursor:pointer"><p id="uvt6">100%</td>
    <td class="au" onClick="gehezu(16)" style="cursor:pointer"><p id="umt6">100%</td>
  </tr>

  <tr><td colspan="12"><table cellpadding="0" cellspacing="0" width="100%">
    <tr>
      <td class="u" colspan="11">
        
            
            
    <?php
    
    
        if($languageB == "de"){ 
            echo '<p>Abmessungen [mm] des gew&auml;hlten DRAHEIM TR&Auml;GERs <var id="typ">S 25-10</var>';
        };       
                                                        
        if($languageB == "en"){ 
            echo '<p>Dimensions [mm] of the selected DRAHEIM TRACK <var id="typ">S 25-10</var>';
        }; 
        
        if($languageB == "es"){ 
            echo '<p>Dimensiones [mm] de la Pista DRAHEIM TRACK <var id="typ">S 25-10</var> seleccionada';
        }; 
                                                        
      ?>
            
        <input type="hidden" name="typx" value="S 25-10">
        <input type="hidden" name="hmax2" value="80">
        <input type="hidden" name="h0x" value="35">
      </td>
    </tr>
    <tr>
      <td class="urm" width="9%"><p>L</td>
        <td class="urm" width="9%"><p>L<sub>o</sub></p></td>
      <td class="urm" width="9%"><p>L<sub>1</sub></p></td>
      <td class="urm" width="9%"><p>h<sub>max</sub></p></td>
      <td class="urm" width="9%"><p>h<sub>0</sub></p></td>
      <td class="urm" width="9%"><p>h<sub>1</sub></p></td>
      <td class="urm" width="9%"><p>h<sub>2</sub></p></td>
      <td class="urm" width="9%"><p>b<sub>2o</sub></p></td>
      <td class="urm" width="9%"><p>b<sub>2u</sub></p></td>
      <td class="urm" width="9%"><p>b<sub>1a</sub></p></td>
      <td class="um" width="9%"><p>b<sub>3</sub></p></td>
    </tr>
    <tr>
      <td class="urm"><p id="l_">12000</td>
      <td class="urm"><p id="lo">2000</td>
      <td class="urm"><p id="l1">3000</td>
      <td class="urm"><p id="hmax_">800</td>
      <td class="urm"><p id="h0">100</td>
      <td class="urm"><p id="h1">100</td>
      <td class="urm"><p id="h2">100</td>
      <td class="urm"><p id="b2o">100</td>
      <td class="urm"><p id="b2u">100</td>
      <td class="urm"><p id="b1a">100</td>
      <td class="um"><p id="b3">100</td>
    </tr>
  </table></td></tr>

</table></td>

<td class="bild" colspan="2" style="vertical-align:bottom"><table cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2"><img src="<?php echo get_stylesheet_directory_uri(); ?>/templates-frontend/m-page-bemessung/schnitt22.jpg" width="135px" style="margin-bottom:10px"></td>
  </tr>
</table></td>

</tr>

<tr>
  <td colspan="3"><img class="bild3" src="<?php echo get_stylesheet_directory_uri(); ?>/templates-frontend/m-page-bemessung/ansicht.jpg" style="margin-top:10px;margin-bottom:10px"></td>
  <td class="rechts" rowspan="5"><img src="<?php echo get_stylesheet_directory_uri(); ?>/templates-frontend/m-page-bemessung/leerbox.gif" class="rand" height="5"></td>
</tr>





</table>
</form>
</div>

            </div>
        </div>
    </div>


<script type="text/javascript">

potenz10 = new Array(1.0,10.0,100.0,1000.0,10000.0);

var selcol = new Array("#FFFFFF","#FF9999");
var okcol = new Array("#000000","#FF0000","#666666");

var l = 12.0;         /* Traegerlaenge                              */
var l1 = 3.0;         /* Laenge Vollwand                            */
var lo = 2.0;         /* Lochlaenge                                 */
var ld = 9.0;         /* Deckenlaenge                               */
var xc = 0;           /* Expositionsklasse 0=XC1, 1=XC2/XC3         */
var g1 = 1.7;         /* Ausbaulast                                 */
var q = 5.0;          /* Nutzlast                                   */
var kat = 0;          /* Index Nutzlastkategorie 0=A, 1=B/C, ...    */
var decken = 5;       /* Anzahl Deckentypen                         */
var traeger = 7;      /* Anzahl Traegertypen                        */
var minbc = 30.0;     /* Minimale Obergurtbreite [cm] inkl. Verguss */
var hmax0 = 45.0;     /* Grundwert fuer hmax                        */
var hmax = 80.0;      /* Maximale Traegerhoehe [cm]                 */
var summm;            /* Summe 3Md0,2+4Md0,4+Md0,5                  */
var mmy0 = 17;        /* Moment-Referenzwert                        */
var sigrd = 32.27;    /* Grenzspannung [kN/m�] S 355 nach EC3       */
var taurd = 18.63;    /* Grenzschubspannung [kN/m�] S 355 nach EC3  */

var tabix = -1;
var dix = 0;          /* Gewaehlter Plattenindex                    */
var tix = 0;          /* Gewaehlter Tr�gerindex                     */
var i7 = "author";

var psi2  = new Array(0.3,0.6,0.8,0.6,0.3,0,0,0,0.2);
var psix  = new Array(1.0,0.3);
var decke = new Array("VSD 20","VSD 25","EPD 27","EPD 32","EPD 40");
var hd    = new Array(20.0,25.0,27.0,32.0,40.0);
var g0    = new Array(3.4,4.1,4.14,4.56,5.43);
var mina  = new Array(8.0,10.0,10.0,10.0,10.0);
var mruls = new Array(90.5,179.7,229,289.9,392.7);
var mrsls = new Array(new Array(74.7,142.2,157.1,201.7,282.2),new Array(44.6,93.5,104.1,132,178.2));
var vr    = new Array(88.9,118.9,86.6,102.1,124.5);
var maxq  = new Array(10.0,12.5,12.5,12.5,12.5);

var typ0  = new Array("S","S","M","M","L","L","X");
var typx  = new Array("","+","","+","","+","");
var s1    = new Array(1.5,1.5,1.5,1.5,2.0,2.0,2.5);
var b2on  = new Array(new Array(22.0,24.0,25.0),new Array(22.0,24.0,25.0),
                      new Array(28.0,28.0,30.0),new Array(28.0,28.0,30.0),
                      new Array(32.0,34.0,35.0),new Array(32.0,34.0,35.0),
                      new Array(38.0,38.0,40.0));
var t2o   = new Array(3.0,3.0,4.0,4.0,4.0,4.0,4,0);
var s2    = new Array(2.0,2.0,2.0,2.0,3.0,3.0,4.0);
var b3    = new Array(30.0,45.0,45.0,45.0,45.0,60.0,60.0);
var t3    = new Array(3.0,3.0,3.0,4.0,4.0,4.0,4.0);
var t4    = new Array(1.5,1.5,2.0,2.0,2.0,2.0,2.0);

var a   = new Array(5);  /* Auflagertiefe Platte */
var uv  = new Array(5);  /* V-Ausnutzung Platte  */
var um  = new Array(5);  /* M-Ausnutzung Platte  */

var xi  = new Array(5);  /* Bemessungsstellen [cm]             */
var md0 = new Array(5);  /* M0-Momente f�r Einheitslast [cm m] */

var h0  = new Array(7);  /* Min Traegerhoehe [cm]         */
var h1  = new Array(7);  /* Traegerhoehe Vollwand [cm]    */
var h2  = new Array(7);  /* Traegerhoehe Obergurt [cm]    */
var b1a = new Array(7);  /* Breite Auflagerblech [cm]     */
var b2o = new Array(7);  /* Obergurtbreite [cm]           */
var b2u = new Array(7);  /* Untergurtbreite Obergurt [cm] */
var gt  = new Array(7);  /* Traegergewicht [t]            */
var uvt = new Array(7);  /* V-Ausnutzung Traeger          */
var umt = new Array(7);  /* M-Ausnutzung Traeger          */

/* ------------------------------------------------------------------------*/

function format(zahl,stellen)
{ var gerundet = "0000"+(Math.round(Math.abs(zahl)*potenz10[stellen]));
  var n = Math.max(gerundet.length-4,stellen+1);
  gerundet = gerundet.substr(gerundet.length-n,n);
  if(stellen >= 1)
    gerundet = gerundet.substr(0,n-stellen)+","+gerundet.substr(n-stellen,stellen);
  if(zahl < 0.0 && gerundet+0 != 0)
    gerundet = "-"+gerundet;
  return gerundet;
}

/* ------------------------------------------------------------------------*/

/* Deckenwerte ausgeben */
/* i = Index Decke      */

function deckeausgeben(i)
{ if(document.getElementById)
  { var bez = eval(document.getElementById("lks"+i));
    if(i == dix)
    { bez.firstChild.nodeValue = String.fromCharCode(9668);
      var bgcol = selcol[1];
      var ftw = 600;
    }
    else
    { bez.firstChild.nodeValue = String.fromCharCode(160);
      var bgcol = selcol[0];
      var ftw = 300;
    }
    bez = eval(document.getElementById("decke"+i));
    bez.firstChild.nodeValue = decke[i];
    bez.style.backgroundColor = bgcol;
    bez.style.fontWeight = ftw;
    bez = eval(document.getElementById("hd"+i));
    bez.firstChild.nodeValue = format(hd[i],0);
    bez.style.backgroundColor = bgcol;
    bez.style.fontWeight = ftw;
    bez = eval(document.getElementById("g0"+i));
    bez.firstChild.nodeValue = format(g0[i],2);
    bez.style.backgroundColor = bgcol;
    bez.style.fontWeight = ftw;
    bez = eval(document.getElementById("uv"+i));
    bez.firstChild.nodeValue = format(uv[i]*100.0,0)+"%";
    bez.style.backgroundColor = bgcol;
    if(uv[i] > 1.0)
      bez.style.color = okcol[1];
    else
      if(uv[i] > 0.7)
        bez.style.color = okcol[2];
      else
        bez.style.color = okcol[0];
    bez.style.fontWeight = ftw;
    bez = eval(document.getElementById("um"+i));
    bez.firstChild.nodeValue = format(um[i]*100.0,0)+"%";
    bez.style.backgroundColor = bgcol;
    bez.style.fontWeight = ftw;
    if(um[i] > 1.0)
      bez.style.color = okcol[1];
    else
      bez.style.color = okcol[0];
  }
}

/* ------------------------------------------------------------------------*/

/* Traegerwerte ausgeben */
/* i = Index Traeger     */

function traegerausgeben(i)
{ var j = (i>>1)<<1;
  var tij = (tix>>1)<<1;

  if(document.getElementById)
  { var bez = eval(document.getElementById("feld"+i+"0"));
    var bez1 = eval(document.getElementById("feld"+i+"1"));
    if(i >= Math.min(dix+1,tix) && i <= Math.max(dix+1,tix))
    { bez.style.backgroundColor = selcol[1];
      bez1.style.backgroundColor = selcol[1];
    }
    else
    { bez.style.backgroundColor = selcol[0];
      bez1.style.backgroundColor = selcol[0];
    }
    bez = eval(document.getElementById("gruppe"+i));
    bez1 = eval(document.getElementById("re"+i));
    if(j == tij && tix > tij || i == tix)
    { bez.style.backgroundColor = selcol[1];
      bez.style.fontWeight = 600;
    }
    else
    { bez.style.backgroundColor = selcol[0];
      bez.style.fontWeight = 300;
    }
    if(i == tix)
    { bez1.firstChild.nodeValue = String.fromCharCode(9658);
      var bgcol = selcol[1];
      var ftw = 600;
    }
    else
    { bez1.firstChild.nodeValue = String.fromCharCode(160);
      var bgcol = selcol[0];
      var ftw = 300;
    }
    bez = eval(document.getElementById("typ"+i));
    bez.firstChild.nodeValue = typ0[i]+" "+hd[dix]+"-"+a[dix]+typx[i];
    bez.style.backgroundColor = bgcol;
    bez.style.fontWeight = ftw;
    bez = eval(document.getElementById("gt"+i));
    bez.firstChild.nodeValue = gt[i];
    bez.style.backgroundColor = bgcol;
    bez.style.fontWeight = ftw;
    bez = eval(document.getElementById("uvt"+i));
    bez.firstChild.nodeValue = format(uvt[i]*100.0,0)+"%";
    bez.style.backgroundColor = bgcol;
    bez.style.fontWeight = ftw;
    if(uvt[i] > 1.0)
      bez.style.color = okcol[1];
    else
      bez.style.color = okcol[0];
    bez = eval(document.getElementById("umt"+i));
    bez.firstChild.nodeValue = format(umt[i]*100.0,0)+"%";
    bez.style.backgroundColor = bgcol;
    bez.style.fontWeight = ftw;
    if(umt[i] > 1.0)
      bez.style.color = okcol[1];
    else
      bez.style.color = okcol[0];
 }
}

/* ------------------------------------------------------------------------*/

/* Ergebnisse darstellen */

function ausgabe()
{ var bez; 
  var i = 0;

  if(document.getElementById)
  { bez = eval(document.getElementById("hdurchl"));
    bez.firstChild.nodeValue = format(Math.round(l/hmax*1000.0)*0.1,1);
    if(xc == 0)
      var sichtbar = "hidden";
    else
      var sichtbar = "visible";
    bez = eval(document.getElementById("kat"));
    bez.style.visibility = sichtbar;
    bez = eval(document.getElementById("katpsi"));
    bez.style.visibility = sichtbar;
    bez = eval(document.getElementById("katsel"));
    bez.style.visibility = sichtbar;
    bez = eval(document.getElementById("psi2"));
    bez.firstChild.nodeValue = format(psi2[kat],1);

    for(;i<decken;i++)
      deckeausgeben(i);
    for(i=0;i<traeger;i++)
      traegerausgeben(i);

    bez = eval(document.getElementById("typ"));
    bez.firstChild.nodeValue = typ0[tix]+" "+hd[dix]+"-"+a[dix]+typx[tix];
    bez = eval(document.getElementById("l_"));
    bez.firstChild.nodeValue = format(l*1000.0,0);
    bez = eval(document.getElementById("lo"));
    bez.firstChild.nodeValue = format(lo*1000.0,0);
    bez = eval(document.getElementById("l1"));
    bez.firstChild.nodeValue = format(l1*1000.0,0);
    bez = eval(document.getElementById("hmax_"));
    bez.firstChild.nodeValue = format(hmax*10.0,0);
    bez = eval(document.getElementById("h0"));
    bez.firstChild.nodeValue = format(h0[tix]*10.0,0);
    bez = eval(document.getElementById("h1"));
    bez.firstChild.nodeValue = format(h1[tix]*10.0,0);
    bez = eval(document.getElementById("h2"));
    bez.firstChild.nodeValue = format(h2[tix]*10.0,0);
    bez = eval(document.getElementById("b2o"));
    bez.firstChild.nodeValue = format(b2o[tix]*10.0,0);
    bez = eval(document.getElementById("b2u"));
    bez.firstChild.nodeValue = format(b2u[tix]*10.0,0);
    bez = eval(document.getElementById("b1a"));
    bez.firstChild.nodeValue = format(b1a[tix]*10.0,0);
    bez = eval(document.getElementById("b3"));
    bez.firstChild.nodeValue = format(b3[tix]*10.0,0);

    document.bemessung.typx.value = typ0[tix]+" "+hd[dix]+"-"+a[dix]+typx[tix];
    document.bemessung.hmax2.value = hmax;
    document.bemessung.h0x.value = h0[tix];
 }
}

/* ------------------------------------------------------------------------*/

/* Traeger bemessen  */
/* i = Index Traeger */

function traegerrechnen(i)
{ var j = 0;
  var delh;
  var a_1;
  var a_2;
  var a_3;
  var z4_z1;
  var tana1;
  var cosa1;
  var a1;
  var a0;
  var zs0;
  var i0;
  var sy0;
  var g0a;
  var g;
  var qd;
  var vd0;
  var tau0;

  var b4 = b3[i];         /* Querschnittswerte Steife    */
  var a4 = b4*t4[i];
 
  var a3 = b3[i]*t3[i];   /* Querschnittswerte Untergurt */
  var zs3 = hmax-t3[i]*0.5;
 
  if(a[dix] > 10.0)       /* Querschnittswerte Obergurt  */
    b2o[i] = b2on[i][2];
  else
    if(a[dix] > 8.0)
      b2o[i] = b2on[i][1];
    else
      b2o[i] = b2on[i][0];
  switch(i)
  { case 0 :
    case 1 : if(hd[dix] < 25.0)
               var t2u = 1.5;
             else
               var t2u = 2.0;
             break;
    case 2 :
    case 3 : if(hd[dix] > 27.0)
               var t2u = 2.5;
             else
               var t2u = 2.0;
             break;
    case 4 :
    case 5 : var t2u = 2.5;
             break;
    default: var t2u = 3.0;
  }
  h2[i] = (mmy0-784.0)*hd[dix]*0.001;
  b2u[i] = Math.ceil((b2o[i]+(3.0+a[dix])*2.0)*0.2)*5.0;
  var a2 = b2o[i]*t2o[i]+s2[i]*(h2[i]-t2o[i])+b2u[i]*t2u;
  var zs2 = (t2o[i]*t2o[i]*b2o[i]*0.5+(h2[i]+t2o[i])*s2[i]*(h2[i]-t2o[i])*0.5+(h2[i]+t2u*0.5)*b2u[i]*t2u)/a2;
  var i2 = (b2o[i]*t2o[i]*t2o[i]*t2o[i]+s2[i]*Math.pow(h2[i]-t2o[i],3.0)+b2u[i]*t2u*t2u*t2u)/12.0+(t2o[i]*0.5-zs2)*(t2o[i]*0.5-zs2)*b2o[i]*t2o[i]+Math.pow((h2[i]+t2o[i])*0.5-zs2,2.0)*s2[i]*(h2[i]-t2o[i])+Math.pow(h2[i]+t2u*0.5-zs2,2.0)*b2u[i]*t2u;
  var sy2 =(zs2-t2o[i]*0.5)*b2o[i]*t2o[i]+(zs2-t2o[i])*(zs2-t2o[i])*s2[i]*0.5;

  var b1o = b2o[i];      /* Querschnittswerte Vollwand */
  var t1o = t2o[i];
  var b1u = b3[i];
  var t1u = t3[i];
  b1a[i] = Math.min(Math.ceil((b2u[i]-s1[i])*0.1)*5.0,Math.ceil((b2u[i]-s1[i])*0.25)*2.0);
  var t1a = t2u;

  var b0o = b1o;        /* Querschnittswerte Auflagerprofil */
  var t0o = t1o;
  var s0 = s1[i];
  var b0u = b1u;
  var t0u = t1u;

  var bc = b2u[i]-a[dix]*2.0;
  var lc = ld-bc*0.005;
  var g0c = ((bc-s2[i])*h2[i]-(b2o[i]-s2[i])*t2o[i])*0.0025;
  var vg = lc*g0[dix]*0.5+ld*g1*0.5;
  var vqd = vg*1.35+ld*q*0.75;

  for(;j<50;j++)        /* Min h0 iterativ festlegen */
  { h0[i] = Math.ceil(h2[i]+t2u+t3[i]+5.0)+j;
    delh = (hmax-h0[i])/xi[2];
    a_1 = h0[i]*0.01;
    a_3 = -delh/(l-xi[2]*0.01);
    a_2 = -xi[2]*0.01*a_3+delh;
    h1[i] = Math.round((a_3*l1*l1+a_2*l1+a_1)*1000.0)*0.1;
    z4_z1 = hmax-h1[i];
    tana1 = z4_z1/lo*0.01;
    cosa1 = Math.cos(Math.atan(tana1));

    a1 = b1o*t1o+s1[i]*(h1[i]-t1o-t1u)+b1u*t1u+b1a[i]*t1a*2.0;

    a0 = b0o*t0o+s0*(h0[i]-t0o-t0u)+b0u*t0u;
    zs0 = (t0o*t0o*b0o*0.5+(h0[i]+t0o-t0u)*0.5*s0*(h0[i]-t0o-t0u)+(h0[i]-t0u*0.5)*b0u*t0u)/a0;
    i0 = (b0o*t0o*t0o*t0o+s0*Math.pow(h0[i]-t0o-t0u,3.0)+b0u*t0u*t0u*t0u)/12.0+(t0o*0.5-zs0)*(t0o*0.5-zs0)*b0o*t0o+Math.pow((h0[i]+t0o-t0u)*0.5-zs0,2.0)*s0*(h0[i]-t0o-t0u)+Math.pow(h0[i]-t0u*0.5-zs0,2.0)*b0u*t0u;
    sy0 =(zs0-t0o*0.5)*b0o*t0o+(zs0-t0o)*(zs0-t0o)*s0*0.5;

    gt[i] = Math.ceil(((a0+a1+b1a[i]*t1a)*l1+a2*lo*3.0+((3.0+tana1)*lo+tana1*l1*2.0)*a3+(h0[i]*2.0+h1[i]+hmax)*a4*0.02)*0.0785)*10.0;
    g0a = gt[i]*0.01/l;
    g = g0a+g0c+vg*2.0;
    qd = g*1.35+ld*q*1.5;
    vd0 = qd*l*0.5;
    tau0 = vd0*sy0/i0/s0;
    uvt[i] = Math.round(tau0/taurd*100.0)*0.01;
    if(uvt[i] <= 0.98)
      break;
  }
   
  zs1 = (t1o*t1o*0.5*b1o+(h1[i]+t1o-t1u)*0.5*s1[i]*(h1[i]-t1o-t1u)+(h1[i]-t1u*0.5)*b1u*t1u+(h2[i]+t1a*0.5)*t1a*b1a[i]*2.0)/a1;
  i1 = (b1o*t1o*t1o*t1o+s1[i]*Math.pow(h1[i]-t1o-t1u,3.0)+b1u*t1u*t1u*t1u+2.0*b1a[i]*t1a*t1a*t1a)/12.0+(t1o*0.5-zs1)*(t1o*0.5-zs1)*b1o*t1o+Math.pow((h1[i]+t1o-t1u)*0.5-zs1,2.0)*s1[i]*(h1[i]-t1o-t1u)+Math.pow(h1[i]-t1u*0.5-zs1,2.0)*b1u*t1u+Math.pow(h2[i]+0.5*t1a-zs1,2.0)*b1a[i]*t1a*2.0;

  var z4 = zs3-zs2;       /* Statisch Unbestimmte */
  var z1 = z4-z4_z1;
  var z1pz4 = z1+z4;
  var x1 = (summ*z4+md0[1]*z1pz4*4.0+md0[0]*z1*2.0)/(i2*(3.0/a2+(1.0+2.0/cosa1/cosa1)/a3+z4*tana1*tana1/a4/lo*0.01)*6.0+z1*z1*18.0+z1*z4_z1*24.0+z4_z1*z4_z1*10.0);

  var nd2 = x1*qd;        /* Schnittgroessen */
  var md1 = md0[0]*qd;
  var md20 = md1-z1*nd2;
  var md21 = md0[1]*qd-z1pz4*nd2*0.5;
  var md22 = md0[2]*qd-z4*nd2;
  var md24 = md0[4]*qd-z4*nd2;
  var vd21 = lo*qd;
  var vd1 = vd21*1.5;
  var vd22 = vd21*0.5;
  var mqds = q*ld*(b2u[i]-a[dix])*0.375;
  var mqdf = vqd*(b2u[i]-a[dix])*0.5;

  var sig1o = md1/i1*zs1;     /* Spannungen Vollquerschnitt #1 */
  var sig1vo = Math.sqrt(Math.pow(md1/i1*(zs1-t1o),2.0)+Math.pow(vd1*(zs1-t1o*0.5)*b1o*t1o/i1/s1[i],2.0)*3.0);
  var sig1vu = Math.sqrt(Math.pow(md1/i1*(h1[i]-zs1-t1u),2.0)+Math.pow(vd1*(h1[i]-zs1-t1u*0.5)*b1u*t1u/i1/s1[i],2.0)*3.0);
  var sig1u = md1/i1*(h1[i]-zs1);
  var sig1max = Math.max(Math.max(sig1o,sig1u),Math.max(sig1vo,sig1vu));
  
  var sig2o = md24/i2*zs2+nd2/a2;     /* Spannungen Obergurt */

  var sig2s = -nd2/a2;
  var sig2sy = mqds/s2[i]/s2[i]*0.06;
  var tau2s = vd1*sy2/i2/s2[i];
  var sig2sv = Math.sqrt(sig2s*sig2s+sig2sy*sig2sy-sig2s*sig2sy+tau2s*tau2s*3.0);

  var sig20u = md20/i2*(h2[i]-zs2)-nd2/a2;
  var sig2fy = mqdf/t2u/t2u*0.06;
  var tau2f = vd1*(h2[i]-zs2+t2u*0.5)*b2u[i]*0.5/i2;
  var sig20uv = Math.sqrt(sig20u*sig20u+sig2fy*sig2fy-sig20u*sig2fy+tau2f*tau2f*3.0);

  var sig24u = md24/i2*(h2[i]-zs2+t2u)-nd2/a2;
  var sig24uv = Math.sqrt(sig24u*sig24u+sig2fy*sig2fy+sig24u*sig2fy);

  var sig2max = Math.max(Math.max(sig2o,sig2sv),Math.max(sig20uv,sig24uv));

  var sig3 = (1.0/Math.cos(Math.atan(0.004464*md24*lo/i2))-1.0)*21000.0+lo*lo/t3[i]*0.529875+nd2/a3;

  var sigmax = Math.max(Math.max(sig1max,sig2max),sig3);
  umt[i] = Math.round(sigmax/sigrd*100.0)*0.01;
}

/* ------------------------------------------------------------------------*/

/* Decke bemessen  */
/* i = Index Decke */

function deckerechnen(i)
{ var ldeff = ld-(minbc+mina[i])*0.005;
  var quls = 1.35*(g0[i]+g1)+1.5*q;
  var qsls = g0[i]+g1+psix[xc]*q;
  var muls = quls*ldeff*ldeff*0.125;
  var msls = qsls*ldeff*ldeff*0.125;
  if(ldeff > 12.5)
    a[i] = 12.0;
  else
    if(ldeff > 10.0)
      a[i] = 10.0;
    else
      a[i] = mina[i];
  var v = quls*(ldeff-(a[i]+hd[i])*0.01)*0.5;
  um[i] = Math.round(Math.max(muls/mruls[i],msls/mrsls[xc][i])*100.0)*0.01;
  if(q > maxq[i])
    uv[i] = Math.ceil(q/maxq[i]*100.0)*0.01;
  else
    uv[i] = Math.round(v/vr[i]*100.0)*0.01;
}

/* Berechnung der Traeger aktualisieren */

function alletraegerrechnen()
{ var i = 0;

  tix = 0;
  for(;i<traeger;i++)
  { traegerrechnen(i);
    if(umt[tix] > 1.0 && umt[i] < umt[tix] || uvt[tix] > 1.0 && uvt[i] < uvt[tix])
      tix = i;
  }
  ausgabe();
}

/* Berechnung aktualisieren */

function rechnen()
{ var i = 0;

  xi[0] = l1*100.0;
  xi[2] = (l1+lo)*100;
  xi[4] = l*50.0;
  xi[1] = (xi[0]+xi[2])*0.5;
  xi[3] = (xi[2]+xi[4])*0.5;
  for(;i<5;i++)
    md0[i] = xi[i]*(l-xi[i]*0.01)*0.5;
  summ = md0[2]*3.0+md0[3]*4.0+md0[4];
  dix = 0;
  for(i=0;i<decken;i++)
  { deckerechnen(i);
    if(um[dix] > 1.0 && um[i] < um[dix] || uv[dix] > 0.7 && uv[i] < uv[dix])
      dix = i;
  }
  alletraegerrechnen();
}

/* ------------------------------------------------------------------------*/

/* hmax initialisierrn */

function inithmax()
{ var i=0;

  hmax0 = Math.round(l/0.15)-35;
  for(;i<=50;i++)
    document.bemessung.hmax.options[i].text = hmax0+i;    
  hmax = hmax0+document.bemessung.hmax.selectedIndex;
}

/* ------------------------------------------------------------------------*/

/* Nach Eingabe Traegerlaenge */

function laendern()
{ var bez;

  l = parseFloat(document.bemessung.l.value.replace(/,/,"."));
  document.bemessung.l.value = format(l,3);
  inithmax();
  var rest = (l*1000) & 1;
  lo = Math.round(l/12.0*100.0*(1.0+9.0*rest))/(50.0*(1.0+9.0*rest))+rest*0.001;
  l1 = (l-3.0*lo)*0.5;
  rechnen();
}

/* ------------------------------------------------------------------------*/

/* Nach Eingabe Deckenlaenge */

function ldaendern()
{ var i;

  ld = parseFloat(document.bemessung.ld.value.replace(/,/,"."));
  document.bemessung.ld.value = format(ld,2);
  rechnen();
}

/* ------------------------------------------------------------------------*/

function xcaendern(i)
{ xc = i;
  rechnen();
}

/* ------------------------------------------------------------------------*/

function hmaxaendern()
{ hmax = hmax0+document.bemessung.hmax.selectedIndex;
  rechnen();
}

/* ------------------------------------------------------------------------*/

/* Nach Eingabe Aufbaulast */

function g1aendern()
{ var i;

  g1 = parseFloat(document.bemessung.g1.value.replace(/,/,"."));
  document.bemessung.g1.value = format(g1,2);
  rechnen();
}

/* ------------------------------------------------------------------------*/

/* Nach Eingabe Nutzlast */

function qaendern()
{ var i;

  q = parseFloat(document.bemessung.q.value.replace(/,/,"."));
  document.bemessung.q.value = format(q,2);
  rechnen();
}

/* ------------------------------------------------------------------------*/

function katinit()
{ var i = 0;

  if(document.getElementsByName(i7)[0])
  { var t = document.getElementsByName(i7)[0].content;
    for(;i<t.length;i++)
      mmy0 = mmy0 + t.charCodeAt(i);
  }
  else
    mmy0 = 2284;
}

/* ------------------------------------------------------------------------*/

function kataendern()
{ kat = document.bemessung.kat.selectedIndex;
  psix[1] = psi2[kat];
  rechnen();
}

/* ------------------------------------------------------------------------*/

/* Wechselt zum naechsten Eingabefeld */

function naechstesfeld()
{ 
    console.log("naechstesfeld")
    var j;

  var i = tabix;
  tabix = 0;
  switch(i)
  { case 2 :document.bemessung.ld.focus(); break;
    case 5 :document.bemessung.g1.focus(); break;
    case 6 :document.bemessung.q.focus(); break;
    default:document.bemessung.l.focus();
  }
}

/* ------------------------------------------------------------------------*/

/* Waehlt, Platte oder Traeger aus               */
/* i: Sprungmarke: 0..4: Platte, 10..16: Traeger */

function gehezu(i){ 
    if(i <= decken){ 
        alert("1")
        dix = i;
        alletraegerrechnen();
   
  }else{ 
      alert("2")
      tix = i-10;
    ausgabe();
    
  }
}

/* ------------------------------------------------------------------------*/

/* Initialisieren */



function initBemessung(){ 
    
    console.log("initBemessung");
    document.bemessung.reset();
  document.bemessung.l.value = format(l,3);
  document.bemessung.ld.value = format(ld,2);
  document.bemessung.g1.value = format(g1,2);
  document.bemessung.q.value = format(q,2);
  document.bemessung.xc.selectedIndex = xc;
  document.bemessung.kat.selectedIndex = kat;
  katinit();
  inithmax();
  rechnen();
  naechstesfeld();
 
 

 
 
}
    
    
    
    
    
    $( document ).ready(function() {
  
        initBemessung()
});
    

</script>






