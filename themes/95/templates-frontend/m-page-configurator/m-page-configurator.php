<?php
    $content = get_sub_field('inhalte');
    $headline = get_field('configurator_headline_calculator', 'option');
    $headlineResult = get_field('configurator_headline_calculator_result', 'option');


    //headline Tag
    /*$htag = $content["headlineTag"]; // h1 h2 div
    if($htag == "" || empty($htag)){
        $htag = "h2";
    }*/
    $htag = "h2";


    $hintArr = Array();
    $countIDArr = 0;


    $languagesKonfigurator = ICL_LANGUAGE_CODE;

    //echo "Sorachvariable Mpage Configurator: ".$languagesKonfigurator;



    $hintArr["section"]["Basisinformation"]["headline"] =       get_field('options_configurator_section_basis_headline', 'option');
    $hintArr["section"]["Basisinformation"]["subheadline"] =    get_field('options_configurator_section_basis_subheadline', 'option');
    $hintArr["section"]["Decke"]["headline"] =                  get_field('options_configurator_section_decke_headline', 'option');
    $hintArr["section"]["Decke"]["subheadline"] =               get_field('options_configurator_section_decke_subheadline', 'option');
    $hintArr["section"]["Betonbalken"]["headline"] =            get_field('options_configurator_section_betonbalken_headline', 'option');
    $hintArr["section"]["Betonbalken"]["subheadline"] =         get_field('options_configurator_section_betonbalken_subheadline', 'option');
    $hintArr["section"]["Oeffnungen"]["headline"]=              get_field('options_configurator_section_oeffnungen_headline', 'option');
    $hintArr["section"]["Oeffnungen"]["subheadline"] =          get_field('options_configurator_section_oeffnungen_subheadline', 'option');
    $hintArr["section"]["Belastungen"]["headline"] =            get_field('options_configurator_section_belastungen_headline', 'option');
    $hintArr["section"]["Belastungen"]["subheadline"] =         get_field('options_configurator_section_belastungen_subheadline', 'option');



                        
            
    $hintArr["form"]["firstname"]["label"] = __('configurator_form_label_firstname', 'neun');
    $hintArr["form"]["lastname"]["label"] = __('configurator_form_label_lastname', 'neun');
    $hintArr["form"]["firm"]["label"] = __('configurator_form_label_firm', 'neun');
    $hintArr["form"]["telefon"]["label"] = __('configurator_form_label_telefon', 'neun');
    $hintArr["form"]["mail"]["label"] = __('configurator_form_label_mail', 'neun');
    $hintArr["form"]["message"]["label"] = __('configurator_form_label_message', 'neun');
                        
                        
                        


    $hintArr["configurator"]["country"]["label"] = get_field('options_configurator_beamup_label_country', 'option');
    $hintArr["configurator"]["country"]["hint"] =  get_field('options_configurator_beamup_hint_country', 'option');

    $hintArr["configurator"]["positionsnumber"]["label"] = get_field('options_configurator_beamup_label_positionnumber', 'option');
    $hintArr["configurator"]["positionsnumber"]["hint"] =  get_field('options_configurator_beamup_hint_positionsnumber', 'option');


    $hintArr["configurator"]["plz"]["label"] = get_field('options_configurator_beamup_label_plz', 'option');
    $hintArr["configurator"]["plz"]["hint"] =  get_field('options_configurator_beamup_hint_plz', 'option');

    $hintArr["configurator"]["einsatzort"]["label"] = get_field('options_configurator_beamup_label_einsatzort', 'option');
    $hintArr["configurator"]["einsatzort"]["hint"] =  get_field('options_configurator_beamup_hint_einsatzort', 'option');



    $hintArr["configurator"]["deckentyp"]["label"] = get_field('options_configurator_beamup_label_Deckentyp', 'option');
    $hintArr["configurator"]["deckentyp"]["hint"] =  get_field('options_configurator_beamup_hint_Deckentyp', 'option');

    $hintArr["configurator"]["deckenhoehe"]["label"] = get_field('options_configurator_beamup_label_deckenhoehe_hc', 'option');
    $hintArr["configurator"]["deckenhoehe"]["hint"] =  get_field('options_configurator_beamup_hint_deckenhoehe_hc', 'option');

    $hintArr["configurator"]["festigkeitsklasse"]["label"] = get_field('options_configurator_beamup_label_festigkeitsklasse', 'option');
    $hintArr["configurator"]["festigkeitsklasse"]["hint"] =  get_field('options_configurator_beamup_hint_festigkeitsklasse', 'option');

    $hintArr["configurator"]["expositionsklasse"]["label"] = get_field('options_configurator_beamup_label_expositionsklasse', 'option');
    $hintArr["configurator"]["expositionsklasse"]["hint"] =  get_field('options_configurator_beamup_hint_expositionsklasse', 'option');


    $hintArr["configurator"]["feuerwiderstandsklasse"]["label"] = get_field('options_configurator_beamup_label_feuerwiderstandsklasse', 'option');
    $hintArr["configurator"]["feuerwiderstandsklasse"]["hint"] =  get_field('options_configurator_beamup_hint_feuerwiderstandsklasse', 'option');


    $hintArr["configurator"]["breiteb"]["label"] = get_field('options_configurator_beamup_label_breite_b', 'option');
    $hintArr["configurator"]["breiteb"]["hint"] =  get_field('options_configurator_beamup_hint_breite_b', 'option');

    $hintArr["configurator"]["hoehehges"]["label"] = get_field('options_configurator_beamup_label_hoehehges', 'option');
    $hintArr["configurator"]["hoehehges"]["hint"] =  get_field('options_configurator_beamup_hint_hoehehges', 'option');

    $hintArr["configurator"]["anzahl_oeffnung"]["label"] = get_field('options_configurator_beamup_label_oeffnung', 'option');
    $hintArr["configurator"]["anzahl_oeffnung"]["hint"] =  get_field('options_configurator_beamup_hint_oeffnung', 'option');

    $hintArr["configurator"]["laenge_oeffnung"]["label"] = get_field('options_configurator_beamup_label_laenge_oeffnung', 'option');
    $hintArr["configurator"]["laenge_oeffnung"]["hint"] =  get_field('options_configurator_beamup_hint_laenge_oeffnung', 'option');

    $hintArr["configurator"]["laenge_einzeloeffnung"]["label"] = get_field('options_configurator_beamup_label_laenge_einzeloeffnung', 'option');
    $hintArr["configurator"]["laenge_einzeloeffnung"]["hint"] =  get_field('options_configurator_beamup_hint_laenge_einzeloeffnung', 'option');

    $hintArr["configurator"]["qEd"]["label"] = get_field('options_configurator_beamup_label_qed', 'option');
    $hintArr["configurator"]["qEd"]["hint"] =  get_field('options_configurator_beamup_hint_qed', 'option');

    $hintArr["configurator"]["med1"]["label"] = get_field('options_configurator_beamup_label_med1', 'option');
    $hintArr["configurator"]["med1"]["hint"] =  get_field('options_configurator_beamup_hint_med1', 'option');

    $hintArr["configurator"]["ved1"]["label"] = get_field('options_configurator_beamup_label_ved1', 'option');
    $hintArr["configurator"]["ved1"]["hint"] =  get_field('options_configurator_beamup_hint_ved1', 'option');

    $hintArr["configurator"]["qedmax"]["label"] = get_field('options_configurator_beamup_label_qed_max', 'option');
    $hintArr["configurator"]["qedmax"]["hint"] =  get_field('options_configurator_beamup_hint_qed_max', 'option');

    $hintArr["configurator"]["spannweite_decke_ld1"]["label"] = get_field('options_configurator_beamup_label_spannweite_decke_ld1', 'option');
    $hintArr["configurator"]["spannweite_decke_ld1"]["hint"] =  get_field('options_configurator_beamup_hint_spannweite_decke_ld1', 'option');

    $hintArr["configurator"]["spannweite_decke_ld2"]["label"] = get_field('options_configurator_beamup_label_spannweite_decke_ld2', 'option');
    $hintArr["configurator"]["spannweite_decke_ld2"]["hint"] =  get_field('options_configurator_beamup_hint_spannweite_decke_ld2', 'option');

    $hintArr["configurator"]["spannweite_balken_l"]["label"] =  get_field('options_configurator_beamup_label_spannweite_decke_ld3', 'option');
    $hintArr["configurator"]["spannweite_balken_l"]["hint"] =   get_field('options_configurator_beamup_hint_spannweite_decke_ld3', 'option');




    $hintArr["configurator"]["global"]["hintheadline"] = __('configurator_noticebox_headline', 'neun');
    $hintArr["configurator"]["global"]["pleaseselect"] = __('configurator_global_pleaseselect', 'neun');    
    $hintArr["configurator"]["global"]["button"]["close"] = __('configurator_button_close', 'neun'); 
    $hintArr["configurator"]["global"]["button"]["submit"] = __('configurator_button_contactsubmit', 'neun');;  
    $hintArr["configurator"]["global"]["modal"]["calculation"]["text"] = get_field('configurator_modal_beamup_modal_calculation_text', 'option');
    $hintArr["configurator"]["global"]["modal"]["contact"]["text"] = get_field('configurator_modal_contact_sended_text', 'option');


    $hintArr["configurator"]["global"]["modal"]["contact"]["headline"] = get_field('configurator_modal_contact_headline', 'option');
    $hintArr["configurator"]["global"]["modal"]["contact"]["subheadline"] = get_field('configurator_modal_contact_headline_subtext', 'option');

    $hintArr["configurator"]["global"]["modal"]["contact"]["subheadline"] = get_field('configurator_modal_contact_dataprotectiontext', 'option');


    $hintArr["configurator"]["global"]["modal"]["contactcalcerror"]["subheadline"] = get_field('configurator_modal_beamup_modal_calculation_text_nocalculation', 'option');


    $surroundColumns1 = "col-12 col-md-7 col-lg-7 col-xl-6";
    $surroundColumns2 = "col-12 col-md-5 col-lg-5 col-xl-6";


    $columns1 = "col-12 col-md-12 col-lg-5 col-xl-5";
    $columns2 = "col-12 col-md-12 col-lg-7 col-xl-7";

 


    $image_betonbalken = get_field('options_configurator_beamup_image_betonbalken', 'option');
    $image_oeffnungen = get_field('options_configurator_beamup_image_oeffnungen', 'option');
    $image_belastungen = get_field('options_configurator_beamup_image_belastungen', 'option');
    $image_ausgabe = get_field('options_configurator_beamup_image_ausgabe', 'option');


    $ausgabeText0 = "ID:";
    $ausgabeText1 = get_field('options_configurator_beamup_ausgabe_gewicht', 'option');
    $ausgabeText2 = get_field('options_configurator_beamup_ausgabe_beschichtungsflaeche', 'option');
    $ausgabeText3 = get_field('options_configurator_beamup_ausgabe_ausnutzungsgrad', 'option');


    $ausgabeText4 = get_field('options_configurator_beamup_ausgabe_lange_beamup', 'option');
    $ausgabeText5 = get_field('options_configurator_beamup_ausgabe_hoehe_beamup', 'option');

    $ausgabeText6 = get_field('options_configurator_beamup_ausgabe_l1', 'option');
    $ausgabeText7 = get_field('options_configurator_beamup_ausgabe_l2', 'option');
    $ausgabeText8 = get_field('options_configurator_beamup_ausgabe_l3', 'option');
    $ausgabeText9 = get_field('options_configurator_beamup_ausgabe_l4', 'option');
    $ausgabeText10 = get_field('options_configurator_beamup_ausgabe_l5', 'option');


    $ausgabeText11 = get_field('options_configurator_beamup_ausgabe_netto_hoehe_oeffnung', 'option');


   
    $ausgabeAnfrageText = get_field('options_configurator_beamup_ausgabe_button_anfrage', 'option');



    $beamUpArr = Array();
    $beamUpArrCount = 0;
    // check if the repeater field has rows of data
    if( have_rows('options_configurator_beamup_repeater_countries','option') ):
        // loop through the rows of data
        while ( have_rows('options_configurator_beamup_repeater_countries','option') ) : the_row();
            // display a sub field value

            $beamUpArr[$beamUpArrCount]["countrylabel"] = get_sub_field( "options_configurator_beamup_repeater_countries_country_label");
            $beamUpArrCount++;
        endwhile;
    endif;


?>


<?php
    $showAutoFillButton = true;
    if($showAutoFillButton == true){
?>

    <div class="testfill" style="background-color: #13274f; color: white; padding: 3px; width: 100%;
        max-width: 300px;
        margin: 0 auto;
        text-align: center; cursor: pointer;">
        Formular mit Test Daten befüllen
    </div>
<?php
    };
?>




<?php

    //prefill via url 

    $isPDF = false;

    if (isset($_GET['pdf'])) {
        $isPDF = true; 
    } else {  
    };

    $get_Date = "";


    $get_country = ""; 
    $get_plz = ""; 
    $get_einsatzort = ""; 
    $get_positionsnumber = "";

    $get_deckentyp = "";
    $get_deckenhoehe_hc = "";
    $get_spannweite_decke_ld1 = "";
    $get_spannweite_decke_ld2 = "";

    $get_festigkeitsklasse_Beton = "";
    $get_expositionsklasse = "";
    $get_feuerwiderstandsklasse = "";
    $get_breite_Betonbalken = "";        
    $get_hoehe_hges = "";
    $get_spannweite_balken_l = "";

    $get_Laenge_Oeffnung_Lges = "";
    $get_anzahl_oeffnungen = "";
    $get_Laenge_der_Einzeloeffnungen_L1 = "";
    $get_Laenge_der_Einzeloeffnungen_L2 = "";
    $get_Laenge_der_Einzeloeffnungen_L3 = "";
    $get_Laenge_der_Einzeloeffnungen_L4 = "";
    $get_Laenge_der_Einzeloeffnungen_L5 = "";


    $get_Belastung_qEd = "";
    $get_Med_1 = "";
    $get_Qed_1 = "";
    $get_Qedmax = "";


    $getR_Gewicht = "";
    $getR_Oberflaeche = "";
    $getR_Lbrutto = "";
    $getR_Hbrutto = "";
    $getR_Hnetto = "";
    $getR_L1netto = "";
    $getR_L2netto = "";
    $getR_L3netto = "";
    $getR_L4netto = "";
    $getR_L5netto = "";
    

        
        
    if (isset($_GET['date'])) {$get_Date = $_GET['date']; } else {};
         
    if (isset($_GET['country'])) {$get_country = $_GET['country']; } else {};
    if (isset($_GET['plz'])) { $get_plz = $_GET['plz'];} else {};
    if (isset($_GET['einsatzort'])) {$get_einsatzort = $_GET['einsatzort'];} else {};
    if (isset($_GET['positionsnumber'])) {$get_positionsnumber = $_GET['positionsnumber']; } else {};

    if (isset($_GET['deckentyp'])) {$get_deckentyp = $_GET['deckentyp']; } else {};
    if (isset($_GET['deckenhoehe_hc'])) {$get_deckenhoehe_hc = $_GET['deckenhoehe_hc']; } else {};
    if (isset($_GET['spannweite_decke_ld1'])) {$get_spannweite_decke_ld1 = $_GET['spannweite_decke_ld1']; } else {};
    if (isset($_GET['spannweite_decke_ld2'])) {$get_spannweite_decke_ld2 = $_GET['spannweite_decke_ld2']; } else {};

    if (isset($_GET['festigkeitsklasse_Beton'])) {$get_festigkeitsklasse_Beton = $_GET['festigkeitsklasse_Beton']; } else {};
    if (isset($_GET['expositionsklasse'])) {$get_expositionsklasse = $_GET['expositionsklasse']; } else {};
    if (isset($_GET['feuerwiderstandsklasse'])) {$get_feuerwiderstandsklasse = $_GET['feuerwiderstandsklasse']; } else {};
    if (isset($_GET['breite_Betonbalken'])) {$get_breite_Betonbalken = $_GET['breite_Betonbalken']; } else {};
    if (isset($_GET['hoehe_hges'])) {$get_hoehe_hges = $_GET['hoehe_hges']; } else {};
    if (isset($_GET['spannweite_balken_l'])) {$get_spannweite_balken_l = $_GET['spannweite_balken_l']; } else {};

    if (isset($_GET['Laenge_Oeffnung_Lges'])) {$get_Laenge_Oeffnung_Lges = $_GET['Laenge_Oeffnung_Lges']; } else {};
    if (isset($_GET['anzahl_oeffnungen'])) {$get_anzahl_oeffnungen = $_GET['anzahl_oeffnungen']; } else {};

    if (isset($_GET['Laenge_der_Einzeloeffnungen_L1'])) {$get_Laenge_der_Einzeloeffnungen_L1 = $_GET['Laenge_der_Einzeloeffnungen_L1']; } else {};
    if (isset($_GET['Laenge_der_Einzeloeffnungen_L2'])) {$get_Laenge_der_Einzeloeffnungen_L2 = $_GET['Laenge_der_Einzeloeffnungen_L2']; } else {};
    if (isset($_GET['Laenge_der_Einzeloeffnungen_L3'])) {$get_Laenge_der_Einzeloeffnungen_L3 = $_GET['Laenge_der_Einzeloeffnungen_L3']; } else {};
    if (isset($_GET['Laenge_der_Einzeloeffnungen_L4'])) {$get_Laenge_der_Einzeloeffnungen_L4 = $_GET['Laenge_der_Einzeloeffnungen_L4']; } else {};
    if (isset($_GET['Laenge_der_Einzeloeffnungen_L5'])) {$get_Laenge_der_Einzeloeffnungen_L5 = $_GET['Laenge_der_Einzeloeffnungen_L5']; } else {};

    if (isset($_GET['Belastung_qEd'])) {$get_Belastung_qEd = $_GET['Belastung_qEd']; } else {};
    if (isset($_GET['Med_1'])) {$get_Med_1 = $_GET['Med_1']; } else {};
    if (isset($_GET['Qed_1'])) {$get_Qed_1 = $_GET['Qed_1']; } else {};
    
    if (isset($_GET['Qedmax'])) {$get_Qedmax = $_GET['Qedmax']; } else {};


    if (isset($_GET['Gewicht'])) {$getR_Gewicht = $_GET['Gewicht']; } else {};
    if (isset($_GET['Oberflaeche'])) {$getR_Oberflaeche = $_GET['Oberflaeche']; } else {};
    if (isset($_GET['Lbrutto'])) {$getR_Lbrutto = $_GET['Lbrutto']; } else {};
    if (isset($_GET['Hbrutto'])) {$getR_Hbrutto = $_GET['Hbrutto']; } else {};
    
    if (isset($_GET['Hnetto'])) {$getR_Hnetto = $_GET['Hnetto']; } else {};

    if (isset($_GET['L1netto'])) {$getR_L1netto = $_GET['L1netto']; } else {};
    if (isset($_GET['L2netto'])) {$getR_L2netto = $_GET['L2netto']; } else {};
    if (isset($_GET['L3netto'])) {$getR_L3netto = $_GET['L3netto']; } else {};
    if (isset($_GET['L4netto'])) {$getR_L4netto = $_GET['L4netto']; } else {};
    if (isset($_GET['L5netto'])) {$getR_L5netto = $_GET['L5netto']; } else {};

?>











<div class="m-page-configurator-loader-modal" id="m-page-configurator-loader-modal"></div>
<div class="m-page-configurator-loader" id="m-page-configurator-loader"></div>
<div 
     class="m-page-configurator"
     
     data-pdf="<?php echo $isPDF; ?>"
     
     >
    <div class="container" >
        <?php
            if ($isPDF == true) {    

                
                
                if($languagesKonfigurator == "de"){
                    $pdfHeadline = "KONFIGURATOR";
                };


                if($languagesKonfigurator == "es"){
                    $pdfHeadline = "CONFIGURADOR";
                };


                if($languagesKonfigurator == "en"){
                    $pdfHeadline = "CONFIGURATOR";
                };
   
                
                
        ?>

        <div class="row">
            <div class="col-12">
                <div class="pdfHeader">   
                    <div class="row pdfHeaderBG">  
                        <div class="col-6 offset-1">
                            <div class="pdfHeadline"><?php echo $pdfHeadline; ?></div>
                        </div>            
                        <div class="col-5">
                            <div class="pdfDate"><?php echo $get_Date; ?></div>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
                
        <?php 
            };
        ?>
        
        
        
        <div class="row">
            <div class="col-12">
                <?php if( $headline != "" ){ ?>

                <?php
                    echo "<".$htag." class='headlineSectionConfig headline headlineSection headlineSectionLineCentered'>";
                    echo $headline;
                    echo "</".$htag.">";
                ?>
                <?php }; ?>
            </div>
        </div>
        

        <div class="row">
			<!-- V_1 -->
            <form 
                  class="form_configurator" 
                  id="p-dra-form" 
                  method="POST" 
                  name="dra_form" 
                  action=""
                  
                  
                  
                  >
            <?php
                // Create nonce for the next step
                // Name of Action , Nonce field
                wp_nonce_field( 'inq_form_config', 'inq_form_nonce_config' );
                // Sets the action of this WP $_POST form 
            ?>
            <div class="col-12 col-xl-11 offset-xl-1">
                
               
                
                <?php
                    // ------------------------------ Next Section -----------------;
                ?>      
                

                
                
                
                
                <div class="row rowgap">
                    <div id="form_input_saction" class="col-12">
                        <div class="headlineContainer">
                            <h3 class="headline_title"><?php echo $hintArr["section"]["Basisinformation"]["headline"]; ?></h3>
                            <?php
                                if($hintArr["section"]["Basisinformation"]["subheadline"] != ""){
                            ?>
                                <h4 class="subheadline_title"><?php echo $hintArr["section"]["Basisinformation"]["subheadline"]; ?></h4>
                            <?php
                                };
                            ?>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">

                            <div class="<?php echo $surroundColumns1; ?>">
                                
                                
                                <div class="row">
                                    <div class="<?php echo $columns1; ?>">
                                        <label for="country"><?php echo $hintArr["configurator"]["country"]["label"]; ?><span class="requiredasterix">*</span></label>
                                        <div class="iconhintMobile d-inline-block d-lg-none" data-hint="hint_country"></div>
                                        <div class="hinttextMobile">
                                            <div class="first">Hinweis</div>
                                            <div class="text"><?php echo $hintArr["configurator"]["country"]["hint"]; ?></div>
                                        </div>
                                    </div>
                                    <div class="<?php echo $columns2; ?>">
                                        <div class="form-group inputContainer">
                                            <select class="form-control" id="country" name="country">
                                              <option value="0" disabled selected><?php echo $hintArr["configurator"]["global"]["pleaseselect"]; ?></option>
                                                <?php
                                                    foreach ($beamUpArr as $item) {
                                                        $country = $item["countrylabel"];

                                                        if($get_country == $country){
                                                            $listitem = "<option selected value='".$country."'>".$country."</option>";
                                                        }else{
                                                           $listitem = "<option value='".$country."'>".$country."</option>"; 
                                                        }

                                                        echo $listitem;
                                                    };
                                                ?>
                                            </select>
                                            <div class="iconhintDesktop d-none d-lg-block" data-hint="hint_country">
                                                <div class="hinttext ">
                                                    <div class="first"><?php echo $hintArr["configurator"]["global"]["hintheadline"]; ?></div>
                                                    <div class="text"><?php echo $hintArr["configurator"]["country"]["hint"]; ?></div>
                                                </div>
                                            </div>
                                           <!--<div class="errorMessage">Lorem Ipsum2</div>-->
                                        </div>
                                    </div>
                                </div>
                                
                                
                                <div class="row">
                                    <div class="<?php echo $columns1; ?>">
                                        <label for="plz"><?php echo $hintArr["configurator"]["plz"]["label"]; ?><span class="requiredasterix">*</span></label>
                                        <div class="iconhintMobile d-inline-block d-lg-none" data-hint="hint_plz"></div>
                                        <div class="hinttextMobile ">
                                            <div class="first"><?php echo $hintArr["configurator"]["global"]["hintheadline"]; ?></div>
                                            <div class="text"><?php echo $hintArr["configurator"]["plz"]["hint"]; ?></div>
                                        </div>
                                        
                                    </div>
                                    <div class="<?php echo $columns2; ?>">

                                        <div class="form-group inputContainer">
                                            <input type="text" class="form-control" id="plz" name="plz" placeholder="PLZ" value="<?php echo $get_plz; ?>">
                                            <div class="iconhintDesktop d-none d-lg-block" data-hint="hint_plz">
                                                <div class="hinttext">
                                                    <div class="first"><?php echo $hintArr["configurator"]["global"]["hintheadline"]; ?></div>
                                                    <div class="text"><?php echo $hintArr["configurator"]["plz"]["hint"]; ?></div>
                                                </div>
                                            </div>
                                            <!--<div class="errorMessage">Lorem Ipsum</div>-->
                                        </div>

                                    </div>
                                </div>
                                
                                
                                <div class="row">
                                    <div class="<?php echo $columns1; ?>">
                                        <label for="einsatzort"><?php echo $hintArr["configurator"]["einsatzort"]["label"]; ?><span class="requiredasterix">*</span></label>
                                        <div class="iconhintMobile d-inline-block d-lg-none" data-hint="hint_einsatzort"></div>
                                        <div class="hinttextMobile ">
                                            <div class="first"><?php echo $hintArr["configurator"]["global"]["hintheadline"]; ?></div>
                                            <div class="text"><?php echo $hintArr["configurator"]["einsatzort"]["hint"]; ?></div>
                                        </div>
                                        
                                    </div>
                                    <div class="<?php echo $columns2; ?>">

                                        <div class="form-group inputContainer">
                                            <input type="text" class="form-control" id="einsatzort" name="einsatzort" placeholder="Einsatzort" value="<?php echo $get_einsatzort; ?>">
                                            <div class="iconhintDesktop d-none d-lg-block" data-hint="hint_einsatzort">
                                                <div class="hinttext">
                                                    <div class="first"><?php echo $hintArr["configurator"]["global"]["hintheadline"]; ?></div>
                                                    <div class="text"><?php echo $hintArr["configurator"]["einsatzort"]["hint"]; ?></div>
                                                </div>
                                            </div>
                                            <!--<div class="errorMessage">Lorem Ipsum</div>-->
                                        </div>

                                    </div>
                                </div>
                                
                                
                                <div class="row">
                                    <div class="<?php echo $columns1; ?>">
                                        <label for="positionsnumber"><?php echo $hintArr["configurator"]["positionsnumber"]["label"]; ?><span class="requiredasterix">*</span></label>
                                        <div class="iconhintMobile d-inline-block d-lg-none" data-hint="hint_country"></div>
                                        <div class="hinttextMobile ">
                                            <div class="first"><?php echo $hintArr["configurator"]["global"]["hintheadline"]; ?></div>
                                            <div class="text"><?php echo $hintArr["configurator"]["positionsnumber"]["hint"]; ?></div>
                                        </div>
                                        
                                    </div>
                                    <div class="<?php echo $columns2; ?>">

                                        <div class="form-group inputContainer">
                                            <input type="text" class="form-control" id="positionsnumber" name="positionsnumber" placeholder="Positionsnummer" value="<?php echo $get_positionsnumber ; ?>">
                                            <div class="iconhintDesktop d-none d-lg-block" data-hint="hint_positionsnumber">
                                                <div class="hinttext">
                                                    <div class="first"><?php echo $hintArr["configurator"]["global"]["hintheadline"]; ?></div>
                                                    <div class="text"><?php echo $hintArr["configurator"]["positionsnumber"]["hint"]; ?></div>
                                                </div>
                                            </div>
                                            <!--<div class="errorMessage">Lorem Ipsum</div>-->
                                        </div>

                                    </div>
                                </div>
                                
                                
                                

                                
                                

                            </div>

                            <div class="<?php echo $surroundColumns2; ?>">
                                <!-- no image -->
                            </div>
                            
                            
                        </div>
                    </div>
                </div>
                
                
  
                <?php
                    // ------------------------------ Next Section -----------------;
                ?>
                
               
                
                <div class="row rowgap">
                    <div class="col-12">
                        <div class="headlineContainer">
                            <h3 class="headline_title"><?php echo $hintArr["section"]["Decke"]["headline"]; ?></h3>
                            <?php
                                if($hintArr["section"]["Decke"]["subheadline"] != ""){
                            ?>
                                <h4 class="subheadline_title"><?php echo $hintArr["section"]["Decke"]["subheadline"]; ?></h4>
                            <?php
                                };
                            ?>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">

                            <div class="<?php echo $surroundColumns1; ?>">
                                
                                
                                <div class="row">
                                    <div class="<?php echo $columns1; ?>">
                                        <label for="Deckentyp"><?php echo $hintArr["configurator"]["deckentyp"]["label"]; ?><span class="requiredasterix">*</span></label>
                                        <div class="iconhintMobile d-inline-block d-lg-none" data-hint="hint_Deckentyp"></div>
                                        <div class="hinttextMobile">
                                            <div class="first"><?php echo $hintArr["configurator"]["global"]["hintheadline"]; ?></div>
                                            <div class="text"><?php echo $hintArr["configurator"]["deckentyp"]["hint"]; ?></div>
                                        </div>
                                    </div>
                                    <div class="<?php echo $columns2; ?>">
                                        <div class="form-group inputContainer">
                                            <select class="form-control" id="Deckentyp" name="Deckentyp">
                                                <option value="0" selected><?php echo $hintArr["configurator"]["global"]["pleaseselect"]; ?></option>

                                                <?php

                                                    if($languagesKonfigurator == "de"){
                                                        
                                                        $deckenytpArr  = array();
                                                        $deckenytpArr[0]["value"] = 1;
                                                        $deckenytpArr[0]["text"] = "Spannbeton-Hohldecken";

                                                        $deckenytpArr[1]["value"] = 2;
                                                        $deckenytpArr[1]["text"] = "Ortbetondecke";

                                                        $deckenytpArr[2]["value"] = 3;
                                                        $deckenytpArr[2]["text"] = "Elementdecke";

                                                        foreach ($deckenytpArr as $item) {
                                                            $val = $item["value"];
                                                            $text = $item["text"];
                                                            if($get_deckentyp == $val){
                                                                 echo '<option selected="selected" value="'.$val.'">'.$text.'</option>';
                                                            }else{
                                                                echo '<option value="'.$val.'">'.$text.'</option>'; 
                                                            }
                                                        };

                                                    };
                                                ?>
                                                
                                                
                                                
                                                <?php

                                                    if($languagesKonfigurator == "es"){
                                                        
                                                        $deckenytpArr  = array();
                                                        $deckenytpArr[0]["value"] = 1;
                                                        $deckenytpArr[0]["text"] = "Techo hueco de hormigón pretensado";

                                                        $deckenytpArr[1]["value"] = 2;
                                                        $deckenytpArr[1]["text"] = "Techo de hormigón in situ";

                                                        $deckenytpArr[2]["value"] = 3;
                                                        $deckenytpArr[2]["text"] = "Techo de elementos";

                                                        foreach ($deckenytpArr as $item) {
                                                            $val = $item["value"];
                                                            $text = $item["text"];
                                                            if($get_deckentyp == $val){
                                                                 echo '<option selected="selected" value="'.$val.'">'.$text.'</option>';
                                                            }else{
                                                                echo '<option value="'.$val.'">'.$text.'</option>'; 
                                                            }
                                                        };
                                                     };

                                                ?>   
                                                
                                                
                                                <?php

                                                    if($languagesKonfigurator == "en"){
                                                        
                                                        $deckenytpArr  = array();
                                                        $deckenytpArr[0]["value"] = 1;
                                                        $deckenytpArr[0]["text"] = "Prestressed concrete hollow ceiling";

                                                        $deckenytpArr[1]["value"] = 2;
                                                        $deckenytpArr[1]["text"] = "In-situ concrete ceiling";

                                                        $deckenytpArr[2]["value"] = 3;
                                                        $deckenytpArr[2]["text"] = "Element ceiling";

                                                        foreach ($deckenytpArr as $item) {
                                                            $val = $item["value"];
                                                            $text = $item["text"];
                                                            if($get_deckentyp == $val){
                                                                 echo '<option selected="selected" value="'.$val.'">'.$text.'</option>';
                                                            }else{
                                                                echo '<option value="'.$val.'">'.$text.'</option>'; 
                                                            }
                                                        };
                                                     };

                                                ?> 
                                                
                         
                                                
                                                
                                            </select>
                                              
                                            <div class="iconhintDesktop d-none d-lg-block" data-hint="hint_Deckentyp">
                                                <div class="hinttext">
                                                    <div class="first"><?php echo $hintArr["configurator"]["global"]["hintheadline"]; ?></div>
                                                    <div class="text"><?php echo $hintArr["configurator"]["deckentyp"]["hint"]; ?></div>
                                                </div>                                     
                                            </div>
                                            <div class="p-form-fields-errors-output-wrapper errorMessage" id="Deckentyp_errors"></div>
                                        </div>
                                    </div>
                                </div>
                                
                                
                                <div class="row">
                                    <div class="<?php echo $columns1; ?>">
                                        <label for="deckenhoehe"><?php echo $hintArr["configurator"]["deckenhoehe"]["label"]; ?><span class="requiredasterix">*</span></label>
                                        <div class="iconhintMobile d-inline-block d-lg-none" data-hint="hint_country"></div>
                                        <div class="hinttextMobile">
                                            <div class="first"><?php echo $hintArr["configurator"]["global"]["hintheadline"]; ?></div>
                                            <div class="text"><?php echo $hintArr["configurator"]["deckenhoehe"]["hint"]; ?></div>
                                        </div>
                                    </div>
                                    <div class="<?php echo $columns2; ?>">

                                        <div class="form-group inputContainer">
                                            <input type="text" class="form-control unitform " id="Deckenhoehe_hc" placeholder="" name="Deckenhoehe_hc" value="<?php echo $get_deckenhoehe_hc ; ?>">
                                            <div class="unit">(cm)</div>
                                            <div class="iconhintDesktop d-none d-lg-block" data-hint="hint_deckenhoehe">
                                                <div class="hinttext">
                                                    <div class="first"><?php echo $hintArr["configurator"]["global"]["hintheadline"]; ?></div>
                                                    <div class="text"><?php echo $hintArr["configurator"]["deckenhoehe"]["hint"]; ?></div>
                                                </div>
                                                
                                            </div>
                                            <!--<div class="errorMessage">Lorem Ipsum</div>-->
                                            <div class="p-form-fields-errors-output-wrapper errorMessage" id="Deckenhoehe_hc_errors"></div>
                                        </div>
                                        
                                        

                                    </div>
                                </div>
                                
                                
                                
                                
                                
                                
                                <!-- Nur bei deckentyp 2,3, bei 1 disabled-->
                                <div class="row" style="display: none; ">
                                    <div class="<?php echo $columns1; ?>">
                                        <label for="spannweite_decke_ld1"><?php echo $hintArr["configurator"]["spannweite_decke_ld1"]["label"]; ?><span class="requiredasterix">*</span></label>
                                        <div class="iconhintMobile d-inline-block d-lg-none" data-hint="hint_spannweite_decke_ld1"></div>
                                        <div class="hinttextMobile">
                                            <div class="first"><?php echo $hintArr["configurator"]["global"]["hintheadline"]; ?></div>
                                            <div class="text"><?php echo $hintArr["configurator"]["spannweite_decke_ld1"]["hint"]; ?></div>
                                        </div>
                                    </div>
                                    <div class="<?php echo $columns2; ?>">
                                        <div class="form-group inputContainer">
                                            <input type="text" class="form-control unitform" id="spannweite_decke_ld1" placeholder="" name="spannweite_decke_ld1" disabled value="<?php echo $get_spannweite_decke_ld1; ?>">
                                            <div class="unit">(m)</div>
                                            <div class="iconhintDesktop d-none d-lg-block" data-hint="hint_spannweite_decke_ld1">
                                                <div class="hinttext">
                                                    <div class="first"><?php echo $hintArr["configurator"]["global"]["hintheadline"]; ?></div>
                                                    <div class="text"><?php echo $hintArr["configurator"]["spannweite_decke_ld1"]["hint"]; ?></div>
                                                </div>
                                            </div>
                                            <!--<div class="errorMessage">Lorem Ipsum</div>-->
											<!-- V_2 -->
											<div class="p-form-fields-errors-output-wrapper errorMessage" id="Spannweite_Decke_LD1_errors"></div>
                                        </div>
                                    </div>
                                </div>
                                
                                
                                
                                
                                
                                <!-- Nur bei deckentyp 2,3, bei 1 disabled-->
                                <div class="row" style="display: none; ">
                                    <div class="<?php echo $columns1; ?>">
                                        <label for="spannweite_decke_ld2"><?php echo $hintArr["configurator"]["spannweite_decke_ld2"]["label"]; ?><span class="requiredasterix">*</span></label>
                                        <div class="iconhintMobile d-inline-block d-lg-none" data-hint="hint_spannweite_decke_ld2"></div>
                                        <div class="hinttextMobile">
                                            <div class="first"><?php echo $hintArr["configurator"]["global"]["hintheadline"]; ?></div>
                                            <div class="text"><?php echo $hintArr["configurator"]["spannweite_decke_ld2"]["hint"]; ?></div>
                                        </div>
                                    </div>
                                    <div class="<?php echo $columns2; ?>">
                                        <div class="form-group inputContainer">
                                            <input type="text" class="form-control unitform" id="spannweite_decke_ld2" placeholder="" name="spannweite_decke_ld2" disabled value="<?php echo $get_spannweite_decke_ld2; ?>" >
                                            <div class="unit">(m)</div>
                                            <div class="iconhintDesktop d-none d-lg-block" data-hint="hint_spannweite_decke_ld2">
                                                <div class="hinttext">
                                                    <div class="first"><?php echo $hintArr["configurator"]["global"]["hintheadline"]; ?></div>
                                                    <div class="text"><?php echo $hintArr["configurator"]["spannweite_decke_ld2"]["hint"]; ?></div>
                                                </div>
                                            </div>
                                            <!--<div class="errorMessage">Lorem Ipsum</div>-->
											<!-- V_3 -->
											<div class="p-form-fields-errors-output-wrapper errorMessage" id="Spannweite_Decke_LD2_errors"></div>
                                        </div>
                                    </div>
                                </div>
                                
                                
                                
                                
                                

                            </div>

                            <div class="<?php echo $surroundColumns2; ?>">
                                <!-- no image -->
                            </div>
                            
                            
                        </div>
                    </div>
                </div>
                
                
                
                
                
                
                
                
                <?php
                    // ------------------------------ Next Section -----------------;
                ?>
                
                <div style="page-break-before:always"></div>
                
                <?php
                    if (  $isPDF == true ) {
                        echo "<div style='display: block; height: 2cm; width: 100%; background-color: white;'></div>";
                    }
                
                ?>
             
                   
                
                
                
                <div class="row rowgap">
                    <div class="col-12">
                        <div class="headlineContainer">
                            <h3 class="headline_title"><?php echo $hintArr["section"]["Betonbalken"]["headline"]; ?></h3>
                            <?php
                                if($hintArr["section"]["Betonbalken"]["subheadline"] != ""){
                            ?>
                                <h4 class="subheadline_title"><?php echo $hintArr["section"]["Betonbalken"]["subheadline"]; ?></h4>
                            <?php
                                };
                            ?>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="<?php echo $surroundColumns1; ?>">
                                
                                
                                <div class="row">
                                    <div class="<?php echo $columns1; ?>">
                                        <label for="festigkeitsklasse"><?php echo $hintArr["configurator"]["festigkeitsklasse"]["label"]; ?><span class="requiredasterix">*</span></label>
                                        <div class="iconhintMobile d-inline-block d-lg-none" data-hint="hint_country"></div>
                                        <div class="hinttextMobile">
                                            <div class="first"><?php echo $hintArr["configurator"]["global"]["hintheadline"]; ?></div>
                                            <div class="text"><?php echo $hintArr["configurator"]["festigkeitsklasse"]["hint"]; ?></div>
                                        </div>
                                    </div>
                                    <div class="<?php echo $columns2; ?>">
                                        <div class="form-group inputContainer">
                                            <select class="form-control" id="Festigkeitsklasse_Beton" name="Festigkeitsklasse_Beton">
                                                <option value="0" selected><?php echo $hintArr["configurator"]["global"]["pleaseselect"]; ?></option>

                                                <?php
 
                                                    $deckenytpArr  = array();
                                                    $deckenytpArr[0]["value"] = 1;
                                                    $deckenytpArr[0]["text"] = "C20/25";

                                                    $deckenytpArr[1]["value"] = 2;
                                                    $deckenytpArr[1]["text"] = "C25/30";

                                                    $deckenytpArr[2]["value"] = 3;
                                                    $deckenytpArr[2]["text"] = "C30/37";

                                                    $deckenytpArr[3]["value"] = 4;
                                                    $deckenytpArr[3]["text"] = "C35/45";

                                                    $deckenytpArr[4]["value"] = 5;
                                                    $deckenytpArr[4]["text"] = "C40/50";

                                                    $deckenytpArr[5]["value"] = 6;
                                                    $deckenytpArr[5]["text"] = "C45/55";

                                                    $deckenytpArr[6]["value"] = 7;
                                                    $deckenytpArr[6]["text"] = "C50/60";


                                                    foreach ($deckenytpArr as $item) {
                                                        $val = $item["value"];
                                                        $text = $item["text"];
                                                        if($get_festigkeitsklasse_Beton == $val){
                                                             echo '<option selected="selected" value="'.$val.'">'.$text.'</option>';
                                                        }else{
                                                            echo '<option value="'.$val.'">'.$text.'</option>'; 
                                                        }
                                                    };

                                                ?>

                                            </select>
                                            <div class="iconhintDesktop d-none d-lg-block" data-hint="hint_festigkeitsklasse">
                                                <div class="hinttext">
                                                    <div class="first"><?php echo $hintArr["configurator"]["global"]["hintheadline"]; ?></div>
                                                    <div class="text"><?php echo $hintArr["configurator"]["festigkeitsklasse"]["hint"]; ?></div>
                                                </div>
                                            </div>
                                            <!--<div class="errorMessage">Lorem Ipsum</div>-->
											<!-- V_4 -->
											<div class="p-form-fields-errors-output-wrapper errorMessage" id="Festigkeitsklasse_Beton_errors"></div>
                                        </div>
                                    </div>
                                </div>
                                
                                

                                <div class="row">
                                    <div class="<?php echo $columns1; ?>">
                                        <label for="expositionsklasse"><?php echo $hintArr["configurator"]["expositionsklasse"]["label"]; ?><span class="requiredasterix">*</span></label>
                                        <div class="iconhintMobile d-inline-block d-lg-none" data-hint="hint_country"></div>
                                        <div class="hinttextMobile">
                                            <div class="first"><?php echo $hintArr["configurator"]["global"]["hintheadline"]; ?></div>
                                            <div class="text"><?php echo $hintArr["configurator"]["expositionsklasse"]["hint"]; ?></div>
                                        </div>
                                    </div>
                                    <div class="<?php echo $columns2; ?>">
                                        <div class="form-group inputContainer">
                                            <select class="form-control" id="Expositionsklasse" name="Expositionsklasse">
                                                <option value="0" selected><?php echo $hintArr["configurator"]["global"]["pleaseselect"]; ?></option>

                                                <option value="1">XC1</option>
                                                <option value="2">XC2/XC3</option>
                                                
                                                <?php

                                                    $deckenytpArr  = array();
                                                    $deckenytpArr[0]["value"] = 1;
                                                    $deckenytpArr[0]["text"] = "XC1";

                                                    $deckenytpArr[1]["value"] = 2;
                                                    $deckenytpArr[1]["text"] = "XC2/XC3";

                                                    foreach ($deckenytpArr as $item) {
                                                        $val = $item["value"];
                                                        $text = $item["text"];
                                                        if($get_expositionsklasse == $val){
                                                             echo '<option selected="selected" value="'.$val.'">'.$text.'</option>';
                                                        }else{
                                                            echo '<option value="'.$val.'">'.$text.'</option>'; 
                                                        }
                                                    };
                                                                        
                                                ?>
                                                
                                                
                                                
                                            </select>
                                            <div class="iconhintDesktop d-none d-lg-block" data-hint="hint_expositionsklasse">
                                                <div class="hinttext">
                                                    <div class="first"><?php echo $hintArr["configurator"]["global"]["hintheadline"]; ?></div>
                                                    <div class="text"><?php echo $hintArr["configurator"]["expositionsklasse"]["hint"]; ?></div>
                                                </div>
                                            </div>
                                            <!--<div class="errorMessage">Lorem Ipsum</div>-->
											<!-- V_5 -->
											<div class="p-form-fields-errors-output-wrapper errorMessage" id="Expositionsklasse_errors"></div>
                                        </div>
                                    </div>
                                </div>
                                
                                
                                <div class="row">
                                    <div class="<?php echo $columns1; ?>">
                                        <label for="feuerwiderstandsklasse"><?php echo $hintArr["configurator"]["feuerwiderstandsklasse"]["label"]; ?><span class="requiredasterix">*</span></label>
                                        <div class="iconhintMobile d-inline-block d-lg-none" data-hint="hint_country"></div>
                                        <div class="hinttextMobile">
                                            <div class="first"><?php echo $hintArr["configurator"]["global"]["hintheadline"]; ?></div>
                                            <div class="text"><?php echo $hintArr["configurator"]["feuerwiderstandsklasse"]["hint"]; ?></div>
                                        </div>
                                    </div>
                                    <div class="<?php echo $columns2; ?>">
                                        <div class="form-group inputContainer">
                                            <select class="form-control" id="feuerwiderstandsklasse" name="feuerwiderstandsklasse">
                                                <option value="0" selected><?php echo $hintArr["configurator"]["global"]["pleaseselect"]; ?></option>
                     

                                                
                                                
                                                <?php
                                                    if($languagesKonfigurator == "de"){

                                                        $deckenytpArr  = array();
                                                        $deckenytpArr[0]["value"] = 1;
                                                        $deckenytpArr[0]["text"] = "F0";

                                                        $deckenytpArr[1]["value"] = 2;
                                                        $deckenytpArr[1]["text"] = "F30";

                                                        $deckenytpArr[2]["value"] = 3;
                                                        $deckenytpArr[2]["text"] = "F60";
                                                        
                                                        $deckenytpArr[2]["value"] = 3;
                                                        $deckenytpArr[2]["text"] = "F90";

                                                        foreach ($deckenytpArr as $item) {
                                                            $val = $item["value"];
                                                            $text = $item["text"];
                                                            if($get_feuerwiderstandsklasse == $val){
                                                                 echo '<option selected="selected" value="'.$val.'">'.$text.'</option>';
                                                            }else{
                                                                echo '<option value="'.$val.'">'.$text.'</option>'; 
                                                            }
                                                        };
                                                     };
                                                 ?>                                   
                                                
                                                
                                                
                                                
                                                
                                                <?php
                                                    if($languagesKonfigurator == "es"){

                                                        $deckenytpArr  = array();
                                                        $deckenytpArr[0]["value"] = 1;
                                                        $deckenytpArr[0]["text"] = "R0";

                                                        $deckenytpArr[1]["value"] = 2;
                                                        $deckenytpArr[1]["text"] = "R30";

                                                        $deckenytpArr[2]["value"] = 3;
                                                        $deckenytpArr[2]["text"] = "R60";
                                                        
                                                        $deckenytpArr[2]["value"] = 3;
                                                        $deckenytpArr[2]["text"] = "R90";

                                                        foreach ($deckenytpArr as $item) {
                                                            $val = $item["value"];
                                                            $text = $item["text"];
                                                            if($get_feuerwiderstandsklasse == $val){
                                                                 echo '<option selected="selected" value="'.$val.'">'.$text.'</option>';
                                                            }else{
                                                                echo '<option value="'.$val.'">'.$text.'</option>'; 
                                                            }
                                                        };
                                                     };
                                                 ?>
                                                
                                                
                                                <?php
                                                    if($languagesKonfigurator == "en"){

                                                        $deckenytpArr  = array();
                                                        $deckenytpArr[0]["value"] = 1;
                                                        $deckenytpArr[0]["text"] = "R0";

                                                        $deckenytpArr[1]["value"] = 2;
                                                        $deckenytpArr[1]["text"] = "R30";

                                                        $deckenytpArr[2]["value"] = 3;
                                                        $deckenytpArr[2]["text"] = "R60";
                                                        
                                                        $deckenytpArr[2]["value"] = 3;
                                                        $deckenytpArr[2]["text"] = "R90";

                                                        foreach ($deckenytpArr as $item) {
                                                            $val = $item["value"];
                                                            $text = $item["text"];
                                                            if($get_feuerwiderstandsklasse == $val){
                                                                 echo '<option selected="selected" value="'.$val.'">'.$text.'</option>';
                                                            }else{
                                                                echo '<option value="'.$val.'">'.$text.'</option>'; 
                                                            }
                                                        };
                                                     };
                                                 ?>                        
                                                

                                                
                                            </select>
                                            <div class="iconhintDesktop d-none d-lg-block" data-hint="hint_feuerwiderstandsklasse">
                                                <div class="hinttext">
                                                    <div class="first"><?php echo $hintArr["configurator"]["global"]["hintheadline"]; ?></div>
                                                    <div class="text"><?php echo $hintArr["configurator"]["feuerwiderstandsklasse"]["hint"]; ?></div>
                                                </div>
                                            </div>
                                            <!--<div class="errorMessage">Lorem Ipsum</div>-->
											<!-- V_6 -->
											<div class="p-form-fields-errors-output-wrapper errorMessage" id="Feuerwiderstandsklasse_errors"></div>
                                        </div>
                                    </div>
                                </div>
                                

                                <div class="row">
                                    <div class="<?php echo $columns1; ?>">
                                        <label for="breiteb"><?php echo $hintArr["configurator"]["breiteb"]["label"]; ?><span class="requiredasterix">*</span></label>
                                        <div class="iconhintMobile d-inline-block d-lg-none" data-hint="hint_country"></div>
                                        <div class="hinttextMobile">
                                            <div class="first"><?php echo $hintArr["configurator"]["global"]["hintheadline"]; ?></div>
                                            <div class="text"><?php echo $hintArr["configurator"]["breiteb"]["hint"]; ?></div>
                                        </div>
                                    </div>
                                    <div class="<?php echo $columns2; ?>">
                                        <div class="form-group inputContainer">
                                            <input type="text" class="form-control unitform " id="Breite_Betonbalken" placeholder="" name="Breite_Betonbalken" value="<?php echo $get_breite_Betonbalken ; ?>">
                                            <div class="unit">(cm)</div>
                                            
                                            <div class="iconhintDesktop d-none d-lg-block" data-hint="hint_breiteb">
                                                <div class="hinttext">
                                                    <div class="first"><?php echo $hintArr["configurator"]["global"]["hintheadline"]; ?></div>
                                                    <div class="text"><?php echo $hintArr["configurator"]["breiteb"]["hint"]; ?></div>
                                                </div>
                                            </div>
                                            <div class="p-form-fields-errors-output-wrapper errorMessage" id="Breite_Betonbalken_errors"></div>
                                        </div>
                                    </div>
                                </div>

                               <div class="row">
                                    <div class="<?php echo $columns1; ?>">
                                        <label for="hoehehges"><?php echo $hintArr["configurator"]["hoehehges"]["label"]; ?><span class="requiredasterix">*</span></label>
                                        <div class="iconhintMobile d-inline-block d-lg-none" data-hint="hint_country"></div>
                                        <div class="hinttextMobile">
                                            <div class="first"><?php echo $hintArr["configurator"]["global"]["hintheadline"]; ?></div>
                                            <div class="text"><?php echo $hintArr["configurator"]["hoehehges"]["hint"]; ?></div>
                                        </div>
                                    </div>
                                    <div class="<?php echo $columns2; ?>">
                                        <div class="form-group inputContainer">
                                            <input type="text" class="form-control unitform" id="Hoehe_hges" placeholder="" name="Hoehe_hges" value="<?php echo $get_hoehe_hges ; ?>">
                                            
                                            
                                            <div class="unit">(cm)</div>
                                            <div class="iconhintDesktop d-none d-lg-block" data-hint="hint_hoehehges">
                                                <div class="hinttext">
                                                    <div class="first"><?php echo $hintArr["configurator"]["global"]["hintheadline"]; ?></div>
                                                    <div class="text"><?php echo $hintArr["configurator"]["hoehehges"]["hint"]; ?></div>
                                                </div>
                                            </div>
                                            <div class="p-form-fields-errors-output-wrapper errorMessage" id="Hoehe_hges_errors"></div>
                                        </div>
                                    </div>
                                </div>     

                                
                                
                               <!-- Nur bei deckentyp 2,3, bei 1 disabled-->
                               <div class="row" style="display: none;">
                                    <div class="<?php echo $columns1; ?>">
                                        <label for="spannweite_balken_l"><?php echo $hintArr["configurator"]["spannweite_balken_l"]["label"]; ?><span class="requiredasterix">*</span></label>
                                        <div class="iconhintMobile d-inline-block d-lg-none" data-hint="hint_spannweite_balken_l"></div>
                                        <div class="hinttextMobile">
                                            <div class="first"><?php echo $hintArr["configurator"]["global"]["hintheadline"]; ?></div>
                                            <div class="text"><?php echo $hintArr["configurator"]["spannweite_balken_l"]["hint"]; ?></div>
                                        </div>
                                    </div>
                                    <div class="<?php echo $columns2; ?>">
                                        <div class="form-group inputContainer">
                                            <input type="text" class="form-control unitform" id="spannweite_balken_l" placeholder="" name="spannweite_balken_l" disabled value="<?php echo $get_spannweite_balken_l; ?>">
                                            <div class="unit">(m)</div>
                                            <div class="iconhintDesktop d-none d-lg-block" data-hint="hint_hoehehges">
                                                <div class="hinttext">
                                                    <div class="first"><?php echo $hintArr["configurator"]["global"]["hintheadline"]; ?></div>
                                                    <div class="text"><?php echo $hintArr["configurator"]["spannweite_balken_l"]["hint"]; ?></div>
                                                </div>
                                            </div>
                                            <!--<div class="errorMessage">Lorem Ipsum</div>-->
											<!-- V_7 -->
											<div class="p-form-fields-errors-output-wrapper errorMessage" id="Spannweite_Balken_L_errors"></div>
                                        </div>
                                    </div>
                                </div>  
                                
                                
                                
                                
 
                            </div>

                            <div class="<?php echo $surroundColumns2; ?>">
                                <div class="graphContainer">
                                    
                                    <?php echo wp_get_attachment_image($image_betonbalken, 'col-md', "", array( "class" => "graph"));?>

                                </div>
                            </div>
                          
                        </div>  

                    </div>
                </div>
                <?php
                    // ------------------------------ Next Section -----------------;
                ?>
                
                
                <div style="page-break-before:always"></div>
                
                                
                <?php
                    if (   $isPDF == true ) {
                        echo "<div style='display: block; height: 2cm; width: 100%; background-color: white;'></div>";
                    }
                
                ?>
                
                
                <div class="row rowgap">
                    <div class="col-12">
                        <div class="headlineContainer">
                            <h3 class="headline_title"><?php echo $hintArr["section"]["Oeffnungen"]["headline"]; ?></h3>
                            <?php
                                if($hintArr["section"]["Oeffnungen"]["subheadline"] != ""){
                            ?>
                                <h4 class="subheadline_title"><?php echo $hintArr["section"]["Oeffnungen"]["subheadline"]; ?></h4>
                            <?php
                                };
                            ?>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            
                            
                            <div class="<?php echo $surroundColumns1; ?>">
                                
                                <div class="row">
                                    <div class="<?php echo $columns1; ?>">
                                        <label for="laenge_oeffnung"><?php echo $hintArr["configurator"]["laenge_oeffnung"]["label"]; ?><span class="requiredasterix">*</span></label>
                                        <div class="iconhintMobile d-inline-block d-lg-none" data-hint="hint_country"></div>
                                        <div class="hinttextMobile">
                                            <div class="first">Hinweis</div>
                                            <div class="text"><?php echo $hintArr["configurator"]["laenge_oeffnung"]["hint"]; ?></div>
                                        </div>
                                    </div>
                                    <div class="<?php echo $columns2; ?>">
                                        <div class="form-group inputContainer">
                                            <input type="text" class="form-control unitform" id="Laenge_Oeffnung_Lges" placeholder="" name="Laenge_Oeffnung_Lges" value="<?php echo $get_Laenge_Oeffnung_Lges ; ?>">
                                            
                                            <div class="unit">(m)</div>
                                            
                                            <div class="iconhintDesktop d-none d-lg-block" data-hint="hint_laenge_oeffnung">
                                                <div class="hinttext">
                                                    <div class="first"><?php echo $hintArr["configurator"]["global"]["hintheadline"]; ?></div>
                                                    <div class="text"><?php echo $hintArr["configurator"]["laenge_oeffnung"]["hint"]; ?></div>
                                                </div>
                                            </div>
                                            <div class="p-form-fields-errors-output-wrapper errorMessage" id="Laenge_Oeffnung_Lges_errors"></div>
                                        </div>
                                    </div>
                                </div>

                               
                                <div class="row">
                                    <div class="<?php echo $columns1; ?>">
                                        <label for="anzahl_oeffnungen"><?php echo $hintArr["configurator"]["anzahl_oeffnung"]["label"]; ?><span class="requiredasterix">*</span></label>
                                        <div class="iconhintMobile d-inline-block d-lg-none" data-hint="hint_country"></div>
                                        <div class="hinttextMobile">
                                            <div class="first"><?php echo $hintArr["configurator"]["global"]["hintheadline"]; ?></div>
                                            <div class="text"><?php echo $hintArr["configurator"]["anzahl_oeffnung"]["hint"]; ?></div>
                                        </div>                                       
                                    </div>
                                    <div class="<?php echo $columns2; ?>">
                                        <div class="form-group inputContainer">
                                            <select class="form-control" id="anzahl_oeffnungen" name="anzahl_oeffnungen">
                                                <option value="0" selected><?php echo $hintArr["configurator"]["global"]["pleaseselect"]; ?></option>
              
                                                
                                                <?php
                                                    

                                                        $deckenytpArr  = array();
                                                        $deckenytpArr[0]["value"] = 1;
                                                        $deckenytpArr[0]["text"] = "1";

                                                        $deckenytpArr[1]["value"] = 2;
                                                        $deckenytpArr[1]["text"] = "2";

                                                        $deckenytpArr[2]["value"] = 3;
                                                        $deckenytpArr[2]["text"] = "3";
      
                                                        $deckenytpArr[3]["value"] = 4;
                                                        $deckenytpArr[3]["text"] = "4";
                                                        
                                                        $deckenytpArr[4]["value"] = 5;
                                                        $deckenytpArr[4]["text"] = "5";

                                                        foreach ($deckenytpArr as $item) {
                                                            $val = $item["value"];
                                                            $text = $item["text"];
                                                            if($get_anzahl_oeffnungen == $val){
                                                                echo '<option selected="selected" value="'.$val.'">'.$text.'</option>';
                                                            }else{
                                                                echo '<option value="'.$val.'">'.$text.'</option>'; 
                                                            }
                                                        };
                                                   
                                                ?>                                   
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                            </select> 
                                            <div class="iconhintDesktop d-none d-lg-block" data-hint="hint_anzahl_oeffnungen">
                                                <div class="hinttext">
                                                    <div class="first"><?php echo $hintArr["configurator"]["global"]["hintheadline"]; ?></div>
                                                    <div class="text"><?php echo $hintArr["configurator"]["anzahl_oeffnung"]["hint"]; ?></div>
                                                </div>
                                            </div>
                                            <div class="p-form-fields-errors-output-wrapper errorMessage" id="Anzahl_der_Oeffnungen_errors"></div>
                                        </div>
                                    </div>
                                </div>
								
								


                                <div class="row" style="display: none; ">
                                    <div class="<?php echo $columns1; ?>">
                                        <label for="laenge_oeffnungen"><?php echo $hintArr["configurator"]["laenge_einzeloeffnung"]["label"]; ?><span class="requiredasterix">*</span></label>
                                        <div class="iconhintMobile d-inline-block d-lg-none" data-hint="hint_country"></div>
                                        <div class="hinttextMobile">
                                            <div class="first"><?php echo $hintArr["configurator"]["global"]["hintheadline"]; ?></div>
                                            <div class="text"><?php echo $hintArr["configurator"]["laenge_einzeloeffnung"]["label"]; ?></div>
                                        </div>
                                    </div>
                                    <div class="<?php echo $columns2; ?>">
                                        <div class="form-group inputContainer">
                                            <ul class="openings"><!--
                                                --><li>
                                                    <div class="desc">L<span class="descsmall">1=</span></div>
                                                    <div class="inputbox"><input type="text" class="form-control" id="Laenge_der_Einzeloeffnungen_L1" placeholder=""  disabled name="Laenge_der_Einzeloeffnungen_L1" value="<?php echo $get_Laenge_der_Einzeloeffnungen_L1 ; ?>"></div>
                                                </li><!--
                                                --><li>
                                                    <div class="desc">L<span class="descsmall">2=</span></div>
                                                    <div class="inputbox"><input type="text" class="form-control" id="Laenge_der_Einzeloeffnungen_L2" placeholder="" disabled name="Laenge_der_Einzeloeffnungen_L2" value="<?php echo $get_Laenge_der_Einzeloeffnungen_L2 ; ?>"></div>
                                                </li><!--
                                                --><li>
                                                    <div class="desc">L<span class="descsmall">3=</span></div>
                                                    <div class="inputbox"><input type="text" class="form-control" id="Laenge_der_Einzeloeffnungen_L3" placeholder=""  disabled name="Laenge_der_Einzeloeffnungen_L3" value="<?php echo $get_Laenge_der_Einzeloeffnungen_L3 ; ?>"></div>
                                                </li><!--
                                                --><li>
                                                    <div class="desc">L<span class="descsmall">4=</span></div>
                                                    <div class="inputbox"><input type="text" class="form-control" id="Laenge_der_Einzeloeffnungen_L4" placeholder=""  disabled name="Laenge_der_Einzeloeffnungen_L4" value="<?php echo $get_Laenge_der_Einzeloeffnungen_L4 ; ?>"></div>
                                                </li><!--
                                                --><li>
                                                    <div class="desc">L<span class="descsmall">5=</span></div>
                                                    <div class="inputbox"><input type="text" class="form-control" id="Laenge_der_Einzeloeffnungen_L5" placeholder=""  disabled name="Laenge_der_Einzeloeffnungen_L5" value="<?php echo $get_Laenge_der_Einzeloeffnungen_L5 ; ?>"></div>
                                                </li><!--

                                                --><li class="uniteinzeln">(m)</li><!--

                                            --></ul>

                                            <div class="iconhintDesktop d-none d-lg-block" data-hint="hint_laenge_oeffnungen">
                                                <div class="hinttext">
                                                    <div class="first"><?php echo $hintArr["configurator"]["global"]["hintheadline"]; ?></div>
                                                    <div class="text"><?php echo $hintArr["configurator"]["laenge_einzeloeffnung"]["hint"]; ?></div>
                                                </div>
                                            </div>
                                            <div class="p-form-fields-errors-output-wrapper errorMessage" id="Laenge_der_Einzeloeffnungen_L1_errors"></div>
                                            <div class="p-form-fields-errors-output-wrapper errorMessage" id="Laenge_der_Einzeloeffnungen_L2_errors"></div>
                                            <div class="p-form-fields-errors-output-wrapper errorMessage" id="Laenge_der_Einzeloeffnungen_L3_errors"></div>
                                            <div class="p-form-fields-errors-output-wrapper errorMessage" id="Laenge_der_Einzeloeffnungen_L4_errors"></div>
                                            <div class="p-form-fields-errors-output-wrapper errorMessage" id="Laenge_der_Einzeloeffnungen_L5_errors"></div>

                                        </div>
                                    </div>
                                </div>   
                                
                                
                                
                                
                                
                            </div>
                            <div class="<?php echo $surroundColumns2; ?>">
                                <div class="graphContainer">
                                   
                                    <?php echo wp_get_attachment_image($image_oeffnungen , 'col-md', "", array( "class" => "graph"));?>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                
                <?php
                    // ------------------------------ Next Section -----------------;
                ?>               

                <div style="page-break-before:always"></div>
                
                                
                <?php
                    if ( $isPDF == true ) {
                        echo "<div style='display: block; height: 2cm; width: 100%; background-color: white;'></div>";
                    }
                
                ?>
                   
                
                <div class="row rowgap">
                    <div class="col-12">
                        <div class="headlineContainer">
                            <h3 class="headline_title"><?php echo $hintArr["section"]["Belastungen"]["headline"]; ?></h3>
                            <?php
                                if($hintArr["section"]["Belastungen"]["subheadline"] != ""){
                            ?>
                                <h4 class="subheadline_title"><?php echo $hintArr["section"]["Belastungen"]["subheadline"]; ?></h4>
                            <?php
                                };
                            ?>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row">
                            <div class="<?php echo $surroundColumns1; ?>">
                                <div class="row">
                                    <div class="<?php echo $columns1; ?>">
                                        <label for="Belastung_qEd"><?php echo $hintArr["configurator"]["qEd"]["label"]; ?><span class="requiredasterix">*</span></label>
                                        <div class="iconhintMobile d-inline-block d-lg-none" data-hint="hint_country"></div>
                                        <div class="hinttextMobile">
                                            <div class="first"><?php echo $hintArr["configurator"]["global"]["hintheadline"]; ?></div>
                                            <div class="text"><?php echo $hintArr["configurator"]["qEd"]["hint"]; ?></div>
                                        </div>
                                    </div>
                                    <div class="<?php echo $columns2; ?>">
                                        <div class="form-group inputContainer">
                                            <input type="text" class="form-control unitform" id="Belastung_qEd" placeholder="" name="Belastung_qEd" value="<?php echo $get_Belastung_qEd ; ?>">
                                            <div class="unit">(kN/m)</div>
                                            
                                            
                                            <div class="iconhintDesktop d-none d-lg-block" data-hint="hint_qEd">
                                                <div class="hinttext">
                                                    <div class="first"><?php echo $hintArr["configurator"]["global"]["hintheadline"]; ?></div>
                                                    <div class="text"><?php echo $hintArr["configurator"]["qEd"]["hint"]; ?></div>
                                                </div>
                                            </div>
                                            <div class="p-form-fields-errors-output-wrapper errorMessage" id="Belastung_qEd_errors"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="<?php echo $columns1; ?>">
                                        <label for="med1"><?php echo $hintArr["configurator"]["med1"]["label"]; ?><span class="requiredasterix">*</span></label>
                                        <div class="iconhintMobile d-inline-block d-lg-none" data-hint="hint_country"></div>
                                        <div class="hinttextMobile">
                                            <div class="first"><?php echo $hintArr["configurator"]["global"]["hintheadline"]; ?></div>
                                            <div class="text"><?php echo $hintArr["configurator"]["med1"]["hint"]; ?></div>
                                        </div>
                                    </div>
                                    <div class="<?php echo $columns2; ?>">
                                        <div class="form-group inputContainer">
                                            <input type="text" class="form-control unitform" id="Med_1" placeholder="" name="Med_1" value="<?php echo $get_Med_1  ; ?>"  >
                                            <div class="unit">(kNm)</div>
                                            
                                            <div class="iconhintDesktop d-none d-lg-block" data-hint="hint_med1">
                                                <div class="hinttext">
                                                    <div class="first"><?php echo $hintArr["configurator"]["global"]["hintheadline"]; ?></div>
                                                    <div class="text"><?php echo $hintArr["configurator"]["med1"]["hint"]; ?></div>
                                                </div>
                                            </div>
                                            <div class="p-form-fields-errors-output-wrapper errorMessage" id="Med_1_errors"></div>
                                        </div>
                                    </div>
                                </div>         
                                <div class="row">
                                    <div class="<?php echo $columns1; ?>">
                                        <label for="qed1"><?php echo $hintArr["configurator"]["ved1"]["label"]; ?><span class="requiredasterix">*</span></label>
                                        <div class="iconhintMobile d-inline-block d-lg-none" data-hint="hint_qed1"></div>
                                        <div class="hinttextMobile">
                                            <div class="first"><?php echo $hintArr["configurator"]["global"]["hintheadline"]; ?></div>
                                            <div class="text"><?php echo $hintArr["configurator"]["ved1"]["hint"]; ?></div>
                                        </div>
                                    </div>
                                    <div class="<?php echo $columns2; ?>">
                                       <div class="form-group inputContainer">
                                            <input type="text" class="form-control unitform" id="Qed_1" placeholder="" name="Qed_1"  value="<?php echo $get_Qed_1  ; ?>"   >
                                            <div class="unit">(kN)</div>
                                           
                                            <div class="iconhintDesktop d-none d-lg-block" data-hint="hint_ved1">
                                                <div class="hinttext">
                                                    <div class="first"><?php echo $hintArr["configurator"]["global"]["hintheadline"]; ?></div>
                                                    <div class="text"><?php echo $hintArr["configurator"]["ved1"]["hint"]; ?></div>
                                                </div>
                                            </div>
                                            <div class="p-form-fields-errors-output-wrapper errorMessage" id="Qed_1_errors"></div>
                                        </div>
                                    </div>
                                </div>     
                                
                                <div class="row">
                                    <div class="<?php echo $columns1; ?>">
                                        <label for="qedmax"><?php echo $hintArr["configurator"]["qedmax"]["label"]; ?><span class="requiredasterix">*</span></label>
                                        <div class="iconhintMobile d-inline-block d-lg-none" data-hint="hint_country"></div>
                                        <div class="hinttextMobile">
                                            <div class="first"><?php echo $hintArr["configurator"]["global"]["hintheadline"]; ?></div>
                                            <div class="text"><?php echo $hintArr["configurator"]["qedmax"]["hint"]; ?></div>
                                        </div>
                                    </div>
                                    <div class="<?php echo $columns2; ?>">
                                        <div class="form-group inputContainer">
                                            <input type="text" class="form-control unitform" id="Qedmax" placeholder="" name="Qedmax"  value="<?php echo $get_Qedmax  ; ?>"  >
                                            <div class="unit">(kN)</div>
                                            
                                            <div class="iconhintDesktop d-none d-lg-block" data-hint="hint_qedmax">
                                                <div class="hinttext">
                                                    <div class="first"><?php echo $hintArr["configurator"]["global"]["hintheadline"]; ?></div>
                                                    <div class="text"><?php echo $hintArr["configurator"]["qedmax"]["hint"]; ?></div>
                                                </div>
                                            </div>
                                            <div class="p-form-fields-errors-output-wrapper errorMessage" id="Qedmax_errors"></div>
                                        </div>
                                    </div>
                                </div>   
                                
                            </div>
                            <div class="<?php echo $surroundColumns2; ?>">
                                <div class="graphContainer">
                                    
                                    <?php echo wp_get_attachment_image($image_belastungen  , 'col-md', "", array( "class" => "graph"));?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                

                <?php
                    // ------------------------------ Next Section -----------------;
                ?>
                

                <?php
                    if ( $isPDF != true ) {
                ?>
               
                <div class="row">
                    <div class="col-12">
                        <input type="hidden" name="language" value="<?php echo ICL_LANGUAGE_CODE; ?>" style="display: none; visibility: hidden; opacity: 0;">
                        <input type="hidden" name="action" value="send_form" style="display: none; visibility: hidden; opacity: 0;">
                        <button type="submit" name="submitconfig" class="submitconfig"><?php _e('configurator_button_calculate', 'neun'); ?></button>
                    </div>
                </div>
                
                <?php
                    };
                ?>

            </div>
            
                
            </form>
            
        </div> 

    </div>
</div>
    


<?php
    if($showAutoFillButton == true){
?>

    <div class="testfill" style="background-color: #13274f; color: white; padding: 3px; width: 100%;
        max-width: 300px;
        margin: 0 auto;
        text-align: center; cursor: pointer;">
        Formular mit Test Daten befüllen
    </div>
<?php
    };
?>




    <div style="page-break-before:always"></div>

    
    <div class="container m-page-configurator-result" id="result">
        <div class="row">
            <div class="col-12" id="output_section">
                    <?php if( $headlineResult != "" ){ ?>
                    <?php
                        echo "<".$htag." class='headlineSectionConfig headline headlineSection headlineSectionLineCentered'>";
                        echo $headlineResult;
                        echo "</".$htag.">";
                    ?>
                    <?php }; ?>
            </div>
        </div>
		
		<div class="row">
            <div class="col-12 text-center">
				<div id="output_Query_time_exceeded"></div>
            </div>
        </div>
		

	
        

		
        <div class="row">

			<div class="col-12 col-lg-6 order-lg-2">
				<div class="resultContainer">
					<ul class="resultList">
                        
						<li style="display: none;">
							<div class="text"><?php echo $ausgabeText0; ?></div>
							<div class="result jsresultid"><span id="ID"></span></div>
						</li>
                        
						<li>
							<div class="text"><?php echo $ausgabeText1; ?></div>
							<div class="result resultunit"><span id="Gewicht"><?php echo $getR_Gewicht; ?></span></div>
                            <div class="unit">(kg)</div>
						</li>
						<li>
							<div class="text"><?php echo $ausgabeText2; ?></div>
							<div class="result resultunit"><span id="Oberflaeche"><?php echo $getR_Oberflaeche; ?></span></div>
                            <div class="unit">(m²)</div>
						</li>
                        
						<li style="display:none;">
							<div class="text"><?php echo $ausgabeText3; ?></div>
							<div class="result resultunit"><span id="Ausnutzungsgrad"></span></div>
                            <div class="unit" style="">%</div>
						</li>
                        
						<li>
							<div class="text"><?php echo $ausgabeText4; ?></div>
							<div class="result resultunit"><span id="Lbrutto"><?php echo $getR_Lbrutto; ?></span></div>
                            <div class="unit" style="">(m)</div>
						</li>
                        
                        
						<li>
							<div class="text"><?php echo $ausgabeText5; ?></div>
							<div class="result resultunit"><span id="Hbrutto"><?php echo $getR_Hbrutto; ?></span></div>
                            <div class="unit" style="">(m)</div>
						</li>
                        

                        
                    
                        
						<li>
							<div class="text"><?php echo $ausgabeText6; ?></div>
							<div class="result resultunit"><span id="L1netto"><?php echo $getR_L1netto; ?></span></div>
                            <div class="unit" style="">(m)</div>
						</li>                       
                        
                        
						<li>
							<div class="text"><?php echo $ausgabeText7; ?></div>
							<div class="result resultunit"><span id="L2netto"><?php echo $getR_L2netto; ?></span></div>
                            <div class="unit" style="">(m)</div>
						</li>                      
                        
          
                        
						<li>
							<div class="text"><?php echo $ausgabeText8; ?></div>
							<div class="result resultunit"><span id="L3netto"><?php echo $getR_L3netto; ?></span></div>
                            <div class="unit" style="">(m)</div>
						</li>                   
                        
                        
                        
						<li>
							<div class="text"><?php echo $ausgabeText9; ?></div>
							<div class="result resultunit"><span id="L4netto"><?php echo $getR_L4netto; ?></span></div>
                            <div class="unit" style="">(m)</div>
						</li>                        
                        
                        
						<li>
							<div class="text"><?php echo $ausgabeText10; ?></div>
							<div class="result resultunit"><span id="L5netto"><?php echo $getR_L5netto; ?></span></div>
                            <div class="unit" style="">(m)</div>
						</li>
                        
                        
                        
                        
						<li>
							<div class="text"><?php echo $ausgabeText11; ?></div>
							<div class="result resultunit"><span id="Hnetto"><?php echo $getR_Hnetto; ?></span></div>
                            <div class="unit" style="">(m)</div>
						</li>
                        
                        
						<li id="Li_Fehlermeldung" class="errorli" style="display:none;">
							<div class="text">Fehlermeldung:</div>
							<div class="result"><span id="Fehlermeldung" style=""></span></div>
						</li>
					</ul>
				</div>
			</div>

			<div class="col-12 col-lg-5 offset-lg-1">
				<div class="resultImageContainer">
				
                    
                    <?php echo wp_get_attachment_image($image_ausgabe   , 'col-md', "", array( "class" => "graph"));?>
                    
				</div>
                
   

			</div>
            
        </div>
        
        
        <?php
            if ($isPDF == true) {    
                $timestamp = time();
                $datum = date("d.m.Y", $timestamp);
                $footerlogoID = get_field("options_footer_contact_logo","option");
                
                $contacttext = get_field('options_footer_adress_single',"option");
                $contacttextPhone = get_field('options_footer_phone_single',"option");
                $contacttextMail = get_field('options_footer_mail_single',"option");
                
        ?>

        <div class="row">
            <div class="col-12">
                <div class="pdfFooter">   
                    <div class="row pdfFooterBG">  
                        
                        <div class="col-2">
                            <div class="logoContainer">
                                <?php echo wp_get_attachment_image($footerlogoID, 'full', "", array( "class" => "footerlogo")); ?>
                            </div>
                        </div>    
                        
                        <div class="col-3">
                            <div class="pdfText">
                                <?php echo $contacttext; ?>
                            </div>
                        </div>  
                        
                        
                        <div class="col-3">
                            <div class="pdfText">
                                <?php echo $contacttextPhone; ?>
                            </div>
                        </div> 
                        
                        
                        <div class="col-3">
                            <div class="pdfText">
                                 <?php echo $contacttextMail; ?>
                            </div>
                        </div> 
                        
                        
                    </div>
                </div>
            </div>
        </div>
                
        <?php 
            };
        ?>
        
        
        <?php
            if ( $isPDF != true ) {
                $pdfButtonTextPrintView = get_field('options_configurator_pdf_modal_buttontext_printview', 'option');
        ?>
        
        <div class="row">
                        <div class="col-12 col-sm-6">
                <div class="resultButtonContainer">
                    <div class="resultButtonPDF"><?php echo $pdfButtonTextPrintView; ?></div>
                </div>
            </div>
            
            <div class="col-12 col-sm-6">
                <div class="resultButtonContainer">
                    <div class="resultButton"><?php echo $ausgabeAnfrageText; ?></div>
                </div>
            </div>
            
            

            


            
        </div>
        
        <?php
            };
        ?>
        
    </div>
    

    
    <?php // start modal contact ?>
    
    <div class="modal fade m-page-configurator-modal-contact"  id="modal_configurator_beamup" tabindex="-1" role="dialog" aria-labelledby="modal_configurator_beamup" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body-form-sended">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12 col-md-10  col-lg-8  col-xl-4 mx-auto">
                                <img class="Confirmationtick" src="<?php echo get_stylesheet_directory_uri(); ?>/images/configurator/Icon_Confirmation-Tick.png">
                                <div class="textbox">
                                    <?php
                                        echo $hintArr["configurator"]["global"]["modal"]["contact"]["text"];
                                    ?>
                                </div>
                                
                                <div class="closeButtonContainer">
                                    <div class="closeResultButton"><?php echo $hintArr["configurator"]["global"]["button"]["close"]; ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-body-form">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12 col-lg-10 mx-auto">
                                <div class="row"> 
                                    <div class="col-12">
                                        <?php
                                            echo "<h2 class='headlinemodal'>";
                                            echo $hintArr["configurator"]["global"]["modal"]["contact"]["headline"];
                                            echo "</h2>";
                                        ?>
                                    </div>
                                    <div class="col-12 col-lg-8 mx-auto">
                                        <div class="subheadline">
                                        <?php
                                            echo $hintArr["configurator"]["global"]["modal"]["contact"]["subheadline"]
                                        ?>
                                        </div>
                                     </div>
                                </div>
                            </div>
                        </div>

                        
              
                        
                        
                        <form id="modalContactForm">
                            <div class="row">
                                <div class="col-12 col-lg-10 offset-lg-1">
                                    <div class="row">
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <label for="firstname"><?php echo $hintArr["form"]["firstname"]["label"]; ?><span class="asterix">*</span></label>
                                                <input type="text" class="form-control" id="firstname"  name="firstname" placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <label for="lastname"><?php echo $hintArr["form"]["lastname"]["label"]; ?><span class="asterix">*</span></label>
                                                <input type="text" class="form-control" id="lastname" name="lastname" placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <label for="firma"><?php echo $hintArr["form"]["firm"]["label"]; ?><span class="asterix">*</span></label>
                                                <input type="text" class="form-control" id="firma" name="firma"  placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <label for="phone"><?php echo $hintArr["form"]["telefon"]["label"]; ?></label>
                                                <input type="text" class="form-control" id="phone"  name="phone" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <label for="firma"><?php echo $hintArr["form"]["mail"]["label"]; ?><span class="asterix">*</span></label>
                                                <input type="text" class="form-control" id="email"  name="email" placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="message"><?php echo $hintArr["form"]["message"]["label"]; ?><span class="asterix">*</span></label>
                                                <textarea class="form-control" id="message" rows="8" name="message"  required></textarea>
                                            </div>
                                        </div>
                                        
                                        <div class="col-12" style="display: none;">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="calcidformcontact"  name="calcidformcontact" placeholder="">
                                            </div>
                                        </div>
                                        
                                        <div class="col-12">
                                            <div class="mandatory"><?php _e('configurator_contact_mandatory', 'neun'); ?></div>
                                            <div class="infotext">                     
                                                <?php
                                                    echo $hintArr["configurator"]["global"]["modal"]["contact"]["subheadline"];
                                                ?> </div>
                                            </div>                  
                                        <div class="col-12">
                                            <div class="form-group">
                                                <button type="submit" class="sendbutton">
                                                    <?php echo $hintArr["configurator"]["global"]["button"]["submit"]; ?>
                                                </button>
                                            </div>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    






    
    
    <?php // end modal contact ?>




    <?php // start error contact ?>



    <div class="modal fade m-page-configurator-modal-contact-error-unable-calculate"  id="modal_configurator_beamup_error_unable_calculate" tabindex="-1" role="dialog" aria-labelledby="modal_configurator_beamup" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body-form-sended-error-calculate">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12 col-md-10  col-lg-8  col-xl-4 mx-auto">
                                <img class="Confirmationtick" src="<?php echo get_stylesheet_directory_uri(); ?>/images/configurator/Icon_Confirmation-Tick.png">
                                <div class="textbox">
                                    <?php
                                        echo $hintArr["configurator"]["global"]["modal"]["contact"]["text"];
                                    ?>
                                </div>
                                
                                <div class="closeButtonContainer">
                                    <div class="closeResultButtonErrorCalculate"><?php echo $hintArr["configurator"]["global"]["button"]["close"]; ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-body-form-error-calculate">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12 col-lg-10 mx-auto">
                                <div class="row"> 
                                    <div class="col-12">
                                        <?php
                                            echo "<h2 class='headlinemodal'>";
                                            echo $hintArr["configurator"]["global"]["modal"]["contact"]["headline"];
                                            echo "</h2>";
                                        ?>
                                    </div>
                                    <div class="col-12 col-lg-8 mx-auto">
                                        <div class="subheadline">
                                            <?php echo $hintArr["configurator"]["global"]["modal"]["contactcalcerror"]["subheadline"]; ?>
                                        </div>
                                     </div>
                                </div>
                            </div>
                        </div>

                        
              
                        
                        
                        <form id="modalContactFormErrorCalculate">
                            <div class="row">
                                <div class="col-12 col-lg-10 offset-lg-1">
                                    <div class="row">
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <label for="firstname"><?php echo $hintArr["form"]["firstname"]["label"]; ?><span class="asterix">*</span></label>
                                                <input type="text" class="form-control" id="firstnameerror"  name="firstname" placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <label for="lastname"><?php echo $hintArr["form"]["lastname"]["label"]; ?><span class="asterix">*</span></label>
                                                <input type="text" class="form-control" id="lastnameerror" name="lastname" placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <label for="firma"><?php echo $hintArr["form"]["firm"]["label"]; ?><span class="asterix">*</span></label>
                                                <input type="text" class="form-control" id="firmaerror" name="firma"  placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <label for="phone"><?php echo $hintArr["form"]["telefon"]["label"]; ?></label>
                                                <input type="text" class="form-control" id="phoneerror"  name="phone" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <label for="firma"><?php echo $hintArr["form"]["mail"]["label"]; ?><span class="asterix">*</span></label>
                                                <input type="text" class="form-control" id="emailerror"  name="email" placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="message"><?php echo $hintArr["form"]["message"]["label"]; ?><span class="asterix">*</span></label>
                                                <textarea class="form-control" id="messageerror" rows="8" name="message"  required></textarea>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="mandatory"><?php _e('configurator_contact_mandatory', 'neun'); ?></div>
                                            <div class="infotext">                     
                                                <?php
                                                    echo $hintArr["configurator"]["global"]["modal"]["contact"]["subheadline"];
                                                ?> </div>
                                            </div>   
                                        
                                        <div class="col-12" style="display: none;">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="calcidformcontacterrorcalulate"  name="calcidformcontacterrorcalulate" placeholder="">
                                            </div>
                                        </div>
                                        
                                        <div class="col-12">
                                            <div class="form-group">
                                                <button type="submit" type="button" class="sendbuttoncontacterrorcalculate">
                                                    <?php echo $hintArr["configurator"]["global"]["button"]["submit"]; ?>
                                                </button>
                                            </div>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>



























    <?php // end error contact ?>





    
    <?php // start error contact ?>
    
    
    <div class="modal fade m-page-configurator-modal-error"  id="modal_configurator_beamup_error" tabindex="-1" role="dialog" aria-labelledby="modal_configurator_beamup_error" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body-form-sended-error">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12 col-md-10  col-lg-8  col-xl-4 mx-auto">
                                <img class="iconError" src="<?php echo get_stylesheet_directory_uri(); ?>/images/configurator/Icon_error.png">
                                <div class="textbox">
                                    <?php
                                        echo $hintArr["configurator"]["global"]["modal"]["contact"]["text"];
                                    ?>
                                </div>
                                
                                <div class="closeButtonContainerError">
                                    <div class="closeResultButton">
                                        <?php echo $hintArr["configurator"]["global"]["button"]["close"]; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-body-form-error">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12 col-lg-10 mx-auto">
                                <div class="row"> 
                                    <div class="col-12">
                                        <img class="iconError" src="<?php echo get_stylesheet_directory_uri(); ?>/images/configurator/Icon_error.png">
                                    </div>
                                    <div class="col-12 col-lg-8 mx-auto">
                                        <?php echo $hintArr["configurator"]["global"]["modal"]["contactcalcerror"]["subheadline"]; ?>
                                        </div>
                                     </div>
                                </div>
                            </div>
                        </div>
 

                        

                        
                        
                        
                        <form id="modalContactFormError">
                            <div class="container-fluid">
                            <div class="row">
                                <div class="col-12 col-lg-10 offset-lg-1">
                                    <div class="row">
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <label for="firstname"><?php echo $hintArr["form"]["firstname"]["label"]; ?><span class="asterix">*</span></label>
                                                <input type="text" class="form-control" id="firstnameError"  name="firstnameerror" placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <label for="lastname"><?php echo $hintArr["form"]["lastname"]["label"]; ?><span class="asterix">*</span></label>
                                                <input type="text" class="form-control" id="lastnameError" name="lastnameerror" placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <label for="firma"><?php echo $hintArr["form"]["firm"]["label"]; ?><span class="asterix">*</span></label>
                                                <input type="text" class="form-control" id="firmaError" name="firmaerror"  placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <label for="phone"><?php echo $hintArr["form"]["telefon"]["label"]; ?></label>
                                                <input type="text" class="form-control" id="phoneError"  name="phoneerror" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <label for="firma"><?php echo $hintArr["form"]["mail"]["label"]; ?><span class="asterix">*</span></label>
                                                <input type="text" class="form-control" id="emailError"  name="emailerror" placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="message"><?php echo $hintArr["form"]["message"]["label"]; ?><span class="asterix">*</span></label>
                                                <textarea class="form-control" id="messageError" rows="8" name="messageerror"  required></textarea>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="mandatory"><?php _e('configurator_contact_mandatory', 'neun'); ?></div>
                                            <div class="infotext">                     
                                                <?php
                                                    echo $hintArr["configurator"]["global"]["modal"]["contact"]["subheadline"];
                                                ?> </div>
                                        </div>
                                        
                                        <div class="col-12" style="display: none;">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="calcidformcontacterror"  name="calcidformcontacterror" placeholder="">
                                            </div>
                                        </div>
                                        
                                        
                                        <div class="col-12">
                                            <div class="form-group">
                                                <button type="submit" class="sendbuttonError"><?php _e('configurator_button_contactsubmit', 'neun'); ?></button>
                                            </div>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                                 </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    
    
    <?php // end error contact ?>
    
    
    
    <?php // start loading modal ?>
    
   
    <div class="modal fade m-page-configurator-modal-beamup-wait"  id="modal_configurator_beamup_wait" tabindex="-1" role="dialog" aria-labelledby="modal_configurator_beamup_wait" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                   
                </div>

                <div class="modal-body-modal-waiting">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12 col-lg-10 mx-auto">
                                <div class="row"> 
                                    <div class="col-12">
                                        <img class="iconError" src="<?php echo get_stylesheet_directory_uri(); ?>/images/configurator/Icon_error.png">
                                    </div>
                                    <div class="col-12 col-lg-12">
                                        <div class="subheadline">
                                            <?php echo $hintArr["configurator"]["global"]["modal"]["calculation"]["text"]; ?>
                                        </div>
                                        
                                        <div class="loadingAnimationContainer">
                                            
                                            <div class="loadingbar"></div>
                                            <span class="percenttext">99</span>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php
    $modalTextPrintPDF = get_field('options_configurator_pdf_modal_text', 'option');
    $modalTextPrintPDFButton = "PDF Download";
?>


    <div class="modal fade m-page-configurator-modal-pdf    pdfcrowd-remove"  id="modal_configurator_pdf" tabindex="-1" role="dialog" aria-labelledby="modal_configurator_pdf" aria-hidden="true">
        
        <i class="far fa-file-pdf"></i>
        
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                   
                </div>

                <div class="modal-body-modal-pdf">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12 col-lg-10 mx-auto">
                                <div class="row"> 
                                    <div class="col-12">
                                        <i class="far fa-file-pdf"></i>
                                        <img class="iconError" src="<?php echo get_stylesheet_directory_uri(); ?>/images/configurator/Icon_pdf.png">
                                    </div>
                                    <div class="subheadline">
                                            <?php echo $modalTextPrintPDF; ?>
                                        </div>
                                    <div class="col-12 col-lg-12">
                                        <div class="pdfButtonContainer">
                                           
                                            
<a class="button95Dark" href="//pdfcrowd.com/url_to_pdf/?width=210mm&height=297mm&pdf_name=Konfigurator beamUp 95"
   onclick="if(!this.p)href+='&url='+encodeURIComponent(location.href);this.p=1">
   <?php echo $modalTextPrintPDFButton; ?>
</a>
                                            
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	




    
    <?php // end loading contact ?>