$(function() {

    
    if ($('.m-page-configurator').length) {
		$( ".m-page-configurator" ).each(function( index ) {
            //console.log("m-page-configurator ajax js");

            var ajaxurl = themeData.ajaxUrl;
            var module = $(this);
            
            var resultmodulAjax = $(".m-page-configurator-result");
            resultmodulAjax.hide();

            window.sendBeamUpConfig = function(form_data){
                //console.log(" window.sendBeamUpConfig");
                
                resultmodulAjax.hide();

                //alert(form_data);
                //console.log("window.sendBeamUpConfig")
                //var dataForm = $('.form_configurator').serialize(); 
       
                form_data.push( { "name" : "security", "value" : ajax_nonce } );

                //Alle Error Felder ausblenden
                var errorDivArray = [];
                errorDivArray = ["Deckentyp_errors","Festigkeitsklasse_Beton_errors","Expositionsklasse_errors","Belastung_qEd_errors", "Laenge_Oeffnung_Lges_errors", "Anzahl_der_Oeffnungen_errors","Laenge_der_Einzeloeffnungen_L1_errors","Laenge_der_Einzeloeffnungen_L2_errors","Laenge_der_Einzeloeffnungen_L3_errors","Laenge_der_Einzeloeffnungen_L4_errors","Laenge_der_Einzeloeffnungen_L5_errors","Breite_Betonbalken_errors","Hoehe_hges_errors","Deckenhoehe_hc_errors","Med_1_errors","Qed_1_errors","Qedmax_errors","Spannweite_Balken_L_errors","Spannweite_Decke_LD1_errors","Spannweite_Decke_LD2_errors","Feuerwiderstandsklasse_errors"];

                $.each(errorDivArray , function(index, val) { 
                    //console.log(index, val)
                    var eleID = val;
                    $("#"+eleID).html("");
                    //$("#"+eleID).hide();
                });
                
               // console.log("form_data: ",form_data);
						
                $.ajax({
                    url : ajax_url, // Here goes our WordPress AJAX endpoint.
                    type : 'post',
                    data : form_data,
                    success : function( response ) {

                        //console.log(response)

                        var responseData = JSON.parse(response);

                        //console.log("responseData  ",responseData)

                        $.each( responseData, function( key, value ) {

                            if(key=="Query_time_exceeded_array"){
                                //alert("Query_time_exceeded_array");
                                //document.getElementById("output_Query_time_exceeded").innerHTML="<span style='color:red; font-size:20px; fontweight:bold;'>"+value+"</span>";

                                //$("html,body").animate({scrollTop:$("#output_section").offset().top-70}, "slow");
                                $('#modal_configurator_beamup_wait').modal('hide');
                                $('#modal_configurator_beamup_error').modal({ keyboard: false, backdrop: 'static'});
                                
                                console.log("responseDataTimeOut: ",responseData);
                                console.log("responseDataTimeOut form_callback_data: ",responseData["form_callback_data"]);
                                console.log("responseDataTimeOut form_callback_data: ",responseData["form_callback_data"]["ID"]);
                                
                                var csvid = responseData["form_callback_data"]["ID"];
                                
                                $("#modalContactFormError #calcidformcontacterror").val(csvid);
                                
                            }

                            if(key=="form_input_errors_array"){
                                //console.log("if form_input_errors_array");                            
                                $('#modal_configurator_beamup_wait').modal('hide');
                                
                               
                                
                                //alert("form_input_errors_array");

                                $.each(errorDivArray , function(index, val) { 
                                    //console.log(index, val)
                                    var eleID = val;
                                    $("#"+eleID).html("");
                                    //$("#"+eleID).hide();
                                });

                                $.each(errorDivArray , function(index, val) { 
                                    //console.log(index, val)
                                    var eleID = val;
                                    $("#"+eleID).show();
                                    //$("#"+eleID).hide();
                                });

                                
                                var countErrorAnimate = 0;
                                
                                $.each( value, function( err_key, err_value ) {
                                    //alert( err_key + ": " + err_value);
                                    document.getElementById(err_key).innerHTML=err_value;
                                    //$("#"+err_key).append(err_value);
                                    
                                    var goto = $("#"+err_key);
                                    if( countErrorAnimate == 0 ){
                                        $('html, body').animate({scrollTop: goto.offset().top -100 }, 'slow');
                                    }
                                    countErrorAnimate = countErrorAnimate + 1;
                                    
                                    
                                });

                            }else if(key=="get_server_result_data"){

                                //alert("get_server_result_data");

                                $.each(errorDivArray , function(index, val) { 
                                    //console.log(index, val)
                                    var eleID = val;
                                    $("#"+eleID).html("");
                                    //$("#"+eleID).hide();
                                });

                                
                               // console.log("value ", value);
                          
                                
                                var resulthasError = false;
                                
                                
                                $.each( value, function( keyIndexlala , keyvalue ) {

               
                                    var keyIndexlala = keyIndexlala.replace(/[\r\n]+$/, '');
                                    console.log(keyIndexlala);
                                    console.log(keyvalue);
 
                                    if(keyIndexlala == "ID" || keyIndexlala == "\ufeffID" ){ 
                                        //console.log("if ID", keyvalue);
                                       $(".m-page-configurator-result .result #ID").text(keyvalue);
                                    }

                                    if(keyIndexlala == "Gewicht"){
                                       // console.log("if gewicht")
                                       $(".m-page-configurator-result .result #Gewicht").text(keyvalue);
                                    }
                                    if(keyIndexlala == "Beschichtungsflaeche"){
                                        //console.log("if Oberflaeche")
                                        $(".m-page-configurator-result .result #Oberflaeche").text(keyvalue);
                                    }
                                    if(keyIndexlala == "Ausnutzungsgrad"){
                                        //console.log("if Ausnutzungsgrad")
                                        $(".m-page-configurator-result .result #Ausnutzungsgrad").text(keyvalue);
                                    }
									if(keyIndexlala == "Lbrutto"){
                                        //console.log("if Lbrutto")
                                        $(".m-page-configurator-result .result #Lbrutto").text(keyvalue);
                                    }
									if(keyIndexlala == "Hbrutto"){
                                        //console.log("if Hbrutto")
                                        $(".m-page-configurator-result .result #Hbrutto").text(keyvalue);
                                    }
									if(keyIndexlala == "Hnetto"){
                                        //console.log("if Hnetto")
                                        $(".m-page-configurator-result .result #Hnetto").text(keyvalue);
                                    }
                                    
									if(keyIndexlala == "L1netto"){
                                        console.log("if L1netto: "+keyvalue);
                                        $(".m-page-configurator-result #L1netto").text(keyvalue);
                                    }else{
                                    }
                                    
									if(keyIndexlala == "L2netto"){
                                        console.log("if L2netto: "+keyvalue);
                                        $(".m-page-configurator-result #L2netto").text(keyvalue);
                                    }else{
                                    }
                                    
                                    
									if(keyIndexlala == "L3netto"){
                                        console.log("if L3netto: "+keyvalue);
                                        $(".m-page-configurator-result #L3netto").text(keyvalue);
                                    }else{
                                    }
                                    
                                    
                                    
									if(keyIndexlala == "L4netto" ){
                                        console.log("if L4netto: "+keyvalue);
                                        $(".m-page-configurator-result #L4netto").text(keyvalue);
                                    }else{
                                    }
                                    
                                    
									if(keyIndexlala == "L5netto" && keyvalue != 0){
                                        console.log("if L5netto: "+keyvalue);
                                        $(".m-page-configurator-result #L5netto").text(keyvalue);
                                    }else{
                                    }
                                    
                                    
                                    
                                    
                                    if(keyIndexlala == "Fehlermeldung"){
                                        //console.log("if Fehlermeldung")
                                        if(keyvalue != 0 || keyvalue != "0" ){
                                            //es gibt einen Fehler,fehler ausgeben
                                            
                                            resulthasError = true;
                                            
                                            //$("#Li_Fehlermeldung").show();
                                            //$(".m-page-configurator-result .result #Fehlermeldung").text(keyvalue);
                                        }else{
                                            //alles gut
                                            //$("#Li_Fehlermeldung").hide();
                                            //$(".m-page-configurator-result .result #Fehlermeldung").text("");
                                        }
                                        
                                    }   

                                });
                                
                                
                                
                                if( $(".m-page-configurator-result #L1netto").text() == 0  || $(".m-page-configurator-result #L1netto").text() == "0" ){
                                   $(".m-page-configurator-result #L1netto").parent().parent().hide();
                                }else{
                                    $(".m-page-configurator-result #L1netto").parent().parent().show();
                                }
                                
                                
                                if( $(".m-page-configurator-result #L2netto").text() == 0  || $(".m-page-configurator-result #L2netto").text() == "0" ){
                                   $(".m-page-configurator-result #L2netto").parent().parent().hide();
                                }else{
                                    $(".m-page-configurator-result #L2netto").parent().parent().show();
                                }     
                                
               
                                if( $(".m-page-configurator-result #L3netto").text() == 0  || $(".m-page-configurator-result #L3netto").text() == "0" ){
                                   $(".m-page-configurator-result #L3netto").parent().parent().hide();
                                }else{
                                    $(".m-page-configurator-result #L3netto").parent().parent().show();
                                }                           
  
                                
                                if( $(".m-page-configurator-result #L4netto").text() == 0  || $(".m-page-configurator-result #L4netto").text() == "0" ){
                                   $(".m-page-configurator-result #L4netto").parent().parent().hide();
                                }else{
                                    $(".m-page-configurator-result #L4netto").parent().parent().show();
                                }                               
                                
                                
                                if( $(".m-page-configurator-result #L5netto").text() == 0  || $(".m-page-configurator-result #L5netto").text() == "0" ){
                                   $(".m-page-configurator-result #L5netto").parent().parent().hide();
                                }else{
                                    $(".m-page-configurator-result #L5netto").parent().parent().show();
                                }                               
                                
                                
                                
                                if(resulthasError == true){
                                    var calcID = $(".resultContainer .jsresultid").text();
                                    //console.log("jsresultid: ",calcID);
                                    $("#calcidformcontacterrorcalulate").val(calcID);
                                    
                                    
                                    $('#modal_configurator_beamup_wait').modal('hide');
                                    $('#modal_configurator_beamup_error_unable_calculate').modal({
                                        keyboard: false,
                                        backdrop: 'static'
                                    });
                      
                                }else{
                                   endLoadingAnimationConfiguratorSuccess(); 
                                }

                            }

                        });
                        
          
                        
                    
                        function endLoadingAnimationConfiguratorSuccess(){
                            var myVar3 = setInterval(myTimer3, 100);
                            var $progressBar2 = $("#modal_configurator_beamup_wait .loadingAnimationContainer .loadingbar");
                            var $progressBartext2 = $("#modal_configurator_beamup_wait .loadingAnimationContainer .percenttext");
                            
                            function myTimer3() {
                                //var perc = $progressBar2.width()
                                //var $width_percentage = (100 * $progressBar2) / $progressBar2.parent().width();
                                var f = $progressBar2.width() / $progressBar2.parent().width() * 100;
                                $progressBartext2.text(Math.round(f) + "%");
                            } 

                            $progressBar2.animate({
                              width: '100%'
                            }, 1000, function() {
                                // Animation complete.
                                $('#modal_configurator_beamup_wait').modal('hide');
                                 resultmodulAjax.show();
                                
                                $("html, body").animate({ scrollTop: $('#result').offset().top }, 100);
                            });   
                            
                        }
                        

                    

                    },
                    fail : function( err ) {
                        //alert( "There was an error: " + err );

                    }
                });
						
				return false;
  
            }

		});	//end each
    }//end length

});//end jrdy


