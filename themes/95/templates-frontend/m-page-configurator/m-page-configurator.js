$(function() {
    

    if ($('.m-page-configurator').length) {
		$( ".m-page-configurator" ).each(function( index ) {
            
            //$('#modal_configurator_beamup_error_unable_calculate').modal({keyboard: false,backdrop: 'static'});
            //$('#modal_configurator_beamup').modal({keyboard: false,backdrop: 'static'});
            //$('#modal_configurator_beamup_error').modal({ keyboard: false, backdrop: 'static'});
            //$('#modal_configurator_beamup').modal({ keyboard: false, backdrop: 'static'});

            
            
            var ajaxurl = themeData.ajaxUrl;
            var module = $(this);
            var form = module.find("form");

            var modalform = module.find("#modalContactForm");
            //console.log("modalform", modalform);
            var modalContactButton = $("#modalContactForm").find(".resultButtonContainer .resultButton");
            var modalContactButtonClose = $("#modal_configurator_beamup").find(".closeButtonContainer .closeResultButton");
            var modalContactSendButton = $("#modalContactForm").find(".sendbutton");
            
            var resultmodul = $(".m-page-configurator-result");
      
            
            
            //--------------------------------------------------------------;
            //------------------ contact form modal ------------------------;
            //--------------------------------------------------------------;
            
            var modalContactButton = resultmodul.find(".resultButtonContainer .resultButton");

            modalContactButton.click(function() {
                $('#modal_configurator_beamup').modal({
                    keyboard: false,
                    backdrop: 'static'
                })
                
                var calcID = $(".resultContainer .jsresultid").text();
                //console.log("jsresultid: ",calcID);
                $("#calcidformcontact").val(calcID);
                
            });   
               
            modalContactButtonClose.click(function() {
                $('#modal_configurator_beamup').modal('hide');
            });    
                     

            modalContactSendButton.click(function(e) {
                e.preventDefault();
                //console.log("modalContactSendButton");
                var modalContactIsValid = true;
                
                var firstname = $("#modalContactForm").find("#firstname");
                var lastname = $("#modalContactForm").find("#lastname");
                var firma = $("#modalContactForm").find("#firma");
                var email = $("#modalContactForm").find("#email");
                var message = $("#modalContactForm").find("#message");
                
            

                if( firstname.val() != ""){ firstname.removeClass("errorBorder");
                }else{
                    firstname.addClass("errorBorder"); modalContactIsValid = false;  
                }
                

                
                if( lastname.val() != ""){ lastname.removeClass("errorBorder");
                }else{
                    lastname.addClass("errorBorder"); modalContactIsValid = false;  
                }
                
                
                if( firma.val() != ""){ firma.removeClass("errorBorder");
                }else{
                    firma.addClass("errorBorder"); modalContactIsValid = false;  
                }
                
                
                if( email.val() != ""){ email.removeClass("errorBorder");
                }else{
                    email.addClass("errorBorder"); modalContactIsValid = false;  
                }
                
                if( message.val() != ""){ message.removeClass("errorBorder");
                }else{
                    message.addClass("errorBorder"); modalContactIsValid = false;  
                }
               
                
                modalContactIsValid = true;
                
                if(modalContactIsValid == true){
                   var dataForm = $("#modalContactForm").serialize(); 
                    //console.log(ajax_url);
                    $.ajax({
                        url : ajax_url, // Here goes our WordPress AJAX endpoint.
                        type : 'post',
                        data: {
                            action: 'configurator_modal_contact_beamup',
                            form: dataForm
                        },
                        success : function( response ) {
                            var responseData = JSON.parse(response);
                            var success = responseData["Success"];
                            //console.log("n",success)
                            if(success == "true"){
                               $("#modal_configurator_beamup").find(".modal-body-form").hide();
                               $("#modal_configurator_beamup").find(".modal-body-form-sended").show();
                            }
                            
                        }
                    });
      
                }

                // This return prevents the submit event to refresh the page.
                return false;

            });
            
            

            
            
            
            
            //--------------------------------------------------------------;
            //------------------ contact form modal error calculate --------;
            //--------------------------------------------------------------;
            
      
            
                $(".closeResultButtonErrorCalculate").click(function() {
                    $('#modal_configurator_beamup_error_unable_calculate').modal('hide');
                });   
          

            $(".sendbuttoncontacterrorcalculate").click(function(e) {
                e.preventDefault();
                //console.log("modalContactSendButton");
                var modalContactIsValid = true;
                
                var firstname = $("#modal_configurator_beamup_error_unable_calculate").find("#firstnameerror");
                var lastname = $("#modal_configurator_beamup_error_unable_calculate").find("#lastnameerror");
                var firma = $("#modal_configurator_beamup_error_unable_calculate").find("#firmaerror");
                var email = $("#modal_configurator_beamup_error_unable_calculate").find("#emailerror");
                var message = $("#modal_configurator_beamup_error_unable_calculate").find("#messageerror");

                if( firstname.val() != ""){ firstname.removeClass("errorBorder");
                }else{
                    firstname.addClass("errorBorder"); modalContactIsValid = false;  
                }
                

                
                if( lastname.val() != ""){ lastname.removeClass("errorBorder");
                }else{
                    lastname.addClass("errorBorder"); modalContactIsValid = false;  
                }
                
                
                if( firma.val() != ""){ firma.removeClass("errorBorder");
                }else{
                    firma.addClass("errorBorder"); modalContactIsValid = false;  
                }
                
                
                if( email.val() != ""){ email.removeClass("errorBorder");
                }else{
                    email.addClass("errorBorder"); modalContactIsValid = false;  
                }
                
                if( message.val() != ""){ message.removeClass("errorBorder");
                }else{
                    message.addClass("errorBorder"); modalContactIsValid = false;  
                }
               
                
                modalContactIsValid = true;
                
                if(modalContactIsValid == true){
                   var dataForm = $("#modalContactFormErrorCalculate").serialize(); 
                    console.log("send error calculate message");
                    //console.log(ajax_url);
                    
                    
                    
                    $.ajax({
                        url : ajax_url, // Here goes our WordPress AJAX endpoint.
                        type : 'post',
                        data: {
                            action: 'configurator_modal_contact_beamup_unable_calculate',
                            form: dataForm
                        },
                        success : function( response ) {
                            var responseData = JSON.parse(response);
                            var success = responseData["Success"];
                            //console.log(success)
                            //console.log("sucess erro calc")
                            if(success == "true"){
                                //console.log("if success")
                                $(".modal-body-form-error-calculate").hide();
                                $(".modal-body-form-sended-error-calculate").show();
                            }
                            
                        }
                    });
      
                }

                // This return prevents the submit event to refresh the page.
                return false;

            });
            

            

            
            
            //--------------------------------------------------------------;
            //-------------------- error form modal ------------------------;
            //--------------------------------------------------------------;      
            var modalformError = $("#modal_configurator_beamup_error");
            var modalContactErrorButtonClose = modalformError.find(".closeButtonContainerError .closeResultButton");
            var modalContactErrorSendButton = modalformError.find(".sendbuttonError");
            
 
            
            //$('#modal_configurator_beamup_error').modal({ keyboard: false, backdrop: 'static'});
            
            

            $("#modal_configurator_beamup_error .closeResultButton").click(function() {
                $('#modal_configurator_beamup_error').modal('hide');
            });    

            modalContactErrorSendButton.click(function() {

                var modalContactErrorIsValid = true;

                
                var firstname = $("#modalContactFormError").find("#firstnameError");
                var lastname = $("#modalContactFormError").find("#lastnameError");
                var firma = $("#modalContactFormError").find("#firmaError");
                var email = $("#modalContactFormError").find("#emailError");
                var message = $("#modalContactFormError").find("#messageError");


                if( firstname.val() != ""){ firstname.removeClass("errorBorder");
                }else{
                    firstname.addClass("errorBorder"); modalContactErrorIsValid = false;  
                }
                

                if( lastname.val() != ""){ lastname.removeClass("errorBorder");
                }else{
                    lastname.addClass("errorBorder"); modalContactErrorIsValid = false;  
                }
                
                
                if( firma.val() != ""){ firma.removeClass("errorBorder");
                }else{
                    firma.addClass("errorBorder"); modalContactErrorIsValid = false;  
                }
                
                
                if( email.val() != ""){ email.removeClass("errorBorder");
                }else{
                    email.addClass("errorBorder"); modalContactErrorIsValid = false;  
                }
                
                if( message.val() != ""){ message.removeClass("errorBorder");
                }else{
                    message.addClass("errorBorder"); modalContactErrorIsValid = false;  
                }
               
                
                
                
                if(modalContactErrorIsValid == true){
                   var dataFormError = $("#modalContactFormError").serialize(); 
                    //console.log(dataFormError);
                    $.ajax({
                        url : ajax_url, // Here goes our WordPress AJAX endpoint.
                        type : 'post',
                        data: {
                            action: 'configurator_modal_contact_beamup_error',
                            formerror: dataFormError
                        },
                        success : function( response ) {
                           // console.log(response)
                            var responseData = JSON.parse(response);
                            //console.log(response)
                            var success = responseData["Success"];
                            //console.log("err", success)
                            if(success == "true"){
                                $("#modal_configurator_beamup_error").find(".modal-body-form-error").hide();
                                $("#modal_configurator_beamup_error").find(".modal-body-form-sended-error").show();
                            }
                            
                        }
                    });
      
                }

                // This return prevents the submit event to refresh the page.
                return false;

            });  
            


                
            $(".testfill").click(function() {
                testfill();
            }); 
 
          function testfill(){
              
              
              
                var $select = form.find("select#country");
                $select.children().filter(function(){
                    return this.text == "Deutschland";
                }).prop('selected', true);
              
              
                form.find("#positionsnumber").val("123456789");
                
                form.find("select#Deckentyp").val("1");
                form.find("#Deckenhoehe_hc").val("25");
              
                form.find("#plz").val("52074");
                form.find("#einsatzort").val("Aachen");
                
                form.find("select#Festigkeitsklasse_Beton").val("3");
                form.find("select#Expositionsklasse").val("1");
                
                form.find("select#feuerwiderstandsklasse").val("1");

                form.find("#Breite_Betonbalken").val("100");
                form.find("#Hoehe_hges").val("100");

                form.find("#Laenge_Oeffnung_Lges").val("2.4");
                form.find("select#anzahl_oeffnungen").val("3");
                form.find("#Laenge_der_Einzeloeffnungen_L1").val("0.8");
                form.find("#Laenge_der_Einzeloeffnungen_L1").prop('disabled', false);
              
                form.find("#Laenge_der_Einzeloeffnungen_L2").val("0.8");
                form.find("#Laenge_der_Einzeloeffnungen_L2").prop('disabled', false);
              
                form.find("#Laenge_der_Einzeloeffnungen_L3").val("0.8");
                form.find("#Laenge_der_Einzeloeffnungen_L3").prop('disabled', false);
                
                form.find("#Belastung_qEd").val("100");
                form.find("#Med_1").val("1300");
                form.find("#Qed_1").val("130");
                form.find("#Qedmax").val("400");
              
              
                $("#Gewicht").text("11111");
                $("#Oberflaeche").text("22222");

              
            }
          
            //testfill();
            
            
            
            
            $(".form_configurator .form-group input").focus(function() {
                var iconhintDesktop = $(this).parent().find(".iconhintDesktop .hinttext");
                iconhintDesktop.show();
            });
            
               
            $(".form_configurator .form-group input").focusout(function() {
                //console.log("focusout")
                var iconhintDesktop = $(this).parent().find(".iconhintDesktop .hinttext");
                iconhintDesktop.hide();
            });
            

            $(".form_configurator .form-group select").on("click", function(){
                var iconhintDesktop = $(this).parent().find(".iconhintDesktop .hinttext");
                if ( iconhintDesktop.css('display') == 'none' || iconhintDesktop.css("visibility") == "hidden"){
                    // element is hidden
                    //console.log("if")
                    iconhintDesktop.show();
                }else{
                    //console.log("else")
                   iconhintDesktop.hide();
                }
            });       
            
            
            $(".form_configurator .form-group select").focusout(function() {
                //console.log("focus out seelct")
                var iconhintDesktop = $(this).parent().find(".iconhintDesktop .hinttext");
                iconhintDesktop.hide();
              
            }); 
            
            //--------------------------------------------------------------;
            //--------------------- Info Icons  ----------------------------;
            //--------------------------------------------------------------;
            
            $( ".m-page-configurator .iconhintDesktop" ).each(function(index) {
                
                
                $(this).on("click mouseover", function(){
                    var dataHint = $(this).data("hint");
                    var allHinttexts = form.find(".hinttext");
                    var clickedHinttext = $(this).find(".hinttext");
                    if ( clickedHinttext.css('display') == 'none' || clickedHinttext.css("visibility") == "hidden"){
                        // element is hidden
                        allHinttexts.hide();
                        clickedHinttext.show();
                    }else{
                        clickedHinttext.hide();
                    }
                });
                
                
                $(this).on("mouseout", function(){
                    var dataHint = $(this).data("hint");
                    var allHinttexts = form.find(".hinttext");
                    var clickedHinttext = $(this).find(".hinttext");
                    allHinttexts.hide();
                });
                
            });
            
            
            $( ".m-page-configurator .iconhintMobile" ).each(function(index) {
                $(this).on("click", function(){

                    var dataHint = $(this).data("hint");
                    var allHinttexts = form.find(".hinttextMobile");

                    var clickedHinttext = $(this).parent().find(".hinttextMobile");

                    if ( clickedHinttext.css('display') == 'none' || clickedHinttext.css("visibility") == "hidden"){
                        // element is hidden
                        allHinttexts.slideUp();
                        clickedHinttext.slideDown();
                    }else{
                        clickedHinttext.slideUp();
                    }
                });
            });
            

            
            //--------------------------------------------------------------;
            //--------------------- Hide/Show Fields -----------------------;
            //--------------------------------------------------------------;
            
            var anzahl_oeffnungenSelect = $("#anzahl_oeffnungen").val();
            var anzahl_oeffnungenValues = $("ul.openings");
            anzahl_oeffnungenValues.find("li .inputbox input").parent().parent().css("opacity",0);
            
            for(var anz = 0; anz < anzahl_oeffnungenSelect ; anz++){
                var ullist = form.find("ul.openings");
                var rowul =  ullist.parent().parent().parent().show();
                ullist.find("li").eq(anz).find(".inputbox input").parent().parent().css("opacity",1);
                ullist.find("li").eq(anz).find(".inputbox input").prop('disabled', false);
            }
            
            
            $('select#anzahl_oeffnungen').on('change', function() {
                var selected = this.value;

                var ullist = form.find("ul.openings");

                ullist.find("li .inputbox input").prop('disabled', true); // nicht genutzt
                ullist.find("li .inputbox input").parent().parent().css("opacity",0);
                
                //console.log(selected);

                for(var i = 0; i < selected ; i++){
                    ullist.find( "li:eq("+i+") .inputbox input" ).prop('disabled', false); // werden genutzt
                    
                    ullist.find( "li:eq("+i+") .inputbox input" ).parent().parent().css("opacity",1);
                }
                
                if(selected == 0){
                    ullist.parent().parent().parent().hide();
                }else{
                    ullist.parent().parent().parent().show();
                }
            });   
            
            
            
            
            
            var selectedDeckentyp = $("#Deckentyp").val();
            //console.log("selectedDeckentyp",selectedDeckentyp);
            
            
            var spannweite_decke_ld1_onload = $("#spannweite_decke_ld1");
            var spannweite_decke_ld2_onload = $("#spannweite_decke_ld2"); 
            var spannweite_balken_l_onload  = $("#spannweite_balken_l");
            
            
            if( selectedDeckentyp == "1"){
                //console.log("i1")
                spannweite_decke_ld1_onload.prop('disabled', true);
                spannweite_decke_ld2_onload.prop('disabled', true);
                spannweite_balken_l_onload.prop('disabled', true);
                
            }else if(selectedDeckentyp == "2" || selectedDeckentyp == "3"){
                //console.log("i2 3")
                spannweite_decke_ld1_onload.prop('disabled', false);
                spannweite_decke_ld2_onload.prop('disabled', false);
                spannweite_balken_l_onload.prop('disabled', false);
                
                
                spannweite_decke_ld1_onload.parent().parent().parent().show();
                spannweite_decke_ld2_onload.parent().parent().parent().show();
                spannweite_balken_l_onload.parent().parent().parent().show();
                
            }
            
            
            
            $('select#Deckentyp').on('change', function() {
                // wenn 1, nicht ausfüllen, disabled
                // wenn 2,3, dann enabled und required
                var selected = this.value;
                
                var spannweite_decke_ld1 = $("#spannweite_decke_ld1");
                var spannweite_decke_ld2 = $("#spannweite_decke_ld2"); 
                var spannweite_balken_l  = $("#spannweite_balken_l");
                
                var inputFields = [];
                inputFields[0] = spannweite_decke_ld1;
                inputFields[1] = spannweite_decke_ld2;
                inputFields[2] = spannweite_balken_l;
                
                if( selected == "1"){
                    $.each(inputFields, function(index, value){
                        //console.log(index)
                        //console.log(value)
                        //Die Felder aus dem InputFieldsArray ausblenden
                        
                        value.parent().parent().parent().hide();
                        
                        value.prop('disabled', true);
                        value.prop('required', false);
                        value.val('');

                    });
                }else if(selected == "2" || selected == "3"){
                    $.each(inputFields, function(index, value){
                        //console.log(index)
                        //console.log(value)
                        //Die Felder aus dem InputFieldsArray einblenden
                        
                        value.parent().parent().parent().show();
                        
                        value.prop('disabled', false);
                        value.prop('required', true);
                        
                        
                    });
                }else if(selected == 0){
                    $.each(inputFields, function(index, value){
                        //console.log(index)
                        //console.log(value)
                        //Die Felder aus dem InputFieldsArray ausblenden
                        
                        value.parent().parent().parent().hide();
                        
                        value.prop('disabled', true);
                        value.prop('required', false);
                        value.val('');

                    }); 
                }

            });  
            
            
            

            
            
            
            //--------------------------------------------------------------;
            //---------------------- form submit ---------------------------;
            //--------------------------------------------------------------;         
            
            

            
            $('.form_configurator').submit(function(e){
               

                //var form_data = new FormData();  
                // Here we add our nonce (The one we created on our functions.php. WordPress needs this code to verify if the request comes from a valid source.
                //form_data.push( { "inq_form_nonce2" : inq_form_nonce2 } );
                //form_data.append( 'inq_form_nonce2', inq_form_nonce2 );

                var formIsValid = true;
                var formIsNotValidArray = [];

                var form = $(this);
                var country = form.find("#country");
                var positionsnumber = form.find("#positionsnumber");
                
                
                var plz = form.find("#plz");
                var einsatzort = form.find("#einsatzort");
               
                
               // console.log("plz", plz);
                 //console.log("einsatzort", einsatzort);

                var Deckentyp = form.find("#Deckentyp");
                var Deckenhoehe_hc = form.find("#Deckenhoehe_hc");
                var spannweite_decke_ld1 = form.find("#spannweite_decke_ld1");
                var spannweite_decke_ld2 = form.find("#spannweite_decke_ld2");
                

                var Festigkeitsklasse_Beton = form.find("#Festigkeitsklasse_Beton");
                var Expositionsklasse = form.find("#Expositionsklasse");
                var Feuerwiderstandsklasse = form.find("#feuerwiderstandsklasse");
                
                var Breite_Betonbalken = form.find("#Breite_Betonbalken");
                var Hoehe_hges = form.find("#Hoehe_hges");
                var spannweite_balken_l = form.find("#spannweite_balken_l");
                
                
                var Laenge_Oeffnung_Lges = form.find("#Laenge_Oeffnung_Lges");
                var Anzahl_der_Oeffnungen = form.find("#anzahl_oeffnungen");
                var Laenge_der_Einzeloeffnungen_L1 = form.find("#Laenge_der_Einzeloeffnungen_L1");
                var Laenge_der_Einzeloeffnungen_L2 = form.find("#Laenge_der_Einzeloeffnungen_L2");
                var Laenge_der_Einzeloeffnungen_L3 = form.find("#Laenge_der_Einzeloeffnungen_L3");
                var Laenge_der_Einzeloeffnungen_L4 = form.find("#Laenge_der_Einzeloeffnungen_L4");
                var Laenge_der_Einzeloeffnungen_L5 = form.find("#Laenge_der_Einzeloeffnungen_L5");
                

                var Belastung_qEd = form.find("#Belastung_qEd");
                var Med_1 = form.find("#Med_1");
                var Qed_1 = form.find("#Qed_1");
                var Qedmax = form.find("#Qedmax");
                

               
  

                //--------------------------------------------------;
                // Validation
                // Check Input Content, Set/remove Error Styles
                //--------------------------------------------------;

                
                if( country.val() == null || country.val() == "null" ){
                    country.addClass("errorBorder");
                    formIsValid = false;
                    formIsNotValidArray.push(country);
                    
                }else{
                    country.removeClass("errorBorder");
                }
                

                
                if( plz.val() != ""){
                    plz.removeClass("errorBorder");
                }else{
                    plz.addClass("errorBorder");
                    plz = false;
                    formIsNotValidArray.push(plz);
                }
                
                
                if( einsatzort.val() != ""){
                    einsatzort.removeClass("errorBorder");
                }else{
                    einsatzort.addClass("errorBorder");
                    formIsValid = false;
                    formIsNotValidArray.push(einsatzort);
                }
                
                
                if( positionsnumber.val() != ""){
                    positionsnumber.removeClass("errorBorder");
                }else{
                    positionsnumber.addClass("errorBorder");
                    formIsValid = false;
                    formIsNotValidArray.push(positionsnumber);
                }

                

                
                

                
                
                
                //console.log("formIsValid2",formIsValid)
                
                if( Deckentyp.val() == null || Deckentyp.val() == "null" ){
                    Deckentyp.addClass("errorBorder");
                    formIsValid = false;
                    formIsNotValidArray.push(Deckentyp);
                    
                }else{
                    Deckentyp.removeClass("errorBorder");
                }
                
                // console.log("formIsValid3",formIsValid)
                
                if(Deckenhoehe_hc.val() != ""){
                    Deckenhoehe_hc.removeClass("errorBorder");
                }else{
                    Deckenhoehe_hc.addClass("errorBorder");
                    formIsValid = false;
                    formIsNotValidArray.push(Deckenhoehe_hc);
                }
                
               // console.log("formIsValid4",formIsValid)
                

                if( Deckentyp.val() == "2" || Deckentyp.val() == "3"){
                    
                    if(spannweite_decke_ld1.val() != ""){
                        spannweite_decke_ld1.removeClass("errorBorder");
                    }else{
                        spannweite_decke_ld1.addClass("errorBorder");
                        formIsValid = false;
                        formIsNotValidArray.push(spannweite_decke_ld1);
                    } 
                    
                    if(spannweite_decke_ld2.val() != ""){
                        spannweite_decke_ld2.removeClass("errorBorder");
                    }else{
                        spannweite_decke_ld2.addClass("errorBorder");
                        formIsValid = false;
                        formIsNotValidArray.push(spannweite_decke_ld2);
                    }
                    
                    if(spannweite_balken_l.val() != ""){
                        spannweite_balken_l.removeClass("errorBorder");
                    }else{
                        spannweite_balken_l.addClass("errorBorder");
                        formIsValid = false;
                        formIsNotValidArray.push(spannweite_balken_l);
                    }
                    
                }else{
                    spannweite_decke_ld1.removeClass("errorBorder");
                    spannweite_decke_ld2.removeClass("errorBorder");
                    spannweite_balken_l.removeClass("errorBorder");
                }        
                
               // console.log("formIsValid5",formIsValid)
                

                
                if( Festigkeitsklasse_Beton.val() == null || Festigkeitsklasse_Beton.val() == "null" ){
                    Festigkeitsklasse_Beton.addClass("errorBorder");
                    formIsValid = false;
                    formIsNotValidArray.push(Festigkeitsklasse_Beton);
                }else{
                    Festigkeitsklasse_Beton.removeClass("errorBorder");
                }            
                
                
                if( Expositionsklasse.val() == null || Expositionsklasse.val() == "null" ){
                    Expositionsklasse.addClass("errorBorder");
                    formIsValid = false;
                    formIsNotValidArray.push(Expositionsklasse);
                }else{
                    Expositionsklasse.removeClass("errorBorder");
                } 
                
                //console.log("formIsValid6",formIsValid)
                
                
                if( Feuerwiderstandsklasse.val() == null || Feuerwiderstandsklasse.val() == "null" ){
                    Feuerwiderstandsklasse.addClass("errorBorder");
                    formIsValid = false;
                    formIsNotValidArray.push(Feuerwiderstandsklasse);
                }else{
                    Feuerwiderstandsklasse.removeClass("errorBorder");
                } 
                
               // console.log("formIsValid7",formIsValid)

                if( Breite_Betonbalken.val() != ""){
                    Breite_Betonbalken.removeClass("errorBorder");
                }else{
                    Breite_Betonbalken.addClass("errorBorder");
                    formIsValid = false;
                    formIsNotValidArray.push(Breite_Betonbalken);
                }
8
               // console.log("formIsValid6",formIsValid)
                if( Hoehe_hges.val() != ""){
                    Hoehe_hges.removeClass("errorBorder");
                }else{
                    Hoehe_hges.addClass("errorBorder");
                    formIsValid = false;
                    formIsNotValidArray.push(Hoehe_hges);
                }

                //console.log("formIsValid9",formIsValid)
                
                
      
                
                
      
                
                if( Laenge_Oeffnung_Lges.val() != ""){
                    Laenge_Oeffnung_Lges.removeClass("errorBorder");
                }else{
                    Laenge_Oeffnung_Lges.addClass("errorBorder");
                    formIsValid = false;
                    formIsNotValidArray.push(Laenge_Oeffnung_Lges);
                }            
                 //console.log("formIsValid10",formIsValid)
                
                if( Anzahl_der_Oeffnungen.val() == null || Anzahl_der_Oeffnungen.val() == "null" ){
                    Anzahl_der_Oeffnungen.addClass("errorBorder");
                    formIsValid = false;
                    formIsNotValidArray.push(Anzahl_der_Oeffnungen);
                }else{
                    Anzahl_der_Oeffnungen.removeClass("errorBorder");
                }       
                
                // console.log("formIsValid 11",formIsValid)
                
                
                if( Anzahl_der_Oeffnungen.val() != null || Anzahl_der_Oeffnungen.val() != "null" ){
                    
                    Laenge_der_Einzeloeffnungen_L1.removeClass("errorBorder");
                    Laenge_der_Einzeloeffnungen_L2.removeClass("errorBorder");
                    Laenge_der_Einzeloeffnungen_L3.removeClass("errorBorder");
                    Laenge_der_Einzeloeffnungen_L4.removeClass("errorBorder");
                    Laenge_der_Einzeloeffnungen_L5.removeClass("errorBorder");
                    
                    var loopCounter = Anzahl_der_Oeffnungen.val();
                    
                    for(var i = 0; i < loopCounter ; i++){
                        
                        var id = i + 1;
                        var ele = form.find("#Laenge_der_Einzeloeffnungen_L"+id);
                        
                        
                        if( ele.val() != ""){
                            ele.removeClass("errorBorder")
                        }else{
                            ele.addClass("errorBorder");
                            formIsValid = false;
                            
                            formIsNotValidArray.push(Anzahl_der_Oeffnungen);
                        } 

                    }

                  
                }else{
                    Laenge_der_Einzeloeffnungen_L1.removeClass("errorBorder");
                    Laenge_der_Einzeloeffnungen_L2.removeClass("errorBorder");
                    Laenge_der_Einzeloeffnungen_L3.removeClass("errorBorder");
                    Laenge_der_Einzeloeffnungen_L4.removeClass("errorBorder");
                    Laenge_der_Einzeloeffnungen_L5.removeClass("errorBorder");
                }               
                
                
                

                if( Belastung_qEd.val() != ""){
                    Belastung_qEd.removeClass("errorBorder");
                }else{
                    Belastung_qEd.addClass("errorBorder");
                    formIsValid = false;
                    
                    formIsNotValidArray.push(Belastung_qEd);
                }            
                
                
                if( Med_1.val() != ""){
                    Med_1.removeClass("errorBorder");
                }else{
                    Med_1.addClass("errorBorder");
                    formIsValid = false;
                    formIsNotValidArray.push(Med_1);
                }            
                
                
                if( Qed_1.val() != ""){
                    Qed_1.removeClass("errorBorder");
                }else{
                    Qed_1.addClass("errorBorder");
                    formIsValid = false;
                    formIsNotValidArray.push(Qed_1);
                }            
                
                
                if( Qedmax.val() != ""){
                    Qedmax.removeClass("errorBorder");
                }else{
                    Qedmax.addClass("errorBorder");
                    formIsValid = false;
                    formIsNotValidArray.push(Qedmax);
                }            
                
                
                
                //console.log("formIsValid",formIsValid)


                //--------------------------------------------------;
                // Send Information, Ajax Call
                //--------------------------------------------------;


                //formIsValid = true;
				if(formIsValid == true){
                    //var dataForm = $('.form_configurator').serialize();
					var dataForm = jQuery( this ).serializeArray();
                    //console.log("dataForm: ",dataForm);
                    window.sendBeamUpConfig(dataForm);
                    
					//console.log("Klick berechnung")
                    
                    $('#modal_configurator_beamup_wait').modal({
                        keyboard: true,
                        backdrop: 'static'
                    });
				
                    
           
                    var myVar2 = setInterval(myTimer2, 100);
                    var $progressBar = $("#modal_configurator_beamup_wait .loadingAnimationContainer .loadingbar");
                    var $progressBartext = $("#modal_configurator_beamup_wait .loadingAnimationContainer .percenttext");
                    
                    
                    $progressBartext.text("0%")
                    $progressBar.css("width","0");
                    
                    
                    function myTimer2() {
                        var perc = $progressBar.width()
                        var $width_percentage = (100 * $progressBar) / $progressBar.parent().width();

                        var f = $progressBar.width() / $progressBar.parent().width() * 100;

                        $progressBartext.text(Math.round(f) + "%");
                    } 
                    
                    $progressBar.stop().animate({
                      width: '80%'
                    }, 15000, function() {
                    // Animation complete.
                    });                   

                    
                    
                }else{
                    
                    //console.log(formIsNotValidArray);
                    //console.log(formIsNotValidArray[0]);
                    var goto = formIsNotValidArray[0];
                    
                    $('html, body').animate({scrollTop: goto.offset().top -100 }, 'slow');
                    
                }
                

                // This return prevents the submit event to refresh the page.
                return false;

            });
            
            
            
            
            
            //--------------------------------------------------------------;
            //----------------------------- PDF ----------------------------;
            //--------------------------------------------------------------;     
            

       
  
            
            $(".resultButtonPDF").on("click", function(){
                // erstmal alle! Eingaben aus den feldern abholen
                // anschliessend eine URL zusammenstellen und aufrufen
                // http://localhost/95/konfigurator-beamup/?country=Deutschland&plz=12345&einsatzort=Musterstadt&positionsnumber=123456
                
                var input = $("#p-dra-form");
                
                
                var country =  input.find("#country").val();
                var plz =  input.find("#plz").val();
                var einsatzort =  input.find("#einsatzort").val();
                var positionsnumber =  input.find("#positionsnumber").val();

                var urlstring1 = "&country="+country+"&plz="+plz+"&einsatzort="+einsatzort+"&positionsnumber="+positionsnumber;
                
                
                
                var deckentyp = input.find("#Deckentyp").val();
                var deckenhoehe_hc = input.find("#Deckenhoehe_hc").val();
                
                var spannweite_decke_ld1 = input.find("#spannweite_decke_ld1").val(); // nur wenn bestimmter deckentyp
                var spannweite_decke_ld2 = input.find("#spannweite_decke_ld2").val(); // nur wenn bestimmter deckentyp
                
                var urlstring2 = "&deckentyp="+deckentyp+"&deckenhoehe_hc="+deckenhoehe_hc+"&spannweite_decke_ld1="+spannweite_decke_ld1+"&spannweite_decke_ld2="+spannweite_decke_ld2;
                
                
                
                
                
                
                var festigkeitsklasse_Beton = input.find("#Festigkeitsklasse_Beton").val();
                var expositionsklasse = input.find("#Expositionsklasse").val();
                var feuerwiderstandsklasse = input.find("#feuerwiderstandsklasse").val();
                var breite_Betonbalken = input.find("#Breite_Betonbalken").val();
                var hoehe_hges = input.find("#Hoehe_hges").val();
                var spannweite_balken_l = input.find("#spannweite_balken_l").val(); // nur wenn bestimmter deckentyp
                
                
                var urlstring3 = "&festigkeitsklasse_Beton="+festigkeitsklasse_Beton+"&expositionsklasse="+expositionsklasse+"&feuerwiderstandsklasse="+feuerwiderstandsklasse+"&breite_Betonbalken="+breite_Betonbalken+"&hoehe_hges="+hoehe_hges+"&spannweite_balken_l="+spannweite_balken_l;
                
                
                
                
                
                var Laenge_Oeffnung_Lges = input.find("#Laenge_Oeffnung_Lges").val();
                var anzahl_oeffnungen = input.find("#anzahl_oeffnungen").val();
                
                var urlstring4 = "&Laenge_Oeffnung_Lges="+Laenge_Oeffnung_Lges+"&anzahl_oeffnungen="+anzahl_oeffnungen;
                
                
                var Laenge_der_Einzeloeffnungen_L1 = input.find("#Laenge_der_Einzeloeffnungen_L1").val();
                var Laenge_der_Einzeloeffnungen_L2 = input.find("#Laenge_der_Einzeloeffnungen_L2").val();
                var Laenge_der_Einzeloeffnungen_L3 = input.find("#Laenge_der_Einzeloeffnungen_L3").val();
                var Laenge_der_Einzeloeffnungen_L4 = input.find("#Laenge_der_Einzeloeffnungen_L4").val();
                var Laenge_der_Einzeloeffnungen_L5 = input.find("#Laenge_der_Einzeloeffnungen_L5").val();
                
                var urlstring5 = "&Laenge_der_Einzeloeffnungen_L1="+Laenge_der_Einzeloeffnungen_L1+"&Laenge_der_Einzeloeffnungen_L2="+Laenge_der_Einzeloeffnungen_L2;
                var urlstring6 = "&Laenge_der_Einzeloeffnungen_L3="+Laenge_der_Einzeloeffnungen_L3+"&Laenge_der_Einzeloeffnungen_L4="+Laenge_der_Einzeloeffnungen_L4;
                var urlstring7 = "&Laenge_der_Einzeloeffnungen_L5="+Laenge_der_Einzeloeffnungen_L5;
                
                
                
    
                
                
                var Belastung_qEd = input.find("#Belastung_qEd").val();
                var Med_1 = input.find("#Med_1").val();
                var Qed_1 = input.find("#Qed_1").val();
                var Qedmax = input.find("#Qedmax").val();
              
                
                var urlstring8 = "&Belastung_qEd="+Belastung_qEd+"&Med_1="+Med_1+"&Qed_1="+Qed_1+"&Qedmax="+Qedmax;
                
                
                
                
                var inputResult = $(".m-page-configurator-result");
                
                var Gewicht = inputResult.find("#Gewicht").text();
                var Oberflaeche = inputResult.find("#Oberflaeche").text();
                var Lbrutto = inputResult.find("#Lbrutto").text();
                var Hbrutto = inputResult.find("#Hbrutto").text();
                var L1netto = inputResult.find("#L1netto").text();
                var L2netto = inputResult.find("#L2netto").text();
                var L3netto = inputResult.find("#L3netto").text();
                var L4netto = inputResult.find("#L4netto").text();
                var L5netto = inputResult.find("#L5netto").text();
                var Hnetto = inputResult.find("#Hnetto").text();
                
                
                //console.log("inputResult",inputResult);
                //console.log("Gewicht",Gewicht);
                //console.log("Oberflaeche",Oberflaeche);
                
                var urlstringResults1 = "&Gewicht="+Gewicht+"&Oberflaeche="+Oberflaeche+"&Lbrutto="+Lbrutto+"&Hbrutto="+Hbrutto+"&Qedmax="+Qedmax+"&Hnetto="+Hnetto;
                var urlstringResults2 = "&L1netto="+L1netto+"&L2netto="+L2netto+"&L3netto="+L3netto+"&L4netto="+L4netto+"&L5netto="+L5netto;
                
                

                
                //http://localhost/95/konfigurator-beamup/?country=Deutschland&plz=52074&einsatzort=Aachen&positionsnumber=123456789&deckentyp=1&deckenhoehe_hc=&spannweite_decke_ld1=&deckenhoehe_hc=&festigkeitsklasse_Beton=3&expositionsklasse=1&feuerwiderstandsklasse=1&breite_Betonbalken=100&hoehe_hges=100&spannweite_balken_l=&Laenge_Oeffnung_Lges=2.4&anzahl_oeffnungen=3&Laenge_der_Einzeloeffnungen_L1=0.8&Laenge_der_Einzeloeffnungen_L2=0.8&Laenge_der_Einzeloeffnungen_L3=0.8&Laenge_der_Einzeloeffnungen_L4=&Laenge_der_Einzeloeffnungen_L5=&Belastung_qEd=100&Med_1=1300&Qed_1=130&Qedmax=400
                
                //http://localhost/95/konfigurator-beamup/?pdf=1&country=Deutschland&plz=52074&einsatzort=Aachen&positionsnumber=123456789&deckentyp=2&deckenhoehe_hc=25&spannweite_decke_ld1=1&spannweite_decke_ld2=1&festigkeitsklasse_Beton=3&expositionsklasse=1&feuerwiderstandsklasse=1&breite_Betonbalken=100&hoehe_hges=100&spannweite_balken_l=3&Laenge_Oeffnung_Lges=2.4&anzahl_oeffnungen=3&Laenge_der_Einzeloeffnungen_L1=0.8&Laenge_der_Einzeloeffnungen_L2=0.8&Laenge_der_Einzeloeffnungen_L3=0.8&Laenge_der_Einzeloeffnungen_L4=&Laenge_der_Einzeloeffnungen_L5=&Belastung_qEd=100&Med_1=1300&Qed_1=130&Qedmax=400&Gewicht=1340&Oberflaeche=13.67&Lbrutto=3.13&Hbrutto=0.93&Qedmax=400&Hnetto=0.71&L1netto=0.785&L2netto=0.785&L3netto=0.785&L4netto=0&L5netto=
                
                
                            
                // live 95construction.com
                // localhost localhost
                var hostname123 = document.location.hostname;
                //console.log("hostname123",hostname123);



                // live https://95construction.com/konfigurator-beamup/
                // local http://localhost/95/konfigurator-beamup/
                var locationHref123 = document.location.href;
                //console.log("locationHref123",locationHref123);
                
                
                var datenow1 = GetTodayDate();
                //console.log("datenow1",datenow1);
                var dateString = "&date="+datenow1;
                
                
                //var fullurl = "http://localhost/95/konfigurator-beamup/?pdf=1"+urlstring1+urlstring2+urlstring3+urlstring4+urlstring5+urlstring6+urlstring7+urlstring8+urlstringResults1+urlstringResults2;
                
                var fullurl = locationHref123+"?pdf=1"+urlstring1+urlstring2+urlstring3+urlstring4+urlstring5+urlstring6+urlstring7+urlstring8+urlstringResults1+urlstringResults2+dateString;
                //console.log("fullurl",fullurl);
                
                window.open(fullurl, '_blank');
             
            });// end $(".resultButtonPDF").on("click", function(){ 
            
            
            
            if(  $(".m-page-configurator").data("pdf") == true   ){
                //console.log("ist pdf Ansicht")
                
                var resultmodulAjax = $(".m-page-configurator-result");    
                var resultList =  resultmodulAjax.find(".resultList");
                
                resultmodulAjax.show();
                
                
                
                var valueL1 = $(".m-page-configurator-result #L1netto").text();
                var valueL2 = $(".m-page-configurator-result #L2netto").text();
                var valueL3 = $(".m-page-configurator-result #L3netto").text();
                var valueL4 = $(".m-page-configurator-result #L4netto").text();
                var valueL5 = $(".m-page-configurator-result #L5netto").text();
                
                
                
                if( valueL1 == 0  || valueL1 == "0" || valueL1 == "" ){
                   $(".m-page-configurator-result #L1netto").parent().parent().hide();
                }else{
                    $(".m-page-configurator-result #L1netto").parent().parent().show();
                }


                if(  valueL2 == 0  || valueL2 == "0" || valueL2 == "" ){
                   $(".m-page-configurator-result #L2netto").parent().parent().hide();
                }else{
                    $(".m-page-configurator-result #L2netto").parent().parent().show();
                }     


                if(  valueL3 == 0  || valueL3 == "0" || valueL3 == "" ){
                   $(".m-page-configurator-result #L3netto").parent().parent().hide();
                }else{
                    $(".m-page-configurator-result #L3netto").parent().parent().show();
                }                           


                if(  valueL4 == 0  || valueL4 == "0" || valueL4 == ""  ){
                   $(".m-page-configurator-result #L4netto").parent().parent().hide();
                }else{
                    $(".m-page-configurator-result #L4netto").parent().parent().show();
                }                               


                if(  valueL5 == 0  || valueL5 == "0" || valueL5 == "" ){
                   $(".m-page-configurator-result #L5netto").parent().parent().hide();
                }else{
                    $(".m-page-configurator-result #L5netto").parent().parent().show();
                }             
                
                
                
                
                
                $('#modal_configurator_pdf').modal({
                    keyboard: false,
                    backdrop: 'static'
                })  

                
                
                
            }// end if(  $(".m-page-configurator").data("pdf") == true   ){

                        
            $('#modal_configurator_pdf').on('shown.bs.modal', function (e) {
                $(".modal-backdrop").addClass("pdfcrowd-remove");
            });
            
            
     
  
            
		});	//end each
    }//end length
    
    
 

    function GetTodayDate() {
   var tdate = new Date();
   var dd = tdate.getDate(); //yields day
   var MM = tdate.getMonth(); //yields month
   var yyyy = tdate.getFullYear(); //yields year
   var currentDate= dd + "." +( MM+1) + "." + yyyy;

   return currentDate;
}            

         
            
    
});//end jrdy


