$(function() {
    if ($('.m-page-hero').length) {
		$( ".m-page-hero" ).each(function( index ) {
            //var ele = $(this);

            var containerSlider = $(this).find(".containerSlider");
            var arrowSlideDown = $(this).find(".arrowSliderDown");

            var buttonNext = $(this).find(".ctrlButtonNext");
            var buttonPrev = $(this).find(".ctrlButtonPrev");

            var triggerResize2 = $(window); // Firefox...

            
            var owl = $('.owlCarouselHero');
            
            buttonNext.click(function() {
                // With optional speed parameter
                // Parameters has to be in square bracket '[]'
                //console.log("buttonnext");
                owl.trigger('prev.owl.carousel', [750]);
            })
            // Go to the previous item
            buttonPrev.click(function() {
                // With optional speed parameter
                // Parameters has to be in square bracket '[]'
                //console.log("buttonprev");
                
                
                owl.trigger('next.owl.carousel', [750]);
            })
            
            
            owl.on('changed.owl.carousel', function(event) {
                //When a property has changed its value.
            })
            
            
            owl.on('initialized.owl.carousel', function(event) {
                //When the plugin has initialized.
                //console.log(event)
                //console.log("When the translation of the stage starts.");
                addTextAnimation();
            })
            
            
            owl.on('translate.owl.carousel', function(event) {
                //When the translation of the stage starts.
                //console.log(event)
                //console.log("When the translation of the stage starts.");
                removeTextAnimation();
            })
            
            owl.on('translated.owl.carousel', function(event) {
                //When the translation of the stage starts.
                //console.log(event)
                //console.log("When the translation of the stage ended.");
                addTextAnimation();
            })
            
            owl.on('changed.owl.carousel', function (e) {
                //console.log("current: ",e.relatedTarget.current())
                //console.log("current: ",e.item.index) //same
                //console.log("total: ",e.item.count)   //total
            })
            
            
            function removeTextAnimation(){
                containerSlider.find(".owl-stage").find(".owl-item").find(".contentContainer").removeClass("contentContainerAnimation");
            }
            
            function addTextAnimation(){
                containerSlider.find(".owl-stage").find(".owl-item.active").find(".contentContainer").addClass("contentContainerAnimation");
            }
            
           
            
            owl.owlCarousel({
                loop:true,
                margin:0,
                responsiveClass:true,
                items:1,
                dots: true,
                autoplay: false,
                autoplayHoverPause: true,
                responsive:{
                    0:{
                        
                    },
                    600:{
                       
                    },
                    1000:{
                        
                        
                    }
                }
            })
            


            arrowSlideDown.click(function() {
                var headerMenueHeight = $("header").height();
                $("html, body").animate({ scrollTop: $('#m-page-hero-below').offset().top - headerMenueHeight }, 1000);
               
            })        
            
            
            $( window ).scroll(function(){
                 arrowSlideDown.fadeOut();
            });
            

            
		});	//end each
    }//end length
});//end jrdy