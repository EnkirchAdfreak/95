<?php
    $content = get_sub_field('inhalte');


    $sliderSize = $content["size"]; // small, fullheight
    $sliderSizeClass;

    if($sliderSize == "small"){
        $sliderSizeClass = "slideHeight";
    }else if($sliderSize == "fullheight"){
        $sliderSizeClass = "slideHeightFull";
    }else{
        $sliderSizeClass = "slideHeight";
    }



    $heroSlides = Array();
    $heroSlidesID = 0;
    if( have_rows('inhalte') ): while ( have_rows('inhalte') ) : the_row();
    if( have_rows('page-hero_repeater_slides') ): while ( have_rows('page-hero_repeater_slides') ) : the_row();
            $heroSlides[$heroSlidesID]["bgimage"] = get_sub_field("page-hero_repeater_slides_backgroundimage");
            $heroSlides[$heroSlidesID]["headline"] = get_sub_field("page-hero_repeater_slides_headline");
            $heroSlides[$heroSlidesID]["button"] = get_sub_field("page-hero_repeater_slides_button");
            $heroSlidesID++;
    endwhile; endif;
    endwhile; endif;


    //print_r($heroSlides);

?>


<div class="m-page-hero">
    <div class="container-fluid px-0">



        <div class="row">

            <div class="col-12 col-lg-1 order-lg-2 d-none d-lg-block">

            </div>

            <div class="col-12 col-lg-12">
                <div class=" row">

                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12 col-sm-10 offset-sm-1">

                                <div class="ctrlButtonContainer <?php echo $sliderSizeClass; ?>">
                                
                                <div class="ctrlButton ctrlButtonNext">
                                    <img class="" src="<?php echo get_stylesheet_directory_uri(); ?>/images/modul/heroslider/Icon_Slider_Arrow_Back_white.png" alt="">
                                </div>
                                <div class="ctrlButton ctrlButtonPrev">
                                    <img class="" src="<?php echo get_stylesheet_directory_uri(); ?>/images/modul/heroslider/Icon_Slider_Arrow_Next_white.png" alt="">
                                </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    
                </div>
                
                

                
            <?php
                if($sliderSize == "fullheight"){
            ?>
                <div class="arrowSliderDown">
                    <div class="arrowSliderDownIcon"></div>
                </div>
            <?php
                };
            ?>
                
                <div class="containerSlider <?php echo $sliderSizeClass; ?>">
                    <div class="owl-carousel owl-theme owlCarouselHero ">
                        <?php
                            //$countRefSlider = 0;

                            foreach ($heroSlides as $item) {
                                $title = $item["headline"];
                                $signaturImage = $item["bgimage"];

                                $imagepath = wp_get_attachment_image_src($signaturImage, "col-md");
                                $imagepath = $imagepath[0];
                                
                                $button = $item["button"];
                                if( $button ){
                                    $buttonURL = $button['url'];
                                    $buttonTitle = $button['title'];
                                    $buttonTarget = $button['target'] ? $button['target'] : '_self';
                                    
                                    
                                   $buttonStyle = "buttonSlide";
                                }
                                

                            ?>
                            <div class="slide " style="background-image: url('<?php echo $imagepath; ?>');"> 
                                <div class="contentContainer">
                                    <div class="text"><?php echo $title; ?></div> 
                                    
                                    <?php if( $button ): ?>
                                        <div class="buttonContainer">
                                            <a class="button <?php echo $buttonStyle; ?>" href="<?php echo esc_url( $buttonURL ); ?>" target="<?php echo esc_attr( $buttonTarget ); ?>"><?php echo esc_html( $buttonTitle ); ?></a>
                                    </div>
                                    <?php endif; ?>
                                    
                                </div>
                                
                                
                                
                            </div>

                            <?php
                                //$countRefSlider++;
                                };//end for each
                            ?>
                            
                    </div>
                </div>
            
            </div>

        </div>

    </div>
    
    <?php
        if($sliderSize == "fullheight"){
    ?>
       <div id="m-page-hero-below"></div>
    <?php
        };
    ?>
    
    
</div>

