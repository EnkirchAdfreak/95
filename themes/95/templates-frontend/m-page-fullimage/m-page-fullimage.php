<?php
    $content = get_sub_field('inhalte');

    $imageid = $content["image"];



    $imageURL = wp_get_attachment_image_src( $imageid, 'full-hd' );

    $bgPositionHorizontal = $content["background_position_horizontal"]; 
    $bgPositionVertical = $content["background_position_vertical"]; 
    $backgroundPosition = "background-position: ".$bgPositionHorizontal. " ".$bgPositionVertical.";";


?>
<div class="m-page-fullimage">
    <div class="container-fluid px-0" >        
        <div class="row">
            <div class="col-12">
                <div class="imageBox" style="background-image: url('<?php echo $imageURL[0]; ?>'); <?php echo $backgroundPosition; ?>  "></div>
            </div>
        </div>
    </div>
</div>