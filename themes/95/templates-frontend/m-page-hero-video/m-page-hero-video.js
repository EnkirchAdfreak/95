$(function() {
    if ($('.m-page-hero-video').length) {
		$( ".m-page-hero-video" ).each(function( index ) {
            var ele = $(this);
            var playButton = ele.find(".playButtonContainer .iconplay");

            var vid = document.getElementById("myVideo");
            var promise = vid.play();


            vid.onplaying = function() {
                //console.log("The video is now playing");
                hidePlayButton();
            };
            
            vid.onplay = function() {
                //console.log("The video is now onplay");
                hidePlayButton();
            };
            
            
            function playVid() {
                vid.play();
            }
            

            function hidePlayButton(){
                playButton.hide();
            }
            
            
            playButton.click(function() {
                playVid();
            });
            
            
            if(promise !== undefined){
                //console.log("autoplay promise") ;
                playVid();
            }else{
                //console.log("nooo  autoplay promise")  
            }
            
            
     
		});	//end each
    }//end length
});//end jrdy