<?php
    $content = get_sub_field('inhalte');

    $playButton = get_stylesheet_directory_uri()."/images/modul/herovideo/playbutton.png";  

    $videoMP4 = $content["video_mp4"];
    $posterimage = $content["poster_image"];

    $poster = wp_get_attachment_image_src( $posterimage, "row-width" );

?>


<div class="m-page-hero-video" >
    <div class="container px-0 containerCustomWidth h-100">
        <div class="row h-100">
            <div class="col-12 h-100">
                <div class="playButtonContainer">
                    <div class="playButtonContainerRelative">
                        <img class="iconplay" src="<?php echo $playButton; ?>"></div>
                    </div>
                    
                <div class="videoHTMLContainer">
                    <video id="myVideo"   muted loop  poster="<?php echo $poster[0]; ?>">
                        <source src="<?php echo $videoMP4; ?>" type="video/mp4" >
                    </video>
                </div>
            </div>
        </div>
    </div>
</div>