<?php
    $content = get_sub_field('inhalte');

    $headline = $content["headline"];

    $anzahlTextspalten = $content["anzahltextspalten"]; // selectvalue, wvl Textspalte

    // je nachdem wvl textspalten es werden, werden hier die columns definiert und im html dann echo
    $column1;
    $column2;


    if($anzahlTextspalten == 1){
        $column1 = "col-12 col-sm-12";
        $column2 = "d-none";
        
    }else if($anzahlTextspalten == 2){
        $column1 = "col-12 col-sm-12 col-lg-6";
        $column2 = "col-12 col-sm-12 col-lg-6";  
    }else{
        $column1 = "col-12 col-sm-12";
        $column2 = "d-none";
    }

    $textbox1 = $content["textbox_1"];
    $textbox2 = $content["textbox_2"];

    $htag = "h2";



?>
<div class="m-page-text">
    <div class="container container-custom-width" >
        
        <?php if($headline != ""): ?>
        <div class="row">
            <div class="col-12">
                <div class="headlines-container">
                    <?php if( $headline != "" ){ ?>
                    
                    <?php
                        echo "<".$htag." class='headline headlineSection headlineSectionLineLeft'>";
                        echo $headline;
                        echo "</".$htag.">";
                    ?>
                    <?php }; ?>

                </div>
            </div>
        </div>
        <?php endif; ?>
        
        <div class="row">
            <div class="<?php echo $column1; ?>">
                <div class="textContainer textContainer1 the-content">
                    <?php echo $textbox1; ?>
                </div>
            </div>
            <div class="<?php echo $column2; ?>">
                <div class="textContainer textContainer2 the-content">
                    <?php echo $textbox2; ?>
                </div>
            </div>
        </div>
        
    </div>
</div>