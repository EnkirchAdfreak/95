<?php
    $content = get_sub_field('inhalte');

    $headline = $content['headline'];
    $htag = "h2";

    $textbox = $content['textbox'];

    $headlineFormular = $content['headline_formular'];
    $htagform = "h2";


    $cf7IDContactProject = $content['cf7_form_id'];

?>



<div class="m-page-contactform">
    <div class="container container-custom-width">
        <div class="row">
            <div class="col-12  col-lg-8 offset-lg-2 col-xl-12 offset-xl-0 ">
                
                <div class="headlineContainer">
                
                    <?php if( $headline != "" ){ ?>
                    
                    <?php
                        echo "<".$htag." class='headline headlineSection headlineSectionLineCentered'>";
                        echo $headline;
                        echo "</".$htag.">";
                    ?>
                    <?php }; ?>
                
                </div>
                
       
            </div>
            
            <div class="col-12  col-md-10 offset-md-1 col-lg-6 offset-lg-3  col-xl-12 offset-xl-0">
                
                <div class="textbox"><?php echo $textbox; ?></div>

            </div>
            
            
            <div class="col-12  col-lg-8 offset-lg-2 col-xl-12 offset-xl-0 ">
                
                <div class="headlineContainerFormular">
                
                    <?php if( $headline != "" ){ ?>
                    
                    <?php
                        echo "<".$htagform." class='headline headlineSection headlineSectionLineCentered'>";
                        echo $headlineFormular;
                        echo "</".$htagform.">";
                    ?>
                    <?php }; ?>
                
                </div>
                
                <div class="contentContainer">
                    <?php echo do_shortcode('[contact-form-7 title="Angebot" id="'.$cf7IDContactProject.'"]'); ?>
                </div>

            </div>
            
        </div> 
    </div>
</div>