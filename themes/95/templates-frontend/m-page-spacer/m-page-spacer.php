<?php
    $content = get_sub_field('inhalte');
    $spacerSize = $content['spacer_size']; // smallest, small, big
    $spacerColor = $content['spacer_color']; // white, transparent


    $spacerHideSmartphone = $content['spacer_hide_smartphone']; // white, transparent
    $spacerHideSmartphoneClass = "";


    if($spacerSize == ""){
        $spacerSize = "smallest";
    }
    if(is_category()){
        $spacerSize = "smallest";
    }


    if($spacerHideSmartphone == true){
        $spacerHideSmartphoneClass = "spacerHideSmartphone";
    }
    

?>

<div class="m-page-spacer m-page-spacer-color-<?php echo $spacerColor; ?> m-page-spacer-size-<?php echo $spacerSize; ?>  <?php echo $spacerHideSmartphoneClass; ?>"></div>