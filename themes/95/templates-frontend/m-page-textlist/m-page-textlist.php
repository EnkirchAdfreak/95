<?php
    $content = get_sub_field('inhalte');


    $headline = $content["headline"];
    



    $backgroundcolor = $content["backgroundcolor"]; // transparent white blue  lightestgrey
    if($backgroundcolor == ""){
        $backgroundcolor = "transparent";
    }


    //headline Tag
    $htag = $content["headlineTag"]; // h1 h2 div
    if($htag == "" || empty($htag)){
        $htag = "h2";
    }
    


    $productsArr = Array();
    $countIDArr = 0;

    if( have_rows('inhalte') ): while ( have_rows('inhalte') ) : the_row(); 
        //echo get_sub_field('horlur');
        if( have_rows('textlist_repeater') ): while ( have_rows('textlist_repeater') ) : the_row(); 

            $productsArr[$countIDArr]["text"] = get_sub_field( "textlist_repeater_text");
            $countIDArr++;

        endwhile; endif;
    endwhile; endif;

?>
<div class="m-page-textlist m-page-textlist-bg-<?php echo $backgroundcolor; ?>    pdfcrowd-remove">
    <div class="container" >
        <div class="row">
            <div class="col-12 col-md-10 col-lg-6 offset-lg-1">
                <div class="textContainer">
                    <?php if( $headline != "" ){ ?>

                    <?php
                        echo "<".$htag." class='headline'>";
                        echo $headline;
                        echo "</".$htag.">";
                    ?>
                    <?php }; ?>
                    <div class="textbox">
                        <ul class="listtext ">
                        <?php
                            foreach ($productsArr as $item) {
                                $text = $item["text"];
                                echo "<li class='contentBox'>";    
                                echo $text;
                                echo "</li>";
                            }
                        ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>