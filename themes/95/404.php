<?php
    //header("HTTP/1.1 301 Moved Permanently");
    //header("Location: ".get_bloginfo('url'));
    //exit();

    get_header();



    $headline = get_field('options_404_headline','option');
    $text = get_field('options_404_text','option');
    //$subheadline = get_field('options_404_subheadline','option');
    //$text = get_field('options_404_text','option');


    $linkbutton = get_field('options_404_button','option');
    if( $linkbutton ): 
        $link_url = $linkbutton['url'];
        $link_title = $linkbutton['title'];
        $link_target = $linkbutton['target'] ? $linkbutton['target'] : '_self';
        
    endif;





    $buttonsearchTitle = get_field('options_404_button2_labeltext','option');
    $buttonsearchURL = get_search_link();



               



?>



     


<div class="page404">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="headlines-container">
                    <div class="headline-container">
                        <?php
                            $htag = "h2";
                            echo "<".$htag." class='headline headlineSection headlineSectionLineCentered'>";
                            echo $headline;
                            echo "</".$htag.">";
    
                        ?>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-12 col-md-10 offset-md-1 col-xl-6 offset-xl-3">
                <div class="textcontainer">
                    <?php echo $text; ?>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-12 col-md-4 offset-md-2 col-lg-3 offset-lg-3 col-xl-2 offset-xl-4">
                <div class="buttonContainer1">
                    <a class="button button95Light" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo $link_target; ?>"><?php echo $link_title; ; ?></a>
                </div>
            </div>
            
            <div class="col-12 col-md-4 col-lg-3 col-xl-2">
                <div class="buttonContainer2">
                    <a class="button button95Light" href="<?php echo esc_url( $buttonsearchURL ); ?>" target="_self"><?php echo $buttonsearchTitle; ?></a>
                </div>
            </div>
            
        </div>
        
    </div>
</div>


<?php
    get_footer();
?>