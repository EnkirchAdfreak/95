<?php
    //Blog Detailseite Standard
    get_header();
    $date = get_the_date();
    $blogimage = get_the_post_thumbnail();
?>

<section class="blogDetailPage">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="headline"><?php the_title()?></h1>
                <div class="date"><?php echo $date; ?></div>
            </div>
        </div> 
    </div>    

    <div class="container">
        <diV class="row">
            <div class="col-12">
                <div class="detailpost-headerimage-container">
                    <?php 
                        //echo wp_get_attachment_image($blogimagearticle, 'col-md', "", array( "class" => "detailpost-headerimage"));
                        echo $blogimage;
                    ?>
                </div>
            </div>
        </diV>
    </div>

    <div class="blog-content">
        <?php af_include_template('flexible-content-wrapper'); ?>
    </div>

</section>

<?php
    get_footer();
?>
