<?php
    get_header();
    /* Template Name: Search Page */

    $pageHeadline = get_field('options_search_page_headline','options');
    $pageResultText = get_field('options_search_page_resultext','options');

    $pageNoResultText = get_field('options_search_page_noresultext','options');
    $pageNoResultText2 = get_field('options_search_page_noresultext_2','options');
?>

<div class="searchContainer">
    <div class="container container-custom-width">
        <div class="row">
            <div class="col-12">
                <div class="pageHeadlineContainer">    
                    <?php
                        $htag = "h2";
                        //$headline = "Suche";
                        echo "<".$htag." class='headline headlineSection headlineSectionLineCentered'>";
                        echo $pageHeadline;
                        echo "</".$htag.">";
                    ?>
                </div>
            </div>
        </div>
        
        
          <div class="row">
                <?php if ( have_posts() ) : ?>
    
                    <div class="col-12"> 

                        <div class="searchFormcontainer">
                            <form id="searchform" method="get" action="<?php echo home_url('/'); ?>">
                                <input type="text" class="search-field" name="s" placeholder="<?php _e('search_placeholder_searchinput', 'neun'); ?>" value="<?php echo the_search_query(); ?>"><!--
                                --><input type="submit" class="searchsubmit" value="Search">
                            </form>
                        </div>

                        <div class="searchText">
                            <div><?php echo $pageResultText; ?> <span class="phrase"><?php echo the_search_query(); ?></span></div>
                        </div>
                        
                    </div>

                    <div class="col-12 animatedParent"  data-sequence='500'> 
                        <?php /* Start the Loop */ 
                            $countSearchResult = 1;
                        
                            while ( have_posts() ) : the_post();

                                $postID = get_the_ID();
                                $title = get_the_title();
                                $excerpt = get_the_excerpt();
                                $image = get_the_post_thumbnail( $postID, 'col-md' );
                                $permaLink = get_the_permalink();
                            
                        ?>
                        <div class="row rowResult animated fadeIn" data-id='<?php echo $countSearchResult; ?>'>
                        
                            <div class="d-none d-md-inline-block col-md-4 col-lg-2 offset-lg-2 col-xl-3 offset-xl-1">
                                <div class="imageContainer">
                                    <a href="<?php echo $permaLink; ?>">
                                    <?php 
                                        if($image != ""){
                                            //Article Image avaiable
                                            echo $image;
                                        }else{
                                            //placeholder image
                                            $image = get_stylesheet_directory_uri()."/images/search/featureImage_placeholder.jpg";
                                            echo "<img src='".$image."'>";
                                        }
                                    ?>
                                    </a>
                                </div>
                            </div>
                           
                            <div class="col-12 col-md-8 col-lg-5 col-xl-6">
                                <div class="content searchResultContainerText">
                                    <div class="searchTitle"><a href="<?php echo $permaLink; ?>"><?php the_title(); ?> </a></div>
                                    <div class="searchExcerpt"><?php the_excerpt(); ?></div>
                                    <div class="searchLinkage"><a class="buttonsearchresult button95Light" href="<?php the_permalink(); ?>"><?php _e('search_topost', 'neun'); ?></a></div>
                                </div>
                            </div>
                            
                            <div class="col-12"><hr class="rowHR"></div>
                            
                            
                        
                        </div>

                        <?php 
                            $countSearchResult++;
                            endwhile;
                            //the_posts_navigation();
                        ?>

                    </div>
              
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="pagination-container">
                                    <?php if (function_exists("pagination")) {pagination($post->max_num_pages); } ?>
                                </div>
                            </div>
                        </div>
                    </div>
              

                <?php else : ?>
              

              
                    <div class="col-12">
                        <div class="searchFormcontainer">
                            <form id="searchform" method="get" action="<?php echo home_url('/'); ?>">
                                <input type="text" class="search-field" name="s" placeholder="<?php _e('search_placeholder_searchinput', 'neun'); ?>" value="<?php echo the_search_query(); ?>"><!--
                                --><input type="submit" class="searchsubmit" value="Search">
                            </form>
                        </div>
                        
                        <div class="noResultsText">
                            <div class="noResultText"><?php echo $pageResultText; ?> <span class="phrase"><?php echo the_search_query(); ?></span></div>
                            <div class=""><?php echo $pageNoResultText2; ?></div>
                        </div>
                        
                    </div>
              
                <?php endif; ?>        

               </div>
        </div>  <!-- end bs container -->   
    </div><!-- end modul container -->



    <?php

        $search_bottom_headline = get_field('options_search_help_headline','options');
        $search_bottom_textbox = get_field('options_search_help_text','options');

        $search_bottom_link =  get_field('options_search_help_link','options');
        if( $search_bottom_link ): 
            $search_bottom_link_url = $search_bottom_link['url'];
            $search_bottom_link_title = $search_bottom_link['title'];
            $search_bottom_link_target = $search_bottom_link['target'] ? $search_bottom_link['target'] : '_self';
        endif;

        $search_bottom_htag = "h2";

        $search_bottom_buttonStyle = "button95Light";

    ?>


    <div class="m-page-text-centered-search">
        <div class="container container-custom-width" >
            <div class="row">
                <div class="col-12 col-md-10 offset-md-1 col-lg-8 offset-lg-2 col-xl-12 offset-xl-0">
                    <div class="textContainer">

                        <?php if( $search_bottom_headline != "" ){ ?>

                        <?php
                            echo "<".$search_bottom_htag." class='headline'>";
                            echo $search_bottom_headline;
                            echo "</".$search_bottom_htag.">";
                        ?>
                        <?php }; ?>


                        <div class="textbox"><?php echo $search_bottom_textbox; ?></div>

                        <?php if( $search_bottom_link ): ?>
                        <div class="buttonContainer">
                            <a class="button <?php echo $search_bottom_buttonStyle; ?>" href="<?php echo esc_url( $search_bottom_link_url ); ?>" target="<?php echo esc_attr( $search_bottom_link_target ); ?>"><?php echo esc_html( $search_bottom_link_title ); ?></a>
                        </div>
                        <?php endif; ?>

                    </div>
                </div>
            </div>
        </div>
    </div>



<?php
    get_footer();
?>