</div> <!-- end content container -->


<?php require_once('template-parts/modal_search.php'); ?>
<?php require_once('template-parts/footer_contact.php'); ?>




<?php

$footerCopyRightText = get_field('options_footer_contacttext_copyright_text', 'option');

?>

<footer id="footer" class="pdfcrowd-remove">
    <div class="container container-custom-width">
        <div class="row">
            <div class="col-12 col-md-10 offset-md-1  col-xl-12 offset-xl-0">
                <div class="textinfo"><?php echo $footerCopyRightText; ?></div>
                 <?php
                    /*wp_nav_menu( array(
                      'menu'              => 'primary',
                      'theme_location'    => 'footer-menu-legal',
                      'depth'             => 0,
                      'container'         => 'ul',
                      'menu_id'   => 'footer-navigation-legal',
                      'container_id'      => '',
                      'menu_class'        => '',
                      'before'             => '<span class="divider">|</span>'
                    )
                    );*/
                  ?>
            </div>
        </div>
    </div>
</footer>

<?php
    wp_footer();
?>

</body>
</html>