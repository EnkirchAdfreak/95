<?php

// Check and Loop through modules

if( have_rows('inhaltsmodule') ):

    // Pre Modules

    while ( have_rows('inhaltsmodule') ) : the_row();

        // Pre Module

        // Module
        if( get_row_layout() != '' ):

            if(get_row_layout() == "page-maps"){
                af_enqueue_styles_googlemaps();
            }

            af_include_module(  get_row_layout() );



        else:

            // Debug
            if( af_is_dev_mode() ) {
                echo '<div class="container">Das Modul "'.get_row_layout().'" konnte nicht gefunden werden.</div>"';
            }

        endif;

        // Post Module

    endwhile;

    // no content found
    // @TODO: redirect to 404?

endif;