<?php
  get_header();
?>
<?php
    $tileimage =  get_field("news_tile_image");
    $location =  get_field("news_tile_location");
    $description =  get_field("news_description");
    $title = get_the_title();


    $refarr = Array();
    $countIDArr = 0;

    // check if the repeater field has rows of data
    if( have_rows('news_repeater_images') ):

        // loop through the rows of data
        while ( have_rows('news_repeater_images') ) : the_row();
            // display a sub field value
            $refarr[$countIDArr]["imageID"] = get_sub_field("news_repeater_images_image");
            $countIDArr++;

        endwhile;
    else :
    endif;



    $totalposts = wp_count_posts("news"); //Das gibt alle Posts ueber alle Sprachen. $totalposts->publish;




	$totalpostsFix = count( get_posts('post_type=news&suppress_filters=0&posts_per_page=-1') );





?>



<?php
    $currentPostID = get_the_ID();
    $currentReferenceIndex;

    $args = array(
     'post_type' => 'news',
     'posts_per_page' => 5,
     'orderby' => 'date',
     'order' => 'DESC'
    );

    $custom_query = new WP_Query($args); 

    if ($custom_query->have_posts()) : while($custom_query->have_posts()) : $custom_query->the_post(); 
  
        if(get_the_ID() == $currentPostID){
   
            $currentReferenceIndex = $custom_query->current_post + 1;
        }
    endwhile; 
    endif;
 
    wp_reset_postdata(); 

    wp_reset_postdata();
    wp_reset_query()


?>






<section class="singlenews">
    

<div class="container container-custom-width">
    <div class="row">
        <div class="col-12 d-xl-none">
            <ul class="referenznavigation referenznavigationMobile">
                <li class="back arrow">
                                             <?php

                        if( get_adjacent_post(false, '', false) ) { 
                           
                            next_post_link('%link', 'Next Post &rarr;');
                        } else { 
                         
                            $last = new WP_Query('posts_per_page=1&orderby=date&order=ASC&post_type=news'); 
                            $last->the_post();
                            echo '<a href="' . get_permalink() . '">Next Post &rarr;</a>';
                            wp_reset_query();
                        }; 
                        ?>  
                        
                </li>
                <li class="pagination"><?php echo $currentReferenceIndex."/".$totalpostsFix; ?></li>
                <li class="next arrow">
                                    <?php
                        if( get_adjacent_post(false, '', true) ) { 
                            $gg = previous_post_link('%link', '&larr; Previous Post');
                            echo  $gg;
                        } else { 
                            $first = new WP_Query('posts_per_page=1&orderby=date&order=DESC&post_type=news'); $first->the_post();
                            echo '<a href="' . get_permalink() . '">&larr; Previous Post</a>';
                            wp_reset_query();
                        }; 
                        ?>
                </li>
            
            </ul>
            
        </div>
    
    </div>

    <div class="row">
        <div class="col-12 col-xl-6 order-xl-2 ">
            <div class="projectscontent">  
                <ul class="referenznavigation d-none d-xl-block">
                    <li class="back arrow">
                        
                                      <?php

                        if( get_adjacent_post(false, '', false) ) { 
                           
                            next_post_link('%link', 'Next Post &rarr;');
                        } else { 
                         
                            $last = new WP_Query('posts_per_page=1&orderby=date&order=ASC&post_type=news'); 
                            $last->the_post();
                            echo '<a href="' . get_permalink() . '">Next Post &rarr;</a>';
                            wp_reset_query();
                        }; 
                        ?>  
                        
                        
              
                        
                        
                    </li>
                    <li class="pagination"><?php echo $currentReferenceIndex."/".$totalpostsFix; ?></li>
                    <li class="next arrow">
                        
                        
                        
        
                                <?php
                        if( get_adjacent_post(false, '', true) ) { 
                            $gg = previous_post_link('%link', '&larr; Previous Post');
                            echo  $gg;
                        } else { 
                            $first = new WP_Query('posts_per_page=1&orderby=date&order=DESC&post_type=news'); $first->the_post();
                            echo '<a href="' . get_permalink() . '">&larr; Previous Post</a>';
                            wp_reset_query();
                        }; 
                        ?>
                           
                        
                        
                    </li>

                </ul>
                
                <div class="projectscontentTextBox animatedParent animateOnce">
                
                    <div class="headline    animated fadeIn">
                        <?php echo $title;?>
                    </div>  
                    <div class="description animated fadeIn the-content">
                    <?php echo $description;?>
                    </div>
                </div>
                
                
            </div>
        </div>
        
        
        <div class="col-12 col-xl-6">

            <ul class="projectsImages animatedParent animateOnce" data-sequence='500'>
                <?php 
                    $countreferenceimages = 1;
                    foreach ($refarr as $item) {
                    $image = $item["imageID"];
                ?>  
                    <li class="animated fadeIn " data-id='<?php echo $countreferenceimages; ?>'>
                         <?php echo wp_get_attachment_image($image, 'col-sm', "false", array( "class" => "articleImage"));?>
                
                    </li>
                <?php 
                    $countreferenceimages++;
                        
                };
                
                ?>
            </ul>
        </div>

    </div>

</div>

</section>




<?php 
    af_include_template('flexible-content-wrapper');
?>

<?php
	get_footer();
?>