module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            options: {
                stripBanners: true,
                banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
                    '<%= grunt.template.today("yyyy-mm-dd") %> */',
            },
            dist: {
                src: ['src/js/vendor/*.js', 'src/js/global.js', 'templates-frontend/**/*.js', 'src/js/animate.js'],
                dest: 'dist/js/scripts.js',
            },
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
            },
            dist: {
                files: [
                    {'dist/js/scripts.min.js': ['dist/js/scripts.js']}
                ]
            },
        },
        sass: {
            dist: {
                options: {
                    sourcemap: 'none'
                },
                files: {
                    'src/css/single/style.css' : 'src/sass/wrapper.sass'
                },
            },
        },
        concat_css: {
            options: {},
            all : {
                src: ["src/css/vendor/*.css","src/css/single/*.css"],
                dest: "dist/css/style.css"
            },
        },
        cssmin: {
            options: {
                mergeIntoShorthands: false,
                roundingPrecision: -1
            },
            target: {
                files: {
                    'dist/css/style.min.css': 'dist/css/style.css'
                },
            },
        },
        watch: {
            scripts: {
                files: ['src/js/*.js', 'templates-frontend/**/*.js'],
                tasks: ['concat', 'uglify'],
                options: {
                    interrupt: true,
                },
            },
            css : {
                files: ['src/sass/*.sass', 'templates-frontend/**/*.sass'],
                tasks: ['sass', 'concat_css', 'cssmin'],
                options: {
                    interrupt: true,
                },
            },
        },
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-concat-css');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['concat', 'uglify', 'sass', 'concat_css', 'cssmin']);
};
