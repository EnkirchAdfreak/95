$( document ).ready(function() {
    var themepath = themeData.themePath;
    var ajaxurl = themeData.ajaxUrl;
    
    var breakPointMD = 768;
    var breakPointLG = 992;
    var breakPointXL = 1200;

    //----------------------------------------------;
    //page reload options
    //----------------------------------------------;

    window.onbeforeunload = function () {
        //Page jumps to position on reload
        //window.scrollTo(0, 0);
    }

    $(window).on('load resize', function(){
        //console.log("load resize")
        //var windowWidth = $(window).width();
    });//end of resize/load

    $(window).on('load', function() {
        //console.log("window onload only")
        //var windowWidth = $(window).width();
    });

    
    $( window ).resize(function() {
        //console.log("window resize only")
        //var windowWidth = $(window).width();
    });
    
    
    //-------------------------------------------------------------------------------------;
    // Mobile check
    //-------------------------------------------------------------------------------------;
    
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        $("body").data("ismobile","true");
        $("body").attr("data-ismobile","true");        
    }else{
        $("body").data("ismobile","false");
        $("body").attr("data-ismobile","false");
    }
    
    
    //----------------------------------------------;
    // Upper menue desktop
    //----------------------------------------------; 
    
    var menueHeightDesktop;
    var animationSpeedDesktop = 250;
    var isMenueOpen = false;
    
    
    $( ".topNavigation .desktopHamburgerContainer" ).click(function($event) {
        //console.log("click hamburger");
        openCloseDesktopMenue();
    });
    
    $(".body-content-wrapper").on('click', function (evt) {
        if(isMenueOpen){
            evt.stopPropagation();
            evt.preventDefault();
            openCloseDesktopMenue();
        }
    }); 
    
    
    function openCloseDesktopMenue(){
        if( $(".menueNavigationDesktop").is(":visible") ){
            $(".menueNavigationDesktop").slideUp();
            
            $(".desktopHamburgerContainer .hamburg").removeClass("checked");
            isMenueOpen = false;
            
            //$(".topNavigation .desktopHamburgerContainer .hamburger" ).show();
            //$(".topNavigation .desktopHamburgerContainer .hamburgerClose" ).hide();
        }else{                                                      
            $(".menueNavigationDesktop").slideDown();
            $(".desktopHamburgerContainer .hamburg").addClass("checked");
            //$(".topNavigation .desktopHamburgerContainer .hamburger" ).hide();
            //$(".topNavigation .desktopHamburgerContainer .hamburgerClose" ).show();
            
            isMenueOpen = true;
        }
        
        
        menueHeightDesktop = $(".menueNavigationDesktop #menueNavigation").height(); 
    }
    
    //openCloseDesktopMenue();
    //isMenueOpen = true;
    
    

    //menue Desktop
    $( "#menueNavigation .menu-item-has-children > a" ).attr("href","")
    

    $( "#menueNavigation .menu-item-has-children > a" ).click(function($event) {
        $( "#menueNavigation .menu-item-has-children > a" ).removeClass("clickedText");
        $(this).addClass("clickedText");
        
    });
    
    
    
    $( "#menueNavigation .menu-item-has-children > a" ).click(function($event) {
        $event.preventDefault();
        //console.log("click menue menu-item-has-children");
        //console.log("menueHeightDesktop: "+menueHeightDesktop)

        var menueItemId = $(this).parent().data("menueitemid");

        //hide other Menue Content
        $(".menueNavigationDesktop #menueNavigation .menu-item-has-children .sub-menu").css({"opacity" : "0","display" : "none"})
        $(".menueNavigationDesktop ul.menueinfotext li").hide();

        var submenue = $(this).parent().find(".sub-menu");
        var submenueheight = $(this).parent().find(".sub-menu").height();
        
        
        if(submenueheight >= menueHeightDesktop){
            $(".menueNavigationDesktop #menueNavigation").stop().animate({
                height: submenueheight
            }, animationSpeedDesktop, function() {
                submenue.css({"opacity" : "1","display" : "block"});
                //var infoText = $(".menueNavigationDesktop .menueinfotext").find("[data-menueitemid="+menueItemId+"]");
                //infoText.fadeIn("fast");
            });
        }else{

            $(".menueNavigationDesktop #menueNavigation").stop().animate({
                height: menueHeightDesktop
            }, animationSpeedDesktop, function() {
                submenue.css({"opacity" : "1","display" : "block"})
                //var infoText = $(".menueNavigationDesktop .menueinfotext").find("[data-menueitemid="+menueItemId+"]");
                //infoText.fadeIn("fast");
            });
        }

    });
    
    
    $( "#menueNavigation .menu-item-has-children > a" ).mouseover(function($event) {
        $(".menueNavigationDesktop .menueinfotext li").css({"display" : "none"})
        $(".menueNavigationDesktop .menueinfotext").css({"display" : "none"})
        
    });
    
    
    $( "#menueNavigation .sub-menu a" ).mouseover(function($event) {
        $(".menueNavigationDesktop .menueinfotext li").css({"display" : "none"})

        var menueItemId = $(this).parent().data("menueitemid");
      
        var infoText = $(".menueNavigationDesktop .menueinfotext").find("[data-menueitemid="+menueItemId+"]");
        var box = $(".menueNavigationDesktop .menueinfotext");
        
        infoText.fadeIn("fast");
        box.fadeIn();
    });
    
    
    //----------------------------------------------;
    // Upper menue mobile
    //----------------------------------------------; 
    
    
    $( ".topNavigation .mobileHamburgerContainer" ).click(function($event) {
        if( $(".menueNavigationMobile").is(":visible") ){
           $(".menueNavigationMobile").slideUp();
            
            $(".mobileHamburgerContainer .hamburg").removeClass("checked");
            

        }else{                                                      
           $(".menueNavigationMobile").slideDown();
            $(".mobileHamburgerContainer .hamburg").addClass("checked");

        }
    });
    
    
    
    $( "#menueNavigationMobile .menu-item-has-children > a" ).attr("href","");
    
    var is_subMenueOpenMobile = false;
    
    $( "#menueNavigationMobile .menu-item-has-children > a" ).click(function($event) {
        $event.preventDefault();
        var headline = $(this).html();
        $(".menueNavigationMobile .submenueHeaderContainer .submenueHeaderContainerHeadline .text").html(headline);

        var allSubMenues = $("#menueNavigationMobile").find(".sub-menu");
        var subMenue = $(this).parent().find(".sub-menu"); 
        allSubMenues.hide();
        subMenue.show();
        
        animateMobileMenue();
    });
    
    $( ".menueNavigationMobile .submenueHeaderContainerHeadline" ).click(function($event) {
        $event.preventDefault();
        is_subMenueOpenMobile = true;
        animateMobileMenue();
    });
    
    function animateMobileMenue(){
        //var fullmenueContainer = $(".menueNavigationMobile .navigationContainer");
        var fullmenueContainerHeadline = $(".menueNavigationMobile .submenueHeaderContainer .submenueHeaderContainerHeadline");
        var fullmenueContainer = $(".menueNavigationMobile .navigationContainer, .searchContainerMobile, .mobileLanguageSwitcherContainer");
        
        if(!is_subMenueOpenMobile){
            is_subMenueOpenMobile = true;

            fullmenueContainer.animate({
                marginLeft: "-100vw"
            }, 300, function() {
            });
            fullmenueContainerHeadline.animate({
                marginLeft: "0vw"
            }, 300, function() {
            });
        }else{
            is_subMenueOpenMobile = false;
            fullmenueContainer.animate({
                marginLeft: "0vw"
            }, 300, function() {

            });
            fullmenueContainerHeadline.animate({
                marginLeft: "100vw"
            }, 300, function() {
            });
        }
    }
    
    //----------------------------------------------;
    // Open Search Overlay Desktop
    //----------------------------------------------; 
    $( ".topNavigation .searchContainer " ).click(function($event) {
        $('#searchmodal').modal({
          keyboard: true
        });
        
        $('#searchmodal').on('shown.bs.modal', function (e) {
          $("#searchmodal .search-field").focus();    
        })

    });
    

    //----------------------------------------------;
    // Open Search Mobile
    //----------------------------------------------; 
    $( ".menueNavigationMobile .searchContainerMobile" ).click(function($event) {
        var searchtext = $( ".menueNavigationMobile .searchContainerMobile .content .text" );
        var searchInput = $( ".menueNavigationMobile .searchContainerMobile .content .searchcontainer" );
        var searchInputField = $( ".menueNavigationMobile .searchContainerMobile .content .searchcontainer .search-field-mobile" );
        
        
        
        searchtext.hide();
        searchInput.show();
        
        searchInputField.focus();
        searchInputField.val("");
        
    });   
    
    
    
    
    //----------------------------------------------;
    // Languages Desktop Top Navigation
    //----------------------------------------------; 
    
    $( ".topNavigation .lngContainer" ).mouseover(function($event) {
        $event.preventDefault();
        var menue = $(".topNavigation .lngContainer .languages");
        menue.animate({
            height: "97px"
        }, {queue: false}, 300, function() {
        });
        
    });
    
    $( ".topNavigation .lngContainer" ).mouseleave(function($event) {
        $event.preventDefault();
        var menue = $(".topNavigation .lngContainer .languages");
        menue.animate({
            height: "30px"
        }, {queue: false}, 300, function() {
        });
    });
    
    
    //----------------------------------------------;
    // Single projects fix the text on Scroll
    //----------------------------------------------; 
    
    var breakPointProjectsSingleScroll = 1200;
    var windowWidtProjectsSingleScroll = $(window).width();

    $(window).scroll(function(){
        
        if(windowWidtProjectsSingleScroll > breakPointProjectsSingleScroll){
            var singleprojectsHeights = $(".singleprojects").height();
            var current = $(document).scrollTop();
            var contentHeight = $(".singleprojects .projectscontent ").height();
            if(current + 57 + contentHeight > singleprojectsHeights){
                $(".singleprojects .projectscontent ").css("position","absolute");
                $(".singleprojects .projectscontent ").css("bottom","15px");
            }else{
                $(".singleprojects .projectscontent ").css("position","");
                $(".singleprojects .projectscontent ").css("bottom","");
            }
        }else{
            $(".singleprojects .projectscontent ").css("position","");
            $(".singleprojects .projectscontent ").css("bottom","");
        }
        

        
    });
    
    

    
    
});//end jqry rdy