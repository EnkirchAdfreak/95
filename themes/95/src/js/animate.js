

$( document ).ready(function() {
    var themepath = themeData.themePath;
    var ajaxurl = themeData.ajaxUrl;
    var isMobile = $("body").data("ismobile");
    

    //-------------------------------------------------------------------------------------;
    //Body Animation, Opacity bei load
    //-------------------------------------------------------------------------------------;
    //$("html, body").delay(350).animate({opacity: 1}, 200); 
    

    //-------------------------------------------------------------------------------------;
    //function fps checker
    //-------------------------------------------------------------------------------------; 
 
    var fpsDropCount = 0;
    var fpsDropCountMax = 25;
    var fpsDropCountMaxLast = 40;
    
    var fpsDropBoolean = false; //Wenn False, dann läuft der FPS zähler
    
    
    var fpsCheckAnimationActive = true; //Animation aus/an
    var fpsCheckAnimationActiveLast = true;
    var minFPS = 30;
    
    var fpsCheckerStarted = false;

    //$('.fpstester_minZahl').html("min value: "+minFPS);
    //$('.fpstester_drop1').html("Drop1 @: "+fpsDropCountMax);
    //$('.fpstester_droplast').html("Drop2 @: "+fpsDropCountMaxLast);
    

    
    
    //------------------------------------------------;
    // Hier werden nur die FPS gezaehlt 
    // und fps data-attribut
    //------------------------------------------------;
    
    window.countFPS = (function () {
      var lastLoop = (new Date()).getMilliseconds();
      var count = 1;
      var fps = 0;

      return function () {
        var currentLoop = (new Date()).getMilliseconds();
        if (lastLoop > currentLoop) {
          fps = count;
          count = 1;
        } else {
          count += 1;
        }
        lastLoop = currentLoop;
        return fps;
      };
    }());


    
    (function loop() {
        requestAnimationFrame(function () {
            
            //$out.html("Current value: " + countFPS());
            //$fpsout.html("Drops Count: "+fpsDropCount+"/"+fpsDropCountMaxLast);
            
            loop();
            
            if(fpsDropBoolean == false){
                //$("body").data("currentFPS",countFPS())
                //$("body").attr("data-currentFPS",countFPS());
                //loop();
            }else{
                //$out.html("aus");
            }
        });
    }());
    

    


    //-------------------------------------------------------------------------------------;
    //onScroll, requestAnimationFrame, raf
    //-------------------------------------------------------------------------------------;
    var raf_animate = window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.msRequestAnimationFrame ||
        window.oRequestAnimationFrame;
    var $window = $(window);
    var lastScrollTop = $window.scrollTop();
    var breakPointScrollAnimation = 1400;
    var ismobileAnimateJS = $("body").data("ismobile");
    var isMobileFlowgram = true;
    
    
    if (raf_animate) {
        loop_raf();
    }

    function loop_raf() {
        var scrollTop = $window.scrollTop();
        if (lastScrollTop === scrollTop) {
            raf_animate(loop_raf);
            return;
        } else {
            lastScrollTop = scrollTop;
            // fire scroll function if scrolls vertically

               afScrollAnimation();

            
            raf_animate(loop_raf);
        }
    }
    
      
 
    
    function afScrollAnimation(){

        var windowWidth = $(window).width();
 


        if ($('.m-page-product-detail_page_desktop').length ) {

           
            var withinViewportModulProductBeamUp = $('.m-page-product-detail_page_desktop').percentWithinViewport({
                'offsetTop': 0
            });

            $.each(withinViewportModulProductBeamUp, function(index) {
                //var myPercVisible = parseInt($(this).attr('data-percent-viewport'));
                var element = $(this);
                var currentScrollPosition = $window.scrollTop();
                var headerHeight = $("header").outerHeight();
                var sectionOuterHeight = $(this).outerHeight();
                var topPosSection = (  $(this).offset().top - (sectionOuterHeight ) ) ;
                var offset = 30;
                var difference = currentScrollPosition - (topPosSection + offset);
 

                if( difference < 0 ){
                    //zu weit unten
                   /* textbox.css({
                      '-webkit-transform' : 'translate3d(0,0,0)',
                      'transform'         : 'translate3d(0,0,0)'
                    });*/

                }else{
       
                    
                    if(difference > 100 ){
                        
                        
                        if($(this).data("firstappearance") == false){
                            var fillStrokeHover = $(this).data("colororange");
                            
                            
                            var svgurl = element.data("svgurl");
                            
                   
                            var svgimg = new Image();
                            svgimg.onload = svgimgloadedScroll;
                            svgimg.src = svgurl;
                            
                            
                            function svgimgloadedScroll(){
                                //console.log("svgimgloadedScroll()");
                                var svgTransparent = "rgba(125,125,125,0)";
                                

                                var a = document.getElementById("svgObjectbeamup");
                                var svgDoc = a.contentDocument;
                                var bss = svgDoc.querySelectorAll('polygon[data-name="click-1"]');


                                var variableHolder = {};
                                variableHolder = bss;
                                
                                //console.log("scroll variableHolder: "+variableHolder)
                                //console.log(variableHolder)

                                $.each(variableHolder, function(index, value) {
                                    //console.log("scroll each")
                                    //console.log("fillStrokeHover: "+fillStrokeHover)
                                    var index = variableHolder[index];
                                    $(index).css("stroke", fillStrokeHover);
                                    $(index).css("fill", svgTransparent);
                                    
                                    
                                     
                                });
                                
                           
                                

                                $(".m-page-product-detail_page_desktop ul.textlist").fadeIn();
                                $(".m-page-product-detail_page_desktop ul.textlist li:first-child").fadeIn();
                                    firstAppearance = true;
                                }


                                $(this).data("firstappearance",true);
                            }
                       
 
                      
                    }
                    
                }
            });

        }//end if module exists        
        
        
        
        



        if ($('.m-page-flowgram').length ) {

            var withinViewportModulFlowgram = $('.m-page-flowgram').percentWithinViewport({
                'offsetTop': -200
            });

            $.each(withinViewportModulFlowgram, function(index) {
                //var myPercVisible = parseInt($(this).attr('data-percent-viewport'));
              
                var isMobile = $("body").data("ismobile");
                var textbox  = (this).find(".flowprocessTextContent");
                var module = $(this);

                var currentScrollPosition = $window.scrollTop();
                var headerHeight = $("header").outerHeight();
                var sectionOuterHeight = $(this).outerHeight();
                var topPosSection = (  $(this).offset().top - (sectionOuterHeight ) ) ;
                var offset = 30;
                //var difference = currentScrollPosition - ( 114 );
                
                var difference = currentScrollPosition - (topPosSection + offset);


                if(isMobileFlowgram == false){
                    //console.log("desktop")

                    if( difference < 0 || windowWidth <= breakPointScrollAnimation){
                        //zu weit unten
                       /* textbox.css({
                          '-webkit-transform' : 'translate3d(0,0,0)',
                          'transform'         : 'translate3d(0,0,0)'
                        });*/

                    }else{
                        //console.log("else");
                        //var step1 = 100;
                        var scrollStep = 0;
                        //console.log(difference);

                        if(difference > 0 && difference < 150){
                           scrollStep = 1;
                        }

                        if(difference > 150 && difference < 300){
                           scrollStep = 2;
                        }

                        if(difference >= 300 && difference < 450){
                           scrollStep = 3;
                        }

                        if(difference >= 450 && difference < 600){
                           scrollStep = 4;
                        }

                        if(difference >= 600 && difference < 750){
                           scrollStep = 5;
                        }
                        if(difference >= 750 && difference < 900){
                           scrollStep = 6;
                        }

                        if(difference > 900){
                           scrollStep = 7;
                        }


                        var textboxtest  = (this).find(".contentBox"+scrollStep);
                        var numberGlowAnimate = (this).find(".numberContainer"+scrollStep+" .numberBg3");
                        var numberGlowRemove = (this).find(".numberContainer .numberBg3");

                        var numberGlowText = (this).find(".numberContainer"+scrollStep+" .numberBg3 .textnumber");

                        var animateClass;
                        var animateClassNumber;


                        for(var i = 0; i < scrollStep ; i++){

                        }


                        if (scrollStep % 2 === 0) {
                            //even
                            animateClass = "animateRight";
                            animateClassNumber = "numberBg3GlowEven";
                        }else{
                            //odd
                            animateClass = "animateLeft";
                            animateClassNumber = "numberBg3GlowOdd";
                        }

                        textboxtest.addClass(animateClass);

                        
                        numberGlowRemove.removeClass("numberBg3GlowEven numberBg3GlowOdd");
                        numberGlowAnimate.addClass(animateClassNumber);
                        numberGlowText.addClass("textnumberAnimate");

                    }
                    
                }else if(isMobileFlowgram == true){
                    
                    
                    
                    if(module.data("firstappearancemobile") == true){
                    
                        module.data("firstappearancemobile","false");
                        module.attr("data-firstappearancemobile","false");
                   
                        for(var i = 1; i < 8 ; i++){
                            if (i % 2 === 0) {
                                //even
                                animateClass = "animateRight";
                                animateClassNumber = "numberBg3GlowEven";
                            }else{
                                //odd
                                animateClass = "animateLeft";
                                animateClassNumber = "numberBg3GlowOdd";
                            }


                            var textboxtest  = (this).find(".contentBox"+i);
                            var numberGlowAnimate = (this).find(".numberContainer"+i+" .numberBg3");
                            var numberGlowText = (this).find(".numberContainer"+i+" .numberBg3 .textnumber");

                            var delay = i * 2000;
                            var numberLoop = i;
                            //console.log("textboxtest",textboxtest)
                            //console.log("i counter",i)
                            //console.log("delay",delay)
                            
                            console.log(delay)
                            console.log(textboxtest);

                            setTimeout(mobileShowStuff(textboxtest,numberGlowText,numberGlowText,animateClass,animateClassNumber, numberLoop), delay)
                            
                            //numberGlowAnimate.addClass(animateClassNumber);

                        };
                        
                        
                        function mobileShowStuff(textboxtest,numberGlowText,numberGlowText,animateClass,animateClassNumber, numberLoop){
                    
                            var tDelay = numberLoop * 200;
                            
                            textboxtest.addClass(animateClass);
                            textboxtest.css({'transition-delay' : tDelay+'ms'});
                          
                            
                            //numberGlowText.addClass(animateClassNumber);
                            numberGlowText.css("opacity","1");
                            numberGlowText.css({'transition-delay' : tDelay+'ms'});
                            
                            
                            
                        }
                        
                    };

                }
                    
            });

        }//end if module exists
        
        
        

 
        
       
    };//end on Scroll Function afScrollAnimation()
    

    
    
});//end jqry rdy