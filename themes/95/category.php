<?php
	get_header();
?>
    <section class="postTilesSection">
        <div class="container">
            <?php 
                get_template_part('template-parts/posttiles'); 
            ?>
        </div>
    </section>
<?php
	get_footer();
?>
