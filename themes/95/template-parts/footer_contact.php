<?php
    $contacttext = get_field('options_footer_contacttext',"option");

    $footerlogoID = get_field("options_footer_contact_logo","option");

?>

<div class="footerContactContainer pdfcrowd-remove">
    
    <div class="container container-custom-width">
        
        <div class="row">
            <div class="col-12 col-md-10 offset-md-1 offset-xl-0 col-xl-12 offset-xl-0">
                <div class="logoContainer">
                    <a href="<?php echo get_home_url();?>">
                        <?php echo wp_get_attachment_image($footerlogoID, 'full', "", array( "class" => "footerlogo")); ?>
                    </a>
                
                </div>
            
            </div>
        </div>
        
        <div class="row">
            
            <div class="col-12 col-sm-6 col-md-4 offset-md-1 col-xl-2 offset-xl-0">
                <div class="footerContactText line marginboxes">
                <?php
                echo $contacttext;
                ?>
                </div>
            </div>
            
            <div class="col-12 col-sm-6 col-md-3 offset-md-0 offset-xl-0 col-xl-2 offset-xl-0">
                <div class="footerContactText line marginboxes">
                    <?php
                        wp_nav_menu( array(
                            'menu'              => 'primary',
                            'theme_location'    => 'footer-menue-contact-2',
                            'depth'             => 0,
                            'container'         => 'ul',
                            'menu_id'   => 'footercontactlinks',
                            'container_id'      => '',
                            'menu_class'        => '',
                            'after'      => ''
                        ));
                      ?> 
                </div>
            </div>
            
            <div class="col-12 col-sm-12 col-md-3 col-xl-3 offset-xl-0">
                <div class="footerContactText line">
                    <ul class="socialContainer">
                    <?php

                    // check if the repeater field has rows of data
                    if( have_rows('options_footer_contact_social_repeater','option') ):

                        // loop through the rows of data
                        while ( have_rows('options_footer_contact_social_repeater','option') ) : the_row();

                            $iconID = get_sub_field("options_footer_contact_social_repeater_icon","option");
                            $link = get_sub_field("options_footer_contact_social_repeater_link","option");
                            $link_url = $link['url'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                        
                            $icon =  wp_get_attachment_image($iconID, 'full', "", array( "class" => "iconocial"));
                        
                            echo "<li>";
                            
                            echo "<a href='".$link_url."' target='".$link_target."' >".$icon."</a>";
                        
                            echo "</li>";



                        endwhile;

                    endif; 

                    ?>   
                    
                    </ul>
                    
                    <ul class="lngContainer">
                        
                        <?php
                            function icl_post_languages_footer(){
                                $languages = icl_get_languages('skip_missing=0');
                                $langsFooter = array();
                                $idFooter = 0;
                                
                                if(1 < count($languages)){
                                   
                                    foreach($languages as $l){
                                        //$code = $l['code'];
                                        //$native_name = $l['native_name'];
                                        //$default_locale = $l['default_locale'];
                                        //$translated_name = $l['translated_name'];
                                        //$country_flag_url = $l['country_flag_url'];
                                        //$language_code = $l['language_code'];

                                        $active = $l['active'];
                                        
                                        $langsFooter[$idFooter]["url"] = $l['url'];
                                        $langsFooter[$idFooter]["native_name"] = $l['native_name'];
                                        $langsFooter[$idFooter]["active"] = $l['active'];
                                        $langsFooter[$idFooter]["default_locale"] = $l['default_locale'];
                                        $langsFooter[$idFooter]["language_code"] = $l['language_code'];
                                        $idFooter++;

                                    }
                                   
                                }
                                
                                $sortLNGFooter = array_sort($langsFooter, 'active', SORT_DESC);

                                foreach ($sortLNGFooter as $item) {
                                    //print_r($item);
                                    $url = $item["url"];
                                    $nativeName = $item["native_name"];
                                    $active = $item["active"];
                                    $default_locale = $item["default_locale"];
                                    $language_code = $item["language_code"];

                                    echo "<li>";
                                    echo "<a href='".$url."'>".$language_code."</a>";
                                    echo "<span class='divider'> | </span>";
                                    echo "</li>";

                                }
                                
                                
                            }
                            icl_post_languages_footer(); 
                        ?>
                           
                    </ul>  
                </div>
            </div>
        </div>
    </div>
</div>