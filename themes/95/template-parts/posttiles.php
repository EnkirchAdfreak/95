<div class="row">
<?php
    $date = get_the_date();
    $optionsPostPerPage = get_option('posts_per_page');
    $paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
    $noposts = false;

    if(is_category()){
        //catergory Loop
        $categories = get_the_category();
        $categoriesCount = count($categories);
       
        if($categoriesCount != 0){
            $category_id = $categories[0]->cat_ID;
            $args = array(
                'post_type' => 'post',
                'post_status' => 'publish',
                'cat' => $category_id,
                'posts_per_page' => $optionsPostPerPage,
                'paged' => $paged
            );  
        }else{
            $noposts = true;
        }

    }else if(is_tag()){
        $queried_object = get_queried_object();
        $queried_objectCount = $queried_object->count;
        
        if($queried_objectCount != 0){
            $term_id = $queried_object->term_id;
            $args = array(
                'post_type' => 'post',
                'post_status' => 'publish',
                'posts_per_page' => $optionsPostPerPage,
                'paged' => $paged,
                'tag_id' => $term_id
            );  
        }else{
            $noposts = true;
        }
        

    }else{
        //blog posts
        
		$optionsPostPerPage = get_option('posts_per_page');
        $paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
        $offset_start = 1;
        $offset = ( $paged - 1 ) * $optionsPostPerPage + $offset_start;
        
        $offset = 0;
        
		$args = array(
			'post_type' => 'post',
			'post_status' => 'publish',
			'posts_per_page' => $optionsPostPerPage,
			'paged' => $paged,
            'offset' => $offset
		);
    }
    
    
    if($noposts == false){

    $post_query_blog = new WP_Query($args);
        if($post_query_blog->have_posts() ) {
            while($post_query_blog->have_posts() ) {
                $post_query_blog->the_post();
				$blogimage = get_the_post_thumbnail();
        ?>
		
		<div class="col-12 col-sm-6 col-md-4">
			<div class="tile-box">
				<div class="image-container">
					<a href="<?php echo get_permalink();?>">
						<?php 
                            //echo wp_get_attachment_image($blogimage, 'col-md', "", array( "class" => "detailpost-headerimage"));
                            echo $blogimage;
                        ?>
					</a>
				</div>
				<h2 class="headline"><a href="<?php echo get_permalink();?>"><?php the_title(); ?></a></h2>
                <div class="date"><?php echo $date; ?></div>
			</div>
		</div>
        
    <?php
  }
}
?>
</div>

<div class="row">
	<div class="col-12">
		<div class="pagination-container">
			<?php 
                if (function_exists("pagination")) {pagination($post_query_blog->max_num_pages); }             
            ?>
		</div>
	</div>
</div>



<?php
    }else{
?>
            
            
<div class="row">
	<div class="col-12">
		<div class="blogNoPostsFound">
		No Posts Found
		</div>
	</div>
</div>


<?php    
    };
?>