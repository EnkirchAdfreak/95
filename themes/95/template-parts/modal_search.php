<div class="modal fade" id="searchmodal" tabindex="-1" role="dialog" aria-labelledby="searchmodal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="content">
            <div class="headline">Suche</div>
            <div class="searchFormcontainer">
                <form id="searchform" method="get" action="<?php echo home_url('/'); ?>">
                    <input type="text" class="search-field" name="s" placeholder="<?php _e('search_placeholder_searchinput', 'neun'); ?>" value="<?php the_search_query(); ?>"><!--
                    --><input type="submit" class="searchsubmit" value="Search">
                </form>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>